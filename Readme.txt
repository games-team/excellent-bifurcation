Excellent Bifurcation

Version 1.0

Copyright 2007 Linley Henzell


This is my entry for the www.shmup-dev.com 2007 Autofire competition!
It's written in C using gcc/MingW (compiler), Allegro (game library) and Code::Blocks (IDE). The 8-bit graphics were done with The GIMP or generated procedurally. The sound and music were done using Psycle, Audacity, and a number of free VSTs (especially Blitz and GameFX by Jan-Marco Edelmann (voskomo)). All the code, graphics and sound are mine, although I have to acknowledge Kenta Cho's rRootage and Parsec47 for inspiring elements of the style of stage 2. Ikaruga also gave me some ideas (although I've never actually played it ).


**********************************************

INSTRUCTIONS etc

**********************************************


The aliens have attacked, and their ability to exist in either or both of their world and ours has given them an almost unsurpassable advantage. Take command of a special fighter craft capable of existing in both realities at once and drive them back!


The form your fighter takes on the left has double fast-firing cannons and can charge to release up to eight seekers. It is ideal for destroying small and weak but numerous enemies. The right-hand form has a single powerful cannon which fires slowly but does far more damage, and is more suited to taking out larger enemies. It can charge up to release a wave which passes through things, damaging them as it goes (unlike the beam weapons, which only hit anything once no matter how thick it is). You can switch them between the worlds depending on what kind of firepower you need where.


Default controls (but they're redefinable):
Move around with the arrow keys;
Z fires;
X charges your charged weapons;
C switches your two forms between the different worlds;
A turns autofire on/off (autofire is just like holding down Z).


The only vulnerable parts of your ship are the white parts at the centre. This means that the small form is much better at dodging than the large one. The only dangerous parts of bullets are the brightest (yellow or white) parts. Also, if you are shot down the form which was hit emits a green shockwave instead of a yellow one. You get an extra life each 1500 points.


There are three difficulty levels: Easy (good for beginners, but not really all that easy); Medium (for people who have got used to the bifurcation); and Hard (for that guy I saw on Youtube playing Ikaruga with both players at once). The game will probably seem impossible at first, but after a bit of practice the two halves of your brain will start to separate and the pathways between bullets will become clear. You can also choose to start on either Stage 1 or Stage 2.


You can change settings like windowed vs fullscreen, volume and whether vsync is used by editing init.txt in any text editor (I didn't want to spend competition time writing a proper menu interface for setting options). Instructions are in the file. Personally I recommend playing in fullscreen, but the default is windowed. If you are having problems with skipping frames, close down other applications and try fullscreen - it seems to help. Turning vsync off should also help performance, but if your computer was built in the last 5 years it shouldn't have any trouble running this game.


I take no responsibility for any damage or alterations this piece of software may cause to your computer or any part of your nervous system. 


I'll probably get around to releasing the source code sometime soon, so if you're interested let me know. You are more than welcome to redistribute and otherwise copy the binary.


If you like Excellent Bifurcation, you might want to look at some of my other games (easily found on the web):
Overgod
Lacewing
Captain Pork's World of Violence (not so good)
Captain Pork's Revenge (not great either)
Crawl 


Any comments/queries/complaints to l_henzell@yahoo.com.au.


Bye!
Linley

