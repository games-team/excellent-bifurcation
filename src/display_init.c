/*
Angry Moth
Copyright (C) 2006 Linley Henzell

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public Licence as published by
    the Free Software Foundation; either version 2 of the Licence, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public Licence for more details.

    You should have received a copy of the GNU General Public Licence
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    The GPL version 2 is included in this distribution in a file called
    LICENCE.TXT. Use any text editor or the TYPE command to read it.

    You should be able to reach me by sending an email to
    l_henzell@yahoo.com.au.

File: display_init.c
History:

This file contains:
 - functions which put stuff onto the screen

*/

#include "config.h"

#include "allegro.h"

#include <math.h>
#include <string.h>

#include "globvars.h"
#include "palette.h"
#include "stuff.h"
#include "display.h"
#include "menu.h"

extern BITMAP *display [3];

extern RLE_SPRITE *aura_rle [2] [13];
extern RLE_SPRITE *player_rle [2] [21];
extern RLE_SPRITE *normal_player_rle [2] [7];

extern RLE_SPRITE *shock_rle [3] [20];
extern RLE_SPRITE *large_shock_rle [3] [50];
extern BITMAP *underlay;
extern BITMAP *underlay2;

extern struct RLE_STRUCT explode_rle [3] [40];
extern struct RLE_STRUCT triangle_bullet [3] [SMALL_ROTATIONS];
extern struct RLE_STRUCT diamond_bullet [3] [SMALL_ROTATIONS];
extern struct RLE_STRUCT star_bullet [3] [SMALL_ROTATIONS];

extern RLE_SPRITE *straight_bullet [2] [SMALL_ROTATIONS];
extern RLE_SPRITE *spin_bullet [2] [SMALL_ROTATIONS];
extern RLE_SPRITE *green_ring [40];
extern struct RLE_STRUCT eRLE_flower [5] [50];


extern struct RLE_STRUCT eRLE_twister [2] [MEDIUM_ROTATIONS];
extern struct RLE_STRUCT eRLE_attacker [3] [2] [2] [MEDIUM_ROTATIONS];
extern struct RLE_STRUCT eRLE_diver [2] [2] [MEDIUM_ROTATIONS];
// that's attacker#, tail or head, pole, number of rotations
extern struct RLE_STRUCT eRLE_large [2] [LARGE_ERLES];
extern struct RLE_STRUCT shock2_rle [3] [20];
extern struct RLE_STRUCT large_shock2_rle [3] [50];


extern RLE_SPRITE *eRLE_basic1 [S_ENEMY_RLES];
extern RLE_SPRITE *basic_bullet [29];
extern RLE_SPRITE *eRLE_big [L_ENEMY_RLES];
extern RLE_SPRITE *platform_RLE [2] [PLATFORM_RLES];
extern RLE_SPRITE *exhaust [10];
extern RLE_SPRITE *multi_bullet [2] [5];

BITMAP *check_pattern;
BITMAP *check_pattern2;
//void new_bitmap(BITMAP *bmp, int x, int y, const char errtxt []);
BITMAP *new_bitmap(int x, int y, const char errtxt []);
RLE_SPRITE *new_rle_sprite(BITMAP *source, const char errtxt []);
void new_rle_struct(BITMAP *source, const char errtxt [], struct RLE_STRUCT *str, int bl);
void prepare_auras(void);
void prepare_player_rles(void);
void draw_ring(BITMAP *bmp, BITMAP *temp, int rad1, int rad2, int col, int pole, int fuzzy, int anchor);
void prepare_shockwaves(void);
void prepare_large_shockwaves(void);
void prepare_shockwaves2(void);
void prepare_large_shockwaves2(void);
void prepare_explodes(void);
void draw_fuzzy_circle(BITMAP *bmp, BITMAP *temp, int rad, int col, int fuzzy, int anchor);
void prepare_underlay(void);
void process_trans_bitmap(BITMAP *bmp, int x, int y);
void convert_trans_bitmap(BITMAP *bmp, int x, int y);
void prepare_large_enemy_rles(void);
void prepare_s_enemy_rles(void);
void prepare_glass_bullets(void);
void convert_pole(BITMAP *bitmap, int pole);
void convert_trans(BITMAP *source, int target);

void prepare_twister(void);
void prepare_attacker(void);
void prepare_diver(void);
void prepare_l_enemy_rles(void);
void prepare_little_rles(void);
void prepare_platform_rles(void);
void prepare_green_rings(void);
void prepare_flowers(void);
void fix_trans(BITMAP *source);

void extract_rle_struct(BITMAP *source, int x1, int y1, int x2, int y2, const char errtxt [], struct RLE_STRUCT *str, int bl);

void prepare_small_sprites(void);
BITMAP *load_up_bitmap(const char fname []);

//void bordered_poly4(BITMAP *target, int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4, int col);
void bordered_poly4(BITMAP *target, int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4, int col, int border_col, int fillx, int filly);
void bordered_triangle(BITMAP *target, int x1, int y1, int x2, int y2, int x3, int y3, int col, int border_col, int fillx, int filly);

RLE_SPRITE *sized_rle_sprite(BITMAP *source, int x, int y, const char errtxt []);
RLE_SPRITE *extract_rle_sprite(BITMAP *source, int x_source, int y_source, int x, int y);
RLE_SPRITE *extract_flip_rle_sprite(BITMAP *source, int x_source, int y_source, int x, int y, int flip_type);
void fix_outline(BITMAP *source);
void fix_underlay(BITMAP *source);


extern RGB palet [256];

void prepare_display(void)
{

 init_palette();


/*
 BITMAP *palt = new_bitmap(16, 16, "Palette");
 int i, j;

 for (i = 0; i < 16; i ++)
 {
  for (j = 0; j < 16; j ++)
  {
   putpixel(palt, i, j, i + j * 16);
  }
 }

 save_bitmap("palt.bmp", palt, palet);*/
//  save_bitmap(sfile, scrshot_bmp, palet);

 //display [0] = new_bitmap(320, 480, "Display 1");
// clear_bitmap(display [0]);

// display [1] = new_bitmap(320, 480, "Display 2");
// clear_bitmap(display [1]);

 display [0] = new_bitmap(640, 480, "Display 1"); // left screen
 clear_bitmap(display [0]);
 display [1] = create_sub_bitmap(display [0], 320, 0, 320, 480); // right screen
 display [2] = create_sub_bitmap(display [0], 0, 0, 640, 480); // both screens - for score etc

 set_clip_rect(display[0], 0, 0, 320, 480);
 set_clip_rect(display[1], 0, 0, 320, 480);


 DATAFILE *datf = load_datafile("gfx//ebdata.dat");
 if (datf == NULL)
 {
  set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
  allegro_message("Error: Couldn't find data.dat! \n");
  allegro_message("\n\r");
  exit(1);
 }

//progress_update("Data.dat loaded");

 font = (FONT *)datf[0].dat;

 loading_screen();

/*

Annoying process of using font:
I needed to create a new bitmap file in which colour 255 is the only one that's 0xffffff.
Then paste the bmp into that. It's a pain.

*/

 //prepare_auras();
 prepare_s_enemy_rles();
 prepare_l_enemy_rles();
 prepare_platform_rles();
 prepare_little_rles();
 prepare_player_rles();
 prepare_shockwaves();
 prepare_large_shockwaves();
 prepare_shockwaves2();
 prepare_large_shockwaves2();
 prepare_explodes();
 prepare_underlay();
 prepare_small_sprites();
 prepare_twister();
 //prepare_attacker();
// prepare_diver();
// prepare_large_enemy_rles();
 prepare_green_rings();
 prepare_flowers();

 prepare_glass_bullets();

}

void prepare_flowers(void)
{
 BITMAP *tmp = new_bitmap(200, 200, "prepare_flowers");

 clear_bitmap(tmp);

 int i, j, angle = 0;

// Flower1

 for (j = 0; j < 16; j ++)
 {

     angle = j * 8;

 for (i = 0; i < 8; i ++)
 {

  bordered_triangle(tmp,
   50, 50,
   50 + xpart(angle + (ANGLE_8 * i) - 42, 32), 50 + ypart(angle + (ANGLE_8 * i) - 42, 32),
   50 + xpart(angle + (ANGLE_8 * i) + 42, 32), 50 + ypart(angle + (ANGLE_8 * i) + 42, 32),
   COL_LRED,COL_OUTLINE,
   50 + xpart(angle + (ANGLE_8 * i), 10), 50 + ypart(angle + (ANGLE_8 * i), 10));

 }

 circlefill(tmp, 50, 50, 9, COL_YELLOW);
 circle(tmp, 50, 50, 9, COL_OUTLINE);

 extract_rle_struct(tmp, 0, 0, 100, 100, "flower RLE", &eRLE_flower [0] [j], -1);

 clear_bitmap(tmp);


 }

// Flower2:

 for (j = 0; j < 20; j ++)
 {

 angle = j * (ANGLE_5 / 20);


 for (i = 0; i < 5; i ++)
 {

  bordered_poly4(tmp,
   50, 50,
   50 + xpart(angle + (ANGLE_5 * i) - 46, 16), 50 + ypart(angle + (ANGLE_5 * i) - 46, 16),
   50 + xpart(angle + (ANGLE_5 * i), 35), 50 + ypart(angle + (ANGLE_5 * i), 35),
   50 + xpart(angle + (ANGLE_5 * i) + 46, 16), 50 + ypart(angle + (ANGLE_5 * i) + 46, 16),
   COL_DGREEN,COL_OUTLINE,
   50 + xpart(angle + (ANGLE_5 * i), 10), 50 + ypart(angle + (ANGLE_5 * i), 10));

 }

 angle = ANGLE_1 - angle;

 for (i = 0; i < 5; i ++)
 {

  bordered_poly4(tmp,
   50, 50,
   50 + xpart(angle + (ANGLE_5 * i) - 56, 16), 50 + ypart(angle + (ANGLE_5 * i) - 56, 16),
   50 + xpart(angle + (ANGLE_5 * i), 45), 50 + ypart(angle + (ANGLE_5 * i), 45),
   50 + xpart(angle + (ANGLE_5 * i) + 56, 16), 50 + ypart(angle + (ANGLE_5 * i) + 56, 16),
   COL_LBLUE,COL_OUTLINE,
   50 + xpart(angle + (ANGLE_5 * i), 10), 50 + ypart(angle + (ANGLE_5 * i), 10));

 }



 circlefill(tmp, 50, 50, 8, COL_LRED);
 circle(tmp, 50, 50, 8, COL_OUTLINE);

 extract_rle_struct(tmp, 0, 0, 100, 100, "flower RLE", &eRLE_flower [1] [j], -1);

 clear_bitmap(tmp);


 }

// Flower 3


 for (j = 0; j < 20; j ++)
 {

 angle = j * (ANGLE_5 / 20);


 for (i = 0; i < 5; i ++)
 {

  bordered_poly4(tmp,
   50, 50,
   50 + xpart(angle + (ANGLE_5 * i) - 46, 23), 50 + ypart(angle + (ANGLE_5 * i) - 46, 23),
   50 + xpart(angle + (ANGLE_5 * i), 30), 50 + ypart(angle + (ANGLE_5 * i), 30),
   50 + xpart(angle + (ANGLE_5 * i) + 46, 23), 50 + ypart(angle + (ANGLE_5 * i) + 46, 23),
   COL_WHITE,COL_OUTLINE,
   50 + xpart(angle + (ANGLE_5 * i), 10), 50 + ypart(angle + (ANGLE_5 * i), 10));

 }

 angle = ANGLE_1 - angle;

 for (i = 0; i < 5; i ++)
 {

  bordered_triangle(tmp,
   50, 50,
   50 + xpart(angle + (ANGLE_5 * i) - 56, 36), 50 + ypart(angle + (ANGLE_5 * i) - 56, 36),
   50 + xpart(angle + (ANGLE_5 * i) + 56, 36), 50 + ypart(angle + (ANGLE_5 * i) + 56, 36),
   COL_LRED,COL_OUTLINE,
   50 + xpart(angle + (ANGLE_5 * i), 10), 50 + ypart(angle + (ANGLE_5 * i), 10));

 }



 circlefill(tmp, 50, 50, 8, COL_LBLUE);
 circle(tmp, 50, 50, 8, COL_OUTLINE);

 extract_rle_struct(tmp, 0, 0, 100, 100, "flower RLE", &eRLE_flower [2] [j], -1);

 clear_bitmap(tmp);


 }


// Flower 4

 int x = 50;
 int y = 50;

 for (j = 0; j < 20; j ++)
 {

 angle = j * (ANGLE_8 / 20);


 for (i = 0; i < 8; i ++)
 {
   circlefill(tmp, x + xpart(angle + ANGLE_8 * i, 27), y + ypart(angle + ANGLE_8 * i, 27), 7, COL_YELLOW);
   circle(tmp, x + xpart(angle + ANGLE_8 * i, 27), y + ypart(angle + ANGLE_8 * i, 27), 7, COL_OUTLINE);

 }

 angle = ANGLE_1 - angle;

 for (i = 0; i < 16; i ++)
 {
   circlefill(tmp, x + xpart(angle + ANGLE_16 * i, 19), y + ypart(angle + ANGLE_16 * i, 19), 3, COL_DRED);
   circle(tmp, x + xpart(angle + ANGLE_16 * i, 19), y + ypart(angle + ANGLE_16 * i, 19), 3, COL_OUTLINE);
 }



 circlefill(tmp, 50, 50, 8, COL_YELLOW);
 circle(tmp, 50, 50, 8, COL_OUTLINE);

 extract_rle_struct(tmp, 0, 0, 100, 100, "flower RLE", &eRLE_flower [3] [j], -1);

 clear_bitmap(tmp);

 }

// Flower5

 for (j = 0; j < 32; j ++)
 {

     angle = j * 8;

 for (i = 0; i < 4; i ++)
 {

  bordered_triangle(tmp,
   50, 50,
   50 + xpart(angle + (ANGLE_4 * i) - 42, 32), 50 + ypart(angle + (ANGLE_4 * i) - 42, 32),
   50 + xpart(angle + (ANGLE_4 * i) + 42, 32), 50 + ypart(angle + (ANGLE_4 * i) + 42, 32),
   COL_DBLUE,COL_OUTLINE,
   50 + xpart(angle + (ANGLE_4 * i), 10), 50 + ypart(angle + (ANGLE_4 * i), 10));

 }

 angle = ANGLE_1 - angle;

 for (i = 0; i < 4; i ++)
 {

  bordered_triangle(tmp,
   50, 50,
   50 + xpart(angle + (ANGLE_4 * i) - 36, 36), 50 + ypart(angle + (ANGLE_4 * i) - 36, 36),
   50 + xpart(angle + (ANGLE_4 * i) + 36, 36), 50 + ypart(angle + (ANGLE_4 * i) + 36, 36),
   COL_LBLUE,COL_OUTLINE,
   50 + xpart(angle + (ANGLE_4 * i), 10), 50 + ypart(angle + (ANGLE_4 * i), 10));

 }

 circlefill(tmp, 50, 50, 9, COL_LBLUE);
 circle(tmp, 50, 50, 9, COL_OUTLINE);

 extract_rle_struct(tmp, 0, 0, 100, 100, "flower RLE", &eRLE_flower [4] [j], -1);

 clear_bitmap(tmp);




 }



 destroy_bitmap(tmp);
}

void prepare_auras(void)
{
/* int i, j;

 BITMAP *temp_bmp1 = new_bitmap(51, 51, "aura temp1");
 BITMAP *temp_bmp2 = new_bitmap(51, 51, "aura temp2");
 check_pattern = new_bitmap(2, 2, "aura check pattern");
 clear_bitmap(check_pattern);
 putpixel(check_pattern, 0, 0, 0);
 putpixel(check_pattern, 1, 1, 0);
 putpixel(check_pattern, 1, 0, 1);
 putpixel(check_pattern, 0, 1, 1);
 check_pattern2 = new_bitmap(4, 4, "aura check pattern2");
 clear_bitmap(check_pattern2);
 putpixel(check_pattern2, 0, 0, 1);
 putpixel(check_pattern2, 2, 2, 1);
 putpixel(check_pattern2, 3, 0, 1);
 putpixel(check_pattern2, 0, 3, 1);
// clear_bitmap(check_pattern2);

 int col1 = TRANS_WHITE1;
 int col2 = TRANS_WHITE2;
 int col3 = TRANS_WHITE3;

 for (i = 0; i < 2; i ++)
 {
  if (i == 1)
  {
   col1 = TRANS_BLACK1;
   col2 = TRANS_BLACK2;
   col3 = TRANS_BLACK3;
  }
  for (j = 0; j < 10; j ++)
  {
    clear_bitmap(temp_bmp1);
    clear_bitmap(temp_bmp2);
//    if (i == 0)
     circlefill(temp_bmp2, 25, 25, 14, col1);
//      else
//       circlefill(temp_bmp2, 25, 25, 14, TRANS_BLACK1);
    if (j >= 8)
     draw_ring(temp_bmp2, temp_bmp1, 23 - (j * 1.5), 14 - j, col1, i, 8, j % 4);
      else
       draw_ring(temp_bmp2, temp_bmp1, 25 - (j * 1.5), 12 - j, col1, i, 8, j % 4);
    if (j < 8)
     draw_ring(temp_bmp2, temp_bmp1, 24 - (j * 1.5), 16 - j, col2, i, 8, j % 4);
    if (j < 6)
     draw_ring(temp_bmp2, temp_bmp1, 23 - (j * 1.5), 19 - j, col3, i, 8, j % 4);
//    if (j < 3)
//     draw_ring(temp_bmp2, temp_bmp1, 22 - (j * 1.5), 21 - j, TRANS_WHITE4, i, j % 4);
//    draw_ring(temp_bmp2, temp_bmp1, 22, 19, TRANS_WHITE5, i);
//    draw_ring(temp_bmp2, temp_bmp1, 23, 21, TRANS_WHITE6, i);
    aura_rle [i] [j] = get_rle_sprite(temp_bmp2);
  }
    clear_bitmap(temp_bmp1);
    clear_bitmap(temp_bmp2);
     circlefill(temp_bmp2, 25, 25, 14, col1);
    draw_ring(temp_bmp2, temp_bmp1, 23, 14, col1, i, 8, 1);
    draw_ring(temp_bmp2, temp_bmp1, 25, 17, col2, i, 8, 1);
    draw_ring(temp_bmp2, temp_bmp1, 24, 20, col3, i, 8, 1);
//    draw_ring(temp_bmp2, temp_bmp1, 23, 22, TRANS_WHITE4, i, 1);
    aura_rle [i] [10] = get_rle_sprite(temp_bmp2);
    clear_bitmap(temp_bmp1);
    clear_bitmap(temp_bmp2);
    if (i == 0)
     circlefill(temp_bmp2, 25, 25, 14, col1);
      else
       circlefill(temp_bmp2, 25, 25, 14, col1);
    draw_ring(temp_bmp2, temp_bmp1, 24, 11, col1, i, 8, 2);
    draw_ring(temp_bmp2, temp_bmp1, 23, 15, col2, i, 8, 2);
    draw_ring(temp_bmp2, temp_bmp1, 22, 18, col3, i, 8, 2);
//    draw_ring(temp_bmp2, temp_bmp1, 23, 22, TRANS_WHITE4, i, 2);
    aura_rle [i] [11] = get_rle_sprite(temp_bmp2);
    clear_bitmap(temp_bmp1);
    clear_bitmap(temp_bmp2);
    if (i == 0)
     circlefill(temp_bmp2, 25, 25, 14, col1);
      else
       circlefill(temp_bmp2, 25, 25, 14, col1);
    draw_ring(temp_bmp2, temp_bmp1, 25, 12, col1, i, 8, 3);
    draw_ring(temp_bmp2, temp_bmp1, 24, 16, col2, i, 8, 3);
    draw_ring(temp_bmp2, temp_bmp1, 23, 19, col3, i, 8, 3);
//    draw_ring(temp_bmp2, temp_bmp1, 23, 22, TRANS_WHITE4, i, 3);
    aura_rle [i] [12] = get_rle_sprite(temp_bmp2);
 }


 destroy_bitmap(temp_bmp1);
 destroy_bitmap(temp_bmp2);
 destroy_bitmap(check_pattern);
 destroy_bitmap(check_pattern2);*/
}


void prepare_shockwaves(void)
{
 int i, j, size;

 BITMAP *temp_bmp1;
 BITMAP *temp_bmp2;
 check_pattern = new_bitmap(2, 2, "shock check pattern");
 clear_bitmap(check_pattern);
 putpixel(check_pattern, 0, 0, 0);
 putpixel(check_pattern, 1, 1, 0);
 putpixel(check_pattern, 1, 0, 1);
 putpixel(check_pattern, 0, 1, 1);
 check_pattern2 = new_bitmap(4, 4, "shock check pattern2");
 clear_bitmap(check_pattern2);
 putpixel(check_pattern2, 0, 0, 1);
 putpixel(check_pattern2, 2, 2, 1);
 putpixel(check_pattern2, 3, 0, 1);
 putpixel(check_pattern2, 0, 3, 1);

 int col1 = TRANS_DDRED;
 int col2 = TRANS_DRED;
 int col3 = TRANS_LRED;
 int col4 = TRANS_YELLOW;


 int rad1, rad2, fuzzy;
 i = 0;
 for (i = 0; i < 3; i ++)
 {
  switch(i)
  {
   case 1:  // 0 is set above
    col1 = TRANS_DDGREEN;
    col2 = TRANS_DGREEN;
    col3 = TRANS_LGREEN;
    col4 = TRANS_YELLOW;
    break;
   case 2:  // 0 is set above
    col1 = TRANS_DDBLUE;
    col2 = TRANS_DBLUE;
    col3 = TRANS_LBLUE;
    col4 = TRANS_WHITE;
    break;
  }
  for (j = 0; j < 20; j ++)
  {
    size = (20 + j) * 2 + 1;
    temp_bmp1 = new_bitmap(size, size, "shock temp1");
    temp_bmp2 = new_bitmap(size, size, "shock temp2");

    clear_bitmap(temp_bmp1);
    clear_bitmap(temp_bmp2);

     rad1 = 20 + j;
     rad2 = j * 2;
     fuzzy = rad1 - rad2;
     if (fuzzy > 12)
      fuzzy = 12;
     if (rad1 > rad2)
      draw_ring(temp_bmp2, temp_bmp1, rad1, rad2, col1, i, fuzzy, j % 4);
     rad1 = 20 + j;
     rad2 = j * 2 + 3;
     fuzzy = rad1 - rad2;
     if (fuzzy > 12)
      fuzzy = 12;
     if (rad1 > rad2)
      draw_ring(temp_bmp2, temp_bmp1, rad1, rad2, col2, i, fuzzy, j % 4);
     rad1 = 20 + j;
     rad2 = j * 2 + 6;
     fuzzy = rad1 - rad2;
     if (fuzzy > 12)
      fuzzy = 12;
     if (rad1 > rad2)
      draw_ring(temp_bmp2, temp_bmp1, rad1, rad2, col3, i, fuzzy, j % 4);
     rad1 = 19 + j;
     rad2 = j * 2 + 9;
     fuzzy = rad1 - rad2;
     if (fuzzy > 12)
      fuzzy = 12;
     if (rad1 > rad2)
      draw_ring(temp_bmp2, temp_bmp1, rad1, rad2, col4, i, fuzzy, j % 4);
/*     rad1 = 18 + j;
     rad2 = j * 2 + 12;
     fuzzy = rad1 - rad2;
     if (fuzzy > 12)
      fuzzy = 12;
     if (rad1 > rad2)
      draw_ring(temp_bmp2, temp_bmp1, rad1, rad2, col1 + 48, i, fuzzy, j % 4);
     rad1 = 17 + j;
     rad2 = j * 2 + 15;
     fuzzy = rad1 - rad2;
     if (fuzzy > 12)
      fuzzy = 12;
     if (rad1 > rad2)
      draw_ring(temp_bmp2, temp_bmp1, rad1, rad2, col1 + 60, i, fuzzy, j % 4);*/

//     draw_ring(temp_bmp2, temp_bmp1, 20 + j, j * 2, col1 + 60, i, 8 - j / 3, j % 4);

    shock_rle [i] [j] = get_rle_sprite(temp_bmp2);
    destroy_bitmap(temp_bmp1);
    destroy_bitmap(temp_bmp2);
  }
 }


 destroy_bitmap(check_pattern);
 destroy_bitmap(check_pattern2);
}



void prepare_large_shockwaves(void)
{
 int i, j, size;

 BITMAP *temp_bmp1;
 BITMAP *temp_bmp2;
 check_pattern = new_bitmap(2, 2, "L shock check pattern");
 clear_bitmap(check_pattern);
 putpixel(check_pattern, 0, 0, 0);
 putpixel(check_pattern, 1, 1, 0);
 putpixel(check_pattern, 1, 0, 1);
 putpixel(check_pattern, 0, 1, 1);
 check_pattern2 = new_bitmap(4, 4, "L shock check pattern2");
 clear_bitmap(check_pattern2);
 putpixel(check_pattern2, 0, 0, 1);
 putpixel(check_pattern2, 2, 2, 1);
 putpixel(check_pattern2, 3, 0, 1);
 putpixel(check_pattern2, 0, 3, 1);

 int col1 = TRANS_DDRED;
 int col2 = TRANS_DRED;
 int col3 = TRANS_LRED;
 int col4 = TRANS_YELLOW;


 int rad1, rad2, fuzzy;
 i = 0;
 for (i = 0; i < 3; i ++)
 {
  switch(i)
  {
   case 1:  // 0 is set above
    col1 = TRANS_DDGREEN;
    col2 = TRANS_DGREEN;
    col3 = TRANS_LGREEN;
    col4 = TRANS_YELLOW;
    break;
   case 2:  // 0 is set above
    col1 = TRANS_DDBLUE;
    col2 = TRANS_DDBLUE;
    col3 = TRANS_DBLUE;
    col4 = TRANS_LBLUE;
    break;
  }
  for (j = 0; j < 50; j ++)
  {
    size = (50 + j) * 2 + 1;
    temp_bmp1 = new_bitmap(size, size, "L shock temp1");
    temp_bmp2 = new_bitmap(size, size, "L shock temp2");

    clear_bitmap(temp_bmp1);
    clear_bitmap(temp_bmp2);

     rad1 = 50 + j;
     rad2 = j * 2 + 20;
     fuzzy = rad1 - rad2;
     if (fuzzy > 12)
      fuzzy = 12;
     if (rad1 > rad2)
      draw_ring(temp_bmp2, temp_bmp1, rad1, rad2, col1, i, fuzzy, j % 4);
     rad1 = 50 + j;
     rad2 = j * 2 + 23;
     fuzzy = rad1 - rad2;
     if (fuzzy > 12)
      fuzzy = 12;
     if (rad1 > rad2)
      draw_ring(temp_bmp2, temp_bmp1, rad1, rad2, col2, i, fuzzy, j % 4);
     rad1 = 50 + j;
     rad2 = j * 2 + 26;
     fuzzy = rad1 - rad2;
     if (fuzzy > 12)
      fuzzy = 12;
     if (rad1 > rad2)
      draw_ring(temp_bmp2, temp_bmp1, rad1, rad2, col3, i, fuzzy, j % 4);
     rad1 = 49 + j;
     rad2 = j * 2 + 29;
     fuzzy = rad1 - rad2;
     if (fuzzy > 12)
      fuzzy = 12;
     if (rad1 > rad2)
      draw_ring(temp_bmp2, temp_bmp1, rad1, rad2, col4, i, fuzzy, j % 4);
/*     rad1 = 48 + j;
     rad2 = j * 2 + 32;
     fuzzy = rad1 - rad2;
     if (fuzzy > 12)
      fuzzy = 12;
     if (rad1 > rad2)
      draw_ring(temp_bmp2, temp_bmp1, rad1, rad2, col1 + 48, i, fuzzy, j % 4);
     rad1 = 47 + j;
     rad2 = j * 2 + 35;
     fuzzy = rad1 - rad2;
     if (fuzzy > 12)
      fuzzy = 12;
     if (rad1 > rad2)
      draw_ring(temp_bmp2, temp_bmp1, rad1, rad2, col1 + 60, i, fuzzy, j % 4);*/

//     draw_ring(temp_bmp2, temp_bmp1, 20 + j, j * 2, col1 + 60, i, 8 - j / 3, j % 4);

    large_shock_rle [i] [j] = get_rle_sprite(temp_bmp2);
    destroy_bitmap(temp_bmp1);
    destroy_bitmap(temp_bmp2);
  }
 }


 destroy_bitmap(check_pattern);
 destroy_bitmap(check_pattern2);
}


void prepare_shockwaves2(void)
{
 int i, j;

 BITMAP *tmp = new_bitmap(90, 90, "prepare_shockwaves2 tmp bitmap");

 int col [4] = {TRANS_YELLOW, TRANS_LRED, TRANS_DRED, TRANS_DDRED};


 int rad1, rad2, c1, c2;
 i = 0;
 for (i = 0; i < 3; i ++)
 {
  switch(i)
  {
   case 1:  // 0 is set above
    col [3] = TRANS_DDGREEN;
    col [2] = TRANS_DGREEN;
    col [1] = TRANS_LGREEN;
    col [0] = TRANS_YELLOW;
    break;
   case 2:  // 0 is set above
    col [3] = TRANS_DDBLUE;
    col [2] = TRANS_DBLUE;
    col [1] = TRANS_LBLUE;
    col [0] = TRANS_WHITE;
    break;
  }
  rad1 = 30;
  rad2 = 1;
  c1 = col [0];
  c2 = col [1];
  for (j = 0; j < 20; j ++)
  {
    clear_bitmap(tmp);

    rad2 += 2;
    if (j % 2 == 0)
     rad1 ++;
/*
     if (j > 8)
      c2 = col[1];
     if (j > 13)
      c2 = col[2];
     if (j > 17)
      c2 = col[3];

     if (j > 13)
      c1 = col[1];
     if (j > 15)
      c1 = col[2];
     if (j > 17)
      c1 = col[3];*/


    circlefill(tmp, 45, 45, rad1, c2);
    circle(tmp, 45, 45, rad1, c1);
    circlefill(tmp, 45, 45, rad2, 0);
    circle(tmp, 45, 45, rad2, c1);

    new_rle_struct(tmp, "shockwaves2 rle", &shock2_rle [i] [j], -1);

  }
 }


 destroy_bitmap(tmp);
}


void prepare_large_shockwaves2(void)
{
 int i, j;

 BITMAP *tmp = new_bitmap(200, 200, "prepare_large_shockwaves2 tmp bitmap");

 int col [4] = {TRANS_YELLOW, TRANS_LRED, TRANS_DRED, TRANS_DDRED};


 int rad1, rad2, c1, c2;
 i = 0;
 for (i = 0; i < 3; i ++)
 {
  switch(i)
  {
   case 1:  // 0 is set above
    col [3] = TRANS_DDGREEN;
    col [2] = TRANS_DGREEN;
    col [1] = TRANS_LGREEN;
    col [0] = TRANS_YELLOW;
    break;
   case 2:  // 0 is set above
    col [3] = TRANS_DDBLUE;
    col [2] = TRANS_DBLUE;
    col [1] = TRANS_LBLUE;
    col [0] = TRANS_WHITE;
    break;
  }
  rad1 = 30;
  rad2 = 1;
  c1 = col [0];
  c2 = col [1];
  for (j = 0; j < 50; j ++)
  {
    clear_bitmap(tmp);

    if (j > 10)
     rad2 += 2;
//    if (j % 2 == 0)
    rad1 ++;
/*
     if (j > 10)
      c2 = col[1];
     if (j > 34)
      c2 = col[2];
     if (j > 45)
      c2 = col[3];

     if (j > 40)
      c1 = col[1];
     if (j > 44)
      c1 = col[2];
     if (j > 47)
      c1 = col[3];*/


    circlefill(tmp, 100, 100, rad1, c2);
    circle(tmp, 100, 100, rad1, c1);
    circlefill(tmp, 100, 100, rad2, 0);
    circle(tmp, 100, 100, rad2, c1);

    new_rle_struct(tmp, "large_shockwaves2 rle", &large_shock2_rle [i] [j], -1);

  }
 }


 destroy_bitmap(tmp);
}







/*
void prepare_shockwaves(void)
{
 int i, j;

 BITMAP *temp_bmp1 = new_bitmap(81, 81, "shock temp1");
 BITMAP *temp_bmp2 = new_bitmap(81, 81, "shock temp2");
 check_pattern = new_bitmap(2, 2, "shock check pattern");
 clear_bitmap(check_pattern);
 putpixel(check_pattern, 0, 0, 0);
 putpixel(check_pattern, 1, 1, 0);
 putpixel(check_pattern, 1, 0, 1);
 putpixel(check_pattern, 0, 1, 1);
 check_pattern2 = new_bitmap(4, 4, "shock check pattern2");
 clear_bitmap(check_pattern2);
 putpixel(check_pattern2, 0, 0, 1);
 putpixel(check_pattern2, 2, 2, 1);
 putpixel(check_pattern2, 3, 0, 1);
 putpixel(check_pattern2, 0, 3, 1);

 int col1 = TRANS_WHITE1;

 int rad1, rad2, fuzzy;

 for (i = 0; i < 3; i ++)
 {
  switch(i)
  {
   case 0: col1 = TRANS_WHITE1; break;
   case 1: col1 = TRANS_BLACK1; break;
   case 2: col1 = TRANS_YELLOW1; break;
  }
  for (j = 0; j < 20; j ++)
  {
    clear_bitmap(temp_bmp1);
    clear_bitmap(temp_bmp2);

     rad1 = 20 + j;
     rad2 = j * 2;
     fuzzy = rad1 - rad2;
     if (fuzzy > 12)
      fuzzy = 12;
     if (rad1 > rad2)
      draw_ring(temp_bmp2, temp_bmp1, rad1, rad2, col1, i, fuzzy, j % 4);
     rad1 = 20 + j;
     rad2 = j * 2 + 3;
     fuzzy = rad1 - rad2;
     if (fuzzy > 12)
      fuzzy = 12;
     if (rad1 > rad2)
      draw_ring(temp_bmp2, temp_bmp1, rad1, rad2, col1 + 12, i, fuzzy, j % 4);
     rad1 = 20 + j;
     rad2 = j * 2 + 6;
     fuzzy = rad1 - rad2;
     if (fuzzy > 12)
      fuzzy = 12;
     if (rad1 > rad2)
      draw_ring(temp_bmp2, temp_bmp1, rad1, rad2, col1 + 24, i, fuzzy, j % 4);
     rad1 = 19 + j;
     rad2 = j * 2 + 9;
     fuzzy = rad1 - rad2;
     if (fuzzy > 12)
      fuzzy = 12;
     if (rad1 > rad2)
      draw_ring(temp_bmp2, temp_bmp1, rad1, rad2, col1 + 36, i, fuzzy, j % 4);
     rad1 = 18 + j;
     rad2 = j * 2 + 12;
     fuzzy = rad1 - rad2;
     if (fuzzy > 12)
      fuzzy = 12;
     if (rad1 > rad2)
      draw_ring(temp_bmp2, temp_bmp1, rad1, rad2, col1 + 48, i, fuzzy, j % 4);
     rad1 = 17 + j;
     rad2 = j * 2 + 15;
     fuzzy = rad1 - rad2;
     if (fuzzy > 12)
      fuzzy = 12;
     if (rad1 > rad2)
      draw_ring(temp_bmp2, temp_bmp1, rad1, rad2, col1 + 60, i, fuzzy, j % 4);

//     draw_ring(temp_bmp2, temp_bmp1, 20 + j, j * 2, col1 + 60, i, 8 - j / 3, j % 4);

    shock_rle [i] [j] = get_rle_sprite(temp_bmp2);
  }
 }


 destroy_bitmap(temp_bmp1);
 destroy_bitmap(temp_bmp2);
 destroy_bitmap(check_pattern);
 destroy_bitmap(check_pattern2);
}


*/

void prepare_explodes(void)
{
 int i, j, size;

 BITMAP *temp_bmp1;
 BITMAP *temp_bmp2;
 check_pattern = new_bitmap(2, 2, "explode check pattern");
 putpixel(check_pattern, 0, 0, 0);
 putpixel(check_pattern, 1, 1, 0);
 putpixel(check_pattern, 1, 0, 1);
 putpixel(check_pattern, 0, 1, 1);
 check_pattern2 = new_bitmap(4, 4, "explode check pattern2");
 clear_bitmap(check_pattern2);
 putpixel(check_pattern2, 0, 0, 1);
 putpixel(check_pattern2, 2, 2, 1);
 putpixel(check_pattern2, 3, 0, 1);
 putpixel(check_pattern2, 0, 3, 1);

 int col1 = TRANS_DDRED;
 int col2 = TRANS_DRED;
 int col3 = TRANS_LRED;
 int col4 = TRANS_YELLOW;


 int rad, fuzzy;
 i = 0;
 for (i = 0; i < 3; i ++)
 {
  switch(i)
  {
//   case 0: col1 = TRANS_WHITE1; break;
   case 1:
    col1 = TRANS_DDGREEN;
    col2 = TRANS_DGREEN;
    col3 = TRANS_LGREEN;
    col4 = TRANS_YELLOW;
    break;
//   case 2: col1 = TRANS_YELLOW1; break;
   case 2:
    col1 = TRANS_DDBLUE;
    col2 = TRANS_DBLUE;
    col3 = TRANS_LBLUE;
    col4 = TRANS_WHITE;
    break;
  }

  for (j = 3; j < 38; j ++)
  {

    size = ((j * 160) / 100) * 2 + 4;
    temp_bmp1 = new_bitmap(size, size, "explode temp1");
    temp_bmp2 = new_bitmap(size, size, "explode temp2");

    clear_bitmap(temp_bmp1);
    clear_bitmap(temp_bmp2);

     rad = (j * 160) / 100 + 0;
     fuzzy = 2;
     if (rad <= 7)
      fuzzy = 1;
     if (rad <= 1)
      fuzzy = 0;

     rad = (j * 160) / 100 + 0;
     draw_fuzzy_circle(temp_bmp2, temp_bmp1, rad, col1, fuzzy, (j + 1) % 4);
     rad = (j * 140) / 100 + 0;
     draw_fuzzy_circle(temp_bmp2, temp_bmp1, rad, col2, fuzzy, j % 4);
     rad = (j * 110) / 100 + 2;
if (j > 1)  draw_fuzzy_circle(temp_bmp2, temp_bmp1, rad, col3, fuzzy, (j + 3) % 4);
//     rad = (j * 100) / 100 + 1;
     //if (j > 2) draw_fuzzy_circle(temp_bmp2, temp_bmp1, rad, col4, fuzzy, (j + 2) % 4);
     rad = (j * 100) / 100 - 1;
     if (rad > 0)
     draw_fuzzy_circle(temp_bmp2, temp_bmp1, rad, col4, fuzzy, (j + 2) % 4);

//    explode_rle [i] [j + 2] = get_rle_sprite(temp_bmp2);
    new_rle_struct(temp_bmp2, "explode_rle", &explode_rle [i] [j + 2], -1);
    destroy_bitmap(temp_bmp1);
    destroy_bitmap(temp_bmp2);
  }

/*
  temp_bmp1 = new_bitmap(5, 5, "explode 5x5 temp2");
  clear_bitmap(temp_bmp1);
  putpixel(temp_bmp1, temp_bmp1->w / 2, temp_bmp1->w / 2, col1);
  putpixel(temp_bmp1, temp_bmp1->w / 2 + 1, temp_bmp1->w / 2, col2);
  putpixel(temp_bmp1, temp_bmp1->w / 2 - 1, temp_bmp1->w / 2, col2);
  putpixel(temp_bmp1, temp_bmp1->w / 2, temp_bmp1->w / 2 + 1, col2);
  putpixel(temp_bmp1, temp_bmp1->w / 2, temp_bmp1->w / 2 - 1, col2);
  explode_rle [i] [1] = get_rle_sprite(temp_bmp1);
  clear_bitmap(temp_bmp1);

  putpixel(temp_bmp1, temp_bmp1->w / 2, temp_bmp1->w / 2, col1);
  explode_rle [i] [0] = get_rle_sprite(temp_bmp1);
  destroy_bitmap(temp_bmp1);*/

 }

 destroy_bitmap(check_pattern);
 destroy_bitmap(check_pattern2);

 BITMAP *file_bitmap = load_up_bitmap("gfx//explodes.bmp");

 fix_trans(file_bitmap);

// new_rle_struct(, "explode_rle", explode_rle [i] [j + 2], -1);

 extract_rle_struct(file_bitmap, 1, 1, 3, 3, "small explodes", &explode_rle [0] [0], -1);
 extract_rle_struct(file_bitmap, 21, 1, 5, 5, "small explodes", &explode_rle [0] [1], -1);
 extract_rle_struct(file_bitmap, 41, 1, 8, 8, "small explodes", &explode_rle [0] [2], -1);
 extract_rle_struct(file_bitmap, 61, 1, 10, 10, "small explodes", &explode_rle [0] [3], -1);
 extract_rle_struct(file_bitmap, 81, 1, 11, 11, "small explodes", &explode_rle [0] [4], -1);

/*
  explode_rle [0] [0] = extract_rle_sprite(file_bitmap, 1, 1, 3, 3);
  explode_rle [0] [1] = extract_rle_sprite(file_bitmap, 21, 1, 5, 5);
  explode_rle [0] [2] = extract_rle_sprite(file_bitmap, 41, 1, 8, 8);
  explode_rle [0] [3] = extract_rle_sprite(file_bitmap, 61, 1, 10, 10);
  explode_rle [0] [4] = extract_rle_sprite(file_bitmap, 81, 1, 11, 11);*/

  BITMAP *green_ex = new_bitmap(106, 21, "green_ex");

  blit(file_bitmap, green_ex, 0, 0, 0, 0, 106, 21);

  convert_trans(green_ex, TRANS_LGREEN);

  extract_rle_struct(green_ex, 1, 1, 3, 3, "small explodes", &explode_rle [1] [0], -1);
  extract_rle_struct(green_ex, 21, 1, 5, 5, "small explodes", &explode_rle [1] [1], -1);
  extract_rle_struct(green_ex, 41, 1, 8, 8, "small explodes", &explode_rle [1] [2], -1);
  extract_rle_struct(green_ex, 61, 1, 10, 10, "small explodes", &explode_rle [1] [3], -1);
  extract_rle_struct(green_ex, 81, 1, 11, 11, "small explodes", &explode_rle [1] [4], -1);

/*  explode_rle [1] [0] = extract_rle_sprite(green_ex, 1, 1, 3, 3);
  explode_rle [1] [1] = extract_rle_sprite(green_ex, 21, 1, 5, 5);
  explode_rle [1] [2] = extract_rle_sprite(green_ex, 41, 1, 8, 8);
  explode_rle [1] [3] = extract_rle_sprite(green_ex, 61, 1, 10, 10);
  explode_rle [1] [4] = extract_rle_sprite(green_ex, 81, 1, 11, 11);*/

  BITMAP *blue_ex = new_bitmap(106, 21, "blue_ex");

  blit(file_bitmap, blue_ex, 0, 0, 0, 0, 106, 21);

  convert_trans(blue_ex, TRANS_LBLUE);

  extract_rle_struct(blue_ex, 1, 1, 3, 3, "small explodes", &explode_rle [2] [0], -1);
  extract_rle_struct(blue_ex, 21, 1, 5, 5, "small explodes", &explode_rle [2] [1], -1);
  extract_rle_struct(blue_ex, 41, 1, 8, 8, "small explodes", &explode_rle [2] [2], -1);
  extract_rle_struct(blue_ex, 61, 1, 10, 10, "small explodes", &explode_rle [2] [3], -1);
  extract_rle_struct(blue_ex, 81, 1, 11, 11, "small explodes", &explode_rle [2] [4], -1);

/*  explode_rle [2] [0] = extract_rle_sprite(blue_ex, 1, 1, 3, 3);
  explode_rle [2] [1] = extract_rle_sprite(blue_ex, 21, 1, 5, 5);
  explode_rle [2] [2] = extract_rle_sprite(blue_ex, 41, 1, 8, 8);
  explode_rle [2] [3] = extract_rle_sprite(blue_ex, 61, 1, 10, 10);
  explode_rle [2] [4] = extract_rle_sprite(blue_ex, 81, 1, 11, 11);*/

 destroy_bitmap(file_bitmap);
 destroy_bitmap(green_ex);
 destroy_bitmap(blue_ex);

/*
 BITMAP *saving = new_bitmap(200, 20, "saving");

 clear_bitmap(saving);

 hline(saving, 0, 0, 200, COL_WHITE);

 for (i = 0; i < 10; i ++)
 {
  vline(saving, i * 20, 0, 20, COL_WHITE);
  draw_rle_sprite(saving, explode_rle [0] [i], i * 20 + 1, 1);
 }

  save_bitmap("explode.bmp", saving, palet);
*/
}

void prepare_glass_bullets(void)
{
 int i, j, k;

 BITMAP *temp_bitmap;

 temp_bitmap = new_bitmap(25, 25, "prepare_glass_bullets");

 int col [4] = {TRANS_YELLOW, TRANS_LRED, TRANS_DRED, TRANS_DDRED};
// int col [4] = {TRANS_WHITE, TRANS_YELLOW, TRANS_LRED, TRANS_DRED};

 int inc = ANGLE_1 / SMALL_ROTATIONS;
 int angle;

 int o;

 for (o = 0; o < 3; o ++)
 {
  switch (o)
  {
   case 1:
    col [0] = TRANS_YELLOW;
    col [1] = TRANS_LGREEN;
    col [2] = TRANS_DGREEN;
    col [3] = TRANS_DDGREEN;
    break;
   case 2:
    col [0] = TRANS_WHITE;
    col [1] = TRANS_LBLUE;
    col [2] = TRANS_DBLUE;
    col [3] = TRANS_DDBLUE;
    break;

  }
 for (i = 0; i < SMALL_ROTATIONS; i ++)
 {
  clear_bitmap(temp_bitmap);
  angle = i * inc;

 bordered_triangle(temp_bitmap,
  10 + xpart(angle, 7), 10 + ypart(angle, 7),
  10 + xpart(angle + ANGLE_4 + ANGLE_8, 7), 10 + ypart(angle + ANGLE_4 + ANGLE_8, 7),
  10 + xpart(angle - ANGLE_4 - ANGLE_8, 7), 10 + ypart(angle - ANGLE_4 - ANGLE_8, 7),
  col [2], col [0], 10, 10);

 for (j = 0; j < 20; j ++)
 {
  for (k = 0; k < 20; k ++)
  {
      if (getpixel(temp_bitmap, j, k) != col [0]
      && (getpixel(temp_bitmap, j + 1, k) == col [0]
       || getpixel(temp_bitmap, j - 1, k) == col [0]
       || getpixel(temp_bitmap, j, k + 1) == col [0]
       || getpixel(temp_bitmap, j, k - 1) == col [0]))
        putpixel(temp_bitmap, j, k, col [1]);
  }
 }

   new_rle_struct(temp_bitmap, "prepare_glass_bullets", &triangle_bullet [o] [i], -1);

// Now let's make diamond bullets:

  clear_bitmap(temp_bitmap);
  angle = i * inc;

 bordered_poly4(temp_bitmap,
  10 + xpart(angle, 7), 10 + ypart(angle, 7),
  10 + xpart(angle + ANGLE_4, 4), 10 + ypart(angle + ANGLE_4, 4),
  10 - xpart(angle, 7), 10 - ypart(angle, 7),
  10 + xpart(angle - ANGLE_4, 4), 10 - ypart(angle + ANGLE_4, 4),
  col [2], col [0], 10, 10);

 for (j = 0; j < 20; j ++)
 {
  for (k = 0; k < 20; k ++)
  {
      if (getpixel(temp_bitmap, j, k) != col [0]
      && (getpixel(temp_bitmap, j + 1, k) == col [0]
       || getpixel(temp_bitmap, j - 1, k) == col [0]
       || getpixel(temp_bitmap, j, k + 1) == col [0]
       || getpixel(temp_bitmap, j, k - 1) == col [0]))
        putpixel(temp_bitmap, j, k, col [1]);
  }
 }

   new_rle_struct(temp_bitmap, "prepare_glass_bullets", &diamond_bullet [o] [i], -1);

// Star bullets!


  clear_bitmap(temp_bitmap);
  angle = i * inc;

 line(temp_bitmap,
  12 + xpart(angle, 9), 12 + ypart(angle, 9),
  12 + xpart(angle + ANGLE_8, 4), 12 + ypart(angle + ANGLE_8, 4), col [0]);

 line(temp_bitmap,
  12 + xpart(angle + ANGLE_8, 4), 12 + ypart(angle + ANGLE_8, 4),
  12 + xpart(angle + ANGLE_4, 9), 12 + ypart(angle + ANGLE_4, 9),
  col [0]);

 line(temp_bitmap,
  12 + xpart(angle + ANGLE_4, 9), 12 + ypart(angle + ANGLE_4, 9),
  12 + xpart(angle + ANGLE_4 + ANGLE_8, 4), 12 + ypart(angle + ANGLE_4 + ANGLE_8, 4),
  col [0]);

 line(temp_bitmap,
  12 + xpart(angle + ANGLE_4 + ANGLE_8, 4), 12 + ypart(angle + ANGLE_4 + ANGLE_8, 4),
  12 + xpart(angle + ANGLE_2, 9), 12 + ypart(angle + ANGLE_2, 9),
  col [0]);

 line(temp_bitmap,
  12 + xpart(angle + ANGLE_2, 9), 12 + ypart(angle + ANGLE_2, 9),
  12 + xpart(angle + ANGLE_2 + ANGLE_8, 4), 12 + ypart(angle + ANGLE_2 + ANGLE_8, 4),
  col [0]);

 line(temp_bitmap,
  12 + xpart(angle + ANGLE_2 + ANGLE_8, 4), 12 + ypart(angle + ANGLE_2 + ANGLE_8, 4),
  12 + xpart(angle + ANGLE_2 + ANGLE_4, 9), 12 + ypart(angle + ANGLE_2 + ANGLE_4, 9),
  col [0]);

 line(temp_bitmap,
  12 + xpart(angle + ANGLE_2 + ANGLE_4, 9), 12 + ypart(angle + ANGLE_2 + ANGLE_4, 9),
  12 + xpart(angle + ANGLE_2 + ANGLE_4 + ANGLE_8, 4), 12 + ypart(angle + ANGLE_2 + ANGLE_4 + ANGLE_8, 4),
  col [0]);

 line(temp_bitmap,
  12 + xpart(angle + ANGLE_2 + ANGLE_4 + ANGLE_8, 4), 12 + ypart(angle + ANGLE_2 + ANGLE_4 + ANGLE_8, 4),
  12 + xpart(angle, 9), 12 + ypart(angle, 9),
  col [0]);

 floodfill(temp_bitmap, 12, 12, col [2]);

 for (j = 0; j < 20; j ++)
 {
  for (k = 0; k < 20; k ++)
  {
      if (getpixel(temp_bitmap, j, k) != col [0]
      && (getpixel(temp_bitmap, j + 1, k) == col [0]
       || getpixel(temp_bitmap, j - 1, k) == col [0]
       || getpixel(temp_bitmap, j, k + 1) == col [0]
       || getpixel(temp_bitmap, j, k - 1) == col [0]))
        putpixel(temp_bitmap, j, k, col [1]);
  }
 }

   new_rle_struct(temp_bitmap, "prepare_glass_bullets", &star_bullet [o] [i], -1);





 }

 }

  destroy_bitmap(temp_bitmap);


}




void prepare_green_rings(void)
{
 int i, rad;

 BITMAP *temp_bitmap;

 for (i = 0; i < 40; i ++)
 {
  rad = (i + 2) * 2;
  temp_bitmap = new_bitmap(rad, rad, "prepare_green_rings");
  clear_bitmap(temp_bitmap);

  circlefill(temp_bitmap, i + 2, i + 2, i + 1, TRANS_DGREEN);
  circlefill(temp_bitmap, i + 2, i + 2, i, TRANS_LGREEN);
  if (i > 0)
   circlefill(temp_bitmap, i + 2, i + 2, i - 1, TRANS_YELLOW);
  if (i > 1)
   circlefill(temp_bitmap, i + 2, i + 2, i - 2, TRANS_LGREEN);
  if (i > 2)
   circlefill(temp_bitmap, i + 2, i + 2, i - 3, TRANS_DGREEN);
  if (i > 3)
   circlefill(temp_bitmap, i + 2, i + 2, i - 4, 0);

  green_ring [i] = get_rle_sprite(temp_bitmap);

  destroy_bitmap(temp_bitmap);
 }


}

/*
void prepare_explodes(void)
{
 int i, j;

 BITMAP *temp_bmp1 = new_bitmap(81, 81, "explode temp1");
 BITMAP *temp_bmp2 = new_bitmap(81, 81, "explode temp2");
 check_pattern = new_bitmap(2, 2, "explode check pattern");
 putpixel(check_pattern, 0, 0, 0);
 putpixel(check_pattern, 1, 1, 0);
 putpixel(check_pattern, 1, 0, 1);
 putpixel(check_pattern, 0, 1, 1);
 check_pattern2 = new_bitmap(4, 4, "explode check pattern2");
 clear_bitmap(check_pattern2);
 putpixel(check_pattern2, 0, 0, 1);
 putpixel(check_pattern2, 2, 2, 1);
 putpixel(check_pattern2, 3, 0, 1);
 putpixel(check_pattern2, 0, 3, 1);

 int col1 = TRANS_WHITE1;

 int rad, fuzzy;

 for (i = 0; i < 3; i ++)
 {
  switch(i)
  {
   case 0: col1 = TRANS_WHITE1; break;
   case 1: col1 = TRANS_BLACK1; break;
   case 2: col1 = TRANS_YELLOW1; break;
  }
  for (j = 0; j < 18; j ++)
  {
//    j = k + 2;
    clear_bitmap(temp_bmp1);
    clear_bitmap(temp_bmp2);

     rad = (j * 160) / 100 + 0;
     fuzzy = 2;
     if (rad <= 7)
      fuzzy = 1;
     if (rad <= 1)
      fuzzy = 0;

     rad = (j * 160) / 100 + 0;
     draw_fuzzy_circle(temp_bmp2, temp_bmp1, rad, col1, fuzzy, (j + 1) % 4);
     rad = (j * 140) / 100 + 0;
     draw_fuzzy_circle(temp_bmp2, temp_bmp1, rad, col1 + 12, fuzzy, j % 4);
     rad = (j * 110) / 100 + 2;
if (j > 1)  draw_fuzzy_circle(temp_bmp2, temp_bmp1, rad, col1 + 24, fuzzy, (j + 3) % 4);
     rad = (j * 100) / 100 + 1;
     draw_fuzzy_circle(temp_bmp2, temp_bmp1, rad, col1 + 36, fuzzy, (j + 2) % 4);
     rad = (j * 110) / 100 + 0;
     draw_fuzzy_circle(temp_bmp2, temp_bmp1, rad, col1 + 48, fuzzy, (j + 1) % 4);
     rad = j + 0;
if (j > 1)           draw_fuzzy_circle(temp_bmp2, temp_bmp1, rad, col1 + 60, fuzzy, (j) % 4);

    explode_rle [i] [j + 2] = get_rle_sprite(temp_bmp2);
  }
  clear_bitmap(temp_bmp2);
  putpixel(temp_bmp1, 40, 40, col1 + 60);
  putpixel(temp_bmp1, 41, 40, col1 + 48);
  putpixel(temp_bmp1, 39, 40, col1 + 48);
  putpixel(temp_bmp1, 40, 41, col1 + 48);
  putpixel(temp_bmp1, 40, 39, col1 + 48);
  putpixel(temp_bmp1, 41, 39, col1 + 24);
  putpixel(temp_bmp1, 41, 41, col1 + 24);
  putpixel(temp_bmp1, 39, 41, col1 + 24);
  putpixel(temp_bmp1, 39, 39, col1 + 24);
  explode_rle [i] [1] = get_rle_sprite(temp_bmp2);
  clear_bitmap(temp_bmp2);
  putpixel(temp_bmp1, 40, 40, col1 + 36);
  putpixel(temp_bmp1, 41, 40, col1 + 12);
  putpixel(temp_bmp1, 39, 40, col1 + 12);
  putpixel(temp_bmp1, 40, 41, col1 + 12);
  putpixel(temp_bmp1, 40, 39, col1 + 12);
  putpixel(temp_bmp1, 41, 39, col1);
  putpixel(temp_bmp1, 41, 41, col1);
  putpixel(temp_bmp1, 39, 41, col1);
  putpixel(temp_bmp1, 39, 39, col1);
  explode_rle [i] [0] = get_rle_sprite(temp_bmp2);

 }

 destroy_bitmap(temp_bmp1);
 destroy_bitmap(temp_bmp2);
 destroy_bitmap(check_pattern);
 destroy_bitmap(check_pattern2);
}

*/

void draw_fuzzy_circle(BITMAP *bmp, BITMAP *temp, int rad, int col, int fuzzy, int anchor)
{
  clear_bitmap(temp);
  int middle = temp->w / 2;


 circlefill(temp, middle, middle, rad, col);

 drawing_mode(DRAW_MODE_MASKED_PATTERN, check_pattern, anchor % 2, 0);
 circlefill(temp, middle, middle, rad + fuzzy, col);
 drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);

 draw_sprite(bmp, temp, 0, 0);

}

void draw_ring(BITMAP *bmp, BITMAP *temp, int rad1, int rad2, int col, int pole, int fuzzy, int anchor)
{
 clear_bitmap(temp);

// if (pole == 1)
//   col += 72;

 int middle = temp->w / 2;

 circlefill(temp, middle, middle, rad1, col);
 circlefill(temp, middle, middle, rad2, 0);

 drawing_mode(DRAW_MODE_MASKED_PATTERN, check_pattern, anchor % 2, 0);
 circlefill(temp, middle, middle, rad2, col);
 circlefill(temp, middle, middle, rad2 - fuzzy, 0);
 drawing_mode(DRAW_MODE_MASKED_PATTERN, check_pattern2, anchor, 0);
// circlefill(temp, 25, 25, rad2 - 4, col);
// circlefill(temp, 25, 25, rad2 - 8, 0);
 drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);

 draw_sprite(bmp, temp, 0, 0);

}

/*
void draw_hollow_circle(BITMAP *bmp, int x, int y, int rad1, int rad2, int col)
{
    BITMAP *temp_bmp = new_bitmap(rad1 * 2 + 1, rad1 * 2 + 1, "hollow circle");

    clear_bitmap(temp_bitmap);

    circlefill(temp_bitmap, x, y,

    destroy_bitmap(temp_bmp);

}
*/

void prepare_player_rles(void)
{


 BITMAP *file_bitmap = load_up_bitmap("gfx//player.bmp");

 BITMAP *temp_bitmap1 = new_bitmap(37, 30, "Prepare player_rles temp_bitmap1");
// BITMAP *temp_bitmap2 = new_bitmap(14, 14, "Prepare player_res temp_bitmap2");

// int i, j;

      fix_outline(file_bitmap);

      clear_bitmap(temp_bitmap1);
      blit(file_bitmap, temp_bitmap1, 1, 1, 0, 0, 21, 13);
//      player_rle [0] [0] = get_rle_sprite(temp_bitmap1);

//      clear_bitmap(temp_bitmap1);
//      blit(file_bitmap, temp_bitmap1, 32, 1, 0, 0, 29, 25);
//      player_rle [1] [0] = get_rle_sprite(temp_bitmap1);
      player_rle [0] [0] = extract_rle_sprite(file_bitmap, 1, 1, 21, 13);
      player_rle [0] [1] = extract_rle_sprite(file_bitmap, 32, 1, 21, 13);
      player_rle [0] [2] = extract_rle_sprite(file_bitmap, 63, 1, 21, 13);
      player_rle [0] [3] = extract_rle_sprite(file_bitmap, 94, 1, 21, 13);
      player_rle [0] [4] = extract_rle_sprite(file_bitmap, 125, 1, 21, 13);
      player_rle [0] [5] = extract_rle_sprite(file_bitmap, 156, 1, 21, 13);

      player_rle [0] [6] = extract_flip_rle_sprite(file_bitmap, 32, 1, 21, 13, 0);
      player_rle [0] [7] = extract_flip_rle_sprite(file_bitmap, 63, 1, 21, 13, 0);
      player_rle [0] [8] = extract_flip_rle_sprite(file_bitmap, 94, 1, 21, 13, 0);
      player_rle [0] [9] = extract_flip_rle_sprite(file_bitmap, 125, 1, 21, 13, 0);
      player_rle [0] [10] = extract_flip_rle_sprite(file_bitmap, 156, 1, 21, 13, 0);



      player_rle [1] [0] = extract_rle_sprite(file_bitmap, 1, 32, 29, 25);
      player_rle [1] [1] = extract_rle_sprite(file_bitmap, 32, 32, 29, 25);
      player_rle [1] [2] = extract_rle_sprite(file_bitmap, 63, 32, 29, 25);
      player_rle [1] [3] = extract_rle_sprite(file_bitmap, 94, 32, 29, 25);
      player_rle [1] [4] = extract_rle_sprite(file_bitmap, 125, 32, 29, 25);
      player_rle [1] [5] = extract_rle_sprite(file_bitmap, 156, 32, 29, 25);

      player_rle [1] [6] = extract_flip_rle_sprite(file_bitmap, 32, 32, 29, 25, 0);
      player_rle [1] [7] = extract_flip_rle_sprite(file_bitmap, 63, 32, 29, 25, 0);
      player_rle [1] [8] = extract_flip_rle_sprite(file_bitmap, 94, 32, 29, 25, 0);
      player_rle [1] [9] = extract_flip_rle_sprite(file_bitmap, 125, 32, 29, 25, 0);
      player_rle [1] [10] = extract_flip_rle_sprite(file_bitmap, 156, 32, 29, 25, 0);

/* for (i = 0; i < 2; i ++)
 {
     for (j = 0; j < 20; j ++)
     {
      clear_bitmap(temp_bitmap1);
      blit(file_bitmap, temp_bitmap1, j * 37 + 1, i * 30 + 1, 0, 0, 36, 29);
      player_rle [i] [j] = get_rle_sprite(temp_bitmap1);
     }
 }*/
/*
 for (i = 0; i < 2; i ++)
 {
     for (j = 0; j < 7; j ++)
     {
      clear_bitmap(temp_bitmap1);
      blit(file_bitmap, temp_bitmap1, j * 37 + 1, i * 30 + 1 + 62, 0, 0, 36, 29);
      normal_player_rle [i] [j] = get_rle_sprite(temp_bitmap1);
     }
 }
*/

 destroy_bitmap(file_bitmap);
 destroy_bitmap(temp_bitmap1);



/*
  int xc;
  int xc2;
  int size_adjust = 0;

  int i;
  int j;
  int x = 18, y = 9;

  int col1 = COLOUR_11, col2 = COLOUR_10, col3 = COLOUR_9;

  BITMAP *temp = new_bitmap(37, 30, "player temp");

  for (i = 0; i < 2; i ++)
  {
   if (i == 1)
   {
    col1 = COLOUR_1;
    col2 = COLOUR_2;
    col3 = COLOUR_3;
   }
   for (j = 0; j < 20; j ++)
   {
    col1 = COLOUR_11 - j / 3;
    col2 = COLOUR_10 - j / 3;
    col3 = COLOUR_9 - j / 3;

  if (i == 1)
    {
     col1 = COLOUR_11 - j / 3;
     col2 = COLOUR_10 - j / 3;
     col3 = COLOUR_9 - j / 3;
    }
    clear_bitmap(temp);
    xc = xpart(j * 26, 15) + 15;
    size_adjust = xpart(j * 17 + ANGLE_4, 2);
    rectfill(temp, x - 16 + xc, y - 2 - size_adjust, x - 13 + xc, y + 9 + size_adjust, col3);
    rect(temp, x - 16 + xc, y - 2 - size_adjust, x - 13 + xc, y + 9 + size_adjust, COLOUR_8);
    xc = xpart(j * 26, 11) + 10;
    rectfill(temp, x - 13 + xc + size_adjust, y - 5 - size_adjust, x - 7 + xc - size_adjust, y + 18 + size_adjust, col2);
    rect(temp, x - 13 + xc + size_adjust, y - 5 - size_adjust, x - 7 + xc - size_adjust, y + 18 + size_adjust, COLOUR_8);

    xc = xpart(j * 26, 8) + 7;
    rectfill(temp, x - 7 + xc, y - 1, x - 3, y + 4, col3);
    rect(temp, x - 7 + xc, y - 1, x - 3, y + 4, COLOUR_8);

    hline(temp, x - 3, y - 1, x + 3, col3);
    rectfill(temp, x - 3, y - 0, x + 3, y + 7, col1);
    rect(temp, x - 3, y - 0, x + 3, y + 7, COLOUR_8);

    xc = xpart(j * 26, 7) + 7;
    xc2 = xpart(j * 26, 4) + 3;
    rectfill(temp, x + 3 - xc2, y - 1, x + 7 - xc, y + 4, col3);
    rect(temp, x + 3 - xc2, y - 1, x + 7 - xc, y + 4, COLOUR_8);

    xc = xpart(j * 26, 11) + 10;
    rectfill(temp, x + 13 - xc - size_adjust, y - 5 + size_adjust, x + 7 - xc + size_adjust, y + 18 - size_adjust, col2);
    rect(temp, x + 13 - xc - size_adjust, y - 5 + size_adjust, x + 7 - xc + size_adjust, y + 18 - size_adjust, COLOUR_8);
    xc = xpart(j * 26, 15) + 15;
    rectfill(temp, x + 16 - xc - size_adjust, y - 2 + size_adjust, x + 13 - xc + size_adjust, y + 9 - size_adjust, col3);
    rect(temp, x + 16 - xc - size_adjust, y - 2 + size_adjust, x + 13 - xc + size_adjust, y + 9 - size_adjust, COLOUR_8);

//    textprintf_ex(temp, font, 10, 10, COLOUR_1, -1, "%i", j);

    player_rle [i] [j] = get_rle_sprite(temp);
   }
  }



   int halved = 0;

  for (i = 0; i < 2; i ++)
  {
   if (i == 1)
   {
    col1 = COLOUR_1;
    col2 = COLOUR_2;
    col3 = COLOUR_3;
   }
    else
     {
      col1 = COLOUR_11;
      col2 = COLOUR_10;
      col3 = COLOUR_9;
     }
   for (j = -3; j < 4; j ++)
   {
    if (j < 0)
     halved = ANGLE_2;
      else
       halved = 0;
    clear_bitmap(temp);
    xc = xpart(j * 26 + halved, 15) + 15;
    size_adjust = xpart(j * 17 + ANGLE_4 + halved, 2);
    rectfill(temp, x - 16 + xc, y - 2 - size_adjust, x - 13 + xc, y + 9 + size_adjust, col3);
    rect(temp, x - 16 + xc, y - 2 - size_adjust, x - 13 + xc, y + 9 + size_adjust, COLOUR_8);
    xc = xpart(j * 26 + halved, 11) + 10;
    rectfill(temp, x - 13 + xc + size_adjust, y - 5 - size_adjust, x - 7 + xc - size_adjust, y + 18 + size_adjust, col2);
    rect(temp, x - 13 + xc + size_adjust, y - 5 - size_adjust, x - 7 + xc - size_adjust, y + 18 + size_adjust, COLOUR_8);

    xc = xpart(j * 26 + halved, 8) + 7;
//    rectfill(temp, x - 7 + xc, y - 1, x - 3, y + 4, col3);
//    rect(temp, x - 7 + xc, y - 1, x - 3, y + 4, COLOUR_8);
    rectfill(temp, x - 8 + xc, y - 1, x - 3, y + 4, col3);
    rect(temp, x - 8 + xc, y - 1, x - 3, y + 4, COLOUR_8);

    hline(temp, x - 3, y - 1, x + 3, col3);
    hline(temp, x - 3, y - 2, x + 3, COLOUR_8);
    rectfill(temp, x - 3, y - 0, x + 3, y + 7, col1);
    rect(temp, x - 3, y - 0, x + 3, y + 7, COLOUR_8);

    xc = xpart(j * 26 + halved, 7) + 7;
    xc2 = xpart(j * 26 + halved, 4) + 3;
    rectfill(temp, x + 3 - xc2, y - 1, x + 7 - xc, y + 4, col3);
    rect(temp, x + 3 - xc2, y - 1, x + 7 - xc, y + 4, COLOUR_8);

    xc = xpart(j * 26 + halved, 11) + 10;
    rectfill(temp, x + 13 - xc - size_adjust, y - 5 + size_adjust, x + 7 - xc + size_adjust, y + 18 - size_adjust, col2);
    rect(temp, x + 13 - xc - size_adjust, y - 5 + size_adjust, x + 7 - xc + size_adjust, y + 18 - size_adjust, COLOUR_8);
    xc = xpart(j * 26 + halved, 15) + 15;
    rectfill(temp, x + 16 - xc - size_adjust, y - 2 + size_adjust, x + 13 - xc + size_adjust, y + 9 - size_adjust, col3);
    rect(temp, x + 16 - xc - size_adjust, y - 2 + size_adjust, x + 13 - xc + size_adjust, y + 9 - size_adjust, COLOUR_8);

//    textprintf_ex(temp, font, 10, 10, COLOUR_1, -1, "%i", j);

    normal_player_rle [i] [j + 3] = get_rle_sprite(temp);
   }
  }


 destroy_bitmap(temp);

 */
/*
 BITMAP *tmp = new_bitmap(750, 150, "big");

 clear_bitmap(tmp);


 for (i = 0; i < 2; i ++)
 {
  for (j = 0; j < 20; j ++)
  {
    draw_rle_sprite(tmp, player_rle [i] [j], j * 37, i * 30);
    rect(tmp, j * 37, i * 30, j * 37 + 37, i * 30 + 30, TRANS_PURPLE2);
  }
 }

 for (i = 0; i < 2; i ++)
 {
  for (j = 0; j < 7; j ++)
  {
    draw_rle_sprite(tmp, normal_player_rle [i] [j], j * 37, i * 30 + 62);
    rect(tmp, j * 37, i * 30 + 62, j * 37 + 37, i * 30 + 30 + 62, TRANS_PURPLE2);
  }
 }


 save_bitmap("player.bmp", tmp, palet);
*/
}




void prepare_platform_rles(void)
{


 BITMAP *file_bitmap = load_up_bitmap("gfx//platform.bmp");

 fix_outline(file_bitmap);

 int i;

 for (i = 0; i < 2; i ++)
 {

      platform_RLE [i] [PLATFORM_LR] = extract_rle_sprite(file_bitmap, 1 + FILE_PLATFORM_LR * 56, 1 + i * 56, 55, 55);
      platform_RLE [i] [PLATFORM_UD] = extract_rle_sprite(file_bitmap, 1 + FILE_PLATFORM_UD * 56, 1 + i * 56, 55, 55);

      platform_RLE [i] [PLATFORM_LR_TLR] = extract_rle_sprite(file_bitmap, 1 + FILE_PLATFORM_LR_TLR * 56, 1 + i * 56, 55, 55);
      platform_RLE [i] [PLATFORM_LR_TL] = extract_rle_sprite(file_bitmap, 1 + FILE_PLATFORM_LR_TL * 56, 1 + i * 56, 55, 55);
      platform_RLE [i] [PLATFORM_LR_TR] = extract_flip_rle_sprite(file_bitmap, 1 + FILE_PLATFORM_LR_TL * 56, 1 + i * 56, 55, 55, 0);

      platform_RLE [i] [PLATFORM_UD_TUD] = extract_rle_sprite(file_bitmap, 1 + FILE_PLATFORM_UD_TUD * 56, 1 + i * 56, 55, 55);
      platform_RLE [i] [PLATFORM_UD_TU] = extract_rle_sprite(file_bitmap, 1 + FILE_PLATFORM_UD_TU * 56, 1 + i * 56, 55, 55);
      platform_RLE [i] [PLATFORM_UD_TD] = extract_flip_rle_sprite(file_bitmap, 1 + FILE_PLATFORM_UD_TU * 56, 1 + i * 56, 55, 55, 1);

      platform_RLE [i] [PLATFORM_UL] = extract_rle_sprite(file_bitmap, 1 + FILE_PLATFORM_UL * 56, 1 + i * 56, 55, 55);
      platform_RLE [i] [PLATFORM_DL] = extract_flip_rle_sprite(file_bitmap, 1 + FILE_PLATFORM_UL * 56, 1 + i * 56, 55, 55, 1);
      platform_RLE [i] [PLATFORM_UR] = extract_flip_rle_sprite(file_bitmap, 1 + FILE_PLATFORM_UL * 56, 1 + i * 56, 55, 55, 0);
      platform_RLE [i] [PLATFORM_DR] = extract_flip_rle_sprite(file_bitmap, 1 + FILE_PLATFORM_UL * 56, 1 + i * 56, 55, 55, 2);

      platform_RLE [i] [PLATFORM_UDL] = extract_rle_sprite(file_bitmap, 1 + FILE_PLATFORM_UDL * 56, 1 + i * 56, 55, 55);
      platform_RLE [i] [PLATFORM_UDR] = extract_flip_rle_sprite(file_bitmap, 1 + FILE_PLATFORM_UDL * 56, 1 + i * 56, 55, 55, 0);

      platform_RLE [i] [PLATFORM_ULR] = extract_rle_sprite(file_bitmap, 1 + FILE_PLATFORM_ULR * 56, 1 + i * 56, 55, 55);
      platform_RLE [i] [PLATFORM_DLR] = extract_flip_rle_sprite(file_bitmap, 1 + FILE_PLATFORM_ULR * 56, 1 + i * 56, 55, 55, 1);

      platform_RLE [i] [PLATFORM_L] = extract_rle_sprite(file_bitmap, 1 + FILE_PLATFORM_L * 56, 1 + i * 56, 55, 55);
      platform_RLE [i] [PLATFORM_R] = extract_flip_rle_sprite(file_bitmap, 1 + FILE_PLATFORM_L * 56, 1 + i * 56, 55, 55, 0);
      platform_RLE [i] [PLATFORM_U] = extract_rle_sprite(file_bitmap, 1 + FILE_PLATFORM_U * 56, 1 + i * 56, 55, 55);
      platform_RLE [i] [PLATFORM_D] = extract_flip_rle_sprite(file_bitmap, 1 + FILE_PLATFORM_U * 56, 1 + i * 56, 55, 55, 1);


      platform_RLE [i] [PLATFORM_UR] = extract_flip_rle_sprite(file_bitmap, 1 + FILE_PLATFORM_UL * 56, 1 + i * 56, 55, 55, 0);
      platform_RLE [i] [PLATFORM_DR] = extract_flip_rle_sprite(file_bitmap, 1 + FILE_PLATFORM_UL * 56, 1 + i * 56, 55, 55, 2);

      platform_RLE [i] [PLATFORM_NODE] = extract_rle_sprite(file_bitmap, 1 + FILE_PLATFORM_NODE * 56, 1 + i * 56, 55, 55);

 }

 destroy_bitmap(file_bitmap);

}


void prepare_s_enemy_rles(void)
{


 BITMAP *file_bitmap = load_up_bitmap("gfx//s_enemy.bmp");

 fix_outline(file_bitmap);

// BITMAP *temp_bitmap1 = new_bitmap(30, 30, "Prepare s_enemy_rles temp_bitmap1");

 //int i, j;

//      clear_bitmap(temp_bitmap1);

//      blit(file_bitmap, temp_bitmap1, i * 30 + 1, 1, 0, 0, 36, 29);
      //RLE_SPRITE  [0] = sized_rle_sprite(BITMAP *source, int x, int y, const char errtxt [])

      eRLE_basic1 [S_ENEMY_BASIC1_CENTRE] = extract_rle_sprite(file_bitmap, 1, 1, 17, 17);
      eRLE_basic1 [S_ENEMY_BASIC1_LWING] = extract_rle_sprite(file_bitmap, 32, 1, 13, 22);
      eRLE_basic1 [S_ENEMY_BASIC1_RWING] = extract_flip_rle_sprite(file_bitmap, 32, 1, 13, 22, 0);
      eRLE_basic1 [S_ENEMY_GLIDER1_LFOOT] = extract_rle_sprite(file_bitmap, 63, 1, 29, 26);
      eRLE_basic1 [S_ENEMY_GLIDER1_RFOOT] = extract_flip_rle_sprite(file_bitmap, 63, 1, 29, 26, 0);
      eRLE_basic1 [S_ENEMY_BASIC2_1] = extract_flip_rle_sprite(file_bitmap, 94, 1, 30, 21, 1);
      eRLE_basic1 [S_ENEMY_BASIC2_2] = extract_flip_rle_sprite(file_bitmap, 125, 1, 30, 21, 1);
      eRLE_basic1 [S_ENEMY_BASIC2_3] = extract_flip_rle_sprite(file_bitmap, 156, 1, 30, 21, 1);
      eRLE_basic1 [S_ENEMY_BASIC2_4] = extract_flip_rle_sprite(file_bitmap, 125, 1, 30, 21, 2);
      eRLE_basic1 [S_ENEMY_BASIC2_5] = extract_flip_rle_sprite(file_bitmap, 156, 1, 30, 21, 2);
      eRLE_basic1 [S_ENEMY_CARRIER_LWING1] = extract_rle_sprite(file_bitmap, 187, 1, 11, 15);
      eRLE_basic1 [S_ENEMY_CARRIER_RWING1] = extract_flip_rle_sprite(file_bitmap, 187, 1, 11, 15, 0);
      eRLE_basic1 [S_ENEMY_CARRIER_LWING2] = extract_rle_sprite(file_bitmap, 199, 1, 16, 23);
      eRLE_basic1 [S_ENEMY_CARRIER_RWING2] = extract_flip_rle_sprite(file_bitmap, 199, 1, 16, 23, 0);
      eRLE_basic1 [S_ENEMY_BASIC3] = extract_rle_sprite(file_bitmap, 187, 1, 27, 15);
      eRLE_basic1 [S_ENEMY_BASIC3_LFOOT] = extract_rle_sprite(file_bitmap, 189, 16, 12, 15);
      eRLE_basic1 [S_ENEMY_BASIC3_RFOOT] = extract_rle_sprite(file_bitmap, 200, 16, 12, 15);
      eRLE_basic1 [S_ENEMY_CROSS_L] = extract_rle_sprite(file_bitmap, 218, 1, 10, 22);
      eRLE_basic1 [S_ENEMY_CROSS_R] = extract_flip_rle_sprite(file_bitmap, 218, 1, 10, 22, 0);
      eRLE_basic1 [S_ENEMY_CROSS2_1] = extract_rle_sprite(file_bitmap, 249, 10, 8, 12);
      eRLE_basic1 [S_ENEMY_CROSS2_2] = extract_flip_rle_sprite(file_bitmap, 249, 10, 8, 21, 0);
      eRLE_basic1 [S_ENEMY_CROSS2_3] = extract_rle_sprite(file_bitmap, 249, 1, 15, 9);
      eRLE_basic1 [S_ENEMY_BFIGHTER_1] = extract_flip_rle_sprite(file_bitmap, 280, 1, 19, 20, 1);
      eRLE_basic1 [S_ENEMY_BFIGHTER_2] = extract_flip_rle_sprite(file_bitmap, 311, 1, 19, 20, 1);
      eRLE_basic1 [S_ENEMY_BFIGHTER_3] = extract_flip_rle_sprite(file_bitmap, 342, 1, 19, 20, 1);
      eRLE_basic1 [S_ENEMY_BFIGHTER_4] = extract_flip_rle_sprite(file_bitmap, 311, 1, 19, 20, 2);
      eRLE_basic1 [S_ENEMY_BFIGHTER_5] = extract_flip_rle_sprite(file_bitmap, 342, 1, 19, 20, 2);

/*      eRLE_basic1 [S_ENEMY_CROSS2_1] = extract_rle_sprite(file_bitmap, 249, 1, 15, 21);
      eRLE_basic1 [S_ENEMY_CROSS2_2] = extract_rle_sprite(file_bitmap, 280, 1, 15, 24);
      eRLE_basic1 [S_ENEMY_CROSS2_3] = extract_rle_sprite(file_bitmap, 311, 1, 15, 28);*/



/*      eRLE_basic1 [2] = extract_flip_rle_sprite(file_bitmap, 32, 1, 13, 22, 0);

      eRLE_basic1 [3] = extract_rle_sprite(file_bitmap, 63, 1, 18, 16);
      eRLE_basic1 [4] = extract_rle_sprite(file_bitmap, 94, 1, 15, 23);
      eRLE_basic1 [5] = extract_flip_rle_sprite(file_bitmap, 94, 1, 15, 23, 2);

      eRLE_basic1 [6] = extract_rle_sprite(file_bitmap, 125, 1, 17, 17);
      eRLE_basic1 [7] = extract_rle_sprite(file_bitmap, 156, 1, 14, 24);
      eRLE_basic1 [8] = extract_flip_rle_sprite(file_bitmap, 156, 1, 14, 24, 2);

      eRLE_basic1 [9] = extract_flip_rle_sprite(file_bitmap, 63, 1, 18, 16, 0);
      eRLE_basic1 [10] = extract_flip_rle_sprite(file_bitmap, 94, 1, 15, 23, 0);
      eRLE_basic1 [11] = extract_flip_rle_sprite(file_bitmap, 94, 1, 15, 23, 1);

      eRLE_basic1 [12] = extract_flip_rle_sprite(file_bitmap, 125, 1, 18, 16, 0);
      eRLE_basic1 [13] = extract_flip_rle_sprite(file_bitmap, 156, 1, 15, 23, 0);
      eRLE_basic1 [14] = extract_flip_rle_sprite(file_bitmap, 156, 1, 15, 23, 1);*/

 destroy_bitmap(file_bitmap);
// destroy_bitmap(temp_bitmap1);

}

void prepare_l_enemy_rles(void)
{


 BITMAP *file_bitmap = load_up_bitmap("gfx//l_enemy.bmp");

 fix_outline(file_bitmap);

//      eRLE_big [0] = extract_rle_sprite(file_bitmap, 1, 1, 82, 99);
      eRLE_big [L_ENEMY_GLIDER1] = extract_rle_sprite(file_bitmap, 1, 1, 87, 64);
      eRLE_big [L_ENEMY_SHOTTER1] = extract_rle_sprite(file_bitmap, 102, 1, 109, 74);
      eRLE_big [L_ENEMY_ZAPPER1] = extract_rle_sprite(file_bitmap, 212, 1, 89, 69);
      eRLE_big [L_ENEMY_DIPPER1_1] = extract_rle_sprite(file_bitmap, 313, 1, 47, 42);
      eRLE_big [L_ENEMY_DIPPER1_2] = extract_rle_sprite(file_bitmap, 362, 1, 47, 42);
      eRLE_big [L_ENEMY_DIPPER1_3] = extract_rle_sprite(file_bitmap, 411, 1, 47, 42);
      eRLE_big [L_ENEMY_DIPPER2_1] = extract_rle_sprite(file_bitmap, 313, 44, 47, 44);
      eRLE_big [L_ENEMY_DIPPER2_2] = extract_rle_sprite(file_bitmap, 362, 44, 47, 44);
      eRLE_big [L_ENEMY_DIPPER2_3] = extract_rle_sprite(file_bitmap, 411, 44, 47, 44);
//      eRLE_big [L_ENEMY_CARRIER] = extract_rle_sprite(file_bitmap, 460, 1, 37, 45);
      eRLE_big [L_ENEMY_CARRIER] = extract_rle_sprite(file_bitmap, 498, 1, 45, 45);
      eRLE_big [L_ENEMY_CARRIER_LWING1] = extract_rle_sprite(file_bitmap, 498, 65, 35, 32);
      eRLE_big [L_ENEMY_CARRIER_RWING1] = extract_flip_rle_sprite(file_bitmap, 498, 65, 35, 32, 0);
      eRLE_big [L_ENEMY_CARRIER_LWING2] = extract_flip_rle_sprite(file_bitmap, 498, 65, 35, 32, 1);
      eRLE_big [L_ENEMY_CARRIER_RWING2] = extract_flip_rle_sprite(file_bitmap, 498, 65, 35, 32, 2);
      eRLE_big [L_ENEMY_BURSTER] = extract_rle_sprite(file_bitmap, 546, 1, 27, 27);
      eRLE_big [L_ENEMY_BURSTER_LWING1] = extract_rle_sprite(file_bitmap, 546, 29, 30, 30);
      eRLE_big [L_ENEMY_BURSTER_RWING1] = extract_flip_rle_sprite(file_bitmap, 546, 29, 30, 30, 0);
      eRLE_big [L_ENEMY_BURSTER_LWING2] = extract_rle_sprite(file_bitmap, 546, 60, 27, 27);
      eRLE_big [L_ENEMY_BURSTER_RWING2] = extract_flip_rle_sprite(file_bitmap, 546, 60, 27, 27, 0);
      eRLE_big [L_ENEMY_BEAMER1] = extract_rle_sprite(file_bitmap, 577, 1, 69, 67);
      eRLE_big [L_ENEMY_BEAMER1_LFIN] = extract_rle_sprite(file_bitmap, 647, 1, 22, 53);
      eRLE_big [L_ENEMY_BEAMER1_RFIN] = extract_flip_rle_sprite(file_bitmap, 647, 1, 22, 53, 0);
      eRLE_big [L_ENEMY_BEAMER1_LFIN2] = extract_flip_rle_sprite(file_bitmap, 647, 1, 22, 53, 1);
      eRLE_big [L_ENEMY_BEAMER1_RFIN2] = extract_flip_rle_sprite(file_bitmap, 647, 1, 22, 53, 2);
      eRLE_big [L_ENEMY_BOSS1] = extract_rle_sprite(file_bitmap, 670, 1, 95, 93);
      eRLE_big [L_ENEMY_BOSS1_L1] = extract_rle_sprite(file_bitmap, 767, 1, 22, 71);
      eRLE_big [L_ENEMY_BOSS1_L2] = extract_flip_rle_sprite(file_bitmap, 790, 1, 22, 71, 2);
      eRLE_big [L_ENEMY_BOSS1_R1] = extract_flip_rle_sprite(file_bitmap, 790, 1, 22, 71, 1);
      eRLE_big [L_ENEMY_BOSS1_R2] = extract_flip_rle_sprite(file_bitmap, 767, 1, 22, 71, 0);
      eRLE_big [L_ENEMY_WINGS_L] = extract_rle_sprite(file_bitmap, 816, 3, 20, 45);
      eRLE_big [L_ENEMY_WINGS_R] = extract_flip_rle_sprite(file_bitmap, 816, 3, 20, 45, 0);
      eRLE_big [L_ENEMY_MB2_L] = extract_rle_sprite(file_bitmap, 859, 9, 31, 71);
      eRLE_big [L_ENEMY_MB2_R] = extract_flip_rle_sprite(file_bitmap, 859, 9, 31, 71, 0);
      eRLE_big [L_ENEMY_MB1] = extract_rle_sprite(file_bitmap, 915, 8, 149, 62);

/*      eRLE_basic1 [1] = extract_rle_sprite(file_bitmap, 32, 1, 13, 22);
      eRLE_basic1 [2] = extract_flip_rle_sprite(file_bitmap, 32, 1, 13, 22, 0);

      eRLE_basic1 [3] = extract_rle_sprite(file_bitmap, 63, 1, 18, 16);
      eRLE_basic1 [4] = extract_rle_sprite(file_bitmap, 94, 1, 15, 23);
      eRLE_basic1 [5] = extract_flip_rle_sprite(file_bitmap, 94, 1, 15, 23, 2);

      eRLE_basic1 [6] = extract_rle_sprite(file_bitmap, 125, 1, 17, 17);
      eRLE_basic1 [7] = extract_rle_sprite(file_bitmap, 156, 1, 14, 24);
      eRLE_basic1 [8] = extract_flip_rle_sprite(file_bitmap, 156, 1, 14, 24, 2);

      eRLE_basic1 [9] = extract_flip_rle_sprite(file_bitmap, 63, 1, 18, 16, 0);
      eRLE_basic1 [10] = extract_flip_rle_sprite(file_bitmap, 94, 1, 15, 23, 0);
      eRLE_basic1 [11] = extract_flip_rle_sprite(file_bitmap, 94, 1, 15, 23, 1);

      eRLE_basic1 [12] = extract_flip_rle_sprite(file_bitmap, 125, 1, 18, 16, 0);
      eRLE_basic1 [13] = extract_flip_rle_sprite(file_bitmap, 156, 1, 15, 23, 0);
      eRLE_basic1 [14] = extract_flip_rle_sprite(file_bitmap, 156, 1, 15, 23, 1);
*/
 destroy_bitmap(file_bitmap);
// destroy_bitmap(temp_bitmap1);

}



void fix_outline(BITMAP *source)
{
 int i, j, px;

 for (i = 0; i < source->w; i ++)
 {
  for (j = 0; j < source->h; j ++)
  {
   px = getpixel(source, i, j);
   switch(px)
   {
       case 17:
        putpixel(source, i, j, COL_OUTLINE);
        break;
   }
  }
 }

}

void fix_underlay(BITMAP *source)
{
 int i, j, px;

 for (i = 0; i < source->w; i ++)
 {
  for (j = 0; j < source->h; j ++)
  {
   px = getpixel(source, i, j);
   if (px == COL_LOWER_BG1 || px == COL_OUTLINE)
    continue;
   if (getpixel(source, i + 1, j) == COL_LOWER_BG1)
    putpixel(source, i + 1, j, COL_OUTLINE);
   if (getpixel(source, i - 1, j) == COL_LOWER_BG1)
    putpixel(source, i - 1, j, COL_OUTLINE);
   if (getpixel(source, i, j + 1) == COL_LOWER_BG1)
    putpixel(source, i, j + 1, COL_OUTLINE);
   if (getpixel(source, i, j - 1) == COL_LOWER_BG1)
    putpixel(source, i, j - 1, COL_OUTLINE);
  }
 }

}

void prepare_little_rles(void)
{

 BITMAP *file_bitmap = load_up_bitmap("gfx//little.bmp");

 fix_trans(file_bitmap);

 basic_bullet [0] = extract_rle_sprite(file_bitmap, 1, 1, 7, 38);
 basic_bullet [1] = extract_rle_sprite(file_bitmap, 32, 1, 7, 38);

 basic_bullet [2] = extract_rle_sprite(file_bitmap, 63, 1, 11, 47);
 basic_bullet [3] = extract_rle_sprite(file_bitmap, 126, 1, 11, 47);
 basic_bullet [4] = extract_rle_sprite(file_bitmap, 138, 1, 11, 47);
 basic_bullet [5] = extract_rle_sprite(file_bitmap, 150, 1, 11, 47);

 BITMAP *temp_shot_bitmap = new_bitmap(161, 63, "temp_shot_bitmap");

 blit(file_bitmap, temp_shot_bitmap, 0, 0, 0, 0, 161, 63);
 convert_trans(temp_shot_bitmap, TRANS_WHITE);
 basic_bullet [6] = extract_rle_sprite(temp_shot_bitmap, 63, 1, 11, 47);
 basic_bullet [7] = extract_rle_sprite(temp_shot_bitmap, 126, 1, 11, 47);
 basic_bullet [8] = extract_rle_sprite(temp_shot_bitmap, 138, 1, 11, 47);
 basic_bullet [9] = extract_rle_sprite(temp_shot_bitmap, 150, 1, 11, 47);

 blit(file_bitmap, temp_shot_bitmap, 0, 0, 0, 0, 161, 63);
 convert_trans(temp_shot_bitmap, TRANS_LGREEN);
 basic_bullet [10] = extract_rle_sprite(temp_shot_bitmap, 63, 1, 11, 47);
 basic_bullet [11] = extract_rle_sprite(temp_shot_bitmap, 126, 1, 11, 47);
 basic_bullet [12] = extract_rle_sprite(temp_shot_bitmap, 138, 1, 11, 47);
 basic_bullet [13] = extract_rle_sprite(temp_shot_bitmap, 150, 1, 11, 47);

 blit(file_bitmap, temp_shot_bitmap, 0, 0, 0, 0, 161, 63);
 convert_trans(temp_shot_bitmap, TRANS_DDGREEN);
 basic_bullet [14] = extract_rle_sprite(temp_shot_bitmap, 63, 1, 11, 47);
 basic_bullet [15] = extract_rle_sprite(temp_shot_bitmap, 126, 1, 11, 47);
 basic_bullet [16] = extract_rle_sprite(temp_shot_bitmap, 138, 1, 11, 47);
 basic_bullet [17] = extract_rle_sprite(temp_shot_bitmap, 150, 1, 11, 47);

 blit(file_bitmap, temp_shot_bitmap, 0, 0, 0, 0, 161, 63);
 convert_trans(temp_shot_bitmap, TRANS_LBLUE);
 basic_bullet [18] = extract_rle_sprite(temp_shot_bitmap, 63, 1, 11, 47);
 basic_bullet [19] = extract_rle_sprite(temp_shot_bitmap, 126, 1, 11, 47);
 basic_bullet [20] = extract_rle_sprite(temp_shot_bitmap, 138, 1, 11, 47);
 basic_bullet [21] = extract_rle_sprite(temp_shot_bitmap, 150, 1, 11, 47);

 blit(file_bitmap, temp_shot_bitmap, 0, 0, 0, 0, 161, 63);
 convert_trans(temp_shot_bitmap, TRANS_LPURPLE);
 basic_bullet [22] = extract_rle_sprite(temp_shot_bitmap, 63, 1, 11, 47);
 basic_bullet [23] = extract_rle_sprite(temp_shot_bitmap, 126, 1, 11, 47);
 basic_bullet [24] = extract_rle_sprite(temp_shot_bitmap, 138, 1, 11, 47);
 basic_bullet [25] = extract_rle_sprite(temp_shot_bitmap, 150, 1, 11, 47);


/*
 blit(file_bitmap, temp_shot_bitmap, 63, 1, 0, 0, 11, 47);
 convert_trans(temp_shot_bitmap, TRANS_WHITE);
 basic_bullet [6] = extract_rle_sprite(temp_shot_bitmap, 0, 0, 11, 47);
 blit(file_bitmap, temp_shot_bitmap, 126, 1, 0, 0, 11, 47);
 convert_trans(temp_shot_bitmap, TRANS_WHITE);
 basic_bullet [3] = extract_rle_sprite(temp_shot_bitmap, 0, 0, 11, 47);


 blit(file_bitmap, temp_shot_bitmap, 137, 1, 0, 0, 11, 47);
 convert_trans(temp_shot_bitmap, TRANS_LGREEN);
 basic_bullet [4] = extract_rle_sprite(temp_shot_bitmap, 0, 0, 11, 47);

 blit(file_bitmap, temp_shot_bitmap, 149, 1, 0, 0, 11, 47);
 convert_trans(temp_shot_bitmap, TRANS_DDGREEN);
 basic_bullet [5] = extract_rle_sprite(temp_shot_bitmap, 0, 0, 11, 47);

 blit(file_bitmap, temp_shot_bitmap, 162, 1, 0, 0, 11, 47);
 convert_trans(temp_shot_bitmap, TRANS_LBLUE);
 basic_bullet [6] = extract_rle_sprite(temp_shot_bitmap, 0, 0, 11, 47);

 blit(file_bitmap, temp_shot_bitmap, 63, 1, 0, 0, 11, 47);
 convert_trans(temp_shot_bitmap, TRANS_LPURPLE);
 basic_bullet [7] = extract_rle_sprite(temp_shot_bitmap, 0, 0, 11, 47);


 destroy_bitmap(temp_shot_bitmap);
*/

 convert_trans(file_bitmap, TRANS_YELLOW);


 exhaust [0] = extract_rle_sprite(file_bitmap, 94, 1, 5, 15);
 exhaust [1] = extract_rle_sprite(file_bitmap, 100, 1, 5, 15);
 exhaust [2] = extract_rle_sprite(file_bitmap, 106, 1, 5, 15);
 exhaust [3] = extract_rle_sprite(file_bitmap, 112, 1, 5, 15);


 BITMAP *temp_bitmap1 = new_bitmap(40, 40, "multi pbullet1");
 BITMAP *temp_bitmap2 = new_bitmap(40, 40, "multi pbullet2");
 clear_bitmap(temp_bitmap1);
 clear_bitmap(temp_bitmap2);

 blit(file_bitmap, temp_bitmap1, 118, 1, 15, 0, 7, 28);

// draw_rle_sprite(temp_bitmap1, basic_bullet [0], 15, 0);

 int i;

 for (i = 0; i < 5; i ++)
 {
  clear_bitmap(temp_bitmap2);
  rotate_sprite(temp_bitmap2, temp_bitmap1, 0, 0, itofix(i * 8));
  multi_bullet [0] [i] = extract_rle_sprite(temp_bitmap2, 0, 0, 40, 40);
  multi_bullet [1] [i] = extract_flip_rle_sprite(temp_bitmap2, 0, 0, 40, 40, 0);
 }


 destroy_bitmap(file_bitmap);
 destroy_bitmap(temp_bitmap1);
 destroy_bitmap(temp_bitmap2);
// destroy_bitmap(temp_bitmap1);

}

void fix_trans(BITMAP *source)
{
 int i, j, px;

 for (i = 0; i < source->w; i ++)
 {
  for (j = 0; j < source->h; j ++)
  {
   px = getpixel(source, i, j);
   switch(px)
   {
       case 12:
        putpixel(source, i, j, TRANS_YELLOW);
        break;
       case 13:
        putpixel(source, i, j, TRANS_LRED);
        break;
       case 14:
        putpixel(source, i, j, TRANS_DRED);
        break;
       case 15:
        putpixel(source, i, j, TRANS_DDRED);
        break;
   }
  }
 }

}

void convert_trans(BITMAP *source, int target)
{
 int i, j, px;
 int col1 = TRANS_YELLOW;
 int col2 = TRANS_LGREEN;
 int col3 = TRANS_DGREEN;
 int col4 = TRANS_DDGREEN;

 if (target == TRANS_DDGREEN)
 {
  col1 = TRANS_WHITE;
  col2 = TRANS_YELLOW;
  col3 = TRANS_LGREEN;
  col4 = TRANS_DGREEN;
 }

 if (target == TRANS_YELLOW)
 {
  col1 = TRANS_YELLOW;
  col2 = TRANS_LRED;
  col3 = TRANS_DRED;
  col4 = TRANS_DDRED;
 }


 if (target == TRANS_LBLUE)
 {
  col1 = TRANS_WHITE;
  col2 = TRANS_LBLUE;
  col3 = TRANS_DBLUE;
  col4 = TRANS_DDBLUE;
 }

 if (target == TRANS_WHITE)
 {
  col1 = TRANS_WHITE;
  col2 = TRANS_YELLOW;
  col3 = TRANS_LRED;
  col4 = TRANS_DDRED;
 }

 if (target == TRANS_DBLUE)
 {
  col1 = TRANS_LBLUE;
  col2 = TRANS_DBLUE;
  col3 = TRANS_DBLUE;
  col4 = TRANS_DDBLUE;
 }

 if (target == TRANS_LPURPLE)
 {
  col1 = TRANS_WHITE;
  col2 = TRANS_LPURPLE;
  col3 = TRANS_DPURPLE;
  col4 = TRANS_DDBLUE;
 }



 for (i = 0; i < source->w; i ++)
 {
  for (j = 0; j < source->h; j ++)
  {
   px = getpixel(source, i, j);
   switch(px)
   {
       case TRANS_YELLOW:
        putpixel(source, i, j, col1);
        break;
       case TRANS_LRED:
        putpixel(source, i, j, col2);
        break;
       case TRANS_DRED:
        putpixel(source, i, j, col3);
        break;
       case TRANS_DDRED:
        putpixel(source, i, j, col4);
        break;
   }
  }
 }

}



/*
void prepare_large_enemy_rles(void)
{


 BITMAP *file_bitmap = load_up_bitmap("gfx//elarge.bmp");


 BITMAP *temp_bitmap1 = new_bitmap(100, 100, "Prepare enemy_rles temp_bitmap1");
 BITMAP *temp_bitmap2 = new_bitmap(100, 100, "Prepare enemy_rles temp_bitmap2");

 int i;

#ifdef XFALSE

int k, l, x = 50, y = 50;

extern volatile unsigned char ticked;
extern int slacktime;

BITMAP *tmp = create_bitmap(640,480);

clear_bitmap(tmp);

do {
clear_bitmap(tmp);


  blit(file_bitmap, tmp, 0, 0, 0, 0, file_bitmap->w, file_bitmap->h);

  drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);

  if (key [KEY_UP]) y --;
  if (key [KEY_DOWN]) y ++;
  if (key [KEY_LEFT]) x --;
  if (key [KEY_RIGHT]) x ++;

  textprintf_ex(tmp, font, 600, 460, COLOUR_11, 1, "%i", getpixel(tmp, x, y));

  putpixel(tmp, x, y, grand(200));

  blit(tmp, screen, 0, 0, 0, 0, 640, 480);


  do
  {
   slacktime ++;
  } while(ticked == 0);
  ticked --;


    } while (key [KEY_SPACE] == 0);

#endif

#ifdef TEST_COLOURS

BITMAP *tmp = create_bitmap(640,480);

do
{
   clear_to_color(tmp, COL_LOWER_BG1);
   rectfill(tmp, 10, 80, 300, 90, COL_LGREY);
   rectfill(tmp, 10, 90, 300, 100, COL_LOWER_BG2);
   drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);
   rectfill(tmp, 10, 10, 50, 300, TRANS_WHITE);
   rectfill(tmp, 20, 20, 60, 300, TRANS_YELLOW);
   rectfill(tmp, 30, 30, 70, 300, TRANS_LBLUE);
   rectfill(tmp, 40, 40, 80, 300, TRANS_DRED);
  drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);
     vsync();
  blit(tmp, screen, 0, 0, 0, 0, 640, 480);

} while (key [KEY_SPACE] == 0);

int k, l;

do
{
for (k = 0; k < 16; k ++)
{
for (l = 0; l < 16; l ++)
{
//   clear_to_color(tmp, COL_LOWER_BG1);
//if (l + k > 0)
if (l == 0)
{
   rectfill(tmp, k * 20, l * 20, k * 20 + 20, l * 20 + 20, k + (l * 16) + 1);
   textprintf_ex(tmp, font, k * 20, l * 20  + 5, COL_WHITE, -1, "%i", k + (l * 16) + 1);
}
    else
    {
     rectfill(tmp, k * 20, l * 20, k * 20 + 20, l * 20 + 20, k + (l * 16) + 1);
     textprintf_ex(tmp, font, k * 20, l * 20 + 5, COL_WHITE, -1, "%i", k + (l * 16) + 1);
    }
}
}

  blit(tmp, screen, 0, 0, 0, 0, 640, 480);

} while (key [KEY_A] == 0);

destroy_bitmap(tmp);

#endif

 drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);
// rectfill(file_bitmap, 0, 0, file_bitmap->w, file_bitmap->h, FIX_BITMAP);
 drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);

 for (i = 0; i < LARGE_ERLES; i ++)
 {
      if (i == LARGE_BEAMER1_RWING)
       continue;
      clear_bitmap(temp_bitmap1);
      blit(file_bitmap, temp_bitmap1, i * 101 + 1, 1, 0, 0, 100, 100);
      new_rle_struct(temp_bitmap1, "elarge", &eRLE_large [1] [i], -1);
      if (i == LARGE_BEAMER1_LWING)
      {
        clear_bitmap(temp_bitmap2);
        draw_sprite_h_flip(temp_bitmap2, temp_bitmap1, 0, 0);
        new_rle_struct(temp_bitmap2, "elarge", &eRLE_large [1] [i + 1], -1);
      }
//      convert_pole(temp_bitmap1, POLE_BLACK);
      new_rle_struct(temp_bitmap1, "elarge", &eRLE_large [0] [i], -1);
      if (i == LARGE_BEAMER1_LWING)
      {
        clear_bitmap(temp_bitmap2);
        draw_sprite_h_flip(temp_bitmap2, temp_bitmap1, 0, 0);
        new_rle_struct(temp_bitmap2, "elarge", &eRLE_large [0] [i + 1], -1);
      }

 }

 destroy_bitmap(file_bitmap);
 destroy_bitmap(temp_bitmap1);
 destroy_bitmap(temp_bitmap2);



}


void convert_pole(BITMAP *bitmap, int pole)
{

 drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);
 rectfill(bitmap, 0, 0, bitmap->w, bitmap->h, CONVERT_POLE);
 drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);

}
*/

void prepare_underlay(void)
{

 underlay = new_bitmap(320, 1000, "underlay");

 clear_to_color(underlay, COL_LOWER_BG1);

 BITMAP *temp_bitmap = load_up_bitmap("gfx//underlay.bmp");

 blit(temp_bitmap, underlay, 0, 0, 0, 0, 320, 480);
 blit(temp_bitmap, underlay, 0, 0, 0, 480, 320, 480);

fix_underlay(underlay);
destroy_bitmap(temp_bitmap);


 underlay2 = new_bitmap(320, 1000, "underlay");

 clear_to_color(underlay2, COL_LOWER_BG1);

 temp_bitmap = load_up_bitmap("gfx//underlay2.bmp");

 blit(temp_bitmap, underlay2, 0, 0, 0, 0, 320, 480);
 blit(temp_bitmap, underlay2, 0, 0, 0, 480, 320, 480);

fix_underlay(underlay2);
destroy_bitmap(temp_bitmap);

}



void prepare_small_sprites(void)
{

#ifdef COLOUR_DISPLAY

int k, l, x = 50, y = 50;

extern volatile unsigned char ticked;
extern int slacktime;

BITMAP *tmp = create_bitmap(640,480);

clear_bitmap(tmp);

do {
clear_bitmap(tmp);

for (k = 0; k < 13; k ++)
{
  rectfill(tmp, 50, 50 + k * 20, 500, 70 + k * 20, k);
}
/*for (k = 0; k < 12; k ++)
{
  rectfill(screen, 50, 50 + k * 20, 500, 70 + k * 20, k * 12 + 1);
}*/

  drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);

  for (l = 1; l < 8; l ++)
  {

    rectfill(tmp, 70 + l * 20, 50, 90 + l * 20, 450, l * 12 + 1);


  }

  for (l = 8; l < 16; l ++)
  {

    rectfill(tmp, 70 + l * 20, 50, 90 + l * 20, 450, l * 12 + 1);


  }

  drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);

  if (key [KEY_UP]) y --;
  if (key [KEY_DOWN]) y ++;
  if (key [KEY_LEFT]) x --;
  if (key [KEY_RIGHT]) x ++;

  textprintf_ex(tmp, font, 600, 460, COLOUR_11, 1, "%i", getpixel(tmp, x, y));

  putpixel(tmp, x, y, grand(200));

  blit(tmp, screen, 0, 0, 0, 0, 640, 480);


  do
  {
   slacktime ++;
  } while(ticked == 0);
  ticked --;


    } while (key [KEY_SPACE] == 0);

#endif

/* palet [7].r = 1;
 set_palette(palet);*/
 BITMAP *file_bitmap = load_up_bitmap("gfx//small.bmp");
/* palet [7].r = 0x8d;
 set_palette(palet);*/

 BITMAP *temp_bitmap1 = new_bitmap(14, 14, "Prepare small sprites temp_bitmap1");
 BITMAP *temp_bitmap2 = new_bitmap(14, 14, "Prepare small sprites temp_bitmap2");

 int i;

 clear_bitmap(temp_bitmap1);
 blit(file_bitmap, temp_bitmap1, 31, 1, 0, 0, 14, 14);
 process_trans_bitmap(temp_bitmap1, 14, 14);

/*
 int ya = 7;
 int getp = getpixel(temp_bitmap1, 1, ya);
 int getp1 = getpixel(temp_bitmap1, 2, ya);
 int getp2 = getpixel(temp_bitmap1, 3, ya);
 int getp3 = getpixel(temp_bitmap1, 4, ya);
 int getp4 = getpixel(temp_bitmap1, 5, ya);
 int getp5 = getpixel(temp_bitmap1, 6, ya);
 int getp6 = getpixel(temp_bitmap1, 7, ya);
 int getp7 = getpixel(temp_bitmap1, 8, ya);
 int getp8 = getpixel(temp_bitmap1, 9, ya);
 int getp9 = getpixel(temp_bitmap1, 10, ya);
  set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
  allegro_message("%i %i %i %i %i %i %i %i %i %i", getp, getp1, getp2, getp3, getp4, getp5, getp6, getp7, getp8, getp9);
  exit(1);
*/


 for (i = 0; i < SMALL_ROTATIONS; i ++)
 {
   clear_bitmap(temp_bitmap2);
   rotate_sprite(temp_bitmap2, temp_bitmap1, 0, 0, itofix(i * 4 + 64));
//   rectfill(temp_bitmap2, 5, 5, 8, 8, TRANS_WHITE5);
//   rectfill(temp_bitmap2, 6, 6, 7, 7, TRANS_WHITE6);
   circlefill(temp_bitmap2, 7, 7, 2, TRANS_DRED);
   circlefill(temp_bitmap2, 7, 7, 1, TRANS_DDRED);
   spin_bullet [0] [i] = get_rle_sprite(temp_bitmap2);
   convert_trans_bitmap(temp_bitmap2, 14, 14);
   spin_bullet [1] [i] = get_rle_sprite(temp_bitmap2);
 }

 destroy_bitmap(file_bitmap);
 destroy_bitmap(temp_bitmap1);
 destroy_bitmap(temp_bitmap2);

}

//void small_sprite_rotation(BITMAP *file_bitmap, BITMAP *temp_bitmap1, BITMAP *temp_bitmap2, int acrorss


void prepare_twister(void)
{
/*
 BITMAP *temp_bitmap1 = new_bitmap(25, 25, "Prepare twister temp_bitmap1");

 int i, j, angle, x, y;

 x = 12;
 y = 12;

 int col [4] = {COLOUR_2, COLOUR_3, COLOUR_4, COLOUR_OUTLINE};

 for (i = 0; i < 2; i ++)
 {
  if (i == 1)
  {
   col [0] = COLOUR_12;
   col [1] = COLOUR_11;
   col [2] = COLOUR_10;
   col [3] = COLOUR_OUTLINE;

  }
  for (j = 0; j < MEDIUM_ROTATIONS; j ++)
  {

   angle = j * (ANGLE_1 / MEDIUM_ROTATIONS);
   x = 12;
   y = 12;

   clear_bitmap(temp_bitmap1);

        circlefill(temp_bitmap1, x, y, 4, col [1]);
        circle(temp_bitmap1, x, y, 4, COLOUR_OUTLINE);

        bordered_triangle(temp_bitmap1,
         x + xpart(angle - ANGLE_8, 2), y + ypart(angle - ANGLE_8, 2),
         x + xpart(angle - ANGLE_64, 13), y + ypart(angle - ANGLE_64, 13),
         x + xpart(angle - ANGLE_4 - ANGLE_32, 8), y + ypart(angle - ANGLE_4 - ANGLE_32, 8),
         col [0], COLOUR_OUTLINE, x + xpart(angle - ANGLE_8, 4), y + ypart(angle - ANGLE_8, 4));

        bordered_triangle(temp_bitmap1,
         x + xpart(angle + ANGLE_8, 2), y + ypart(angle + ANGLE_8, 2),
         x + xpart(angle + ANGLE_64, 13), y + ypart(angle + ANGLE_64, 13),
         x + xpart(angle + ANGLE_4 + ANGLE_32, 8), y + ypart(angle + ANGLE_4 + ANGLE_32, 8),
         col [0], COLOUR_OUTLINE, x + xpart(angle + ANGLE_8, 4), y + ypart(angle + ANGLE_8, 4));

        x -= xpart(angle, 3);
        y -= ypart(angle, 3);

        bordered_poly4(temp_bitmap1,
         x + xpart(angle + ANGLE_8, 3), y + ypart(angle + ANGLE_8, 3),
         x + xpart(angle + ANGLE_4 + ANGLE_32, 8), y + ypart(angle + ANGLE_4 + ANGLE_32, 8),
         x + xpart(angle + ANGLE_4 + ANGLE_8, 11), y + ypart(angle + ANGLE_4 + ANGLE_8, 11),
         x + xpart(angle + ANGLE_4 + ANGLE_8 + ANGLE_16, 6), y + ypart(angle + ANGLE_4 + ANGLE_8 + ANGLE_16, 6),
         col [0], COLOUR_OUTLINE, x + xpart(angle + ANGLE_4 + ANGLE_16 + ANGLE_64, 7), y + ypart(angle + ANGLE_4 + ANGLE_16 + ANGLE_64, 7));

        bordered_poly4(temp_bitmap1,
         x + xpart(angle - ANGLE_8, 3), y + ypart(angle - ANGLE_8, 3),
         x + xpart(angle - ANGLE_4 - ANGLE_32, 8), y + ypart(angle - ANGLE_4 - ANGLE_32, 8),
         x + xpart(angle - ANGLE_4 - ANGLE_8, 11), y + ypart(angle - ANGLE_4 - ANGLE_8, 11),
         x + xpart(angle - ANGLE_4 - ANGLE_8 - ANGLE_16, 6), y + ypart(angle - ANGLE_4 - ANGLE_8 - ANGLE_16, 6),
         col [0], COLOUR_OUTLINE, x + xpart(angle - ANGLE_4 - ANGLE_16 - ANGLE_64, 7), y + ypart(angle - ANGLE_4 - ANGLE_16 - ANGLE_64, 7));


     new_rle_struct(temp_bitmap1, "twister", eRLE_twister [i], j);
//   eRLE_twister [i] [j] = new_rle_sprite(temp_bitmap1, "twister");
  }
 }

 destroy_bitmap(temp_bitmap1);
*/
}

void prepare_attacker(void)
{
/*
 BITMAP *temp_bitmap1 = new_bitmap(91, 91, "Prepare attacker temp_bitmap1");

 int i, j, angle, x, y, which_attacker = 0;

 x = 45;
 y = 45;

 int col [4];// = {COLOUR_2, COLOUR_3, COLOUR_4, COLOUR_OUTLINE};

 for (which_attacker = 0; which_attacker < 3; which_attacker ++)
 {
 for (i = 0; i < 2; i ++)
 {
  if (i == 1)
  {
   col [0] = COLOUR_12;
   col [1] = COLOUR_11;
   col [2] = COLOUR_10;
   col [3] = COLOUR_OUTLINE;

  }
   else
   {
    col [0] = COLOUR_2;
    col [1] = COLOUR_3;
    col [2] = COLOUR_4;
    col [3] = COLOUR_OUTLINE;
   }
  for (j = 0; j < MEDIUM_ROTATIONS; j ++)
  {

   angle = j * (ANGLE_1 / MEDIUM_ROTATIONS);

   clear_bitmap(temp_bitmap1);

   switch(which_attacker)
   {
     case 0:   // enemy_attacker1

      bordered_poly4(temp_bitmap1, x + xpart(angle, 3), y + ypart(angle, 3),
          x + xpart(angle - ANGLE_4, 8), y + ypart(angle - ANGLE_4, 8),
          x - xpart(angle, 9), y - ypart(angle, 9),
          x + xpart(angle + ANGLE_4, 8), y + ypart(angle + ANGLE_4, 8),
          col [2], COLOUR_OUTLINE, x, y);

      bordered_poly4(temp_bitmap1, x - xpart(angle, 3), y - ypart(angle, 3),
          x + xpart(angle - ANGLE_4 - ANGLE_8 - ANGLE_32, 13), y + ypart(angle - ANGLE_4 - ANGLE_8 - ANGLE_32, 13),
          x - xpart(angle, 28), y - ypart(angle, 28),
          x + xpart(angle + ANGLE_4 + ANGLE_8 + ANGLE_32, 13), y + ypart(angle + ANGLE_4 + ANGLE_8 + ANGLE_32, 13),
          col [1], COLOUR_OUTLINE, x - xpart(angle, 6), y - ypart(angle, 6));

      bordered_poly4(temp_bitmap1, x - xpart(angle, 9), y - ypart(angle, 9),
          x + xpart(angle - ANGLE_4 - ANGLE_8 - 42, 18), y + ypart(angle - ANGLE_4 - ANGLE_8 - 42, 18),
          x - xpart(angle, 28), y - ypart(angle, 28),
          x + xpart(angle + ANGLE_4 + ANGLE_8 + 42, 18), y + ypart(angle + ANGLE_4 + ANGLE_8 + 42, 18),
          col [0], COLOUR_OUTLINE, x - xpart(angle, 12), y - ypart(angle, 12));

     new_rle_struct(temp_bitmap1, "attacker1_2 0", eRLE_attacker [which_attacker] [0] [i], j);

// fall through case 0!

     case 1:   // enemy_attacker2
     case 2:   // enemy_attacker3

     clear_bitmap(temp_bitmap1);
      if (which_attacker == 0)
      {
       bordered_poly4(temp_bitmap1, x + xpart(angle + ANGLE_32, 2), y + ypart(angle + ANGLE_32, 2),
          x + xpart(angle + ANGLE_4 + ANGLE_8, 11), y + ypart(angle + ANGLE_4 + ANGLE_8, 11),
          x + xpart(angle + ANGLE_4, 21), y + ypart(angle + ANGLE_4, 21),
          x + xpart(angle + ANGLE_16, 9), y + ypart(angle + ANGLE_16, 9),
          col [0], COLOUR_OUTLINE, x + xpart(angle + ANGLE_4, 15), y + ypart(angle + ANGLE_4, 15));

       bordered_poly4(temp_bitmap1, x + xpart(angle - ANGLE_32, 2), y + ypart(angle - ANGLE_32, 2),
          x + xpart(angle - ANGLE_4 - ANGLE_8, 11), y + ypart(angle - ANGLE_4 - ANGLE_8, 11),
          x + xpart(angle - ANGLE_4, 21), y + ypart(angle - ANGLE_4, 21),
          x + xpart(angle - ANGLE_16, 9), y + ypart(angle - ANGLE_16, 9),
          col [0], COLOUR_OUTLINE, x + xpart(angle - ANGLE_4, 15), y + ypart(angle - ANGLE_4, 15));

      new_rle_struct(temp_bitmap1, "attacker1 1", eRLE_attacker [which_attacker] [1] [i], j);
    }
      if (which_attacker == 1)
      {
       bordered_poly4(temp_bitmap1, x + xpart(angle + ANGLE_4 + ANGLE_32, 4), y + ypart(angle + ANGLE_4 + ANGLE_32, 4),
          x + xpart(angle + ANGLE_4 + ANGLE_8, 14), y + ypart(angle + ANGLE_4 + ANGLE_8, 14),
          x + xpart(angle + ANGLE_4, 24), y + ypart(angle + ANGLE_4, 24),
          x + xpart(angle + ANGLE_8, 11), y + ypart(angle + ANGLE_8, 11),
          col [0], COLOUR_OUTLINE, x + xpart(angle + ANGLE_4, 15), y + ypart(angle + ANGLE_4, 15));

       bordered_poly4(temp_bitmap1, x + xpart(angle - ANGLE_4 - ANGLE_32, 4), y + ypart(angle - ANGLE_4 - ANGLE_32, 4),
          x + xpart(angle - ANGLE_4 - ANGLE_8, 14), y + ypart(angle - ANGLE_4 - ANGLE_8, 14),
          x + xpart(angle - ANGLE_4, 24), y + ypart(angle - ANGLE_4, 24),
          x + xpart(angle - ANGLE_8, 11), y + ypart(angle - ANGLE_8, 11),
          col [0], COLOUR_OUTLINE, x + xpart(angle - ANGLE_4, 15), y + ypart(angle - ANGLE_4, 15));

       bordered_poly4(temp_bitmap1, x - xpart(angle, 4), y - ypart(angle, 4),
          x + xpart(angle + ANGLE_4 - ANGLE_8, 8), y + ypart(angle + ANGLE_4 - ANGLE_8, 8),
          x + xpart(angle, 11), y + ypart(angle, 11),
          x + xpart(angle - ANGLE_4 + ANGLE_8, 8), y + ypart(angle - ANGLE_4 + ANGLE_8, 8),
          col [0], COLOUR_OUTLINE, x + xpart(angle, 5), y + ypart(angle, 5));

      new_rle_struct(temp_bitmap1, "attacker2 1", eRLE_attacker [which_attacker] [1] [i], j);
     }
      if (which_attacker == 2)
      {

       bordered_poly4(temp_bitmap1, x + xpart(angle + ANGLE_32, 2), y + ypart(angle + ANGLE_32, 2),
          x + xpart(angle + ANGLE_4 + ANGLE_8, 11), y + ypart(angle + ANGLE_4 + ANGLE_8, 11),
          x + xpart(angle + ANGLE_4, 21), y + ypart(angle + ANGLE_4, 21),
          x + xpart(angle + ANGLE_16, 9), y + ypart(angle + ANGLE_16, 9),
          col [1], COLOUR_OUTLINE, x + xpart(angle + ANGLE_4, 10), y + ypart(angle + ANGLE_4, 10));

       bordered_poly4(temp_bitmap1, x + xpart(angle - ANGLE_32, 2), y + ypart(angle - ANGLE_32, 2),
          x + xpart(angle - ANGLE_4 - ANGLE_8, 11), y + ypart(angle - ANGLE_4 - ANGLE_8, 11),
          x + xpart(angle - ANGLE_4, 21), y + ypart(angle - ANGLE_4, 21),
          x + xpart(angle - ANGLE_16, 9), y + ypart(angle - ANGLE_16, 9),
          col [1], COLOUR_OUTLINE, x + xpart(angle - ANGLE_4, 10), y + ypart(angle - ANGLE_4, 10));

       bordered_poly4(temp_bitmap1,
          x + xpart(angle + ANGLE_32, 6), y + ypart(angle + ANGLE_32, 6),
          x + xpart(angle + ANGLE_4 + ANGLE_16 + ANGLE_32, 14), y + ypart(angle + ANGLE_4 + ANGLE_16 + ANGLE_32, 14),
          x + xpart(angle + ANGLE_4, 25), y + ypart(angle + ANGLE_4, 25),
          x + xpart(angle + ANGLE_16, 14), y + ypart(angle + ANGLE_16, 14),
          col [0], COLOUR_OUTLINE, x + xpart(angle + ANGLE_4 - ANGLE_32, 13), y + ypart(angle + ANGLE_4 - ANGLE_32, 13));

       bordered_poly4(temp_bitmap1,
          x + xpart(angle - ANGLE_32, 6), y + ypart(angle - ANGLE_32, 6),
          x + xpart(angle - ANGLE_4 - ANGLE_16 - ANGLE_32, 14), y + ypart(angle - ANGLE_4 - ANGLE_16 - ANGLE_32, 14),
          x + xpart(angle - ANGLE_4, 25), y + ypart(angle - ANGLE_4, 25),
          x + xpart(angle - ANGLE_16, 14), y + ypart(angle - ANGLE_16, 14),
          col [0], COLOUR_OUTLINE, x + xpart(angle - ANGLE_4 + ANGLE_32, 13), y + ypart(angle - ANGLE_4 + ANGLE_32, 13));



      new_rle_struct(temp_bitmap1, "attacker3 1", eRLE_attacker [which_attacker] [1] [i], j);
    }
    break;

   }

  }
 }
 }

 destroy_bitmap(temp_bitmap1);
*/
}




void prepare_diver(void)
{
/*
 BITMAP *temp_bitmap1 = new_bitmap(91, 91, "Prepare diver temp_bitmap1");

 int i, j, angle, x, y, xa, ya;

 x = 45;
 y = 45;

 int col [4];// = {COLOUR_2, COLOUR_3, COLOUR_4, COLOUR_OUTLINE};

 for (i = 0; i < 2; i ++)
 {
  if (i == 1)
  {
   col [0] = COLOUR_12;
   col [1] = COLOUR_11;
   col [2] = COLOUR_10;
   col [3] = COLOUR_OUTLINE;

  }
   else
   {
    col [0] = COLOUR_2;
    col [1] = COLOUR_3;
    col [2] = COLOUR_4;
    col [3] = COLOUR_OUTLINE;
   }
  for (j = 0; j < MEDIUM_ROTATIONS; j ++)
  {

   angle = j * (ANGLE_1 / MEDIUM_ROTATIONS);

   clear_bitmap(temp_bitmap1);



              xa = x - xpart(angle, 4);
              ya = y - ypart(angle, 4);

        bordered_poly4(temp_bitmap1,
              xa + xpart(angle + ANGLE_8, 7), ya + ypart(angle + ANGLE_8, 7),
              xa + xpart(angle - ANGLE_8, 7), ya + ypart(angle - ANGLE_8, 7),
              xa - xpart(angle + ANGLE_8, 7), ya - ypart(angle + ANGLE_8, 7),
              xa - xpart(angle - ANGLE_8, 7), ya - ypart(angle - ANGLE_8, 7),
              col [1], COLOUR_OUTLINE, xa, ya);

              xa = x + xpart(angle + ANGLE_4, 9);
              ya = y + ypart(angle + ANGLE_4, 9);

        bordered_poly4(temp_bitmap1,
              xa + xpart(angle + ANGLE_16, 10), ya + ypart(angle + ANGLE_16, 10),
              xa + xpart(angle - ANGLE_16 + 21, 17), ya + ypart(angle - ANGLE_16 + 21, 17),
              xa - xpart(angle + ANGLE_16, 12), ya - ypart(angle + ANGLE_16, 12),
              xa - xpart(angle - ANGLE_16, 12), ya - ypart(angle - ANGLE_16, 12),
              col [0], COLOUR_OUTLINE, xa, ya);

              xa = x - xpart(angle + ANGLE_4, 9);
              ya = y - ypart(angle + ANGLE_4, 9);

        bordered_poly4(temp_bitmap1,
              xa + xpart(angle + ANGLE_16 - 21, 17), ya + ypart(angle + ANGLE_16 - 21, 17),
              xa + xpart(angle - ANGLE_16, 10), ya + ypart(angle - ANGLE_16, 10),
              xa - xpart(angle + ANGLE_16, 12), ya - ypart(angle + ANGLE_16, 12),
              xa - xpart(angle - ANGLE_16, 12), ya - ypart(angle - ANGLE_16, 12),
              col [0], COLOUR_OUTLINE, xa, ya);

      new_rle_struct(temp_bitmap1, "diver 1", eRLE_diver [0] [i], j);

      clear_bitmap(temp_bitmap1);


              xa = x + xpart(angle + ANGLE_4, 15);
              ya = y + ypart(angle + ANGLE_4, 15);

        bordered_poly4(temp_bitmap1,
              xa + xpart(angle + ANGLE_16, 8), ya + ypart(angle + ANGLE_16, 8),
              xa + xpart(angle - ANGLE_16, 8), ya + ypart(angle - ANGLE_16, 8),
              xa - xpart(angle + ANGLE_16, 8), ya - ypart(angle + ANGLE_16, 8),
              xa - xpart(angle - ANGLE_16, 8), ya - ypart(angle - ANGLE_16, 8),
              col [2], COLOUR_OUTLINE, xa, ya);

              xa = x - xpart(angle + ANGLE_4, 15);
              ya = y - ypart(angle + ANGLE_4, 15);

        bordered_poly4(temp_bitmap1,
              xa + xpart(angle + ANGLE_16, 8), ya + ypart(angle + ANGLE_16, 8),
              xa + xpart(angle - ANGLE_16, 8), ya + ypart(angle - ANGLE_16, 8),
              xa - xpart(angle + ANGLE_16, 8), ya - ypart(angle + ANGLE_16, 8),
              xa - xpart(angle - ANGLE_16, 8), ya - ypart(angle - ANGLE_16, 8),
              col [2], COLOUR_OUTLINE, xa, ya);

      new_rle_struct(temp_bitmap1, "diver 2", eRLE_diver [1] [i], j);

  }
 }*/
}


void bordered_poly4(BITMAP *target, int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4, int col, int border_col, int fillx, int filly)
{

  BITMAP *temp_bitmap = new_bitmap(target->w, target->h, "Bordered poly4 temp_bitmap");

  clear_bitmap(temp_bitmap);

  line(temp_bitmap, x1, y1, x2, y2, border_col);
  line(temp_bitmap, x2, y2, x3, y3, border_col);
  line(temp_bitmap, x3, y3, x4, y4, border_col);
  line(temp_bitmap, x4, y4, x1, y1, border_col);

  floodfill(temp_bitmap, fillx, filly, col);

  draw_sprite(target, temp_bitmap, 0, 0);

  destroy_bitmap(temp_bitmap);

}

void bordered_triangle(BITMAP *target, int x1, int y1, int x2, int y2, int x3, int y3, int col, int border_col, int fillx, int filly)
{

  BITMAP *temp_bitmap = new_bitmap(target->w, target->h, "Bordered triangle temp_bitmap");

  clear_bitmap(temp_bitmap);

  line(temp_bitmap, x1, y1, x2, y2, border_col);
  line(temp_bitmap, x2, y2, x3, y3, border_col);
  line(temp_bitmap, x3, y3, x1, y1, border_col);

  floodfill(temp_bitmap, fillx, filly, col);

  draw_sprite(target, temp_bitmap, 0, 0);

  destroy_bitmap(temp_bitmap);

}




// because either the GIMP or Allegro mangles indexed palettes somewhere: it replaces
//  colours with any earlier identical colours.
void process_trans_bitmap(BITMAP *bmp, int x, int y)
{
 int i, j, px;

 for (i = 0; i < x; i ++)
 {
  for (j = 0; j < y; j ++)
  {
   px = getpixel(bmp, i, j);
   if (px == 7)
    putpixel(bmp, i, j, 72);
   if (px == 25)
    putpixel(bmp, i, j, 36);
  }
 }

}


void convert_trans_bitmap(BITMAP *bmp, int x, int y)
{
 int i, j, px;

 for (i = 0; i < x; i ++)
 {
  for (j = 0; j < y; j ++)
  {
   px = getpixel(bmp, i, j);
   if (px == 0)
    continue;
   putpixel(bmp, i, j, px + 72);
  }
 }

}


BITMAP *load_up_bitmap(const char fname [])
{
 RGB temp_palette [256];

 BITMAP *temp_bitmap = load_bitmap(fname, temp_palette);
 if (temp_bitmap == NULL)
 {
  set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
  allegro_message("Failed to load in bitmap! (File not found?)  \n%s", fname);
  exit(1);
 }

 return temp_bitmap;

}

BITMAP *new_bitmap(int x, int y, const char errtxt [])
{

 BITMAP *bmp = create_bitmap(x, y);
 if (bmp == NULL)
 {
  set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
  allegro_message("Bitmap creation failed!  \n%s\n\n\r%s", allegro_error, errtxt);
  exit(1);
 }

 return bmp;

}

RLE_SPRITE *extract_rle_sprite(BITMAP *source, int x_source, int y_source, int x, int y)
{

 BITMAP *tmp = new_bitmap(x, y, "extract_rle_sprite");

 blit(source, tmp, x_source, y_source, 0, 0, x, y);

 RLE_SPRITE *retval = get_rle_sprite(tmp);

 if (retval == NULL)
 {
  set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
  allegro_message("Rle_sprite extraction failed!  \n%s\n\n\r%s", allegro_error, "extract_rle_sprite");
  exit(1);
 }

 destroy_bitmap(tmp);

 return retval;

}

RLE_SPRITE *extract_flip_rle_sprite(BITMAP *source, int x_source, int y_source, int x, int y, int flip_type)
{

 BITMAP *tmp = new_bitmap(x, y, "extract_hflip_rle_sprite");
 BITMAP *tmp2 = new_bitmap(x, y, "extract_hflip_rle_sprite");

 clear_bitmap(tmp2);
 blit(source, tmp, x_source, y_source, 0, 0, x, y);

 switch(flip_type)
 {
  case 0: draw_sprite_h_flip(tmp2, tmp, 0, 0); break;
  case 1: draw_sprite_v_flip(tmp2, tmp, 0, 0); break;
  case 2: draw_sprite_vh_flip(tmp2, tmp, 0, 0); break;
 }

 RLE_SPRITE *retval = get_rle_sprite(tmp2);

 if (retval == NULL)
 {
  set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
  allegro_message("Rle_sprite extraction failed!  \n%s\n\n\r%s", allegro_error, "extract_rle_sprite");
  exit(1);
 }

 destroy_bitmap(tmp);
 destroy_bitmap(tmp2);

 return retval;

}



RLE_SPRITE *sized_rle_sprite(BITMAP *source, int x, int y, const char errtxt [])
{
 BITMAP *tmp = new_bitmap(x, y, errtxt);

 blit(source, tmp, 0, 0, 0, 0, x, y);

 RLE_SPRITE *retval = get_rle_sprite(tmp);

 if (retval == NULL)
 {
  set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
  allegro_message("Rle_sprite sized creation failed!  \n%s\n\n\r%s", allegro_error, errtxt);
  exit(1);
 }

 destroy_bitmap(tmp);

 return retval;

}



RLE_SPRITE *new_rle_sprite(BITMAP *source, const char errtxt [])
{

 RLE_SPRITE *retval = get_rle_sprite(source);
 if (retval == NULL)
 {
  set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
  allegro_message("Rle_sprite creation failed!  \n%s\n\n\r%s", allegro_error, errtxt);
  exit(1);
 }

 return retval;

}

void extract_rle_struct(BITMAP *source, int x1, int y1, int x2, int y2, const char errtxt [], struct RLE_STRUCT *str, int bl)
{

 BITMAP *tmp = new_bitmap(x2, y2, "extract_rle_struct temp bitmap");

 blit(source, tmp, x1, y1, 0, 0, x2, y2);

 new_rle_struct(tmp, errtxt, str, bl);

 destroy_bitmap(tmp);

}


void new_rle_struct(BITMAP *source, const char errtxt [], struct RLE_STRUCT *str, int bl)
{


 int x1 = -1, y1 = -1, x2 = -1, y2 = -1;

 int i, j;

 for (i = 0; i < source->w; i ++)
 {
  for (j = 0; j < source->h; j ++)
  {
    if (getpixel(source, i, j) != 0)
    {
     x1 = i;
     break;
    }
  }
  if (x1 != -1)
   break;
 }

  for (j = 0; j < source->h; j ++)
  {
   for (i = 0; i < source->w; i ++)
   {
    if (getpixel(source, i, j) != 0)
    {
     y1 = j;
     break;
    }
  }
  if (y1 != -1)
   break;
 }

 for (i = source->w - 1; i >= 0; i --)
 {
  for (j = 0; j < source->h; j ++)
  {
    if (getpixel(source, i, j) != 0)
    {
     x2 = i;
     break;
    }
  }
  if (x2 != -1)
   break;
 }


  for (j = source->h - 1; j >= 0; j --)
  {
   for (i = 0; i < source->w; i ++)
   {
    if (getpixel(source, i, j) != 0)
    {
     y2 = j;
     break;
    }
  }
  if (y2 != -1)
   break;
 }



 BITMAP *tmp = new_bitmap(x2 - x1 + 1, y2 - y1 + 1, "new_rle_struct temp bitmap");

 blit(source, tmp, x1, y1, 0, 0, x2 - x1 + 1, y2 - y1 + 1);

// rect(tmp, 0, 0, tmp->w - 1, tmp->h - 1, COLOUR_1);

// floodfill(tmp, 1, 1, COLOUR_8);

 if (bl == -1)
 {
   str->sprite = get_rle_sprite(tmp);
   str->x = source->w / 2 - x1;
   str->y = source->h / 2 - y1;
 }
  else
  {
   str [bl].sprite = get_rle_sprite(tmp);
   str [bl].x = source->w / 2 - x1;
   str [bl].y = source->h / 2 - y1;
  }

 destroy_bitmap(tmp);
}
// str->sprite = new_rle_sprite(tmp, "new_rle_struct creation");
// str->x = source->w / 2 - x1;
// str->y = source->h / 2 - y1;
