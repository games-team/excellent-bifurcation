/*
Angry Moth
Copyright (C) 2006 Linley Henzell

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public Licence as published by
    the Free Software Foundation; either version 2 of the Licence, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public Licence for more details.

    You should have received a copy of the GNU General Public Licence
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    The GPL version 2 is included in this distribution in a file called
    LICENCE.TXT. Use any text editor or the TYPE command to read it.

    You should be able to reach me by sending an email to
    l_henzell@yahoo.com.au.

File: Main.c
History:

This file contains:
 - main()
 - various initialisation functions
 - miscellaneous stuff


*/



#include "config.h"

#include "allegro.h"

#include <string.h>
#include <math.h>

#include "globvars.h"
#include "stuff.h"
#include "game.h"
#include "display_init.h"
#include "menu.h"
#include "sound.h"

// timer interupt functions and variables:
void framecount(void);

volatile int framecounter;
volatile int frames_per_second;

volatile int inputcounter = 0;
volatile int inputs_per_second = 0;

volatile int turncounter = 0;
volatile int turns_per_second = 0;

void tickover(void);

volatile unsigned char ticked;
//volatile unsigned char tick_counter;
int slacktime;
// --- end timer interupt

// init functions
void init_at_startup(void);
void begin_game(void);
// --- end init functions


void game_loop(void);

struct optionstruct options;
struct arenastruct arena;

struct playerstruct player;
struct enemystruct enemy [2] [NO_ENEMIES];
struct pbulletstruct pbullet [2] [NO_PBULLETS];
struct ebulletstruct ebullet [2] [NO_EBULLETS];
struct cloudstruct cloud [2] [NO_CLOUDS];
struct seekerstruct seeker [2] [NO_SEEKERS];
struct pickupstruct pickup [2] [NO_PICKUPS];
struct bossstruct boss;

void framecount(void)
{
   frames_per_second = framecounter;
   framecounter = 0;
}
END_OF_FUNCTION (framecount);


void tickover(void)
{
 ticked ++;
}
END_OF_FUNCTION (tickover);



int main(void)
{

int allint =  allegro_init();
   if (allint == -1)
   {
      set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
      allegro_message("Failed to initialise Allegro! This isn't going to work, sorry.");
      exit(1);
   }

   set_config_file("init.txt");

   install_keyboard();
   install_timer();

   three_finger_flag = 0;
   key_led_flag = 0;


 init_at_startup();

 loading_screen_wait();
 startup_menu();
 /*arena.level = arena.starting_level;
 ticked = 0;
 new_game();
 game_loop();*/

 return 0;

}

END_OF_MAIN();


void init_at_startup(void)
{


   LOCK_FUNCTION (framecount);
   LOCK_FUNCTION (tickover);
   LOCK_VARIABLE (ticked);
//   LOCK_VARIABLE (tick_counter);
   LOCK_VARIABLE (frames_per_second);
   LOCK_VARIABLE (framecounter);
   LOCK_VARIABLE (turns_per_second);
   LOCK_VARIABLE (turncounter);
//   LOCK_VARIABLE (inputs_per_second);
//   LOCK_VARIABLE (inputcounter);

   install_int (framecount, 1000);
   install_int (tickover, 25);

   set_color_depth(8);

//   set_config_file("amoth.cfg");

//   options.resolution = get_config_int("Options", "Resolution", 0);

   int randseed = get_config_int("Misc", "Seed", 0);
//   set_config_int("Misc", "Seed", grand(5000));
   options.run_vsync = get_config_int("Misc", "vsync", 0);

   options.highscore [0] = get_config_int("Misc", "Hscore0", 100);
   options.highscore [1] = get_config_int("Misc", "Hscore1", 100);
   options.highscore [2] = get_config_int("Misc", "Hscore2", 100);
//   set_config_int("Misc", "vsync", 0);
   srand(randseed);

//   int windowed = GFX_AUTODETECT_FULLSCREEN;
int windowed2 = get_config_int("Misc", "Windowed", 0);
//   windowed2 = 0;//GFX_AUTODETECT_FULLSCREEN;

//set_config_int("Misc", "Windowed", 0);

 int windowed;
 switch(windowed2)
 {
    default:
     case 1: windowed = GFX_AUTODETECT_WINDOWED; break;
     case 0: windowed = GFX_AUTODETECT_FULLSCREEN; break;
 }

   if (set_gfx_mode(windowed, 640, 480, 0, 0) != 0)
   {
      set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
      allegro_message("Unable to set 640x480 display mode\n%s\n", allegro_error);
      exit(1);
   }
 init_trig();

 prepare_display();
 init_sound();
//   init_trig();

//   init_palette();

//   init_display();
//   init_menus_once_only();

//   init_sound(); // must come after init_menus_once_only, as that's where
    // options.sound_enabled is set.

/*

****************************************************


NOTE:

    CHECK if it is faster to use Allegro compiled without debugging stuff?




****************************************************

*/
 player.key [CKEY_UP] = KEY_UP;
 player.key [CKEY_DOWN] = KEY_DOWN;
 player.key [CKEY_LEFT] = KEY_LEFT;
 player.key [CKEY_RIGHT] = KEY_RIGHT;
 player.key [CKEY_FIRE] = KEY_Z;
 player.key [CKEY_SWITCH] = KEY_C;
 player.key [CKEY_CHARGE] = KEY_X;
 player.key [CKEY_AUTOFIRE] = KEY_A;

 player.key [CKEY_UP] = get_config_int("Misc", "key_up", KEY_UP);
 player.key [CKEY_DOWN] = get_config_int("Misc", "key_down", KEY_DOWN);
 player.key [CKEY_LEFT] = get_config_int("Misc", "key_left", KEY_LEFT);
 player.key [CKEY_RIGHT] = get_config_int("Misc", "key_right", KEY_RIGHT);
 player.key [CKEY_FIRE] = get_config_int("Misc", "key_fire", KEY_Z);
 player.key [CKEY_CHARGE] = get_config_int("Misc", "key_charge", KEY_X);
 player.key [CKEY_SWITCH] = get_config_int("Misc", "key_switch", KEY_C);
 player.key [CKEY_AUTOFIRE] = get_config_int("Misc", "key_autofire", KEY_A);


 player.x = 270000;
 player.y = 350000;
 player.in_play = 1;

 arena.starting_level = 1;
 arena.difficulty = 1;
 arena.just_got_highscore = -1;
/*
 player[1].key [CKEY_UP] = KEY_8_PAD;
 player[1].key [CKEY_DOWN] = KEY_2_PAD;
 player[1].key [CKEY_LEFT] = KEY_4_PAD;
 player[1].key [CKEY_RIGHT] = KEY_6_PAD;
 player[1].key [CKEY_FIRE] = KEY_0_PAD;
 player[1].key [CKEY_SWITCH] = KEY_5_PAD;
 player[1].key [CKEY_RELEASE] = KEY_ENTER_PAD;
 player[1].x = 330000;
 player[1].y = 350000;
 player[1].in_play = 1;
*/
}



