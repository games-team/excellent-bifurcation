#include "config.h"

#include "allegro.h"

#include "globvars.h"

#include "stuff.h"
#include "enemy.h"
#include "pbullet.h"
#include "ebullet.h"
#include "display.h"

#include "palette.h"

#include <math.h>

#include "sound.h"

#define NO_TRACKS 3
#define TRACK_SIZE 10


enum
{
TRACK_WANDER,
TRACK_SIDES,
TRACK_DIVER,
TRACK_GLIDER,
TRACK_MARCHER,
TRACK_ATTACKER,
TRACK_BEAMER,
TRACK_BOSS1,
TRACK_MB2,
TRACK_BOSS2,
TRACK_MB1



};

enum
{
DIST_RANDOM,
DIST_ALL_W,
DIST_ALL_B,
DIST_ALTERNATE_WB,
DIST_ALTERNATE_BW,
DIST_PART_WB,
DIST_PART_BW

};


struct trackstruct
{
  int type;
  int multiple;
  int count;
  int count2;
  int next_track;
  int delay;
  int total;
  int x [TRACK_SIZE + 1];
  int y [TRACK_SIZE + 1];
  int w [TRACK_SIZE + 1];
  int mw;
  int reverse;

  int style;
  int pole_distribution;
  int pole_switch; // for DIST_PARTS

  int x_target [MAX_TARGETS];
  int y_target [MAX_TARGETS];
  int target_time;

};

struct trackstruct track [NO_TRACKS];

int track_flip_w;

/*
struct overtrackstruct
{
  int subtrack [NO_TRACKS];
  int position;
};

struct overtrackstruct overtrack [OVERTRACK_SIZE];
*/

enum
{
T_EMPTY,
T_OVERTRACK_END,
T_GLIDER1,
T_BASIC1,
EMPTY_TRACK4,
EMPTY_TRACK5,
T_SHOTTER1,
T_ZAPPER1,
T_DIPPER1,
T_DIPPER2,
T_BASIC2,
T_CARRIER,
T_BURSTER,
T_BASIC3,
T_BEAMER1,
T_BOSS1,
T2_CROSS,
T2_FLOWER1,
T2_FLOWER2,
T2_FLOWER3,
T2_FLOWER4,
T2_FLOWER5,
T2_CROSS2,
T2_WINGS,
T2_MB2,
T2_CARRIER2,
T2_BOSS2,
T_MB1
};


int overtrack_pos;
int overtrack_pos2;

int overtrack [2] [51] [8] =
{
{
{T_BASIC1, 2, T_EMPTY, 0, T_EMPTY, 0, 10, 5}, // doesn't appear
//{T_BOSS1, 0, T_BOSS1, 1, T_EMPTY, 2, 150, 100},
//{T_DIPPER2, 2, T_EMPTY, 0, T_EMPTY, 0, 200, 80},
//{T_MB1, 2, T_EMPTY, 0, T_EMPTY, 0, 160, 80},
//{T_SHOTTER1, 2, T_EMPTY, 0, T_EMPTY, 0, 160, 80},
//{T_BURSTER, 2, T_EMPTY, 0, T_EMPTY, 0, 160, 80},
//{T_MB1, 2, T_EMPTY, 0, T_EMPTY, 0, 160, 80},
//{T_CARRIER, 2, T_EMPTY, 0, T_EMPTY, 0, 160, 80},

{T_BASIC1, 2, T_EMPTY, 0, T_EMPTY, 0, 160, 80},
{T_DIPPER1, 0, T_BASIC1, 1, T_EMPTY, 0, 200, 100},
{T_DIPPER1, 0, T_CARRIER, 1, T_BASIC1, 2, 250, 200},
{T_BASIC1, 0, T_BASIC2, 1, T_EMPTY, 0, 200, 100},
{T_GLIDER1, 0, T_BASIC1, 1, T_EMPTY, 0, 200, 100},
{T_GLIDER1, 0, T_BASIC3, 1, T_BASIC1, 1, 200, 100},
{T_SHOTTER1, 0, T_CARRIER, 1, T_ZAPPER1, 2, 250, 200},
{T_BASIC3, 0, T_BASIC2, 1, T_EMPTY, 1, 200, 100},
{T_DIPPER1, 2, T_BASIC3, 1, T_EMPTY, 1, 200, 100},
{T_SHOTTER1, 0, T_BASIC3, 1, T_EMPTY, 1, 200, 100},
{T_BASIC1, 0, T_BASIC2, 1, T_EMPTY, 1, 150, 70},
{T_BASIC1, 0, T_BASIC2, 1, T_EMPTY, 1, 150, 70},
{T_ZAPPER1, 2, T_BASIC2, 0, T_BASIC2, 1, 250, 100},
{T_GLIDER1, 0, T_CARRIER, 1, T_ZAPPER1, 2, 250, 200},
{T_DIPPER2, 0, T_BASIC2, 2, T_EMPTY, 1, 200, 100},
{T_DIPPER2, 1, T_BASIC3, 0, T_EMPTY, 1, 200, 100},
{T_BURSTER, 0, T_EMPTY, 1, T_EMPTY, 1, 200, 100},
{T_BASIC1, 0, T_BASIC2, 1, T_EMPTY, 1, 200, 100},
{T_BURSTER, 0, T_GLIDER1, 1, T_BASIC3, 1, 200, 100},
{T_GLIDER1, 0, T_CARRIER, 1, T_ZAPPER1, 2, 400, 100},
{T_MB1, 2, T_EMPTY, 0, T_EMPTY, 0, 160, 80},
{T_DIPPER2, 0, T_BASIC2, 1, T_EMPTY, 1, 300, 100},
{T_BEAMER1, 0, T_CARRIER, 1, T_EMPTY, 1, 200, 100},
{T_BEAMER1, 0, T_BEAMER1, 1, T_EMPTY, 2, 200, 100},
{T_BURSTER, 0, T_BASIC1, 1, T_EMPTY, 1, 200, 100},
{T_GLIDER1, 0, T_EMPTY, 1, T_EMPTY, 1, 200, 100},
{T_BURSTER, 0, T_BASIC3, 1, T_BASIC3, 0, 200, 100},
{T_DIPPER2, 0, T_BASIC2, 2, T_EMPTY, 1, 200, 100},
{T_SHOTTER1, 0, T_GLIDER1, 2, T_EMPTY, 1, 200, 100},
{T_BURSTER, 0, T_BURSTER, 1, T_EMPTY, 1, 200, 100},
{T_BASIC2, 0, T_CARRIER, 1, T_EMPTY, 1, 200, 100},
{T_BEAMER1, 0, T_BASIC3, 1, T_EMPTY, 1, 200, 100},
{T_BEAMER1, 1, T_BASIC2, 1, T_BASIC2, 1, 200, 100},
{T_BEAMER1, 0, T_DIPPER2, 1, T_BASIC2, 1, 200, 100},
{T_BEAMER1, 0, T_DIPPER2, 1, T_EMPTY, 1, 200, 100},
{T_BASIC3, 0, T_BASIC3, 1, T_EMPTY, 1, 300, 100},
{T_CARRIER, 0, T_EMPTY, 1, T_EMPTY, 1, 200, 100},
{T_BOSS1, 0, T_EMPTY, 1, T_EMPTY, 2, 150, 100},
{T_OVERTRACK_END, 0, T_EMPTY, 0, T_EMPTY, 0, 400}
/*{T_, 0, T_, 1, T_, 1, 200, 100},
{T_, 0, T_, 1, T_, 1, 200, 100},
{T_, 0, T_, 1, T_, 1, 200, 100},
{T_, 0, T_, 1, T_, 1, 200, 100},
{T_, 0, T_, 1, T_, 1, 200, 100},
{T_, 0, T_, 1, T_, 1, 200, 100},

{T_, 0, T_, 1, T_, 1, 200, 100},*/

},
{
{T_BASIC1, 2, T_EMPTY, 0, T_EMPTY, 0, 10, 5}, // doesn't appear
//{T2_MB2, 0, T2_CROSS, 1, T_EMPTY, 2, 170, 150},

//{T2_BOSS2, 2, T_EMPTY, 0, T_EMPTY, 0, 170, 150},

{T2_CROSS, 2, T_EMPTY, 0, T_EMPTY, 0, 170, 150},
{T2_CROSS, 1, T2_CROSS, 0, T_EMPTY, 1, 170, 150},
{T2_FLOWER1, 0, T2_CROSS, 1, T_EMPTY, 2, 170, 150},
{T2_FLOWER2, 0, T2_FLOWER1, 2, T_EMPTY, 2, 170, 150},
{T2_CROSS, 2, T2_FLOWER1, 0, T2_FLOWER3, 1, 170, 150},
{T2_CROSS, 2, T2_CROSS, 0, T2_FLOWER2, 1, 170, 150},
{T2_CARRIER2, 2, T_EMPTY, 0, T_EMPTY, 0, 170, 150},
{T2_FLOWER1, 2, T2_FLOWER2, 0, T2_FLOWER3, 1, 170, 150},
{T2_CROSS, 2, T2_CROSS, 0, T_EMPTY, 1, 170, 150},
{T2_CROSS2, 2, T2_FLOWER1, 0, T_EMPTY, 1, 170, 150},
{T2_WINGS, 0, T2_WINGS, 1, T_EMPTY, 1, 170, 150},
{T2_CROSS2, 2, T2_FLOWER1, 0, T2_WINGS, 1, 170, 150},
{T2_CARRIER2, 2, T_EMPTY, 0, T_EMPTY, 0, 170, 150},
{T2_WINGS, 0, T2_CROSS2, 1, T_EMPTY, 1, 170, 150},
{T2_CROSS, 0, T2_CROSS2, 1, T_EMPTY, 1, 170, 150},
{T2_WINGS, 0, T2_FLOWER3, 1, T_EMPTY, 1, 170, 150},
{T2_WINGS, 0, T2_FLOWER3, 1, T2_CROSS2, 2, 170, 150},
{T2_CROSS, 0, T2_CROSS2, 1, T_EMPTY, 1, 170, 150},
{T2_FLOWER1, 0, T2_CROSS, 1, T_EMPTY, 2, 170, 150},
{T2_CARRIER2, 2, T_EMPTY, 0, T_EMPTY, 0, 350, 150},
{T2_MB2, 2, T_EMPTY, 0, T_EMPTY, 1, 170, 150},
{T2_CROSS, 2, T2_CROSS2, 0, T_EMPTY, 1, 170, 150},
{T2_FLOWER1, 0, T2_CROSS, 1, T_EMPTY, 2, 170, 150},
{T2_FLOWER2, 0, T2_FLOWER1, 2, T_EMPTY, 2, 170, 150},
{T2_CROSS, 2, T2_FLOWER4, 0, T2_FLOWER3, 1, 150, 150},
{T2_CROSS2, 2, T2_FLOWER5, 0, T2_FLOWER3, 1, 150, 150},
{T2_CARRIER2, 2, T_EMPTY, 0, T_EMPTY, 0, 250, 150},
{T2_WINGS, 0, T2_CROSS2, 1, T_EMPTY, 1, 170, 150},
{T2_CROSS, 0, T2_CROSS2, 1, T2_FLOWER2, 1, 170, 150},
{T2_WINGS, 0, T2_FLOWER4, 1, T_EMPTY, 1, 170, 150},
{T2_WINGS, 0, T2_FLOWER5, 1, T2_CROSS2, 2, 170, 150},
{T2_CROSS, 2, T2_FLOWER4, 0, T2_FLOWER3, 1, 150, 150},
{T2_CROSS2, 2, T2_FLOWER5, 0, T2_FLOWER3, 1, 150, 150},
{T2_CARRIER2, 2, T_EMPTY, 0, T_EMPTY, 0, 350, 150},
{T2_BOSS2, 2, T_EMPTY, 0, T_EMPTY, 1, 170, 150},

//{T_BOSS1, 0, T_BOSS1, 1, T_EMPTY, 2, 150, 100},
{T_OVERTRACK_END, 0, T_EMPTY, 0, T_EMPTY, 0, 400}


}
/*
{T_, 0, T_, 1, T_, 1, 200, 100},
*/

//{T_, , T_, , T_, };
//{T_, , T_, , T_, };
//{T_, , T_, , T_, };

};



void spawn_enemy(int w, int type, int pole);
void make_track(int t);
void run_tracks(void);

void make_dancer_track(int t, int type);
void set_dancer(int w, int e, int t);
void set_diver(int w, int e, int t);
void set_glider(int w, int e, int t);
void set_attacker(int w, int e, int t);
 void set_beamer(int w, int e, int t);
void set_pauser(int w, int e, int t);
void place_marchers(int t);
void make_boss1_track(int type);
void make_mb2_track(int type);
void make_boss2_track(int type);
void make_mb1_track(void);



void init_level(void)
{
 init_pbullets();
 init_ebullets();
 init_enemies();

 int i;

 for (i = 0; i < NO_TRACKS; i ++)
 {
   track[i].type = ENEMY_NONE;

 }

 overtrack_pos = 0;
 overtrack_pos2 = 0;
 boss.fight = 0;

 track_flip_w = grand(2);

 arena.new_level_sign = 100;
 arena.beam = 0;
 arena.old_beam = 0;

 arena.underlay_position = 0;

 init_mtracks();

 level_display_init();

}

void run_level(void)
{

     run_tracks();

/*     arena.track ++;

     if (arena.track % 50 == 0)
     {
      spawn_enemy(ENEMY_DIVER, POLE_RANDOM);
     }*/

}
/*
void spawn_enemy(int type, int pole)
{
     int x = grand(X_MAX - X_MIN * 2) + X_MIN;
//     x = 100000;
     int y = -50000;

     if (pole == POLE_RANDOM)
      pole = POLE_WHITE + grand(2);

    create_enemy(type, pole, x, y, 0, 0, 2500, ATT3, ATT4, ATT5, ATT6, ATT7);

}
*/

void run_tracks(void)
{
 int t;
 int e, w;
// int pole = POLE_WHITE;

// for (t = 0; t < NO_TRACKS; t ++)
if (boss.fight || arena.level_finished > 0)
 return;

for (t = 0; t < 3; t ++)
 {
     track[t].next_track --;
     if (track[t].next_track <= 0)
     {
      track[t].type = ENEMY_NONE;
      make_track(t);
      track[t].next_track = overtrack [arena.level - 1] [overtrack_pos] [6]; // see also above
//      if (arena.difficulty == 1)
       //track[t].next_track += 10;
      if (arena.difficulty == 0)
       track[t].next_track += 50;
     }
     if (track[t].type == ENEMY_NONE || track[t].multiple == 0)
     {
       continue;
     }
     if (track[t].count <= 0)
     {
       if (track[t].multiple == 0)
       {
        track[t].delay --;
/*        if (track[t].delay <= 0)
        {
         track[t].type = ENEMY_NONE;
        }*/
        continue;
       }
       if (track[t].style == TRACK_MARCHER)
       {
        place_marchers(t);
        track[t].multiple = 0;
        track[t].count = 0;
//        track[t].delay = 0;
        track[t].type = ENEMY_NONE;
        //track[t].delay = grand(overtrack [overtrack_pos] [7]);
        continue;
       }
       if (track[t].style == TRACK_BOSS1
          || track[t].style == TRACK_MB2
          || track[t].style == TRACK_MB1
          || track[t].style == TRACK_BOSS2)
        continue;
/*       switch(track[t].pole_distribution)
       {
        case DIST_RANDOM: pole = POLE_WHITE + grand(2); break;
        case DIST_ALL_W: pole = POLE_WHITE; break;
        case DIST_ALL_B: pole = POLE_BLACK; break;
        case DIST_ALTERNATE_WB:
            if (track[t].multiple % 2 == 1)
             pole = POLE_BLACK;
            break;
        case DIST_ALTERNATE_BW:
            if (track[t].multiple % 2 == 0)
             pole = POLE_BLACK;
            break;
        case DIST_PART_WB:
            if (track[t].multiple <= track[t].pole_switch)
             pole = POLE_BLACK;
            break;
        case DIST_PART_BW:
            if (track[t].multiple > track[t].pole_switch)
             pole = POLE_BLACK;
            break;
       }*/
       w = track[t].w [track[t].multiple];
       e = create_enemy(w, track[t].type, 0, track[t].x [track[t].multiple], track[t].y [track[t].multiple], ANGLE_4, 0, 2500, ATT3, ATT4, ATT5, ATT6, ATT7);
       if (e != -1)
       {
        switch(eclass[enemy[w][e].type].ai)
        {
          default:
          case AI_DANCER:
           set_dancer(w, e, t);
           break;
          case AI_DIVER:
           set_diver(w, e, t);
           break;
          case AI_GLIDER:
           set_glider(w, e, t);
           break;
          case AI_PAUSER:
           set_pauser(w, e, t);
           break;
          case AI_ATTACKER:
           set_attacker(w, e, t);
           break;
          case AI_BEAMER:
           set_beamer(w, e, t);
           break;
        }
       }
       track[t].count = track[t].count2;
       track[t].multiple --;

     } // count > 0. so -- it
     track[t].count --;

 }



}

void make_track(int t)
{

  int etype;

  track[t].total = 0;

  track[t].reverse = -1;

  track[t].pole_distribution = DIST_RANDOM;

  int track_number = grand(3);

  if (t == 0 && grand(2) == 0)
   track_number = 3;

  int s_etype = 3;
/*
  switch(grand(13))
  {
   case 0: s_etype = 3; break;// 3
   case 1: s_etype = 2; break;
   case 2: s_etype = 6; break;// 6
   case 3: s_etype = 7; break;
   case 4: s_etype = 8; break;
   case 5: s_etype = 9; break;
   case 6: s_etype = 10; break;
   case 7: s_etype = 11; break;
   case 8: s_etype = 12; break;
   case 9: s_etype = 13; break;
   default: s_etype = 13; break;

  }*/

  overtrack_pos2 ++;
  if (overtrack_pos2 > 2)
  {
   overtrack_pos2 = 0;
   overtrack_pos ++;
   if (overtrack [arena.level - 1] [overtrack_pos] [0] == T_OVERTRACK_END)
    overtrack_pos = 0;
   track_flip_w = grand(2);
  }
  if (overtrack [arena.level - 1] [overtrack_pos] [overtrack_pos2 * 2] == T_EMPTY)
  {
   track[t].multiple = 0;
   return;
  }

  s_etype = overtrack [arena.level - 1] [overtrack_pos] [overtrack_pos2 * 2];
  track[t].mw = overtrack [arena.level - 1] [overtrack_pos] [overtrack_pos2 * 2 + 1];
  if (track_flip_w)
  {
    if (track[t].mw == 0)
     track[t].mw = 1;
      else
      {
       if (track[t].mw == 1)
        track[t].mw = 0;
      }
   }





  switch(s_etype)
  {
/*   case 0: etype = ENEMY_TWISTER;
    track[t].type = etype;
    track[t].multiple = 6;
    track[t].pole_switch = track[t].multiple / 2;
    track[t].count = 10;
    track[t].count2 = track[t].count;
    track[t].delay = 100;
    track[t].style = TRACK_WANDER;
    if (grand(2) == 0)
     track[t].style = TRACK_SIDES;
    make_dancer_track(t, etype);
    break;
   case 1: etype = ENEMY_DIVER;
    track[t].type = etype;
    track[t].multiple = 3;
    track[t].pole_switch = track[t].multiple / 2;
    track[t].pole_distribution = DIST_ALL_W + grand(2);
    track[t].count = 40;
    track[t].count2 = track[t].count;
    track[t].delay = 150;
    track[t].style = TRACK_DIVER;
    make_dancer_track(t, etype);
    track[t].style = TRACK_DIVER;
    break;*/
   case 2: etype = ENEMY_GLIDER1;
    track[t].type = etype;
    track[t].multiple = 1;
//    track[t].pole_switch = track[t].multiple / 2;
    track[t].count = 150;
    track[t].count2 = track[t].count;
    track[t].delay = 50;
    track[t].style = TRACK_GLIDER;
    make_dancer_track(t, etype);
    break;
   case 3: etype = ENEMY_BASIC1;
    track[t].type = etype;
    track[t].multiple = 4;
//    track[t].pole_switch = track[t].multiple / 2;
    track[t].count = 20;
    track[t].count2 = track[t].count;
    track[t].delay = 80;
    track[t].style = TRACK_WANDER;
    make_dancer_track(t, etype);
    break;
/*   case 4: etype = ENEMY_ATTACKER1 + grand(3);
    track[t].type = etype;
    track[t].multiple = 4;
    track[t].pole_switch = track[t].multiple / 2;
    track[t].pole_distribution = DIST_ALL_W + grand(2);
    track[t].count = 60;
    track[t].count2 = track[t].count;
    track[t].delay = 100;
    track[t].style = TRACK_ATTACKER;
    make_dancer_track(t, etype);
    break;*/
   case 6: etype = ENEMY_SHOTTER1;
    track[t].type = etype;
    track[t].multiple = 1;
//    track[t].pole_switch = track[t].multiple / 2;
    track[t].count = 150;
    track[t].count2 = track[t].count;
    track[t].delay = 50;
    track[t].style = TRACK_GLIDER;
    make_dancer_track(t, etype);
    break;
   case 7: etype = ENEMY_ZAPPER1;
    track[t].type = etype;
    track[t].multiple = 1;
    track[t].count = 150;
    track[t].count2 = track[t].count;
    track[t].delay = 50;
    track[t].style = TRACK_GLIDER;
    make_dancer_track(t, etype);
    break;
   case 8: etype = ENEMY_DIPPER1;
    track[t].type = etype;
    track[t].multiple = 3;
    track[t].count = 30;
    track[t].count2 = track[t].count;
    track[t].delay = 150;
    track[t].style = TRACK_ATTACKER;
    make_dancer_track(t, etype);
    break;
   case 9: etype = ENEMY_DIPPER2;
    track[t].type = etype;
    track[t].multiple = 3;
    track[t].count = 30;
    track[t].count2 = track[t].count;
    track[t].delay = 150;
    track[t].style = TRACK_ATTACKER;
    make_dancer_track(t, etype);
    break;
   case 10: etype = ENEMY_BASIC2;
    track[t].type = etype;
    track[t].multiple = 4;
//    track[t].pole_switch = track[t].multiple / 2;
    track[t].count = 20;
    track[t].count2 = track[t].count;
    track[t].delay = 80;
    track[t].style = TRACK_SIDES;
    make_dancer_track(t, etype);
    break;
   case 11: etype = ENEMY_CARRIER;
    track[t].type = etype;
    track[t].multiple = 1;
//    track[t].pole_switch = track[t].multiple / 2;
    track[t].count = 150;
    track[t].count2 = track[t].count;
    track[t].delay = 50;
    track[t].style = TRACK_GLIDER;
    make_dancer_track(t, etype);
    break;
   case 12: etype = ENEMY_BURSTER;
    track[t].type = etype;
    track[t].multiple = 1;
//    track[t].pole_switch = track[t].multiple / 2;
    track[t].count = 150;
    track[t].count2 = track[t].count;
    track[t].delay = 50;
    track[t].style = TRACK_GLIDER;
    make_dancer_track(t, etype);
    break;
   case 13: etype = ENEMY_BASIC3;
    track[t].type = etype;
    track[t].multiple = 6;
    track[t].count = 20;
    track[t].count2 = track[t].count;
    track[t].delay = 80;
    track[t].style = TRACK_MARCHER;
    make_dancer_track(t, etype);
    break;
   case T_BEAMER1:
    etype = ENEMY_BEAMER1;
    track[t].type = etype;
    track[t].multiple = 1;
    track[t].count = 20;
    track[t].count2 = track[t].count;
    track[t].delay = 200;
    track[t].style = TRACK_BEAMER;
    make_dancer_track(t, etype);
    break;
   case T_BOSS1:
    etype = ENEMY_BOSS1;
    track[t].type = etype;
    track[t].multiple = 0;
//    track[t].pole_switch = track[t].multiple / 2;
    track[t].count = 150;
    track[t].count2 = track[t].count;
    track[t].delay = 50;
    track[t].style = TRACK_BOSS1;
    make_boss1_track(etype);
//    make_dancer_track(t, etype);
    break;
   case T2_BOSS2:
    etype = ENEMY_BOSS2;
    track[t].type = etype;
    track[t].multiple = 0;
    track[t].count = 150;
    track[t].count2 = track[t].count;
    track[t].delay = 50;
    track[t].style = TRACK_BOSS1;
    make_boss2_track(etype);
    break;
   case T2_MB2:
    etype = ENEMY_MB2;
    track[t].type = etype;
    track[t].multiple = 0;
//    track[t].pole_switch = track[t].multiple / 2;
    track[t].count = 150;
    track[t].count2 = track[t].count;
    track[t].delay = 50;
    track[t].style = TRACK_MB2;
    make_mb2_track(etype);
//    make_dancer_track(t, etype);
    break;

   case T2_CROSS:
    etype = ENEMY_CROSS;
    track[t].type = etype;
    track[t].multiple = 5;
//    track[t].pole_switch = track[t].multiple / 2;
    track[t].count = 30;
    track[t].count2 = track[t].count;
    track[t].delay = 80;
    track[t].style = TRACK_WANDER;
    make_dancer_track(t, etype);
    break;
   case T2_FLOWER1:
    etype = ENEMY_FLOWER1;
    track[t].type = etype;
    track[t].multiple = 1;
//    track[t].pole_switch = track[t].multiple / 2;
    track[t].count = 150;
    track[t].count2 = track[t].count;
    track[t].delay = 50;
    track[t].style = TRACK_GLIDER;
    make_dancer_track(t, etype);
    break;
   case T2_FLOWER2:
    etype = ENEMY_FLOWER2;
    track[t].type = etype;
    track[t].multiple = 1;
//    track[t].pole_switch = track[t].multiple / 2;
    track[t].count = 150;
    track[t].count2 = track[t].count;
    track[t].delay = 50;
    track[t].style = TRACK_GLIDER;
    make_dancer_track(t, etype);
    break;
   case T2_FLOWER3:
    etype = ENEMY_FLOWER3;
    track[t].type = etype;
    track[t].multiple = 1;
//    track[t].pole_switch = track[t].multiple / 2;
    track[t].count = 150;
    track[t].count2 = track[t].count;
    track[t].delay = 50;
    track[t].style = TRACK_GLIDER;
    make_dancer_track(t, etype);
    break;
   case T2_FLOWER4:
    etype = ENEMY_FLOWER4;
    track[t].type = etype;
    track[t].multiple = 1;
//    track[t].pole_switch = track[t].multiple / 2;
    track[t].count = 150;
    track[t].count2 = track[t].count;
    track[t].delay = 50;
    track[t].style = TRACK_GLIDER;
    make_dancer_track(t, etype);
    break;
   case T2_FLOWER5:
    etype = ENEMY_FLOWER5;
    track[t].type = etype;
    track[t].multiple = 1;
//    track[t].pole_switch = track[t].multiple / 2;
    track[t].count = 150;
    track[t].count2 = track[t].count;
    track[t].delay = 50;
    track[t].style = TRACK_GLIDER;
    make_dancer_track(t, etype);
    break;
   case T2_CROSS2:
    etype = ENEMY_CROSS2;
    track[t].type = etype;
    track[t].multiple = 5;
//    track[t].pole_switch = track[t].multiple / 2;
    track[t].count = 15;
    track[t].count2 = track[t].count;
    track[t].delay = 80;
    track[t].style = TRACK_WANDER;
    make_dancer_track(t, etype);
    break;
   case T2_WINGS:
    etype = ENEMY_WINGS;
    track[t].type = etype;
    track[t].multiple = 1;
//    track[t].pole_switch = track[t].multiple / 2;
    track[t].count = 15;
    track[t].count2 = track[t].count;
    track[t].delay = 80;
    track[t].style = TRACK_WANDER;
    make_dancer_track(t, etype);
    break;
   case T2_CARRIER2: etype = ENEMY_CARRIER2;
    track[t].type = etype;
    track[t].multiple = 1;
//    track[t].pole_switch = track[t].multiple / 2;
    track[t].count = 150;
    track[t].count2 = track[t].count;
    track[t].delay = 50;
    track[t].style = TRACK_GLIDER;
    make_dancer_track(t, etype);
    break;
   case T_MB1:
    etype = ENEMY_MB1;
    track[t].type = etype;
    track[t].multiple = 0;
    track[t].count = 150;
    track[t].count2 = track[t].count;
    track[t].delay = 50;
    track[t].style = TRACK_MB1;
    make_mb1_track();
//    make_dancer_track(t, etype);
    break;

  }

  track[t].count = grand(overtrack [arena.level - 1] [overtrack_pos] [7]);

}

void make_dancer_track(int t, int type)
{
// REMEMBER - MARCHERS don't use this!!!

 int a;//, b;

 int i;
 int tw = track[t].mw;

 switch(track[t].style)
 {
  default:
  case TRACK_WANDER:
   track[t].x [0] = RAND_X;
   track[t].y [0] = -30000;
// int tw = grand(2);
   if (tw == 2)
    tw = grand(2);
   for (i = 1; i < TRACK_SIZE; i ++)
   {
    track[t].x [i] = track[t].x [0];
    track[t].y [i] = track[t].y [0];
    track[t].w [i] = tw;
   }

   track[t].x_target [0] = RAND_X;
   track[t].y_target [0] = RAND_Y / 3;

   track[t].x_target [1] = RAND_X;
   track[t].y_target [1] = (RAND_Y / 3) * 2;

   track[t].x_target [2] = -1;
   track[t].y_target [2] = -1;

   track[t].target_time = grand(80) + 80;
   break;
  case TRACK_SIDES:
   track[t].reverse = grand(2);
   a = grand(2);
   track[t].x [0] = X_MIN + 40000 + grand(30000);
//   track[t].x [0] = 100000;
   if (a == 1)
    track[t].x [0] = X_MAX - 40000 - grand(30000);

   track[t].y [0] = -30000;

   for (i = 1; i < track[t].multiple + 1; i ++)
   {
    if (i % 2 == 1)
     track[t].x [i] = X_MAX - track[t].x [0];
      else
       track[t].x [i] = track[t].x [0];
    track[t].y [i] = track[t].y [0];

    if (tw == 2)
     tw = grand(2);
     track[t].w [i] = tw;
   }


   track[t].x_target [0] = X_MIN + RAND_X / 4;
   if (a == 1)
    track[t].x_target [0] = X_MAX - RAND_X / 4;
   track[t].y_target [0] = Y_MIN + RAND_Y / 3;

   track[t].x_target [1] = X_MIDDLE;
   track[t].y_target [1] = Y_MIDDLE; // rand?

   track[t].x_target [2] = X_MAX - RAND_X / 4;
   if (a == 1)
    track[t].x_target [2] = X_MIN + RAND_X / 4;
   track[t].y_target [2] = 500000;

   track[t].x_target [3] = -1;
   track[t].y_target [3] = -1;

   track[t].target_time = grand(20) + 80;
   track[t].w [0] = grand(2);
   track[t].w [1] = grand(2);
   track[t].w [2] = grand(2);
   break;
   case TRACK_DIVER:
//        exit(1);
   track[t].reverse = grand(2);
   a = 0;

   track[t].x [0] = X_MIN + 40000 + grand(30000);
   if (a == 1)
    track[t].x [0] = X_MAX - 50000;

   track[t].y [0] = -30000;

   for (i = 1; i < track[t].multiple + 1; i ++)
   {
    track[t].x [i] = track[t].x [0] + i * 75000;
    if (a == 1)
     track[t].x [i] = track[t].x [0] - i * 75000;
    track[t].y [i] = track[t].y [0];
    track[t].w [i] = grand(2);
   }

   track[t].target_time = grand(30) + 20;
    break;
   case TRACK_GLIDER:
   track[t].x [0] = RAND_X;
   track[t].y [0] = -70000 + grand(23000);

   track[t].x_target [0] = track[t].x [0];
   track[t].y_target [0] = 500000;
   if (tw == 2)
    tw = grand(2);


   for (i = 1; i < track[t].multiple + 1; i ++)
   {
    track[t].x [i] = track[t].x [0];
    track[t].y [i] = track[t].y [0];
    track[t].x_target [0] = track[t].x [0];
    track[t].y_target [0] = 500000;
    track[t].w [i] = tw;
   }
        break;
   case TRACK_ATTACKER:

// note i = 0
   t = t;
   int str_x;
   int str_y = -20000 - grand(55000);
   int w = grand(2);
   if (grand(2) == 0)
   {
//    str_x = 30000 + grand(50000);
    str_x = -30000 + grand(75000);
    for (i = 0; i < track[t].multiple + 1; i ++)
    {
     track[t].x [i] = str_x + i * 90000;
     track[t].y [i] = str_y;
     track[t].w [i] = w;//grand(2);
    }
   } else
   {
//    str_x = X_MAX - (30000 + grand(50000));
    str_x = X_MAX - (-40000 + grand(45000));
    for (i = 0; i < track[t].multiple + 1; i ++)
    {
     track[t].x [i] = str_x - (i * 90000);
     track[t].y [i] = str_y;
     if (tw == 2)
      tw = grand(2);
     track[t].w [i] = tw;//grand(2);
    }
   }


//   track[t].target_time = grand(30) + 30;
    break;
   case TRACK_BEAMER:
   if (tw == 2)
    tw = grand(2);
   for (i = 0; i < track[t].multiple + 1; i ++)
   {
    track[t].x [i] = RAND_X;
    track[t].y [i] = -70000;
    track[t].w [i] = tw;
   }
    break;

 }

}


void set_dancer(int w, int e, int t)
{
 int i;


 if (track[t].reverse != -1 && track[t].multiple % 2 == track[t].reverse)
 {
  for (i = 0; i < MAX_TARGETS; i ++)
  {
    enemy[w] [e].x_target [i] = X_MAX - track[t].x_target [i];
    if (track[t].x_target [i] == -1)
     enemy[w] [e].x_target [i] = -1;
    enemy[w] [e].y_target [i] = Y_MIN + track[t].y_target [i];
  }
 }
  else
  {
   for (i = 0; i < MAX_TARGETS; i ++)
   {
      enemy[w] [e].x_target [i] = X_MIN + track[t].x_target [i];
      if (track[t].x_target [i] == -1)
       enemy[w] [e].x_target [i] = -1;
      enemy[w] [e].y_target [i] = Y_MIN + track[t].y_target [i];
   }
  }

 enemy[w] [e].target_time = track[t].target_time;
 enemy[w] [e].max_target_time = track[t].target_time;
 enemy[w] [e].target = 0;
}

void set_diver(int w, int e, int t)
{
// int i;
 enemy[w] [e].target_time = track[t].target_time;
 enemy[w] [e].target = 0;

 enemy[w] [e].x_speed = 0;
 enemy[w] [e].angle1 = ANGLE_4;
}


void set_glider(int w, int e, int t)
{
// int i;
 enemy[w][e].target = 0;
 enemy[w][e].x_target [0] = track[t].x_target [0];
 enemy[w][e].y_target [0] = track[t].y_target [0];

 enemy[w][e].angle1 = radians_to_angle(atan2(enemy[w] [e].y_target [0] - enemy[w] [e].y, enemy[w] [e].x_target [0] - enemy[w] [e].x));

 int speed = eclass[enemy[w] [e].type].speed1;

 if (enemy[w][e].type >= ENEMY_FLOWER1 && enemy[w][e].type <= ENEMY_FLOWER5)
  speed += grand(1000);

 enemy[w][e].x_speed = xpart(enemy[w] [e].angle1, speed);
 enemy[w][e].y_speed = ypart(enemy[w] [e].angle1, speed);

}

void set_attacker(int w, int e, int t)
{
// int i;

 enemy[w] [e].angle1 = ANGLE_4;

 enemy[w] [e].x_speed = 0;
 enemy[w] [e].y_speed = 3000;
 enemy[w] [e].target_time = grand(30) + 10;
//   track[t].target_time = grand(30) + 30;

}

// note that Dippers use the attacker track but have AI_PAUSER
void set_pauser(int w, int e, int t)
{

 enemy[w] [e].angle1 = ANGLE_4;

 enemy[w] [e].x_speed = 0;
 enemy[w] [e].y_speed = 3000;
 enemy[w] [e].target_time = 30;//grand(30) + 10;

}


void set_beamer(int w, int e, int t)
{

 enemy[w] [e].angle1 = ANGLE_4;

 enemy[w] [e].x_speed = 0;
 enemy[w] [e].y_speed = 2000;
 enemy[w] [e].target_time = grand(30) + 50;
 enemy[w] [e].target = 0;

}


void place_marchers(int t)
{
// int pole = POLE_WHITE + grand(2);


 int i, e;

 int x = X_MIN + grand(40000);
 int y = -30000;

 if (track[t].mw == 2)
  track[t].mw = grand(2);

 for (i = 0; i < track[t].multiple; i ++)
// for (i = 0; i < 2; i ++)
 {

  e = create_enemy(track[t].mw, track[t].type, 0, x + i * 40000, y, ANGLE_4, 0, 2200 + grand(500), ATT3, ATT4, ATT5, ATT6, ATT7);

 }

}

void make_boss2_track(int type)
{

boss.side1 = grand(2);
 if (boss.side1 == 0)
  boss.side2 = 1;
   else
    boss.side2 = 0;

 int e1 = create_enemy(boss.side1, ENEMY_BOSS2, 0, 160000, 10000, ANGLE_4, 0, 0, ATT3, ATT4, ATT5, ATT6, ATT7);
// I guess I should check these values, but I don't think it's actually possible for these functions to fail here.
 boss.e1 = e1;

  boss.hp = 150000 + arena.difficulty * 12000;
  boss.max_hp = boss.hp;
  boss.moving = 0;
  boss.phase = 0;
  boss.size = 10;
  boss.size_inc = 0;
  boss.colour = 0;
  boss.bflower_recycle = 100;

 boss.fight = 1;

 enemy[boss.side1][e1].y_speed = 0;
 enemy[boss.side1][e1].x_speed = 0;
 enemy[boss.side1][e1].x = 160000;
 enemy[boss.side1][e1].y = 120000;

 enemy[boss.side1][e1].a1 = 0;
 enemy[boss.side1][e1].b1 = 0;
 enemy[boss.side1][e1].c1 = 0;


}


void make_boss1_track(int type)
{

// int a;//, b;

// int i;

// switch stage

 int e1 = create_enemy(0, ENEMY_BOSS1, 0, 160000, -80000, ANGLE_4, 0, 0, ATT3, ATT4, ATT5, ATT6, ATT7);
 boss.e1 = e1;
 int e2 = create_enemy(1, ENEMY_BOSS1, 0, 160000, -80000, ANGLE_4, 0, 0, ATT3, ATT4, ATT5, ATT6, ATT7);
// I guess I should check these values, but I don't think it's actually possible for these functions to fail here.
 boss.e2 = e2;
 boss.side1 = 0;
 boss.side2 = 1;

  boss.hp = 150000 + arena.difficulty * 12000;
  boss.max_hp = boss.hp;
  boss.moving = BOSS1_APPROACH;
  boss.arm1 = 0;
  boss.arm1_change = 0;
  boss.arm2 = 0;
  boss.arm2_change = 0;
  boss.e1 = e1;
  boss.e2 = e2;
  boss.moving = 0;
  boss.pulse = 0;
  boss.arm_colour = -1;
  boss.arm_recycle = 100;
  boss.arm_change = 140;

// track[t].multiple = 0;

 boss.fight = 1;
 boss.bpattern = BOSS1_FIRE_NONE;

 enemy[0][e1].y_speed = 2000;
 enemy[0][e1].x_speed = 0;

}


void make_mb2_track(int type)
{

 boss.side1 = grand(2);
 if (boss.side1 == 0)
  boss.side2 = 1;
   else
    boss.side2 = 0;

 int e1 = create_enemy(boss.side1, ENEMY_MB2, 0, 160000, -80000, ANGLE_4, 0, 0, ATT3, ATT4, ATT5, ATT6, ATT7);
// int e2 = create_enemy(1, ENEMY_BOSS1, 0, 160000, -80000, ANGLE_4, 0, 0, ATT3, ATT4, ATT5, ATT6, ATT7);
// I guess I should check these values, but I don't think it's actually possible for these functions to fail here.
 boss.e1 = e1;

  boss.hp = 80000 + arena.difficulty * 10000;
  boss.max_hp = boss.hp;
  boss.moving = 0;
/*  boss.arm1 = 0;
  boss.arm1_change = 0;
  boss.arm2 = 0;
  boss.arm2_change = 0;
  boss.e1 = e1;
  boss.e2 = e2;
  boss.moving = 0;
  boss.pulse = 0;
  boss.arm_colour = -1;
  boss.arm_recycle = 100;
  boss.arm_change = 140;*/

// track[t].multiple = 0;

 boss.fight = 1;
// boss.bpattern = BOSS1_FIRE_NONE;

 enemy[boss.side1][e1].y_speed = 2000;
 enemy[boss.side1][e1].x_speed = 0;

 enemy[boss.side1][e1].a1 = 0;
 enemy[boss.side1][e1].b1 = 0;
 enemy[boss.side1][e1].c1 = 0;

 int i;

 for (i = 0; i < 10; i ++)
 {
  boss.old_angle [i] = 0;
 }

 boss.spot_angle = 0;
 boss.spot_angle_inc = 0;
 boss.spot_angle_inc_inc = 1;

 boss.move_time = 0;

}


void make_mb1_track(void)
{

 boss.side1 = grand(2);
 if (boss.side1 == 0)
  boss.side2 = 1;
   else
    boss.side2 = 0;

 int e1 = create_enemy(boss.side1, ENEMY_MB1, 0, 160000, -80000, ANGLE_4, 0, 0, ATT3, ATT4, ATT5, ATT6, ATT7);
// I guess I should check these values, but I don't think it's actually possible for these functions to fail here.
 boss.e1 = e1;

  boss.hp = 60000 + arena.difficulty * 6000;
  boss.max_hp = boss.hp;
  boss.moving = 0;
  boss.angle_1 = 0;
  boss.angle_2 = 0;
  boss.angle_3 = 0;
  boss.bflower_recycle = 100;
  boss.trecycle = 250;
  boss.bpattern = 0;
  boss.bpattern2 = 450;
  boss.btime = 200;
  boss.brecycle = 150;

 boss.fight = 1;

 enemy[boss.side1][e1].y_speed = 2000;
 enemy[boss.side1][e1].x_speed = 0;


}


