#include "config.h"

#include "allegro.h"

#include "globvars.h"

#include "stuff.h"
#include "enemy.h"

#include "palette.h"

void run_clouds(void);
void destroy_cloud(int w, int c);




void init_clouds(void)
{

 int c, w;
 for (w = 0; w < 2; w ++)
 {
  for (c = 0; c < NO_CLOUDS; c ++)
  {
   cloud[w][c].type = CLOUD_NONE;
  }
 }

}

int create_cloud(int w, int type, int type2, int x, int y, int xs, int ys, int timer)
{
  int c;

  for (c = 0; c < NO_CLOUDS; c ++)
  {
      if (cloud[w][c].type == CLOUD_NONE)
       break;
      if (c == NO_CLOUDS - 1)
       return -1;

  }

 cloud[w][c].type = type;
 cloud[w][c].x = x;
 cloud[w][c].y = y;
 cloud[w][c].x_speed = xs;
 cloud[w][c].y_speed = ys;
 cloud[w][c].type2 = type2;
 cloud[w][c].timeout = timer;

     return c;
}

void run_clouds(void)
{
//    if (counter % 20 != 0)
     //return;
  int c, c2, w;

  for (w = 0; w < 2; w ++)
  {
  for (c = 0; c < NO_CLOUDS; c ++)
  {
    if (cloud[w][c].type == CLOUD_NONE)
     continue;

    switch(cloud[w][c].type)
    {
         case CLOUD_LARGE_SHOCKWAVE2:
          cloud[w][c].timeout --;
          break;
         case CLOUD_PURPLE_WAVE:
          cloud[w][c].type2 = cloud[w][c].timeout / 2;
          break;
         case CLOUD_BLUE_WAVE:
          cloud[w][c].type2 = cloud[w][c].timeout / 5;
          break;
         case CLOUD_GREEN_WAVE:
          cloud[w][c].timeout += cloud[w][c].delay;
          if (arena.counter % 3 == 0)
           cloud[w][c].delay -= 1;
          if (cloud[w][c].delay < -2)
           cloud[w][c].delay = -2;
          cloud[w][c].type2 = cloud[w][c].timeout / 3;
          break;
         case CLOUD_RED_WAVE:
          cloud[w][c].timeout += cloud[w][c].delay;
          if (arena.counter % 3 == 0)
           cloud[w][c].delay -= 1;
          if (cloud[w][c].delay < -2)
           cloud[w][c].delay = -2;
          cloud[w][c].type2 = cloud[w][c].timeout / 2;// / 3;
          break;
         case CLOUD_SPRAY:
           do
           {
            create_cloud(w, CLOUD_SLOW_EXPLODE, cloud[w][c].type2, cloud[w][c].x + grand(5000) - grand(5000), cloud[w][c].y + grand(5000) - grand(5000), grand(2000) - grand(2000), grand(2000) - grand(2000), 13 + grand(15) + 10 - cloud[w][c].timeout);
            cloud[w][c].timeout --;
           } while (cloud[w][c].timeout > 0);
            break;
         case CLOUD_LARGE_SPRAY:

           do
           {
//            cloud[w][c].x_speed += grand(2001) - 1000;
//            cloud[w][c].y_speed += grand(2001) - 1000;
            cloud[w][c].x += cloud[w][c].x_speed;
            cloud[w][c].y += cloud[w][c].y_speed;
            cloud[w][c].x_speed *= 95;
            cloud[w][c].x_speed /= 100;
            cloud[w][c].y_speed *= 95;
            cloud[w][c].y_speed /= 100;
//            if (cloud[w][c].timeout % 3 != 0)
//             break;
            c2 = create_cloud(w,CLOUD_DELAY_EXPLODE, cloud[w][c].type2, cloud[w][c].x + grand(18000) - grand(18000), cloud[w][c].y + grand(18000) - grand(18000), cloud[w][c].x_speed, cloud[w][c].y_speed, 5 + grand(5) + cloud[w][c].timeout / 4);
            if (c2 != -1)
             cloud[w][c2].delay = 20 - cloud[w][c].timeout / 2;
            cloud[w][c].timeout --;
           } while (cloud[w][c].timeout > 0);
            break;
         case CLOUD_SPAWNER:
            cloud[w][c].x += cloud[w][c].x_speed;
            cloud[w][c].y += cloud[w][c].y_speed;
            cloud[w][c].x_speed *= 95;
            cloud[w][c].x_speed /= 100;
            cloud[w][c].y_speed *= 95;
            cloud[w][c].y_speed /= 100;
            if (cloud[w][c].timeout % 3 != 0)
             break;
            c2 = create_cloud(w, CLOUD_EXPLODE, cloud[w][c].type2, cloud[w][c].x + grand(55000) - grand(55000), cloud[w][c].y + grand(55000) - grand(55000), cloud[w][c].x_speed, cloud[w][c].y_speed, 5 + grand(15) + cloud[w][c].timeout / 2);
            if (c2 != -1)
             cloud[w][c2].delay = 20 - cloud[w][c].timeout / 2;
            break;
         case CLOUD_DELAY_EXPLODE:
            cloud[w][c].x_speed *= 85;
            cloud[w][c].x_speed /= 100;
            cloud[w][c].y_speed *= 85;
            cloud[w][c].y_speed /= 100;
            if (cloud[w][c].delay > 0)
            {
             cloud[w][c].delay --;
             cloud[w][c].timeout ++;
            }
             else cloud[w][c].type = CLOUD_EXPLODE;

            break;
         case CLOUD_EXPLODE:
         case CLOUD_DRAG_EXPLODE:
//            cloud[w][c].x += cloud[w][c].x_speed;
//            cloud[w][c].y += cloud[w][c].y_speed;
            cloud[w][c].x_speed *= 90;
            cloud[w][c].x_speed /= 100;
            cloud[w][c].y_speed *= 90;
            cloud[w][c].y_speed /= 100;
            break;
    }

            cloud[w][c].x += cloud[w][c].x_speed;
            cloud[w][c].y += cloud[w][c].y_speed;


    if (cloud[w][c].x <= -50000 || cloud[w][c].y <= -50000
     || cloud[w][c].x >= 510000 || cloud[w][c].y >= 510000)
     destroy_cloud(w, c);

    cloud[w][c].timeout --;

    if (cloud[w][c].timeout <= 0)
     destroy_cloud(w, c);


  }
  }
}


void destroy_cloud(int w, int c)
{
  cloud[w][c].type = CLOUD_NONE;
}

