
#include "config.h"

#include "allegro.h"

#include "globvars.h"

#include "stuff.h"
#include "ebullet.h"
#include "pbullet.h"
#include "cloud.h"

#include "sound.h"

#include "palette.h"
#include <math.h>
#include "stuff.h"


// I don't know why, but Code::Blocks refuses to recognise the contents of pickup.h. Nothing I can do.
#include "pickup.h"
void run_pickups(void);
void init_pickups(void);
//void destroy_pickup(int w, int p);
int create_pickup(int w, int colour, int x, int y);
void explode_pickup(int w, int p);

#define BOSS1_Y_MOD 7000

void move_drifter(int w, int e);
//void register_enemy_destroyed(int w, int e);
void destroy_enemy(int w, int e);
void move_enemy(int w, int e);
//void hurt_enemy(int e, int dam, int source);
void hurt_enemy(int w, int e, int dam, int pole, int source);
void move_dancer(int w, int e);
void move_diver(int w, int e);
void enemy_fire(int w, int e);
void enemy_explodes(int w, int e, int bullets, int source);
void explode_bullets(int e, int pole_match, int p);
void drag_enemy(int w, int e, int drag);
void track_target(int w, int e, int x, int y, int angle, int turn);
void move_glider(int w, int e);
void move_attacker(int w, int e);
void move_marcher(int w, int e);
void move_beamer(int w, int e);
void move_pauser(int w, int e);
void move_boss1(int w, int e);
void move_boss2(int w, int e);
void move_bflower(int w, int e);
void move_bfighter(int w, int e);
void move_mb2(int w, int e);
void move_mb1(int w, int e);
int attack_angle(int w, int e);
void boss1_fire_circle(int w, int e, int col);
void boss1_fire_scatter(int w, int e, int col);
void set_boss1_firing(int pattern);
void boss1_fire_spiral(int w, int e, int col);
void boss1_fire_burst(int w, int e, int col);
void new_boss1_firing(void);
void boss1_new_pattern(void);
void change_boss1_arms(int col);
void boss1_arms(void);
void boss1_arm_redshot(void);
void boss1_arm_blueshot(void);
void boss1_arm_greenbeam(void);
void boss1_exploding(void);
void kill_boss1(void);
void kill_boss2(void);
void kill_mb2(void);
void kill_mb1(void);
//void boss1_gives_powerup(void);
void player_score(int amount);

void new_mb2_firing(void);
void new_mb2_firing2(void);
void boss2_swap(void);
void spawn_bflower(void);
void spawn_bfighter(void);

void init_enemies(void)
{

 int e, w;

 for (w = 0; w < 2; w ++)
 {
  for (e = 0; e < NO_ENEMIES; e ++)
  {
   enemy[w] [e].type = ENEMY_NONE;
  }
 }
}

int create_enemy(int w, int type, int pole, int x, int y, int angle, int att1, int att2, int att3, int att4, int att5, int att6, int att7)
{

  int e;

  for (e = 0; e < NO_ENEMIES + 1; e ++)
  {
    if (e == NO_ENEMIES)
     return -1;
    if (enemy[w] [e].type == ENEMY_NONE)
     break;
  }

  enemy[w] [e].type = type;
  enemy[w] [e].pole = pole;
  enemy[w] [e].x = x;
  enemy[w] [e].y = y;
  enemy[w] [e].angle1 = angle;
  enemy[w] [e].angle2 = angle;
  enemy[w] [e].angle3 = angle;
  enemy[w] [e].persistent = 0;
  enemy[w] [e].hurt_pulse = 0;
  enemy[w] [e].hp = eclass[enemy[w] [e].type].max_hp;
  enemy[w] [e].angle1_turning = 0;
  enemy[w] [e].angle1_turning_count = 0;
  enemy[w] [e].recycle = 0;
  enemy[w] [e].burst_recycle = 0;
  enemy[w] [e].turn_direction = 0;
  enemy[w] [e].target = 0;

  switch(type)
  {
        default:
   case ENEMY_TWISTER:
        enemy[w] [e].x2 = att1;
        enemy[w] [e].y2 = att2;
        break;
   case ENEMY_GLIDER1:
        enemy[w] [e].turn_direction = pos_or_neg(10);
        enemy[w] [e].angle2 = grand(ANGLE_1);
        enemy[w] [e].angle3 = 0;
        enemy[w] [e].recycle = 4;
        enemy[w] [e].burst_recycle = 4 + arena.difficulty;
        break;
   case ENEMY_BURSTER:
        enemy[w][e].angle2 = 0;
        enemy[w][e].angle3 = 0;
        enemy[w][e].recycle = 50;
        break;
   case ENEMY_CARRIER:
        enemy[w] [e].turn_direction = pos_or_neg(10);
        enemy[w][e].angle1 = 0;
        enemy[w][e].angle2 = 0;
        enemy[w][e].angle3 = 0;
        break;
   case ENEMY_CARRIER2:
        enemy[w] [e].turn_direction = pos_or_neg(24);
        enemy[w][e].angle1 = 0;
        enemy[w][e].angle2 = 0;
        enemy[w][e].angle3 = 0;
        break;
   case ENEMY_MARCHER1:
        enemy[w] [e].x2 = att1;
        enemy[w] [e].y2 = att2;
        break;
   case ENEMY_ATTACKER3:
        enemy[w] [e].recycle = 15;
        enemy[w] [e].burst_recycle = 4;
        break;
   case ENEMY_BEAMER1:
        enemy[w] [e].angle3 = 0;
        enemy[w] [e].angle2 = 0;
        break;
   case ENEMY_SHOTTER1:
        enemy[w] [e].recycle = 90;
        break;
   case ENEMY_ZAPPER1:
        enemy[w] [e].recycle = 70;
        enemy[w] [e].burst_recycle = 3;
        break;
   case ENEMY_DIPPER1:
   case ENEMY_DIPPER2:
        enemy[w][e].angle2 = 0;
        enemy[w][e].recycle = 50;
        break;
   case ENEMY_BASIC1:
        enemy[w] [e].recycle = 3000;
        break;
   case ENEMY_BASIC3:
        enemy[w] [e].recycle = 50;
        enemy[w] [e].x2 = att1;
        enemy[w] [e].y2 = att2;
        break;
   case ENEMY_CROSS:
   case ENEMY_CROSS2:
        enemy[w][e].a1 = 0;
        enemy[w][e].b1 = 0;
        enemy[w][e].c1 = 0;
        break;
   case ENEMY_WINGS:
        enemy[w][e].a1 = 0;
        enemy[w][e].b1 = 0;
        enemy[w][e].c1 = 0;
        enemy[w][e].d1 = 0;
        enemy[w][e].recycle = 60;
        break;
   case ENEMY_FLOWER1:
        enemy[w][e].angle1 = 0;
        enemy[w][e].recycle = 70 + grand(20);
        break;
   case ENEMY_FLOWER2:
   case ENEMY_FLOWER3:
   case ENEMY_FLOWER4:
   case ENEMY_FLOWER5:
        enemy[w][e].angle1 = 0;
        enemy[w][e].recycle = 70 + grand(20);
        break;
   case ENEMY_MB2:
   case ENEMY_MB1:
    enemy[w] [e].persistent = 1;
    break;
   case ENEMY_BFLOWER:
        enemy[w][e].recycle = 55;
        break;


  }

 return e;

}



void run_enemies(void)
{

 int e, w;

 for (w = 0; w < 2; w ++)
 {
 for (e = 0; e < NO_ENEMIES; e ++)
 {
  if (enemy[w] [e].type == ENEMY_NONE)
   continue;
  if (!enemy[w] [e].persistent && (enemy[w] [e].x <= -150000 || enemy[w] [e].y <= -150000
    || enemy[w] [e].x >= 390000 || enemy[w] [e].y >= 550000))
  {
     destroy_enemy(w, e);
     continue;
  }
  if (enemy[w] [e].hurt_pulse > 0)
   enemy[w] [e].hurt_pulse --;
   switch(eclass[enemy[w] [e].type].ai) // replace with a switch for AI type
   {
    case AI_DANCER:
         move_dancer(w, e);
         break;
    case AI_DIVER:
         move_diver(w, e);
         break;
    case AI_GLIDER:
         move_glider(w, e);
         break;
    case AI_PAUSER:
         move_pauser(w, e);
         break;
    case AI_MARCH:
         move_marcher(w, e);
         break;
    case AI_ATTACKER:
         move_attacker(w, e);
         break;
    case AI_BEAMER:
         move_beamer(w, e);
         break;
    case AI_BOSS1:
         move_boss1(w, e);
         break;
    case AI_BOSS2:
         move_boss2(w, e);
         break;
    case AI_MB2:
         move_mb2(w, e);
         break;
    case AI_MB1:
         move_mb1(w, e);
         break;
    case AI_BFLOWER:
         move_bflower(w, e);
         break;
    case AI_BFIGHTER:
         move_bfighter(w, e);
         break;
   }
 }
 }
// remember - enemy may have been destroyed by now!

}



void move_mb1(int w, int e)
{

 if (boss.fight == 0)
 {
  destroy_enemy(w, e);
  return;
 }

 enemy[w][e].x += enemy[w][e].x_speed;
 enemy[w][e].y += enemy[w][e].y_speed;

 int angle;


 int i;
 if (grand(3) != 0)
  create_cloud(boss.side1, CLOUD_SLOW_EXPLODE, 0, enemy[w][e].x - 41000 + grand(5000), enemy[w][e].y + 47000, grand(1001) - 500, 3000 + grand(3000), 15 + grand(10));
 if (grand(3) != 0)
  create_cloud(boss.side1, CLOUD_SLOW_EXPLODE, 0, enemy[w][e].x + 41000 - grand(5000), enemy[w][e].y + 47000, grand(1001) - 500, 3000 + grand(3000), 15 + grand(10));

 switch(boss.moving)
 {
  case 0:
   if (enemy[w][e].y > 80000)
   {
       drag_enemy(w, e, 960);
   }
   if (enemy[w][e].y_speed < 500)
   {
    enemy[w][e].y_speed = 0;
    boss.moving = 1;
   }
   break;
  case 1:
  if (enemy[w][e].x_speed < -2000 || enemy[w][e].x_speed > 2000)
   drag_enemy(w, e, 960);
   if (enemy[w][e].x < 160000)
    enemy[w][e].x_speed += 50;
   if (enemy[w][e].x < 9000)
    enemy[w][e].x_speed += 100;
   if (enemy[w][e].y < 100000)
    enemy[w][e].y_speed += 50;
   if (enemy[w][e].y < 9000)
    enemy[w][e].y_speed += 100;
   if (enemy[w][e].x > 160000)
    enemy[w][e].x_speed -= 50;
   if (enemy[w][e].x > 311000)
    enemy[w][e].x_speed -= 100;
   if (enemy[w][e].y > 100000)
    enemy[w][e].y_speed -= 50;
   if (enemy[w][e].y > 200000)
    enemy[w][e].y_speed -= 100;
   if (arena.counter % 80 == 0)
   {
     angle = grand(ANGLE_1);
     enemy[w][e].x_speed += xpart(angle, 2500);
     enemy[w][e].y_speed += ypart(angle, 500);
   }
   break;

 }

 int t1x = enemy[w][e].x - 39000;
 int ty = enemy[w][e].y;
 int t2x = enemy[w][e].x + 39000;

 boss.angle_1 = radians_to_angle(atan2(player.y - ty, player.x - t1x));
 boss.angle_2 = radians_to_angle(atan2(player.y - ty, player.x - t2x));

 if (boss.side1 == 0)
 {
  boss.angle_3 += 16;
 }
   else
    boss.angle_3 -= 16;
 boss.angle_3 &= 1023;

 boss.bflower_recycle --;

 if (boss.bflower_recycle <= 0)
 {
     spawn_bfighter();
 }

 boss.trecycle --;

 if (boss.trecycle <= 0)
 {
         create_ebullet(w, EBULLET_SHOT3, 2, enemy[w][e].x - 39000, enemy[w][e].y + 20000, xpart(boss.angle_1, 2700 + arena.difficulty * 300), ypart(boss.angle_1, 2700 + arena.difficulty * 300), boss.angle_1);
         create_cloud(w, CLOUD_EXPLODE, 2, enemy[w][e].x - 39000, enemy[w][e].y + 20000, 0, 0, 15 + grand(5));
         create_ebullet(w, EBULLET_SHOT3, 2, enemy[w][e].x + 39000, enemy[w][e].y + 20000, xpart(boss.angle_2, 2700 + arena.difficulty * 300), ypart(boss.angle_2, 2700 + arena.difficulty * 300), boss.angle_2);
         create_cloud(w, CLOUD_EXPLODE, 2, enemy[w][e].x + 39000, enemy[w][e].y + 20000, 0, 0, 15 + grand(5));
         boss.trecycle = 110 - arena.difficulty * 20;
         play_effectwfvx(w, NWAV_SHOT, 600 + grand(50), 150, enemy[w][e].x - 39000);
         play_effectwfvx(w, NWAV_SHOT, 600 + grand(50), 150, enemy[w][e].x + 39000);
 }

 boss.brecycle --;
 int b;
 int turn = pos_or_neg(10);
 int x = enemy[w][e].x;
 int y = enemy[w][e].y;

 boss.bpattern2 --;

 if (boss.bpattern2 == 0)
 {
  int new_pattern = grand(3);

  while(new_pattern == boss.bpattern)
  {
     new_pattern = grand(3);
  }

  boss.bpattern = new_pattern;
  boss.bpattern2 = 350 + grand(100);

 }

 if (boss.brecycle <= 0)
 {
  switch(boss.bpattern)
  {
   case 0:
    angle = boss.angle_3;
    play_effectwfvx(w, NWAV_BLAST, 1400 + grand(150), 150, enemy[w][e].x);
    for (i = 0; i < 3; i ++)
    {
       b = create_ebullet(w, EBULLET_SPIN, 2,
               x + xpart(angle, 5000),
               y + ypart(angle, 5000) + 20000,
               xpart(angle, 2000), ypart(angle, 2000), 0);
       if (b == -1) continue;
       create_cloud(w, CLOUD_EXPLODE, 2, x + xpart(angle, 5000), y + ypart(angle, 5000) + 20000, 0,0, 12 + grand(4));
       ebullet[w][b].turn_direction = turn;
       angle += ANGLE_6;
       turn *= -1;

       b = create_ebullet(w, EBULLET_SPIN, 0,
               x + xpart(angle, 5000),
               y + ypart(angle, 5000) + 20000,
               xpart(angle, 800), ypart(angle, 800), 0);
       if (b == -1) continue;
       create_cloud(w, CLOUD_EXPLODE, 0, x + xpart(angle, 5000), y + ypart(angle, 5000) + 20000, 0,0, 12 + grand(4));
       ebullet[w][b].turn_direction = turn;
       angle += ANGLE_6;
       turn *= -1;
    }
    boss.brecycle = 90 - arena.difficulty * 12;
    break;
   case 1:
    angle = boss.angle_3;
    play_effectwfvx(w, NWAV_BLAST, 1600 + grand(150), 150, enemy[w][e].x);
    for (i = 0; i < 3; i ++)
    {
       b = create_ebullet(w, EBULLET_SHOT, 1,
               x + xpart(angle, 5000),
               y + ypart(angle, 5000) + 20000,
               xpart(angle, 2000), ypart(angle, 2000), angle);
       create_cloud(w, CLOUD_EXPLODE, 1, x + xpart(angle, 5000), y + ypart(angle, 5000) + 20000, xpart(angle, 2000),ypart(angle, 2000), 25 + grand(4));
       b = create_ebullet(w, EBULLET_SHOT, 1,
               x + xpart(angle, 5000),
               y + ypart(angle, 5000) + 20000,
               xpart(angle, 1500), ypart(angle, 1500), angle);
       b = create_ebullet(w, EBULLET_SHOT, 1,
               x + xpart(angle, 5000),
               y + ypart(angle, 5000) + 20000,
               xpart(angle, 1000), ypart(angle, 1000), angle);
       angle += ANGLE_3;
    }
    boss.brecycle = 80 - arena.difficulty * 10;
    break;
   case 2:
    angle = boss.angle_3;// - boss.bpattern2 * ANGLE_3;
    play_effectwfvx(w, NWAV_POP, 800, 200, enemy[w][e].x);
       b = create_ebullet(w, EBULLET_SHOT, 0,
               x + xpart(angle, 5000),
               y + ypart(angle, 5000) + 20000,
               xpart(angle, 2000), ypart(angle, 2000), angle);
       create_cloud(w, CLOUD_EXPLODE, 0, x + xpart(angle, 5000), y + ypart(angle, 5000) + 20000, xpart(angle, 2000),ypart(angle, 2000), 12 + grand(4));
    boss.brecycle = 6 - arena.difficulty;
    break;

  }


 }

}




void spawn_bfighter(void)
{

 int i;
 int bfs = 0;

 for (i = 0; i < NO_ENEMIES; i ++)
 {
    if (enemy[boss.side2][i].type == ENEMY_BFIGHTER)
     bfs ++;
 }

 if (bfs > 8)
  return;

  int bfx = -10000;
  if (grand(2))
   bfx = 330000;
  int bfy = 120000 + grand(150000);
  int e2 = create_enemy(boss.side2, ENEMY_BFIGHTER, 0, bfx, bfy, 0, 0,0,0,0,0,0,0);

  if (e2 != -1)
  {
   enemy[boss.side2][e2].angle3 = grand(3);
   enemy[boss.side2][e2].x_speed = 2000 + grand(1000);
   if (bfx > 0)
    enemy[boss.side2][e2].x_speed = -2000 - grand(1000);
   enemy[boss.side2][e2].y_speed = 0;
   enemy[boss.side2][e2].recycle = 100 - arena.difficulty * 25;
  }
  boss.bflower_recycle = 50 - arena.difficulty * 9;

}

void move_bfighter(int w, int e)
{
 enemy[w][e].x += enemy[w][e].x_speed;
 enemy[w][e].y += enemy[w][e].y_speed;

 drag_enemy(w, e, 995);

 enemy[w][e].y_speed -= 15;

 enemy[w][e].recycle --;

 if (enemy[w][e].x < 160000)
  enemy[w][e].x_speed -= 10;
   else
    enemy[w][e].x_speed += 10;

 int xd = 0;
 if (enemy[w][e].x_speed < -600)
  xd = 3000;
 if (enemy[w][e].x_speed < -150)
  xd = 1000;
 if (enemy[w][e].x_speed > 600)
  xd = -3000;
 if (enemy[w][e].x_speed > 150)
  xd = -1000;

 if (grand(3) == 0)
  create_cloud(w, CLOUD_SLOW_EXPLODE, 1, enemy[w][e].x + xd, enemy[w][e].y + 10000, grand(1001) - 500, 2000 + grand(2000), 10);


 if (enemy[w][e].recycle <= 0)
 {
        create_cloud(w, CLOUD_EXPLODE, 1, enemy[w][e].x, enemy[w][e].y, 0, 0, 14 + grand(5));
        enemy_fire(w, e);
        enemy[w][e].recycle = 100 - arena.difficulty * 20;
 }


 if (enemy[w][e].y <= -15000)
 {
  destroy_enemy(w, e);
 }

}

void move_mb2(int w, int e)
{

 if (boss.fight == 0)
 {
  destroy_enemy(w, e);
  return;
 }


 enemy[w][e].x += enemy[w][e].x_speed;
 enemy[w][e].y += enemy[w][e].y_speed;

 int angle;

// if (boss.moving > 1)

 int i;

 boss.old_angle [0] = enemy[w][e].angle3;

 for (i = 9; i > 0; i --)
 {
  boss.old_angle [i] = boss.old_angle [i - 1];
 }

 enemy[w][e].angle3 = enemy[w][e].angle2;

 {
  if (enemy[w][e].a1 == 0)
  {
   enemy[w][e].b1 -= 8;
   if (enemy[w][e].b1 < -25)
   enemy[w][e].b1 = -25;
   enemy[w][e].angle2 += enemy[w][e].b1;
   if (enemy[w][e].angle2 <= ANGLE_8 - ANGLE_4)
    enemy[w][e].a1 = 1;
  } else
  {
   enemy[w][e].b1 += 12;
   if (enemy[w][e].b1 > 50)
   enemy[w][e].b1 = 50;
   enemy[w][e].angle2 += enemy[w][e].b1;
   if (enemy[w][e].angle2 >= ANGLE_2 - ANGLE_4 - ANGLE_8)
    enemy[w][e].a1 = 0;
  }
  /*  if (enemy[w][e].a1 == 0)
  {
   enemy[w][e].b1 -= 7;
   if (enemy[w][e].b1 < -15)
   enemy[w][e].b1 = -15;
   enemy[w][e].angle2 += enemy[w][e].b1;
   if (enemy[w][e].angle2 <= ANGLE_8 - ANGLE_4)
    enemy[w][e].a1 = 1;
  } else
  {
   enemy[w][e].b1 += 7;
   if (enemy[w][e].b1 > 30)
   enemy[w][e].b1 = 30;
   enemy[w][e].angle2 += enemy[w][e].b1;
   if (enemy[w][e].angle2 >= ANGLE_2 - ANGLE_4 - ANGLE_4 - ANGLE_16 - ANGLE_64)
    enemy[w][e].a1 = 0;
  }*/
 }



 switch(boss.moving)
 {
  case 0:
   if (enemy[w][e].y > 80000)
   {
       drag_enemy(w, e, 960);
   }
   if (enemy[w][e].y_speed < 500)
   {
    enemy[w][e].y_speed = 0;
    boss.moving = 1;
    boss.move_time = 0;
   }

   break;
  case 1:
    boss.move_time += 1;
    if (boss.move_time >= 60)
    {
     boss.moving = 2;
     boss.bpattern = 1;
     boss.bpattern2 = 0;
     boss.new_bpattern = 400;
     boss.new_bpattern2 = 200;
    }
   break;
  case 2:
  if (enemy[w][e].x_speed < -2000 || enemy[w][e].x_speed > 2000)
   drag_enemy(w, e, 960);
   if (enemy[w][e].x < 160000)
    enemy[w][e].x_speed += 50;
   if (enemy[w][e].x < 9000)
    enemy[w][e].x_speed += 100;
   if (enemy[w][e].y < 100000)
    enemy[w][e].y_speed += 50;
   if (enemy[w][e].y < 9000)
    enemy[w][e].y_speed += 100;
   if (enemy[w][e].x > 160000)
    enemy[w][e].x_speed -= 50;
   if (enemy[w][e].x > 311000)
    enemy[w][e].x_speed -= 100;
   if (enemy[w][e].y > 100000)
    enemy[w][e].y_speed -= 50;
   if (enemy[w][e].y > 200000)
    enemy[w][e].y_speed -= 100;
   if (arena.counter % 80 == 0)
   {
     angle = grand(ANGLE_1);
     enemy[w][e].x_speed += xpart(angle, 1500);
     enemy[w][e].y_speed += ypart(angle, 500);
   }
   break;

 }

 int x = enemy[w][e].x;
 int y = enemy[w][e].y - 14000;
 int xa;

 if (boss.bpattern > 0)
 {
  boss.new_bpattern --;
  if (boss.new_bpattern <= 0)
  {
   new_mb2_firing();
   boss.new_bpattern = 300;
   enemy[w][e].recycle = 40;
  }
  boss.new_bpattern2 --;
  if (boss.new_bpattern2 <= 0)
  {
   new_mb2_firing2();
   boss.new_bpattern2 = 400;
   boss.brecycle = 40;
  }
 }

 switch(boss.bpattern)
 {
     case 0:
      break;
  case 1: // starburst
   enemy[w][e].recycle --;
   if (enemy[w][e].recycle > 0)
    break;
    xa = 1;
    if (grand(2) == 0)
     xa = -1;
    angle = grand(ANGLE_1);
    for (i = 0; i < 4; i ++)
    {
     shoot_diamond(w, x, y, xpart(angle + i * ANGLE_4, 3000), ypart(angle + i * ANGLE_4, 3000), angle, 2, xa);
    }
    xa *= -1;
    for (i = 0; i < 4; i ++)
    {
     shoot_diamond(w, x, y, xpart(angle + i * ANGLE_4 + ANGLE_8, 1000), ypart(angle + i * ANGLE_4 + ANGLE_8, 1000), angle, 2, xa);
    }
    xa *= -1;
    for (i = 0; i < 8; i ++)
    {
     shoot_diamond(w, x, y, xpart(angle + i * ANGLE_8 + ANGLE_16, 2000), ypart(angle + i * ANGLE_8 + ANGLE_16, 2000), angle, 1, xa);
    }
    enemy[w][e].recycle = 120 - arena.difficulty * 20;
    play_effectwfvx(w, NWAV_TONE, 1800, 200, enemy[w][e].x);
   break;
  case 2: // circle
   enemy[w][e].recycle --;
   if (enemy[w][e].recycle > 0)
    break;
//    angle = grand(ANGLE_1);
//     angle = radians_to_angle(atan2(player.y - (y + ypart(angle + i * ANGLE_8, 50000)), player.x - (x + xpart(angle + i * ANGLE_8, 50000))));
    angle = attack_angle(w, e);
    for (i = 0; i < 8; i ++)
    {
     shoot_triangle(w, x + xpart(angle + i * ANGLE_8, 50000), y + ypart(angle + i * ANGLE_8, 50000), angle, 2);
    }
    enemy[w][e].recycle = 100 - arena.difficulty * 20;
    play_effectwfvx(w, NWAV_TONE, 2200, 200, enemy[w][e].x);
   break;
   default:
  case 3: // random
   enemy[w][e].recycle --;
   if (enemy[w][e].recycle > 0)
    break;
    xa = 1;
    if (grand(2) == 0)
     xa = -1;
    angle = grand(ANGLE_1);
    shoot_diamond(w, x, y, xpart(angle, 3000), ypart(angle, 3000), angle, 0, xa);
    enemy[w][e].recycle = 4 - arena.difficulty;
    play_effectwfvx(w, NWAV_TAP, 2200, 80, enemy[w][e].x);
   break;

 }


 switch(boss.bpattern2)
 {
  case 0: break;
   case 1: // inner spiral
    boss.brecycle --;
    if (boss.brecycle > 0)
     break;
    shoot_triangle(boss.side2, enemy[w][e].x + xpart(boss.spot_angle, 100000), enemy[w][e].y + ypart(boss.spot_angle, 100000), ANGLE_1 - boss.spot_angle, 0);
    shoot_triangle(boss.side2, enemy[w][e].x + xpart(boss.spot_angle + ANGLE_3, 100000), enemy[w][e].y + ypart(boss.spot_angle + ANGLE_3, 100000), ANGLE_1 - (boss.spot_angle + ANGLE_3), 1);
    shoot_triangle(boss.side2, enemy[w][e].x + xpart(boss.spot_angle - ANGLE_3, 100000), enemy[w][e].y + ypart(boss.spot_angle - ANGLE_3, 100000), ANGLE_1 - (boss.spot_angle - ANGLE_3), 2);
    boss.brecycle = 10 - arena.difficulty * 2;
    play_effectwfvx(w, NWAV_POP, 1500, 100, enemy[w][e].x);
    break;
   case 2: // bursts
    boss.brecycle --;
    if (boss.brecycle > 0)
     break;
    for (i = 0; i < 4; i ++)
    {
     shoot_triangle(boss.side2, enemy[w][e].x + xpart(boss.spot_angle, 100000), enemy[w][e].y + ypart(boss.spot_angle, 100000), ANGLE_1 - boss.spot_angle + i * ANGLE_4, 0);
    }
    for (i = 0; i < 4; i ++)
    {
     shoot_triangle(boss.side2, enemy[w][e].x + xpart(boss.spot_angle + ANGLE_3, 100000), enemy[w][e].y + ypart(boss.spot_angle + ANGLE_3, 100000), ANGLE_1 - (boss.spot_angle + ANGLE_3) + i * ANGLE_4, 1);
    }
    for (i = 0; i < 4; i ++)
    {
     shoot_triangle(boss.side2, enemy[w][e].x + xpart(boss.spot_angle - ANGLE_3, 100000), enemy[w][e].y + ypart(boss.spot_angle - ANGLE_3, 100000), ANGLE_1 - (boss.spot_angle - ANGLE_3) + i * ANGLE_4, 2);
    }
    boss.brecycle = 40 - arena.difficulty * 10;
    play_effectwfvx(w, NWAV_TAP, 2000, 70, enemy[w][e].x);
    break;
   default:
   case 3: // circle bursts
    boss.brecycle --;
    if (boss.brecycle > 0)
     break;
    switch(grand(3))
    {
      case 0:
       for (i = 0; i < 16; i ++)
       {
         shoot_circle(boss.side2, enemy[w][e].x + xpart(boss.spot_angle, 100000), enemy[w][e].y + ypart(boss.spot_angle, 100000), xpart(i * ANGLE_16, 3000), ypart(i * ANGLE_16, 3000), i * ANGLE_16, 0);
       }
       break;
      case 1:
       for (i = 0; i < 16; i ++)
       {
         shoot_circle(boss.side2, enemy[w][e].x + xpart(boss.spot_angle + ANGLE_3, 100000), enemy[w][e].y + ypart(boss.spot_angle + ANGLE_3, 100000), xpart(i * ANGLE_16, 3000), ypart(i * ANGLE_16, 3000), i * ANGLE_16, 1);
       }
       break;
      case 2:
       for (i = 0; i < 16; i ++)
       {
         shoot_circle(boss.side2, enemy[w][e].x + xpart(boss.spot_angle - ANGLE_3, 100000), enemy[w][e].y + ypart(boss.spot_angle - ANGLE_3, 100000), xpart(i * ANGLE_16, 3000), ypart(i * ANGLE_16, 3000), i * ANGLE_16, 2);
       }
       break;
    }
    boss.brecycle = 60 - arena.difficulty * 15;
    play_effectwfvx(w, NWAV_TAP, 2200, 90, enemy[w][e].x);
    break;
 }


 boss.spot_angle += boss.spot_angle_inc;
 boss.spot_angle &= 1023;

  boss.spot_angle_inc += boss.spot_angle_inc_inc;

  if (boss.spot_angle_inc < -8)
   boss.spot_angle_inc = -8;
  if (boss.spot_angle_inc > 8)
   boss.spot_angle_inc = 8;


 if (arena.counter == 0)
 {
  boss.spot_angle_inc_inc *= -1;
 }

}


void new_mb2_firing(void)
{
 int new_pattern = grand(3) + 1;

 while(new_pattern == boss.bpattern)
 {
     new_pattern = grand(3) + 1;
 }

 //set_boss1_firing(new_pattern);
 boss.bpattern = new_pattern;

}

void new_mb2_firing2(void)
{
 int new_pattern = grand(3) + 1;

 while(new_pattern == boss.bpattern2)
 {
     new_pattern = grand(3) + 1;
 }

 boss.bpattern2 = new_pattern;


}

void move_boss2(int w, int e)
{

 if (boss.fight == 0)
 {
  destroy_enemy(w, e);
  return;
 }

    if (w == 0)
     enemy[w][e].angle1 += 4;
      else
       enemy[w][e].angle1 -= 4;
    enemy[w][e].angle1 &= 1023;

    int x = enemy[w][e].x;
    int y = enemy[w][e].y;
    int xa, xb, i;

    enemy[w][e].angle2 += 12;
    enemy[w][e].angle2 &= 1023;

    if (boss.phase == 1)
    {
     enemy[w][e].recycle --;
     if (enemy[w][e].recycle <= 0)
     {
      switch(boss.colour)
      {
       case 0:
        xa = grand(ANGLE_1);
        for (i = 0; i < 16; i ++)
        {
          shoot_circle(w, x + xpart(xa, 20000), y + ypart(xa, 20000), xpart(xa, 1500), ypart(xa, 1500), xa, 0);
          xa += ANGLE_16;
        }
        play_effectwfvx(w, NWAV_TAP, 1200, 150, enemy[w][e].x);
        enemy[w][e].recycle = 50 - arena.difficulty * 10;
        break;
       case 1:
        xb = grand(ANGLE_1);
        xa = 1;
        if (grand(2) == 0)
         xa = -1;
        shoot_star(w, x + xpart(xb, 20000), y + ypart(xb, 20000), xpart(xb, 1500), ypart(xb, 1500), xb, 1, xa);
        play_effectwfvx(w, NWAV_TAP, 1200, 100, enemy[w][e].x);
        enemy[w][e].recycle = 30 - arena.difficulty * 5;
        break;
       case 2:
        xb = (enemy[w][e].angle2) & 1023;
        shoot_triangle(w, x + xpart(xb, 20000), y + ypart(xb, 20000), xb, 2);
        shoot_triangle(w, x + xpart((ANGLE_1 + ANGLE_4 + ANGLE_4) - xb, 20000), y + ypart((ANGLE_1 + ANGLE_4 + ANGLE_4) - xb, 20000), (ANGLE_1 + ANGLE_4 + ANGLE_4) - xb, 2);
        play_effectwfvx(w, NWAV_POP, 1200, 130, enemy[w][e].x);
        enemy[w][e].recycle = 7 - arena.difficulty;
        break;

      }
     }


    }

    switch(boss.phase)
    {
     case 0: // growing
      boss.size += boss.size_inc;
      if (arena.counter % 6 == 0)
       boss.size_inc ++;
      if (boss.size_inc > 6)
       boss.size_inc = 6;
      if (boss.size >= 100)
      {
       boss.size = 100;
       boss.phase = 1;
       boss.phase_count = 270;
       enemy[w][e].recycle = 10;
      }
      break;
      case 1:
       boss.phase_count --;
       if (boss.phase_count <= 0)
       {
         boss.phase = 2;
         boss.size_inc = 0;
       }
       boss.bflower_recycle --;
       if (boss.bflower_recycle <= 0)
       {
        spawn_bflower();
        boss.bflower_recycle = 50 - arena.difficulty * 9;
       }
       break;
     case 2: // shrinking
      boss.size += boss.size_inc;
      if (arena.counter % 6 == 0)
       boss.size_inc --;
      if (boss.size_inc < -6)
       boss.size_inc = -6;
      if (boss.size <= 20)
      {
       boss.size = 20;
       boss.phase = 0;
       boss.size_inc = 1;
       boss2_swap();
      }
      break;
    }
}

void boss2_swap(void)
{

 create_cloud(0, CLOUD_SHOCKWAVE2, 2, 160000, 120000, 0, 0, 20);
 create_cloud(0, CLOUD_EXPLODE2, 0, 160000, 120000, 0, 0, 20);
 create_cloud(1, CLOUD_SHOCKWAVE2, 2, 160000, 120000, 0, 0, 20);
 create_cloud(1, CLOUD_EXPLODE2, 0, 160000, 120000, 0, 0, 20);

 play_effectwfvx(boss.side1, NWAV_BLOP, 700, 180, enemy[boss.side1][boss.e1].x);

 if (boss.side1 == 0)
  boss.side1 = 1;
   else
    boss.side1 = 0;

 if (boss.side1 == 0)
  boss.side2 = 1;
   else
    boss.side2 = 0;

 int w = boss.side2;
 int e = boss.e1;

 int e2 = create_enemy(boss.side1, ENEMY_BOSS2, 0, enemy[w][e].x, enemy[w][e].y, enemy[w][e].angle1, 0,0,0,0,0,0,0);

 enemy[boss.side1] [e2].persistent = 1;

 boss.e1 = e2;
 boss.colour = grand(3);

 destroy_enemy(boss.side2, e);


}

void spawn_bflower(void)
{
 int i;
 int bfs = 0;

 for (i = 0; i < NO_ENEMIES; i ++)
 {
    if (enemy[boss.side2][i].type == ENEMY_BFLOWER)
     bfs ++;
 }

 if (bfs > 8)
  return;

 int angle = grand(ANGLE_1);
 int distance = 30000 + grand(70000);
 int angle2 = grand(ANGLE_1);

 int x = 160000 + xpart(angle, distance);
 int y = 120000 + ypart(angle, distance);

 int e = create_enemy(boss.side2, ENEMY_BFLOWER, 0, x, y, 0, 0,0,0,0,0,0,0);

 if (e != -1)
 {
  enemy[boss.side2][e].angle3 = grand(3);
  angle = grand(ANGLE_1);
  distance = 200 + grand(200);
  enemy[boss.side2][e].x_speed = xpart(angle, distance);
  enemy[boss.side2][e].y_speed = ypart(angle, distance);
 }

// angle1 = angle
// angle2 = size
// angle3 = colour

}

void move_bflower(int w, int e)
{
 enemy[w][e].x += enemy[w][e].x_speed;
 enemy[w][e].y += enemy[w][e].y_speed;

 if (w == 0)
  enemy[w][e].angle1 += 8;
   else
    enemy[w][e].angle1 -= 8;

 if (enemy[w][e].angle2 < 100)
  enemy[w][e].angle2 += 2;

 enemy[w][e].recycle --;

 int angle;
 int xa;

 if (enemy[w][e].recycle <= 0)
 {
  switch(enemy[w][e].angle3)
  {
   case 0: // single aimed triangle
    angle = attack_angle(w, e);
    shoot_triangle(w, enemy[w][e].x, enemy[w][e].y, angle, 0);
    enemy[w][e].recycle = 90 - arena.difficulty * 15;
    play_effectwfvx(w, NWAV_TAP, 2200, 80, enemy[w][e].x);
    break;
   case 1: // double aimed diamond
    angle = attack_angle(w, e);
    xa = 1;
    if (grand(2) == 0)
     xa = -1;
    shoot_diamond(w, enemy[w][e].x, enemy[w][e].y, xpart(angle - ANGLE_32, 2000), ypart(angle - ANGLE_32, 2000), angle - ANGLE_32, 1, xa);
    xa *= -1;
    shoot_diamond(w, enemy[w][e].x, enemy[w][e].y, xpart(angle + ANGLE_32, 2000), ypart(angle + ANGLE_32, 2000), angle + ANGLE_32, 1, xa);
    enemy[w][e].recycle = 130 - arena.difficulty * 20;
    play_effectwfvx(w, NWAV_TAP, 2000, 90, enemy[w][e].x);
    break;
   case 2: // triple random triangle
    angle = grand(ANGLE_1);
    shoot_triangle(w, enemy[w][e].x, enemy[w][e].y, angle, 2);
    shoot_triangle(w, enemy[w][e].x, enemy[w][e].y, angle - ANGLE_3, 2);
    shoot_triangle(w, enemy[w][e].x, enemy[w][e].y, angle + ANGLE_3, 2);
    enemy[w][e].recycle = 110 - arena.difficulty * 20;
    play_effectwfvx(w, NWAV_TAP, 1800, 100, enemy[w][e].x);
    break;

  }

 }

  if (enemy[w] [e].x <= -22000 || enemy[w] [e].y <= -22000
    || enemy[w] [e].x >= 342000 || enemy[w] [e].y >= 502000)
  {
     destroy_enemy(w, e);
  }

}

void move_boss1(int w, int e)
{
 if (boss.fight == 0)
 {
  destroy_enemy(w, e);
  return;
 }

 if (w == 0)
  enemy[w][e].angle1 += 4;
   else
    enemy[w][e].angle1 -= 4;
 enemy[w][e].angle1 &= 1023;

 if (w == 1)
 {
  enemy[w][e].x = enemy[0][boss.e1].x;
  enemy[w][e].y = enemy[0][boss.e1].y;
  return;
 }

 if (boss.exploding > 0)
 {
  boss1_exploding();
  return;
 }

 switch(boss.moving)
 {
  case BOSS1_APPROACH:
   if (enemy[w][e].y > 100000)
   {
       drag_enemy(w, e, 960);
   }
   if (enemy[w][e].y_speed < 500)
   {
    enemy[w][e].y_speed = 0;
    boss.moving = BOSS1_OPEN;
   }

   break;
  case BOSS1_OPEN:
   if (boss.arm1 < 60)
    boss.arm1 += 2;
     else
      boss.arm2 += 2;
    if (boss.arm2 >= 60)
    {
     boss.moving = BOSS1_SIDES;
     boss.move_x = pos_or_neg(1);
     boss.move_time = 4;
     set_boss1_firing(BOSS1_FIRE_CIRCLES);
    }
   break;
  case BOSS1_SIDES:
   boss.pulse += 8;
   if (enemy[w][e].y < 60000)
    enemy[w][e].y_speed += 200;
   if (enemy[w][e].y > 170000)
    enemy[w][e].y_speed -= 200;
   if (boss.move_x == -1)
   {
    enemy[w][e].x_speed -= 250;
    drag_enemy(w, e, 850);
    if (enemy[w][e].x < 90000)
    {
     boss.move_x = 1;
     boss.move_time --;
    }
   }
    else
    {
     enemy[w][e].x_speed += 250;
     drag_enemy(w, e, 850);
     if (enemy[w][e].x > 230000)
     {
      boss.move_x = -1;
      boss.move_time --;
     }
    }
   if (boss.move_time == 0)
   {
    boss1_new_pattern();
    new_boss1_firing();
   }
   break;
  case BOSS1_SWIRL:
  boss.pulse += 32;
  if (enemy[w][e].x < 160000 && enemy[w][e].y < 100000)
   enemy[w][e].x_speed += 200;
  if (enemy[w][e].x >= 160000 && enemy[w][e].y < 100000)
   enemy[w][e].y_speed += 160;
  if (enemy[w][e].x >= 160000 && enemy[w][e].y >= 100000)
   enemy[w][e].x_speed -= 200;
  if (enemy[w][e].x < 160000 && enemy[w][e].y >= 100000)
   enemy[w][e].y_speed -= 160;
  drag_enemy(w, e, 970);
  boss.move_time --;
  if (boss.move_time == 0)
  {
    boss1_new_pattern();
     new_boss1_firing();
  }
   break;
  case BOSS1_SWIRL2:
  boss.pulse += 32;
  if (enemy[w][e].x < 160000 && enemy[w][e].y < 100000)
   enemy[w][e].y_speed += 160;
  if (enemy[w][e].x >= 160000 && enemy[w][e].y < 100000)
   enemy[w][e].x_speed -= 200;
  if (enemy[w][e].x >= 160000 && enemy[w][e].y >= 100000)
   enemy[w][e].y_speed -= 160;
  if (enemy[w][e].x < 160000 && enemy[w][e].y >= 100000)
   enemy[w][e].x_speed += 200;
  drag_enemy(w, e, 970);
  boss.move_time --;
  if (boss.move_time == 0)
  {
    boss1_new_pattern();
     new_boss1_firing();
  }
   break;
  case BOSS1_BEAM1:
  boss.pulse += 8;
   if (enemy[w][e].y < 110000)
    enemy[w][e].y_speed += 200;
   if (enemy[w][e].y > 130000)
    enemy[w][e].y_speed -= 200;
   if (enemy[w][e].x < 150000)
    enemy[w][e].x_speed += 200;
   if (enemy[w][e].x > 170000)
    enemy[w][e].x_speed -= 200;
   if (enemy[w][e].x > 150000 && enemy[w][e].x < 170000
       && enemy[w][e].y > 110000 && enemy[w][e].y < 130000)
       {
        boss.moving = BOSS1_BEAM2;
       }
   drag_enemy(w, e, 950);
   break;
  case BOSS1_BEAM2:
  boss.pulse += 4;
  drag_enemy(w, e, 900);
  if (boss.arm_beam <= 0)
   boss.move_time --;
  if (boss.move_time <= 0)
  {
    boss.arm_change = 1;
    boss1_new_pattern();
     new_boss1_firing();
  }
  break;
 }

 switch(boss.bpattern)
 {
  case BOSS1_FIRE_CIRCLES:
   boss.brecycle --;
   if (boss.brecycle <= 0)
   {
    if (boss.bstatus1 == 0)
    {
     boss1_fire_circle(0, boss.e1, 0);//boss.bstatus2);
     boss.bstatus1 = 1;
    }
     else
     {
      boss1_fire_circle(1, boss.e2, 0);//boss.bstatus2);
      boss.bstatus1 = 0;
     }
/*     boss.bstatus2 ++;
     if (boss.bstatus2 == 3)
      boss.bstatus2 = 0;*/
     boss.brecycle = 18 - arena.difficulty * 2;

   }
   break;
  case BOSS1_FIRE_SCATTER:
   boss.brecycle --;
   boss.bstatus1 = grand(2);
   if (boss.brecycle <= 0)
   {
    if (boss.bstatus1 == 0)
    {
     boss1_fire_scatter(0, boss.e1, 1);//boss.bstatus2);
    }
     else
     {
      boss1_fire_scatter(1, boss.e2, 1);//boss.bstatus2);
     }
     boss.brecycle = 3 - arena.difficulty;

   }
   break;
  case BOSS1_FIRE_SPIRAL:
   boss.brecycle --;
   if (boss.brecycle <= 0)
   {
/*    if (boss.bstatus1 == 0)
    {
     boss1_fire_spiral(0, boss.e1, 2);//boss.bstatus2);
     boss.bstatus1 = 1;
    }
     else
     {
      boss1_fire_spiral(1, boss.e2, 2);//boss.bstatus2);
      boss.bstatus1 = 0;
     }*/
     boss1_fire_spiral(0, boss.e1, 2);//boss.bstatus2);
     boss.brecycle = 4 - arena.difficulty;

   }
   break;
  case BOSS1_FIRE_BURST:
   boss.brecycle --;
   if (boss.brecycle <= 0)
   {
     if (boss.bstatus1 == 0)
     {
      boss1_fire_burst(0, boss.e1, 0);//boss.bstatus2);
     }
      else
      {
       boss1_fire_burst(1, boss.e2, 0);//boss.bstatus2);
      }
     boss.brecycle = 6 - arena.difficulty;
     boss.bstatus2 --;
     if (boss.bstatus2 == 0)
     {
         if (boss.bstatus1 == 0)
          boss.bstatus1 = 1;
           else
            boss.bstatus1 = 0;
          boss.bstatus2 = 7;
          boss.brecycle = 6 - arena.difficulty;
     }
   }
   break;

 }


 enemy[w][e].x += enemy[w][e].x_speed;
 enemy[w][e].y += enemy[w][e].y_speed;

 boss1_arms();

 boss.pulse &= 1023;

}
/*enum
{
    BOSS1_APPROACH,
    BOSS1_HOLD,
    BOSS1_SIDES,
    BOSS1_SWIRL,
    BOSS1_DART
};
*/

void boss1_arms(void)
{

 if (boss.moving == BOSS1_APPROACH)
  return;

 if (boss.moving == BOSS1_OPEN && boss.arm2 == 58)
 {
  change_boss1_arms(2);
  boss.arm_change = 150;
  return;
 }

  if (boss.moving == BOSS1_BEAM1 || boss.moving == BOSS1_BEAM2)
  {
   if (boss.arm_colour != 1)
    change_boss1_arms(1);
   boss.arm_change = 50;
   boss.arm_recycle --;
   if (boss.arm_beam > 0)
   {
    boss1_arm_greenbeam();
    return;
   }
   if (boss.arm_recycle == 0)
   {
    boss1_arm_greenbeam();
    boss.arm_recycle = 200;
   }
   return;
  }


 boss.arm_change --;
 if (boss.arm_change == 0)
 {
  if (boss.arm_colour == 0)
   change_boss1_arms(2);
    else
     change_boss1_arms(0);
 //change_boss1_arms(grand(2) * 2); // 0 or 2
  boss.arm_change = 300;
  return;
 }

 boss.arm_recycle --;
 if (boss.arm_recycle == 0)
 {
  switch(boss.arm_colour)
  {
   case 0:
   default:
  boss1_arm_redshot();
  boss.arm_recycle = 250 - arena.difficulty * 50;
    break;
   case 1:
    boss1_arm_greenbeam();
    boss.arm_recycle = 250 - arena.difficulty * 50;
    break;
   case 2:
  boss1_arm_blueshot();
  boss.arm_recycle = 125 - arena.difficulty * 25;
    break;

  }
 }


}

void boss1_arm_greenbeam(void)
{

 int e = boss.e1;

   if (boss.arm_beam == 0)
   {
    boss.arm_beam = 300;
    boss.arm_beam_side = grand(2);
    play_effectwfvx(boss.arm_beam_side, NWAV_BEAM1, 600 + grand(50), 250, enemy[boss.arm_beam_side][e].x);
   }


 int xa = boss.arm1 * (1000 / 16);
 int xb = boss.arm1 * (1000 / 8);
 int ya = boss.arm2 * (1000 / 8);
 int yb = xpart(boss.pulse, 4000);

 int w = 0, b;
 int i;


 int x, y;
 //int angle;

 for (i = 0; i < 4; i ++)
 {
  switch(i)
  {
   case 0: x = enemy[w][e].x - 48000 - xa;
   y = enemy[w][e].y - 40000 - ya - yb;
   break;
   case 1: x = enemy[w][e].x - 29000 - xb;
   y = enemy[w][e].y + 40000 + ya + yb;
   break;
   case 2: x = enemy[w][e].x + 29000 + xb;
   y = enemy[w][e].y + 40000 + ya + yb;
   break;
   case 3: x = enemy[w][e].x + 48000 + xa;
   y = enemy[w][e].y - 40000 - ya - yb;
   break;
  }
  if (boss.arm_beam == 300)
  {
   boss.arm_beam_angle [i] = radians_to_angle(atan2(player.y - y, player.x - x));
  }
   if (arena.counter % 3 == 0)
   {
    if ((i + 1) % 2 == 0)
     boss.arm_beam_angle [i] ++;
      else
       boss.arm_beam_angle [i] --;
   }
  b = create_ebullet(boss.arm_beam_side, EBULLET_BEAM1, 1, x, y, xpart(boss.arm_beam_angle [i], 4000), ypart(boss.arm_beam_angle [i], 4000), boss.arm_beam_angle [i]);
  if (b != -1)
  {
    ebullet[boss.arm_beam_side][b].timeout = 2;
    ebullet[boss.arm_beam_side][b].status = boss.arm_beam;
    ebullet[boss.arm_beam_side][b].status2 = 1;
   }

 }

 boss.arm_beam -= 2;

}


void boss1_arm_redshot(void)
{

// int angle = attack_angle(w, e); //radians_to_angle(atan2(player.y - enemy[w] [e].y, player.x - enemy[w] [e].x));

 int xa = boss.arm1 * (1000 / 16);
 int xb = boss.arm1 * (1000 / 8);
 int ya = boss.arm2 * (1000 / 8);
 int yb = xpart(boss.pulse, 4000);

 int e = boss.e1;
 int w = 0, b;
 int i;


 int x, y;
 int angle;

 for (i = 0; i < 4; i ++)
 {
  switch(i)
  {
   case 0: x = enemy[w][e].x - 48000 - xa;
   y = enemy[w][e].y - 40000 - ya - yb;
   break;
   case 1: x = enemy[w][e].x - 29000 - xb;
   y = enemy[w][e].y + 40000 + ya + yb;
   break;
   case 2: x = enemy[w][e].x + 29000 + xb;
   y = enemy[w][e].y + 40000 + ya + yb;
   break;
   case 3: x = enemy[w][e].x + 48000 + xa;
   y = enemy[w][e].y - 40000 - ya - yb;
   break;
  }
  angle = radians_to_angle(atan2(player.y - y, player.x - x));
  b = create_ebullet(i % 2, EBULLET_FAT_SHOT, 0, x, y, xpart(angle, 4500), ypart(angle, 4500), angle);
  create_ebullet(i % 2, EBULLET_FAT_SHOT, 0, x, y, xpart(angle, 3500), ypart(angle, 3500), angle);
  create_ebullet(i % 2, EBULLET_FAT_SHOT, 0, x, y, xpart(angle, 2500), ypart(angle, 2500), angle);
  if (b != -1)
   create_cloud(i % 2, CLOUD_EXPLODE, 0, x, y, ebullet[i % 2][b].x_speed, ebullet[i % 2][b].y_speed, 14 + grand(5));

 }

/*create_cloud(w, CLOUD_SHOCKWAVE, col, enemy[w][e].x - 29000 - xb, enemy[w][e].y + 40000 + ya + yb, enemy[w][e].x_speed, enemy[w][e].y_speed, 20);
 create_cloud(w, CLOUD_SHOCKWAVE, col, enemy[w][e].x + 29000 + xb, enemy[w][e].y + 40000 + ya + yb, enemy[w][e].x_speed, enemy[w][e].y_speed, 20);
 create_cloud(w, CLOUD_SHOCKWAVE, col, enemy[w][e].x - 48000 - xa, enemy[w][e].y - 40000 - ya - yb, enemy[w][e].x_speed, enemy[w][e].y_speed, 20);
 create_cloud(w, CLOUD_SHOCKWAVE, col, enemy[w][e].x + 48000 + xa, enemy[w][e].y - 40000 - ya - yb, enemy[w][e].x_speed, enemy[w][e].y_speed, 20);


      create_ebullet(w, EBULLET_SHOT, 0, enemy[w][e].x, enemy[w][e].y, xpart(angle, 4000), ypart(angle, 4000), angle);

*/

}

void boss1_arm_blueshot(void)
{

// int angle = attack_angle(w, e); //radians_to_angle(atan2(player.y - enemy[w] [e].y, player.x - enemy[w] [e].x));

 int xa = boss.arm1 * (1000 / 16);
 int xb = boss.arm1 * (1000 / 8);
 int ya = boss.arm2 * (1000 / 8);
 int yb = xpart(boss.pulse, 4000);

 int e = boss.e1;
 int w = 0, b;
 int i;


 int x, y;
 int angle;

 for (i = 0; i < 4; i ++)
 {
  switch(i)
  {
   case 0: x = enemy[w][e].x - 29000 - xb;
   y = enemy[w][e].y + 40000 + ya + yb;
   break;
   case 1: x = enemy[w][e].x - 48000 - xa;
   y = enemy[w][e].y - 40000 - ya - yb;
   break;
   case 2: x = enemy[w][e].x + 48000 + xa;
   y = enemy[w][e].y - 40000 - ya - yb;
   break;
   case 3: x = enemy[w][e].x + 29000 + xb;
   y = enemy[w][e].y + 40000 + ya + yb;
   break;
  }
  angle = radians_to_angle(atan2(player.y - y, player.x - x));
  b = create_ebullet(i % 2, EBULLET_FATTER_SHOT, 2, x, y, xpart(angle, 3500 + arena.difficulty * 500), ypart(angle, 3500 + arena.difficulty * 500), angle);
  if (b != -1)
   create_cloud(i % 2, CLOUD_EXPLODE, 2, x, y, ebullet[i % 2][b].x_speed, ebullet[i % 2][b].y_speed, 14 + grand(5));

 }

/*create_cloud(w, CLOUD_SHOCKWAVE, col, enemy[w][e].x - 29000 - xb, enemy[w][e].y + 40000 + ya + yb, enemy[w][e].x_speed, enemy[w][e].y_speed, 20);
 create_cloud(w, CLOUD_SHOCKWAVE, col, enemy[w][e].x + 29000 + xb, enemy[w][e].y + 40000 + ya + yb, enemy[w][e].x_speed, enemy[w][e].y_speed, 20);
 create_cloud(w, CLOUD_SHOCKWAVE, col, enemy[w][e].x - 48000 - xa, enemy[w][e].y - 40000 - ya - yb, enemy[w][e].x_speed, enemy[w][e].y_speed, 20);
 create_cloud(w, CLOUD_SHOCKWAVE, col, enemy[w][e].x + 48000 + xa, enemy[w][e].y - 40000 - ya - yb, enemy[w][e].x_speed, enemy[w][e].y_speed, 20);


      create_ebullet(w, EBULLET_SHOT, 0, enemy[w][e].x, enemy[w][e].y, xpart(angle, 4000), ypart(angle, 4000), angle);

*/

}


void change_boss1_arms(int col)
{

 boss.arm_colour = col;

/* int xa = boss.arm1 * (1000 / 16);
 int xb = boss.arm1 * (1000 / 8);
 int ya = boss.arm2 * (1000 / 8);
 int yb = xpart(boss.pulse, 4000);

 int e = boss.e1;
 int w = 0;*/
/*create_cloud(w, CLOUD_SHOCKWAVE, col, enemy[w][e].x - 29000 - xb, enemy[w][e].y + 40000 + ya + yb, enemy[w][e].x_speed, enemy[w][e].y_speed, 20);
 create_cloud(w, CLOUD_SHOCKWAVE, col, enemy[w][e].x + 29000 + xb, enemy[w][e].y + 40000 + ya + yb, enemy[w][e].x_speed, enemy[w][e].y_speed, 20);
 create_cloud(w, CLOUD_SHOCKWAVE, col, enemy[w][e].x - 48000 - xa, enemy[w][e].y - 40000 - ya - yb, enemy[w][e].x_speed, enemy[w][e].y_speed, 20);
 create_cloud(w, CLOUD_SHOCKWAVE, col, enemy[w][e].x + 48000 + xa, enemy[w][e].y - 40000 - ya - yb, enemy[w][e].x_speed, enemy[w][e].y_speed, 20);

 w = 1;
 create_cloud(w, CLOUD_SHOCKWAVE, col, enemy[w][e].x - 29000 - xb, enemy[w][e].y + 40000 + ya + yb, enemy[w][e].x_speed, enemy[w][e].y_speed, 20);
 create_cloud(w, CLOUD_SHOCKWAVE, col, enemy[w][e].x + 29000 + xb, enemy[w][e].y + 40000 + ya + yb, enemy[w][e].x_speed, enemy[w][e].y_speed, 20);
 create_cloud(w, CLOUD_SHOCKWAVE, col, enemy[w][e].x - 48000 - xa, enemy[w][e].y - 40000 - ya - yb, enemy[w][e].x_speed, enemy[w][e].y_speed, 20);
 create_cloud(w, CLOUD_SHOCKWAVE, col, enemy[w][e].x + 48000 + xa, enemy[w][e].y - 40000 - ya - yb, enemy[w][e].x_speed, enemy[w][e].y_speed, 20);*/
/*
 create_cloud(w, CLOUD_SHOCKWAVE, col, enemy[w][e].x - 29000 - xb, enemy[w][e].y + 40000 + ya + yb, 0, 0, 20);
 create_cloud(w, CLOUD_SHOCKWAVE, col, enemy[w][e].x + 29000 + xb, enemy[w][e].y + 40000 + ya + yb, 0, 0, 20);
 create_cloud(w, CLOUD_SHOCKWAVE, col, enemy[w][e].x - 48000 - xa, enemy[w][e].y - 40000 - ya - yb, 0, 0, 20);
 create_cloud(w, CLOUD_SHOCKWAVE, col, enemy[w][e].x + 48000 + xa, enemy[w][e].y - 40000 - ya - yb, 0, 0, 20);

 w = 1;
 create_cloud(w, CLOUD_SHOCKWAVE, col, enemy[w][e].x - 29000 - xb, enemy[w][e].y + 40000 + ya + yb, 0, 0, 20);
 create_cloud(w, CLOUD_SHOCKWAVE, col, enemy[w][e].x + 29000 + xb, enemy[w][e].y + 40000 + ya + yb, 0, 0, 20);
 create_cloud(w, CLOUD_SHOCKWAVE, col, enemy[w][e].x - 48000 - xa, enemy[w][e].y - 40000 - ya - yb, 0, 0, 20);
 create_cloud(w, CLOUD_SHOCKWAVE, col, enemy[w][e].x + 48000 + xa, enemy[w][e].y - 40000 - ya - yb, 0, 0, 20);
*/
}

void boss1_new_pattern(void)
{
 if (boss.arm_beam > 0)
  boss.arm_beam = 0;

  if (boss.moving != BOSS1_BEAM2 && boss.hp <= boss.max_hp / 2)
  {
    boss.moving = BOSS1_BEAM1;
    boss.move_time = 150 + grand(100);
    //set_boss1_firing(BOSS1_FIRE_SCATTER);//grand(3) + 1);
    return;
  }



 if ((boss.moving == BOSS1_SWIRL && grand(2) == 0)
  || (boss.moving == BOSS1_SWIRL2 && grand(2) == 0)
  || (boss.moving == BOSS1_BEAM2 && grand(2) == 0))
 {
     boss.moving = BOSS1_SIDES;
     boss.move_x = pos_or_neg(1);
     boss.move_time = 3 + grand(3);
     return;
 }

// if (boss.moving == BOSS1_SIDES)
 {
     if (boss.moving == BOSS1_SWIRL2)
      boss.moving = BOSS1_SWIRL;
       else
       {
        if (boss.moving == BOSS1_SWIRL)
         boss.moving = BOSS1_SWIRL2;
          else
          {
            boss.moving = BOSS1_SWIRL;
            if (grand(2) == 0)
             boss.moving = BOSS1_SWIRL2;
          }
       }
    boss.move_time = 500 + grand(500);
    return;
 }


}

void new_boss1_firing(void)
{
 int new_pattern = grand(4) + 1;

 while(new_pattern == boss.bpattern)
 {
     new_pattern = grand(4) + 1;
 }

 set_boss1_firing(new_pattern);

}

void set_boss1_firing(int pattern)
{

    switch(pattern)
    {
     case BOSS1_FIRE_CIRCLES:
    boss.bpattern = BOSS1_FIRE_CIRCLES;
    boss.brecycle = 50;
    boss.bstatus1 = 0;
    boss.bstatus2 = 0;

      break;
     case BOSS1_FIRE_SCATTER:
    boss.bpattern = BOSS1_FIRE_SCATTER;
    boss.brecycle = 50;
    boss.bstatus1 = 0;
    boss.bstatus2 = 0;
      break;
     case BOSS1_FIRE_SPIRAL:
    boss.bpattern = BOSS1_FIRE_SPIRAL;
    boss.brecycle = 50;
    boss.bstatus1 = 0;
    boss.bstatus2 = 0;
      break;
     case BOSS1_FIRE_BURST:
    boss.bpattern = BOSS1_FIRE_BURST;
    boss.brecycle = 50;
    boss.bstatus1 = 0;
    boss.bstatus2 = 10;
      break;

    }

}

void boss1_fire_spiral(int w, int e, int col)
{

 int i = boss.bstatus2;
 int b;
 int angle = enemy[w][e].angle1 + i * ANGLE_8;
w = 0;
e = boss.e1;
         play_effectwfvx(w, NWAV_POP, 1500 + grand(50), 120, enemy[w][e].x);

     b = create_ebullet(w, EBULLET_SPIN, col, enemy[w][e].x + xpart(angle, 9000), enemy[w] [e].y + ypart(angle, 9000) + BOSS1_Y_MOD, xpart(angle, 2500), ypart(angle, 2500), angle);
     if (b != -1)
     {
       create_cloud(w, CLOUD_EXPLODE, col, ebullet[w][b].x, ebullet[w][b].y + BOSS1_Y_MOD, ebullet[w][b].x_speed, ebullet[w][b].y_speed, 14 + grand(5));
       ebullet[w][b].turn_direction = 6;
     }

    i = 7 - boss.bstatus2;
     w = 1;
     e = boss.e2;
     angle = ANGLE_1 - enemy[w][e].angle1;
     angle += i * ANGLE_8;
     b = create_ebullet(w, EBULLET_SPIN, col, enemy[w][e].x + xpart(angle, 9000), enemy[w] [e].y + ypart(angle, 9000) + BOSS1_Y_MOD, xpart(angle, 2500), ypart(angle, 2500), angle);
     if (b != -1)
     {
       create_cloud(w, CLOUD_EXPLODE, col, ebullet[w][b].x, ebullet[w][b].y + BOSS1_Y_MOD, ebullet[w][b].x_speed, ebullet[w][b].y_speed, 14 + grand(5));
       ebullet[w][b].turn_direction = -6;
     }

 boss.bstatus2 --;
 if (boss.bstatus2 == -1)
  boss.bstatus2 = 7;

}



void boss1_fire_scatter(int w, int e, int col)
{

 int i = grand(8);
 int b;
         play_effectwfvx(w, NWAV_POP, 1250 + grand(50), 120, enemy[w][e].x);

     b = create_ebullet(w, EBULLET_SPIN, col, enemy[w][e].x + xpart(enemy[w] [e].angle1 + i * ANGLE_8, 9000), enemy[w] [e].y + ypart(enemy[w] [e].angle1 + i * ANGLE_8, 9000) + BOSS1_Y_MOD, xpart(enemy[w] [e].angle1 + i * ANGLE_8, 3000), ypart(enemy[w] [e].angle1 + i * ANGLE_8, 3000), enemy[w] [e].angle1 + i * ANGLE_8);
     if (b != -1)
     {
       create_cloud(w, CLOUD_EXPLODE, col, ebullet[w][b].x, ebullet[w][b].y, ebullet[w][b].x_speed, ebullet[w][b].y_speed, 14 + grand(5));
       if (w == 0)
        ebullet[w][b].turn_direction = 6 + grand(10);
         else
          ebullet[w][b].turn_direction = -6 - grand(10);
     }


}

void boss1_fire_burst(int w, int e, int col)
{

 int angle = enemy[w][e].angle1;
 int b;
 int i;
         play_effectwfvx(w, NWAV_POP, 1000 + grand(50), 120, enemy[w][e].x);


 for (i = 0; i < 3; i ++)
 {

     b = create_ebullet(w, EBULLET_SPIN, col, enemy[w][e].x + xpart(angle, 9000), enemy[w] [e].y + ypart(angle, 9000) + BOSS1_Y_MOD, xpart(angle, 3000), ypart(angle, 3000), angle);
     if (b != -1)
     {
       create_cloud(w, CLOUD_EXPLODE, col, ebullet[w][b].x, ebullet[w][b].y, ebullet[w][b].x_speed, ebullet[w][b].y_speed, 14 + grand(5));
       if (w == 0)
        ebullet[w][b].turn_direction = 6 + boss.bstatus2 * 2;
         else
          ebullet[w][b].turn_direction = -6 - boss.bstatus2 * 2;
     }
    angle += ANGLE_1 / 3;
 }
/*    angle += ANGLE_4;
     b = create_ebullet(w, EBULLET_SPIN, col, enemy[w][e].x + xpart(angle, 9000), enemy[w] [e].y + ypart(angle, 9000) + BOSS1_Y_MOD, xpart(angle, 3000), ypart(angle, 3000), angle);
     if (b != -1)
     {
       create_cloud(w, CLOUD_EXPLODE, col, ebullet[w][b].x, ebullet[w][b].y, ebullet[w][b].x_speed, ebullet[w][b].y_speed, 14 + grand(5));
       if (w == 0)
        ebullet[w][b].turn_direction = 6 + boss.bstatus2 * 2;
         else
          ebullet[w][b].turn_direction = -6 - boss.bstatus2 * 2;
     }*/

}


// this was supposed to be a circle, but I made a mistake in the loop and I like it better this way.
void boss1_fire_circle(int w, int e, int col)
{

 int i;
 int b;

 play_effectwfvx(w, NWAV_SHOT, 1400 + grand(50), 120, enemy[w][e].x);

 for (i = 0; i < 6; i ++)
 {
     b = create_ebullet(w, EBULLET_FAT_SHOT, col, enemy[w][e].x + xpart(enemy[w] [e].angle1 + i * ANGLE_8, 9000), enemy[w] [e].y + ypart(enemy[w] [e].angle1 + i * ANGLE_8, 9000) + BOSS1_Y_MOD, xpart(enemy[w] [e].angle1 + i * ANGLE_8, 3000), ypart(enemy[w] [e].angle1 + i * ANGLE_8, 3000), enemy[w] [e].angle1 + i * ANGLE_8);
     if (b != -1)
     {
       create_cloud(w, CLOUD_EXPLODE, col, ebullet[w][b].x, ebullet[w][b].y, ebullet[w][b].x_speed, ebullet[w][b].y_speed, 14 + grand(5));
     }
 }

}

void kill_boss1(void)
{
    if (boss.exploding > 0)
     return;

    int e = boss.e1;

     play_effectwfvx(0, NWAV_BIGBANG, 1600 + grand(350), 120, enemy[0][e].x);
     play_effectwfvx(1, NWAV_BIGBANG, 500 + grand(150), 120, enemy[1][e].x);

   create_cloud(0, CLOUD_LARGE_SHOCKWAVE, 0, enemy[0] [e].x, enemy[0] [e].y + 13000, 0, 0, 50);
   create_cloud(0, CLOUD_EXPLODE, 0, enemy[0] [e].x, enemy[0] [e].y + 13000, 0, 0, 30 + grand(10));
   create_cloud(1, CLOUD_LARGE_SHOCKWAVE, 0, enemy[0] [e].x, enemy[0] [e].y + 13000, 0, 0, 50);
   create_cloud(1, CLOUD_EXPLODE, 0, enemy[0] [e].x, enemy[0] [e].y + 13000, 0, 0, 30 + grand(10));

 int i, j, angle, speed, distance = 15000, angle2;

 for (i = 0; i < 4; i ++)
 {
  angle += ANGLE_8 + grand(ANGLE_4);
  for (j = 0; j < 5; j ++)
  {
   speed = 2000 + grand(3500) + grand(3000);
   angle2 = angle + grand(ANGLE_16);
   create_cloud(0, CLOUD_DRAG_EXPLODE, 0, enemy[0] [e].x + xpart(angle2, distance), enemy[0] [e].y + ypart(angle2, distance) + 13000, xpart(angle2, speed), ypart(angle2, speed), 40 + grand(20));
   create_cloud(1, CLOUD_DRAG_EXPLODE, 0, enemy[0] [e].x + xpart(angle2, distance), enemy[0] [e].y + ypart(angle2, distance) + 13000, xpart(angle2, speed), ypart(angle2, speed), 40 + grand(20));
  }
 }

 boss.exploding = 80;

 if (enemy[0][e].x < 160000)
  enemy[0][e].x_speed += 4000;
   else
    enemy[0][e].x_speed -= 4000;

 if (enemy[0][e].y < 160000)
  enemy[0][e].y_speed += 4000;
   else
    enemy[0][e].y_speed -= 4000;

}

void boss1_exploding(void)
{
    boss.exploding --;
    drag_enemy(0, boss.e1, 960);
    int w = 0;
    int e = boss.e1;
    enemy[0][e].x += enemy[0][e].x_speed;
    enemy[0][e].y += enemy[0][e].y_speed;

    if (boss.exploding <= 10)
    {
        boss.exploding = 10;
     create_cloud(0, CLOUD_LARGE_SHOCKWAVE, 0, enemy[w] [e].x, enemy[w] [e].y + 13000, 0, 0, 50);
     create_cloud(0, CLOUD_EXPLODE, 0, enemy[w] [e].x, enemy[w] [e].y + 13000, 0, 0, 40 + grand(20));
     create_cloud(0, CLOUD_EXPLODE, 0, enemy[w] [e].x + 35000 + grand(10000), enemy[w] [e].y + 10000 - grand(10000), 0, 0, 40 + grand(20));
     create_cloud(0, CLOUD_EXPLODE, 0, enemy[w] [e].x - 35000 - grand(10000), enemy[w] [e].y + 10000 - grand(10000), 0, 0, 40 + grand(20));
     create_cloud(1, CLOUD_LARGE_SHOCKWAVE, 0, enemy[w] [e].x, enemy[w] [e].y + 13000, 0, 0, 50);
     create_cloud(1, CLOUD_EXPLODE, 0, enemy[w] [e].x, enemy[w] [e].y + 13000, 0, 0, 40 + grand(20));
     create_cloud(1, CLOUD_EXPLODE, 0, enemy[w] [e].x + 35000 + grand(10000), enemy[w] [e].y + 10000 - grand(10000), 0, 0, 40 + grand(20));
     create_cloud(1, CLOUD_EXPLODE, 0, enemy[w] [e].x - 35000 - grand(10000), enemy[w] [e].y + 10000 - grand(10000), 0, 0, 40 + grand(20));
     create_cloud(0, CLOUD_SPAWNER, 0, enemy[w][e].x, enemy[w][e].y, 0, 0, 60);
     create_cloud(1, CLOUD_SPAWNER, 0, enemy[w][e].x, enemy[w][e].y, 0, 0, 60);
  create_cloud(0, CLOUD_LARGE_SPRAY, 0, enemy[w][e].x + 9000, enemy[w][e].y - 9000, xpart(-ANGLE_8, 6000), ypart(-ANGLE_8, 6000), 15 + grand(7));
  create_cloud(0, CLOUD_LARGE_SPRAY, 0, enemy[w][e].x + 9000, enemy[w][e].y + 9000, xpart(ANGLE_8, 6000), ypart(ANGLE_8, 6000), 15 + grand(7));
  create_cloud(0, CLOUD_LARGE_SPRAY, 0, enemy[w][e].x - 9000, enemy[w][e].y + 9000, xpart(ANGLE_4 + ANGLE_8, 6000), ypart(ANGLE_4 + ANGLE_8, 6000), 15 + grand(7));
  create_cloud(0, CLOUD_LARGE_SPRAY, 0, enemy[w][e].x - 9000, enemy[w][e].y - 9000, xpart(-ANGLE_4 - ANGLE_8, 6000), ypart(- ANGLE_4 - ANGLE_8, 6000), 15 + grand(7));
  create_cloud(1, CLOUD_LARGE_SPRAY, 0, enemy[w][e].x + 9000, enemy[w][e].y - 9000, xpart(-ANGLE_8, 6000), ypart(-ANGLE_8, 6000), 15 + grand(7));
  create_cloud(1, CLOUD_LARGE_SPRAY, 0, enemy[w][e].x + 9000, enemy[w][e].y + 9000, xpart(ANGLE_8, 6000), ypart(ANGLE_8, 6000), 15 + grand(7));
  create_cloud(1, CLOUD_LARGE_SPRAY, 0, enemy[w][e].x - 9000, enemy[w][e].y + 9000, xpart(ANGLE_4 + ANGLE_8, 6000), ypart(ANGLE_4 + ANGLE_8, 6000), 15 + grand(7));
  create_cloud(1, CLOUD_LARGE_SPRAY, 0, enemy[w][e].x - 9000, enemy[w][e].y - 9000, xpart(-ANGLE_4 - ANGLE_8, 6000), ypart(- ANGLE_4 - ANGLE_8, 6000), 15 + grand(7));

     play_effectwfvx(0, NWAV_BIGBANG, 1000 + grand(350), 120, enemy[0][e].x);
     play_effectwfvx(1, NWAV_BIGBANG, 200 + grand(150), 120, enemy[1][e].x);

     player_score(eclass[enemy[w][e].type].score);

     destroy_enemy(0, boss.e1);
     destroy_enemy(1, boss.e2);
//     boss.fight = 0;
     arena.level_finished = 250;
//     boss.exploding = 0;
     return;
    }

  create_cloud(0, CLOUD_EXPLODE, 0, enemy[w] [e].x - grand(80000) + grand(80000), enemy[w] [e].y - grand(80000) + grand(80000), 0, 0, 10 + grand(10));
  create_cloud(1, CLOUD_EXPLODE, 0, enemy[w] [e].x - grand(80000) + grand(80000), enemy[w] [e].y - grand(80000) + grand(80000), 0, 0, 10 + grand(10));

}

void kill_mb1(void)
{
    int e = boss.e1;
    int w = boss.side1;
    int i;

  play_effectwfvx(w, NWAV_BIGBANG, 1000 + grand(350), 120, enemy[w][e].x);
  play_effectwfvx(w, NWAV_BIGBANG, 400 + grand(150), 120, enemy[w][e].x);
 create_cloud(w, CLOUD_LARGE_SHOCKWAVE, 2, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 50);
 create_cloud(w, CLOUD_LARGE_SHOCKWAVE, 0, enemy[w] [e].x - 39000, enemy[w] [e].y + 20000, 0, 0, 40);
 create_cloud(w, CLOUD_LARGE_SHOCKWAVE, 0, enemy[w] [e].x + 39000, enemy[w] [e].y + 20000, 0, 0, 40);
 for (i = 0; i < 4; i ++)
 {
  create_cloud(w, CLOUD_EXPLODE, 0, enemy[w] [e].x + xpart(i * ANGLE_4 + ANGLE_8, 9000), enemy[w] [e].y + ypart(i * ANGLE_4 + ANGLE_8, 9000), xpart(i * ANGLE_4 + ANGLE_8, 3000), ypart(i * ANGLE_4 + ANGLE_8, 3000), 36);
  create_cloud(w, CLOUD_EXPLODE, 2, enemy[w] [e].x + xpart(i * ANGLE_4, 9000), enemy[w] [e].y + ypart(i * ANGLE_4, 9000), xpart(i * ANGLE_4, 1500), ypart(i * ANGLE_4, 1500), 36);
 }
 for (i = 0; i < 8; i ++)
 {
  create_cloud(w, CLOUD_EXPLODE, 1, enemy[w] [e].x + xpart(i * ANGLE_8 + ANGLE_16, 9000), enemy[w] [e].y + ypart(i * ANGLE_8 + ANGLE_16, 9000), xpart(i * ANGLE_8 + ANGLE_16, 2300), ypart(i * ANGLE_8 + ANGLE_16, 2300), 30);
 }
 create_cloud(w, CLOUD_EXPLODE, 0, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 40);

 create_cloud(w, CLOUD_EXPLODE, 0, enemy[w] [e].x - 30000, enemy[w] [e].y + 20000, 0, 0, 40 + grand(10));
 create_cloud(w, CLOUD_EXPLODE, 0, enemy[w] [e].x - 40000, enemy[w] [e].y + 25000, 0, 0, 40 + grand(10));
 create_cloud(w, CLOUD_EXPLODE, 0, enemy[w] [e].x - 50000, enemy[w] [e].y + 30000, 0, 0, 40 + grand(10));
 create_cloud(w, CLOUD_EXPLODE, 0, enemy[w] [e].x - 60000, enemy[w] [e].y + 35000, 0, 0, 40 + grand(10));

 create_cloud(w, CLOUD_EXPLODE, 0, enemy[w] [e].x + 30000, enemy[w] [e].y + 20000, 0, 0, 40 + grand(10));
 create_cloud(w, CLOUD_EXPLODE, 0, enemy[w] [e].x + 40000, enemy[w] [e].y + 25000, 0, 0, 40 + grand(10));
 create_cloud(w, CLOUD_EXPLODE, 0, enemy[w] [e].x + 50000, enemy[w] [e].y + 30000, 0, 0, 40 + grand(10));
 create_cloud(w, CLOUD_EXPLODE, 0, enemy[w] [e].x + 60000, enemy[w] [e].y + 35000, 0, 0, 40 + grand(10));


     player_score(eclass[enemy[w][e].type].score);
     destroy_enemy(boss.side1, boss.e1);
//     destroy_enemy(1, boss.e2);
//     boss.fight = 0;
     boss.fight = 0;
     return;

}



void kill_mb2(void)
{
    int e = boss.e1;
    int w = boss.side1;
    int i;

  play_effectwfx(w, NWAV_BLIP2, 400, enemy[w][e].x);

 create_cloud(w, CLOUD_LARGE_SHOCKWAVE2, 2, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 50);
 for (i = 0; i < 4; i ++)
 {
  create_cloud(w, CLOUD_EXPLODE2, 0, enemy[w] [e].x + xpart(i * ANGLE_4 + ANGLE_8, 9000), enemy[w] [e].y + ypart(i * ANGLE_4 + ANGLE_8, 9000), xpart(i * ANGLE_4 + ANGLE_8, 3000), ypart(i * ANGLE_4 + ANGLE_8, 3000), 36);
  create_cloud(w, CLOUD_EXPLODE2, 2, enemy[w] [e].x + xpart(i * ANGLE_4, 9000), enemy[w] [e].y + ypart(i * ANGLE_4, 9000), xpart(i * ANGLE_4, 1500), ypart(i * ANGLE_4, 1500), 36);
 }
 for (i = 0; i < 8; i ++)
 {
  create_cloud(w, CLOUD_EXPLODE2, 1, enemy[w] [e].x + xpart(i * ANGLE_8 + ANGLE_16, 9000), enemy[w] [e].y + ypart(i * ANGLE_8 + ANGLE_16, 9000), xpart(i * ANGLE_8 + ANGLE_16, 2300), ypart(i * ANGLE_8 + ANGLE_16, 2300), 30);
 }
 create_cloud(w, CLOUD_EXPLODE2, 0, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 40);


     player_score(eclass[enemy[w][e].type].score);
     destroy_enemy(boss.side1, boss.e1);
//     destroy_enemy(1, boss.e2);
//     boss.fight = 0;
     boss.fight = 0;
     return;

}


void kill_boss2(void)
{
    int e = boss.e1;
    int w = boss.side1;
    int i;

 play_effectwfx(w, NWAV_BLIP2, 330, enemy[w][e].x);

 create_cloud(w, CLOUD_LARGE_SHOCKWAVE2, 2, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 50);
 for (i = 0; i < 4; i ++)
 {
  create_cloud(w, CLOUD_EXPLODE2, 0, enemy[w] [e].x + xpart(i * ANGLE_4 + ANGLE_8, 9000), enemy[w] [e].y + ypart(i * ANGLE_4 + ANGLE_8, 9000), xpart(i * ANGLE_4 + ANGLE_8, 3000), ypart(i * ANGLE_4 + ANGLE_8, 3000), 36);
  create_cloud(w, CLOUD_EXPLODE2, 2, enemy[w] [e].x + xpart(i * ANGLE_4, 9000), enemy[w] [e].y + ypart(i * ANGLE_4, 9000), xpart(i * ANGLE_4, 1500), ypart(i * ANGLE_4, 1500), 36);
 }
 for (i = 0; i < 8; i ++)
 {
  create_cloud(w, CLOUD_EXPLODE2, 1, enemy[w] [e].x + xpart(i * ANGLE_8 + ANGLE_16, 9000), enemy[w] [e].y + ypart(i * ANGLE_8 + ANGLE_16, 9000), xpart(i * ANGLE_8 + ANGLE_16, 2300), ypart(i * ANGLE_8 + ANGLE_16, 2300), 30);
 }
 create_cloud(w, CLOUD_EXPLODE2, 0, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 40);


//     destroy_enemy(1, boss.e2);
     player_score(eclass[enemy[w][e].type].score);
     destroy_enemy(boss.side1, boss.e1);
     arena.level_finished = 250;
//     boss.fight = 0;
     boss.fight = 0;
     return;

}



void move_drifter(int w, int e)
{
     enemy[w] [e].x_speed = enemy[w] [e].x2;
     enemy[w] [e].y_speed = enemy[w] [e].y2;

     move_enemy(w, e);
}

void move_marcher(int w, int e)
{

     switch(enemy[w][e].type)
     {
      case ENEMY_BASIC3:
       enemy[w][e].recycle --;
       if (enemy[w][e].recycle == 0)
       {
        create_cloud(w, CLOUD_EXPLODE, 2, enemy[w][e].x, enemy[w][e].y, enemy[w][e].x2, enemy[w][e].y2, 14 + grand(5));
        enemy_fire(w, e);
        enemy[w][e].recycle = 100 - arena.difficulty * 15;
       }
       break;

     }

     enemy[w] [e].x_speed = enemy[w] [e].x2;
     enemy[w] [e].y_speed = enemy[w] [e].y2;

     move_enemy(w, e);
}

void move_dancer(int w, int e)
{

 enemy[w][e].recycle --;
 if (enemy[w][e].recycle == 0 && enemy[w][e].type != ENEMY_WINGS)
 {
  if (enemy[w][e].y < 480000)
  {
   create_cloud(w, CLOUD_EXPLODE, 0, enemy[w][e].x, enemy[w][e].y, enemy[w][e].x_speed, enemy[w][e].y_speed, 14 + grand(5));
   enemy_fire(w, e);
  }
  enemy[w][e].recycle = 5000;
 }


     switch(enemy[w][e].type)
     {
      case ENEMY_CROSS:
       enemy[w][e].angle3 = enemy[w][e].angle2;
       if (enemy[w][e].a1 == 0)
       {
        enemy[w][e].b1 -= 15;
        if (enemy[w][e].b1 < -40)
         enemy[w][e].b1 = -40;
        enemy[w][e].angle2 += enemy[w][e].b1;
        if (enemy[w][e].angle2 <= ANGLE_8 - ANGLE_4)
         enemy[w][e].a1 = 1;
       } else
       {
        enemy[w][e].b1 += 15;
        if (enemy[w][e].b1 > 90)
         enemy[w][e].b1 = 90;
        enemy[w][e].angle2 += enemy[w][e].b1;
        if (enemy[w][e].angle2 >= ANGLE_2 - ANGLE_4 - ANGLE_4 )
         enemy[w][e].a1 = 0;
       }
       break;
      case ENEMY_CROSS2:
       enemy[w][e].angle3 = enemy[w][e].angle2;
       if (enemy[w][e].a1 == 0)
       {
        enemy[w][e].b1 -= 35;
        if (enemy[w][e].b1 < -60)
         enemy[w][e].b1 = -60;
        enemy[w][e].angle2 += enemy[w][e].b1;
        if (enemy[w][e].angle2 <= ANGLE_8 - ANGLE_4)
         enemy[w][e].a1 = 1;
       } else
       {
        enemy[w][e].b1 += 35;
        if (enemy[w][e].b1 > 150)
         enemy[w][e].b1 = 150;
        enemy[w][e].angle2 += enemy[w][e].b1;
        if (enemy[w][e].angle2 >= ANGLE_2 - ANGLE_4 - ANGLE_4 )
         enemy[w][e].a1 = 0;
       }
       break;
      case ENEMY_WINGS:
       if (enemy[w][e].recycle == 0)
       {
        int angle = attack_angle(w, e);
        if (enemy[w][e].y < 480000)
        {
         shoot_triangle(w, enemy[w][e].x, enemy[w][e].y, angle, 1);
         shoot_triangle(w, enemy[w][e].x, enemy[w][e].y, angle - ANGLE_32, 1);
         shoot_triangle(w, enemy[w][e].x, enemy[w][e].y, angle + ANGLE_32, 1);
        }
        play_effectwfvx(w, NWAV_TAP, 1400, 120, enemy[w][e].x);
        enemy[w][e].recycle = 50 - arena.difficulty * 7;
       }
       enemy[w][e].d1 = enemy[w][e].c1;
       enemy[w][e].c1 = enemy[w][e].angle3;
       enemy[w][e].angle3 = enemy[w][e].angle2;
       if (enemy[w][e].a1 == 0)
       {
        enemy[w][e].b1 -= 15;
        if (enemy[w][e].b1 < -40)
         enemy[w][e].b1 = -40;
        enemy[w][e].angle2 += enemy[w][e].b1;
        if (enemy[w][e].angle2 <= ANGLE_8 - ANGLE_4)
         enemy[w][e].a1 = 1;
       } else
       {
        enemy[w][e].b1 += 15;
        if (enemy[w][e].b1 > 90)
         enemy[w][e].b1 = 90;
        enemy[w][e].angle2 += enemy[w][e].b1;
        if (enemy[w][e].angle2 >= ANGLE_2 - ANGLE_4 - ANGLE_4 )
         enemy[w][e].a1 = 0;
       }
/*       if (enemy[w][e].a1 == 0)
       {
        enemy[w][e].b1 -= 15;
        if (enemy[w][e].b1 < -20)
         enemy[w][e].b1 = -20;
        enemy[w][e].angle2 += enemy[w][e].b1;
        if (enemy[w][e].angle2 <= ANGLE_8 - ANGLE_4 - ANGLE_8)
         enemy[w][e].a1 = 1;
       } else
       {
        enemy[w][e].b1 += 15;
        if (enemy[w][e].b1 > 60)
         enemy[w][e].b1 = 60;
        enemy[w][e].angle2 += enemy[w][e].b1;
        if (enemy[w][e].angle2 >= ANGLE_2 - ANGLE_4 - ANGLE_4 + ANGLE_16)
         enemy[w][e].a1 = 0;
       }*/
       break;
     }

 if (enemy[w] [e].target_time > 0 && enemy[w] [e].x_target [enemy[w] [e].target] != -1)
  enemy[w] [e].target_time --;
   else
   {
    if (enemy[w] [e].x_target [enemy[w] [e].target] != -1)
    {
     enemy[w] [e].target_time = enemy[w] [e].max_target_time;
     enemy[w] [e].target ++;
     enemy[w][e].recycle = 30;
    }
   }


    if (enemy[w] [e].x / 15000 == enemy[w] [e].x_target [enemy[w] [e].target] / 15000
      && enemy[w] [e].y / 15000 == enemy[w] [e].y_target [enemy[w] [e].target] / 15000)
    {
     if (enemy[w] [e].x_target [enemy[w] [e].target] != -1)
     {
      enemy[w] [e].target_time = enemy[w] [e].max_target_time;
      enemy[w] [e].target ++;
      enemy[w][e].recycle = 30;
     }
    }

//if (enemy[w] [e].x_target [enemy[w] [e].target] == -1) exit(1);

    if (enemy[w] [e].x_target [enemy[w] [e].target] != -1 && enemy[w] [e].y < 420000)
    {
//      enemy[w] [e].angle1 = turn_towards_xy(enemy[w] [e].x, enemy[w] [e].y,
//          enemy[w] [e].x_target [enemy[w] [e].target], enemy[w] [e].y_target [enemy[w] [e].target],
//          enemy[w] [e].angle1, 8);
   track_target(w, e, enemy[w] [e].x_target [enemy[w] [e].target], enemy[w] [e].y_target [enemy[w] [e].target], enemy[w] [e].angle1, eclass[enemy[w] [e].type].speed2);

   enemy[w] [e].angle1 %= ANGLE_1;
//      radians_to_angle(atan2((actor[enemy[w] [e].attacking].y - enemy[w] [e].y), (actor[enemy[w] [e].attacking].x - enemy[w] [e].x)));
    }
    enemy[w] [e].x_speed = xpart (enemy[w] [e].angle1, eclass[enemy[w] [e].type].speed1);
    enemy[w] [e].y_speed = ypart (enemy[w] [e].angle1, eclass[enemy[w] [e].type].speed1);

    move_enemy(w,e);


}

void move_diver(int w, int e)
{
/*
 if (enemy[w] [e].target == 0)
 {
  if (enemy[w] [e].target_time > 0)
  {
   enemy[w] [e].target_time --;
   enemy[w] [e].y_speed = eclass[enemy[w] [e].type].speed3;
   enemy[w] [e].y += enemy[w] [e].y_speed;
   return;
  }
  enemy[w] [e].target = 1;
  enemy[w] [e].target_time = 50;
//  enemy[w] [e].y_speed = 2000;
  enemy[w] [e].x_speed = 0;
  return;
 }

 if (enemy[w] [e].target == 1)
 {
   drag_enemy(e, 950);
   move_enemy(e);
//   enemy[w] [e].angle1 = turn_towards_xy(enemy[w] [e].x, enemy[w] [e].y,
//       player[0].x, player[0].y,
//       enemy[w] [e].angle1, 8);
   track_target(e, player[0].x, player[0].y, enemy[w] [e].angle1, eclass[enemy[w] [e].type].speed2);
   enemy[w] [e].target_time --;
   if (enemy[w] [e].target_time <= 0)
   {
    enemy[w] [e].target = 2;
    // thrust explosion!
    enemy[w] [e].y_speed = 0;
    enemy[w] [e].x_speed = 0;
    enemy[w] [e].recycle = 15;
    return;

   }
   return;
 }

 if (enemy[w] [e].recycle > 0)
  enemy[w] [e].recycle --;
   else
   {
    enemy[w] [e].recycle = 15;
    create_ebullet(EBULLET_SHOT, enemy[w] [e].pole, enemy[w] [e].x + xpart(enemy[w] [e].angle1 + ANGLE_4, 4000), enemy[w] [e].y + ypart(enemy[w] [e].angle1 + ANGLE_4, 4000), xpart(enemy[w] [e].angle1 + ANGLE_4, 4000), ypart(enemy[w] [e].angle1 + ANGLE_4, 4000), enemy[w] [e].angle1 + ANGLE_4);
    create_ebullet(EBULLET_SHOT, enemy[w] [e].pole, enemy[w] [e].x + xpart(enemy[w] [e].angle1 - ANGLE_4, 4000), enemy[w] [e].y + xpart(enemy[w] [e].angle1 - ANGLE_4, 4000), xpart(enemy[w] [e].angle1 - ANGLE_4, 4000), ypart(enemy[w] [e].angle1 - ANGLE_4, 4000), enemy[w] [e].angle1 - ANGLE_4);
   }

 enemy[w] [e].x_speed += xpart(enemy[w] [e].angle1, eclass[enemy[w] [e].type].speed1);
 enemy[w] [e].y_speed += ypart(enemy[w] [e].angle1, eclass[enemy[w] [e].type].speed1);
 drag_enemy(e, 950);
    move_enemy(e);*/
}

void move_glider(int w, int e)
{

 int i, b, x, y, xa, xb;

 switch(enemy[w][e].type)
 {
  case ENEMY_FLOWER1:
   enemy[w][e].angle1 ++;
   if (enemy[w][e].angle1 >= 16)
    enemy[w][e].angle1 = 0;

   if (w == 0)
    enemy[w][e].angle2 += 4;
     else
      enemy[w][e].angle2 -= 4;
   enemy[w][e].angle2 &= 1023;
   enemy[w][e].recycle --;
   if (enemy[w][e].recycle <= 0)
   {
    x = enemy[w][e].x;
    y = enemy[w][e].y + 9000;
    shoot_triangle(w, x + xpart(enemy[w][e].angle2, 7000), y + ypart(enemy[w][e].angle2, 7000), enemy[w][e].angle2, 2);
    shoot_triangle(w, x + xpart(enemy[w][e].angle2 + ANGLE_3, 7000), y + ypart(enemy[w][e].angle2 + ANGLE_3, 7000), enemy[w][e].angle2 + ANGLE_3, 2);
    shoot_triangle(w, x + xpart(enemy[w][e].angle2 - ANGLE_3, 7000), y + ypart(enemy[w][e].angle2 - ANGLE_3, 7000), enemy[w][e].angle2 - ANGLE_3, 2);
    play_effectwfvx(w, NWAV_TAP, 1700, 200, enemy[w][e].x);
    enemy[w][e].recycle = 20 - arena.difficulty * 3;
/*
           play_effectwfvx(w, NWAV_ZAP, 1800 + grand(50), 30, enemy[w    b = create_ebullet(w, EBULLET_TRIANGLE, 2,
        x + xpart(enemy[w][e].angle2, 7000), y + ypart(enemy[w][e].angle2, 7000),
        0, 0, enemy[w][e].angle2);
    b = create_ebullet(w, EBULLET_TRIANGLE, 2,
        x + xpart(enemy[w][e].angle2 + ANGLE_3, 7000), y + ypart(enemy[w][e].angle2 + ANGLE_3, 7000),
        0, 0, enemy[w][e].angle2 + ANGLE_3);
    b = create_ebullet(w, EBULLET_TRIANGLE, 2,
        x + xpart(enemy[w][e].angle2 - ANGLE_3, 7000), y + ypart(enemy[w][e].angle2 - ANGLE_3, 7000),
        0, 0, enemy[w][e].angle2 - ANGLE_3);
       enemy[w][e].recycle = 20 - arena.difficulty * 3;*/
   }
   break;
  case ENEMY_FLOWER2:
   enemy[w][e].angle1 ++;
   if (enemy[w][e].angle1 >= 20)
    enemy[w][e].angle1 = 0;

   if (w == 0)
    enemy[w][e].angle2 += 4;
     else
      enemy[w][e].angle2 -= 4;
   enemy[w][e].angle2 &= 1023;
   enemy[w][e].recycle --;
   if (enemy[w][e].recycle <= 0)
   {
    x = enemy[w][e].x;
    y = enemy[w][e].y + 9000;
    xa = 1;
    if (grand(2) == 0)
     xa = -1;
    for (i = 0; i < 12; i ++)
    {
     shoot_diamond(w, x + xpart(enemy[w][e].angle2 + i * (ANGLE_1 / 12), 7000), y + ypart(enemy[w][e].angle2 + i * (ANGLE_1 / 12), 7000), xpart(enemy[w][e].angle2 + i * (ANGLE_1 / 12), 2000), ypart(enemy[w][e].angle2 + i * (ANGLE_1 / 12), 2000), enemy[w][e].angle2 + i * (ANGLE_1 / 12), 1, xa);
    }
    enemy[w][e].angle2 = grand(ANGLE_1);
    xa *= -1;
    for (i = 0; i < 8; i ++)
    {
     shoot_diamond(w, x + xpart(enemy[w][e].angle2 + i * (ANGLE_8), 7000), y + ypart(enemy[w][e].angle2 + i * (ANGLE_8), 7000), xpart(enemy[w][e].angle2 + i * (ANGLE_8), 1300), ypart(enemy[w][e].angle2 + i * (ANGLE_8), 1300), enemy[w][e].angle2 + i * (ANGLE_1 / 12), 2, xa);
    }
    enemy[w][e].angle2 = grand(ANGLE_1);
    xa *= -1;
    for (i = 0; i < 5; i ++)
    {
     shoot_diamond(w, x + xpart(enemy[w][e].angle2 + i * (ANGLE_5), 7000), y + ypart(enemy[w][e].angle2 + i * (ANGLE_8), 7000), xpart(enemy[w][e].angle2 + i * (ANGLE_5), 900), ypart(enemy[w][e].angle2 + i * (ANGLE_5), 900), enemy[w][e].angle2 + i * (ANGLE_5), 0, xa);
    }
    play_effectwfvx(w, NWAV_TONE, 2800, 200, enemy[w][e].x);
    enemy[w][e].recycle = 150 - arena.difficulty * 20;
   }
   break;

  case ENEMY_FLOWER3:
   enemy[w][e].angle1 ++;
   if (enemy[w][e].angle1 >= 20)
    enemy[w][e].angle1 = 0;
   enemy[w][e].angle2 &= 1023;
   enemy[w][e].recycle --;
   if (enemy[w][e].recycle <= 0)
   {
    x = enemy[w][e].x;
    y = enemy[w][e].y + 9000;
    xa = radians_to_angle(atan2((player.y) - enemy[w][e].y, player.x - enemy[w][e].x));
    for (i = 0; i < 4; i ++)
    {
     shoot_triangle(w, x + xpart(enemy[w][e].angle2 + ANGLE_4 * i, 30000), y + ypart(enemy[w][e].angle2 + ANGLE_4 * i, 30000), xa, 1);
    }
    enemy[w][e].recycle = 100 - arena.difficulty * 15;
    play_effectwfvx(w, NWAV_TONE, 1500, 200, enemy[w][e].x);
   }
   break;
  case ENEMY_FLOWER4:
   enemy[w][e].angle1 ++;
   if (enemy[w][e].angle1 >= 20)
    enemy[w][e].angle1 = 0;
   enemy[w][e].angle2 &= 1023;
   enemy[w][e].recycle --;
   if (enemy[w][e].recycle <= 0)
   {
    x = enemy[w][e].x;
    y = enemy[w][e].y + 16000;
//    xa = grand(ANGLE_1);
//    for (i = 0; i < 8; i ++)
    {
//     xb = xa + grand(ANGLE_32) - grand(ANGLE_32);
     xb = grand(ANGLE_1);
     xa = grand(3);
     shoot_triangle(w, x + xpart(xb, 10000), y + ypart(xb, 10000), xb, xa);
     shoot_triangle(w, x + xpart(xb + ANGLE_2, 10000), y + ypart(xb + ANGLE_2, 10000), xb + ANGLE_2, xa);
    }
//    enemy[w][e].recycle = 70 - arena.difficulty * 15;
    play_effectwfvx(w, NWAV_TAP, 1300, 100, enemy[w][e].x);
    enemy[w][e].recycle = 30 - arena.difficulty * 4;
   }
   break;
  case ENEMY_FLOWER5:
   enemy[w][e].angle1 ++;
   if (enemy[w][e].angle1 >= 32)
    enemy[w][e].angle1 = 0;
   enemy[w][e].angle2 &= 1023;
   enemy[w][e].recycle --;
   if (enemy[w][e].recycle <= 0)
   {
    x = enemy[w][e].x;
    y = enemy[w][e].y + 16000;
//    xa = grand(ANGLE_1);
//    for (i = 0; i < 8; i ++)
    {
//     xb = xa + grand(ANGLE_32) - grand(ANGLE_32);
     xb = grand(ANGLE_1);
    xa = 1;
    if (grand(2) == 0)
     xa = -1;
     shoot_star(w, x + xpart(xb, 10000), y + ypart(xb, 10000), xpart(xb, 1500), ypart(xb, 1500), xb, 0, xa);

    }
//    enemy[w][e].recycle = 70 - arena.difficulty * 15;
    enemy[w][e].recycle = 30 - arena.difficulty * 5;
    play_effectwfvx(w, NWAV_TONE, 1900, 200, enemy[w][e].x);
   }
   break;

  case ENEMY_CARRIER:
  if (enemy[w][e].y > 25000 && enemy[w][e].y < 400000 && enemy[w][e].angle3 < 60)
   enemy[w][e].angle3 += 1;
  if (enemy[w][e].y > 25000 && enemy[w][e].y < 400000 && enemy[w][e].angle2 < 40)
   enemy[w][e].angle2 += 1;
//  enemy[w][e].angle1 += 4;
  if (arena.difficulty == 0)
   enemy[w][e].angle1 += 2;
  if (arena.difficulty == 1)
   enemy[w][e].angle1 += 4;
  if (arena.difficulty == 2)
   enemy[w][e].angle1 += 8;
  if (enemy[w][e].y > 50000 && enemy[w][e].angle1 % ANGLE_4 == 0)
  {
   x = enemy[w][e].x + xpart(enemy[w][e].angle1, 9000);
   y = enemy[w][e].y + ypart(enemy[w][e].angle1, 9000);
   create_cloud(w, CLOUD_EXPLODE, 2, x, y, enemy[w][e].x_speed, enemy[w][e].y_speed, 12 + grand(4));
      for (i = -1; i < 2; i ++)
      {
       b = create_ebullet(w, EBULLET_SPIN, 2,
               x, y,
               xpart(enemy[w] [e].angle1 + ANGLE_16 * i, 2500) + enemy[w] [e].x_speed, ypart(enemy[w] [e].angle1 + ANGLE_16 * i, 2500) + enemy[w] [e].y_speed, 0);
       if (b == -1) continue;
       ebullet[w][b].turn_direction = enemy[w][e].turn_direction;
      }
      x = enemy[w][e].x - xpart(enemy[w][e].angle1, 9000);
      y = enemy[w][e].y - ypart(enemy[w][e].angle1, 9000);
      create_cloud(w, CLOUD_EXPLODE, 2, x, y, enemy[w][e].x_speed, enemy[w][e].y_speed, 12 + grand(4));
      for (i = -1; i < 2; i ++)
      {
       b = create_ebullet(w, EBULLET_SPIN, 2,
               x, y,
               xpart(enemy[w] [e].angle1 + ANGLE_2 + ANGLE_16 * i, 2500) + enemy[w] [e].x_speed, ypart(enemy[w] [e].angle1 + ANGLE_2 + ANGLE_16 * i, 2500) + enemy[w] [e].y_speed, 0);
       if (b == -1) continue;
       ebullet[w][b].turn_direction = enemy[w][e].turn_direction * -1;
      }
//      play_effectwfvx(w, NWAV_ZAP, 600 + grand(50), 30, enemy[w][e].x);
//      play_effectwfvx(w, NWAV_SHOT, 1000 + grand(150), 150, enemy[w][e].x);
    play_effectwfvx(w, NWAV_POP, 500, 200, enemy[w][e].x);
  }
  break;
  case ENEMY_GLIDER1:
   if (enemy[w][e].y > 25000 && enemy[w][e].y < 400000 && enemy[w][e].angle3 < 50)
    enemy[w][e].angle3 ++;
   if (enemy[w][e].y > 370000 && enemy[w][e].angle3 > 0)
    enemy[w][e].angle3 --;
  enemy[w][e].angle2 += enemy[w] [e].turn_direction;// * 2;
  if (enemy[w][e].angle2 <= 0)
   enemy[w][e].angle2 += ANGLE_1;

  enemy[w][e].angle2 %= ANGLE_1;

  x = enemy[w][e].x - xpart(enemy[w][e].angle1, 2000);
  y = enemy[w][e].y - ypart(enemy[w][e].angle1, 2000);

  if (enemy[w][e].recycle > 0)
  {
   if (enemy[w][e].angle3 == 50)
    enemy[w][e].recycle --;
  }
    else
    {
     if (enemy[w][e].burst_recycle > 0)
     {
      enemy[w][e].burst_recycle --;
      enemy[w][e].recycle = 14;
      play_effectwfvx(w, NWAV_SHOT, 1200 + grand(150), 150, enemy[w][e].x);
      for (i = 0; i < 6; i ++)
      {
       b = create_ebullet(w, EBULLET_SPIN, 0,
               x + xpart(enemy[w] [e].angle2 + ANGLE_6 * i, 11000),
               y + ypart(enemy[w] [e].angle2 + ANGLE_6 * i, 11000) + GLIDER_Y_ADJUST * 1000,
               xpart(enemy[w] [e].angle2 + ANGLE_6 * i, 2000) + enemy[w] [e].x_speed, ypart(enemy[w] [e].angle2 + ANGLE_6 * i, 2000) + enemy[w] [e].y_speed, 0);
       if (b == -1) continue;
       create_cloud(w, CLOUD_EXPLODE, 0, x + xpart(enemy[w][e].angle2 + ANGLE_6 * i, 11000), y + ypart(enemy[w] [e].angle2 + ANGLE_6 * i, 11000) + GLIDER_Y_ADJUST * 1000, enemy[w][e].x_speed, enemy[w][e].y_speed + 1000, 12 + grand(4));
//      b = create_ebullet(EBULLET_SHOT, enemy[w] [e].pole, x + xpart(enemy[w] [e].angle2 + ANGLE_8 * i, 4000), y + ypart(enemy[w] [e].angle2 + ANGLE_8 * i, 4000), xpart(enemy[w] [e].angle2 + ANGLE_8 * i, 2000) + enemy[w] [e].x_speed, ypart(enemy[w] [e].angle2 + ANGLE_8 * i, 2000) + enemy[w] [e].y_speed, enemy[w] [e].angle2 + ANGLE_8 * i);
       ebullet[w][b].turn_direction = enemy[w][e].turn_direction;
      }
     }
      else
      {
       enemy[w][e].recycle = 70 - arena.difficulty * 10;
       enemy[w][e].burst_recycle = 4 + arena.difficulty; // also in creation
      }
//    create_ebullet(EBULLET_SPIN, enemy[w] [e].pole, enemy[w] [e].x + xpart(enemy[w] [e].angle1 - ANGLE_8, 4000), enemy[w] [e].y + xpart(enemy[w] [e].angle1 - ANGLE_4, 4000), xpart(enemy[w] [e].angle1 - ANGLE_4, 2000), ypart(enemy[w] [e].angle1 - ANGLE_4, 2000), enemy[w] [e].angle1 + ANGLE_8);
    }
    break;
  case ENEMY_CARRIER2:
   enemy[w][e].angle1 += enemy[w][e].turn_direction;
   enemy[w][e].angle1 &= 1023;
   enemy[w][e].recycle --;
   if (enemy[w][e].recycle <= 0)
   {
    xa = grand(ANGLE_1);
    shoot_circle(w, enemy[w][e].x + xpart(xa, 10000), enemy[w][e].y + ypart(xa, 10000), xpart(xa, 3000), ypart(xa, 3000), xa, 2);
    enemy[w][e].recycle = 12 - arena.difficulty * 3;
   }
  break;

 }


    move_enemy(w, e);
}

void move_beamer(int w, int e)
{
 int b;

 if (enemy[w] [e].target == 0)
 {
  if (enemy[w] [e].target_time > 0)
  {
   enemy[w] [e].target_time --;
//   enemy[w] [e].y_speed = eclass[enemy[w] [e].type].speed1;
//   enemy[w] [e].y += enemy[w] [e].y_speed;
   move_enemy(w, e);
   return;
  }
  enemy[w] [e].target = 1;
  enemy[w] [e].target_time = 100;
//  enemy[w] [e].x_speed = 0;
  return;
 }

 if (enemy[w] [e].target == 1)
 {
   drag_enemy(w, e, 950);
   move_enemy(w, e);
   if (enemy[w][e].angle2 < 60)
    enemy[w][e].angle2 ++;
   switch(arena.difficulty)
   {
    case 1:
     track_target(w, e, player.x, player.y, enemy[w] [e].angle1, 4);
     break;
    case 2:
     track_target(w, e, player.x, player.y, enemy[w] [e].angle1, 6);
     break;
   }
   enemy[w] [e].target_time --;
   if (enemy[w] [e].target_time % 5 == 0 && enemy[w] [e].angle3 < 5)
    enemy[w] [e].angle3 ++;
   if (enemy[w] [e].target_time <= 0)
   {
    enemy[w] [e].target = 2;
    enemy[w] [e].target_time = 300;
    play_effectwfvx(w, NWAV_BEAM1, 700 + grand(50), 200, enemy[w][e].x);
    return;
   }
   return;
 }

 if (enemy[w] [e].target == 2)
 {
//   b = create_ebullet(w, EBULLET_BEAM1, 0, enemy[w] [e].x + xpart(enemy[w] [e].angle1 + ANGLE_2, 4000), enemy[w] [e].y + ypart(enemy[w] [e].angle1 + ANGLE_2, 4000), xpart(enemy[w] [e].angle1, 4000), ypart(enemy[w] [e].angle1, 4000), enemy[w] [e].angle1);
   b = create_ebullet(w, EBULLET_BEAM1, 0, enemy[w] [e].x + xpart(enemy[w] [e].angle1 + ANGLE_2, 4000), enemy[w] [e].y + ypart(enemy[w] [e].angle1 + ANGLE_2, 4000), xpart(enemy[w] [e].angle1, 4000), ypart(enemy[w] [e].angle1, 4000), enemy[w] [e].angle1);
   track_target(w, e, player.x, player.y, enemy[w] [e].angle1, 1);
   if (b != -1)
   {
    ebullet[w][b].timeout = 2;
    ebullet[w][b].status = enemy[w] [e].target_time;
    ebullet[w][b].status2 = 0;//-1;
   }
   enemy[w] [e].target_time --;
   if (enemy[w] [e].target_time > 180)
    enemy[w] [e].target_time -= 2;
//   if (enemy[w] [e].target_time > 140)
//    enemy[w] [e].target_time --;
   if (enemy[w] [e].target_time <= 0)
   {
    enemy[w] [e].target = 3;
    enemy[w] [e].y_speed = 0;
    enemy[w] [e].x_speed = 0;
    enemy[w] [e].target_time = 500;
    return;
   }
   return;
 }


   if (enemy[w][e].angle2 > 0)
    enemy[w][e].angle2 --;
// enemy[w] [e].x_speed += xpart(enemy[w] [e].angle1, eclass[enemy[w] [e].type].speed3);
 enemy[w] [e].target_time --;
 if (enemy[w] [e].target_time % 5 == 0 && enemy[w] [e].angle3 > 0)
  enemy[w] [e].angle3 --;
 enemy[w] [e].y_speed += eclass[enemy[w] [e].type].speed3;////ypart(enemy[w] [e].angle1, eclass[enemy[w] [e].type].speed3);
 drag_enemy(w, e, 950);
    move_enemy(w, e);
}

void move_attacker(int w, int e)
{
/*
 if (enemy[w] [e].target == 0)
 {
  if (enemy[w] [e].target_time > 0)
  {
   enemy[w] [e].target_time --;
   move_enemy(e);
   return;
  }
  enemy[w] [e].target = 1;
  enemy[w] [e].target_time = 250;
//  enemy[w] [e].y_speed = eclass[enemy[w] [e].type].speed1;
//  enemy[w] [e].x_speed = 0;
  return;
 }

 if (enemy[w] [e].target == 1)
 {
   drag_enemy(e, 970);
   move_enemy(e);
   track_target(e, player[0].x, player[0].y, enemy[w] [e].angle1, eclass[enemy[w] [e].type].speed2);
   enemy[w] [e].target_time --;
 if (enemy[w] [e].recycle > 0)
  enemy[w] [e].recycle --;
   else
   {
    switch(enemy[w] [e].type)
    {
     case ENEMY_ATTACKER1:
      enemy[w] [e].recycle = 50;
      create_ebullet(EBULLET_SHOT, enemy[w] [e].pole, enemy[w] [e].x + xpart(enemy[w] [e].angle1 + ANGLE_8, 9000), enemy[w] [e].y + ypart(enemy[w] [e].angle1 + ANGLE_8, 9000), xpart(enemy[w] [e].angle1, 5000), ypart(enemy[w] [e].angle1, 5000), enemy[w] [e].angle1);
      create_ebullet(EBULLET_SHOT, enemy[w] [e].pole, enemy[w] [e].x + xpart(enemy[w] [e].angle1 - ANGLE_8, 9000), enemy[w] [e].y + ypart(enemy[w] [e].angle1 - ANGLE_8, 9000), xpart(enemy[w] [e].angle1, 5000), ypart(enemy[w] [e].angle1, 5000), enemy[w] [e].angle1);
      break;
     case ENEMY_ATTACKER2:
      enemy[w] [e].recycle = 50;
      create_ebullet(EBULLET_SHOT, enemy[w] [e].pole, enemy[w] [e].x + xpart(enemy[w] [e].angle1, 9000), enemy[w] [e].y + ypart(enemy[w] [e].angle1, 9000), xpart(enemy[w] [e].angle1, 5000), ypart(enemy[w] [e].angle1, 5000), enemy[w] [e].angle1);
      create_ebullet(EBULLET_SHOT, enemy[w] [e].pole, enemy[w] [e].x + xpart(enemy[w] [e].angle1 - ANGLE_16, 9000), enemy[w] [e].y + ypart(enemy[w] [e].angle1 - ANGLE_8, 9000), xpart(enemy[w] [e].angle1 - ANGLE_16, 5000), ypart(enemy[w] [e].angle1 - ANGLE_16, 5000), enemy[w] [e].angle1 - ANGLE_16);
      create_ebullet(EBULLET_SHOT, enemy[w] [e].pole, enemy[w] [e].x + xpart(enemy[w] [e].angle1 + ANGLE_16, 9000), enemy[w] [e].y + ypart(enemy[w] [e].angle1 + ANGLE_8, 9000), xpart(enemy[w] [e].angle1 + ANGLE_16, 5000), ypart(enemy[w] [e].angle1 + ANGLE_16, 5000), enemy[w] [e].angle1 + ANGLE_16);
      break;
     case ENEMY_ATTACKER3:
      if (enemy[w] [e].burst_recycle > 0)
      {
       enemy[w] [e].recycle = 15;
       enemy[w] [e].burst_recycle --;
       create_ebullet(EBULLET_SHOT2, enemy[w] [e].pole, enemy[w] [e].x + xpart(enemy[w] [e].angle1 + ANGLE_8, 9000), enemy[w] [e].y + ypart(enemy[w] [e].angle1 + ANGLE_8, 9000), xpart(enemy[w] [e].angle1, 8000), ypart(enemy[w] [e].angle1, 8000), enemy[w] [e].angle1);
       create_ebullet(EBULLET_SHOT2, enemy[w] [e].pole, enemy[w] [e].x + xpart(enemy[w] [e].angle1 - ANGLE_8, 9000), enemy[w] [e].y + ypart(enemy[w] [e].angle1 - ANGLE_8, 9000), xpart(enemy[w] [e].angle1, 8000), ypart(enemy[w] [e].angle1, 8000), enemy[w] [e].angle1);
     }
      else
      {
       enemy[w] [e].recycle = 115;
       enemy[w] [e].burst_recycle = 4;
      }
      break;
    }
   }
// attack here!
   if (enemy[w] [e].target_time <= 0)
   {
    enemy[w] [e].target = 2;
    // thrust explosion!
    enemy[w] [e].y_speed = 0;
    enemy[w] [e].x_speed = 0;
    return;
   }
   return;
 }


// enemy[w] [e].x_speed += xpart(enemy[w] [e].angle1, 280);
 enemy[w] [e].y_speed -= eclass[enemy[w] [e].type].speed1; //ypart(enemy[w] [e].angle1, 280);
 drag_enemy(e, 950);
 move_enemy(e);*/
}



void move_pauser(int w, int e)
{
 int i;
 int angle;

 if (enemy[w][e].target == 0)
 {
  if (enemy[w][e].target_time > 0)
  {
   enemy[w][e].target_time --;
   move_enemy(w, e);
   return;
  }
  enemy[w][e].target = 1;
  enemy[w][e].target_time = 250;
//  enemy[w] [e].y_speed = eclass[enemy[w] [e].type].speed1;
//  enemy[w] [e].x_speed = 0;
  return;
 }

 if (enemy[w][e].target == 1)
 {
   drag_enemy(w, e, 970);
   move_enemy(w, e);
//   track_target(e, player[0].x, player[0].y, enemy[w] [e].angle1, eclass[enemy[w] [e].type].speed2);
   enemy[w][e].target_time --;

    switch(enemy[w] [e].type)
    {
     case ENEMY_BURSTER:
     if (enemy[w][e].target_time < 17)
     {
      enemy[w][e].angle2 --;
      break;
     }
     if (enemy[w][e].target_time < 200)
     {
      if (enemy[w][e].angle2 < 17)
      {
       enemy[w][e].angle2 ++;
       enemy[w][e].recycle ++;
      }
     }
      break;
     case ENEMY_DIPPER1:
     case ENEMY_DIPPER2:
     if (enemy[w][e].target_time < 15)
     {
      enemy[w][e].angle2 --;
      break;
     }
     if (enemy[w][e].target_time < 200)
     {
      if (enemy[w][e].angle2 < 15)
      {
       enemy[w][e].angle2 ++;
       enemy[w][e].recycle ++;
      }
     }
       break;
     case ENEMY_ZAPPER1:
     track_target(w, e, player.x, player.y, enemy[w][e].angle1, eclass[enemy[w][e].type].speed2);
      break;
    }
 if (enemy[w][e].recycle > 0)
  enemy[w][e].recycle --;
   else
   {
    switch(enemy[w] [e].type)
    {
        case ENEMY_DIPPER1:
         angle = attack_angle(w, e);
         create_ebullet(w, EBULLET_SHOT3, 0, enemy[w][e].x, enemy[w][e].y, xpart(angle, 3500), ypart(angle, 3500), angle);
         create_cloud(w, CLOUD_EXPLODE, 0, enemy[w][e].x, enemy[w][e].y, 0, 0, 20 + grand(5));
         enemy[w][e].recycle = 60 - arena.difficulty * 10;
         play_effectwfvx(w, NWAV_SHOT, 600 + grand(50), 150, enemy[w][e].x);
         break;
        case ENEMY_DIPPER2:
         angle = attack_angle(w, e);
         create_ebullet(w, EBULLET_SHOT3, 1, enemy[w][e].x, enemy[w][e].y, xpart(angle, 4000), ypart(angle, 4000), angle);
         create_cloud(w, CLOUD_EXPLODE, 1, enemy[w][e].x, enemy[w][e].y, 0, 0, 20 + grand(5));
         enemy[w][e].recycle = 50 - arena.difficulty * 10;
         play_effectwfvx(w, NWAV_SHOT, 800 + grand(50), 150, enemy[w][e].x);
         break;
        case ENEMY_ZAPPER1:
         angle = 1;
         if (enemy[w][e].burst_recycle == 3)
         {
          int bx = enemy[w][e].x + xpart(enemy[w][e].angle1, 15000);
          int by = enemy[w][e].y + ypart(enemy[w][e].angle1, 15000);
          create_ebullet(w, EBULLET_SHOT2, 0, bx, by, xpart(enemy[w][e].angle1, 4000), ypart(enemy[w][e].angle1, 4000), enemy[w][e].angle1);
          create_cloud(w, CLOUD_EXPLODE, 0, bx, by, 0, 0, 20 + grand(5));

          angle = radians_to_angle(atan2((player.y) - enemy[w][e].y, player.x - enemy[w][e].x));
          enemy[w][e].x_target [0] = enemy[w][e].x + xpart(angle, 100000);
          enemy[w][e].y_target [0] = enemy[w][e].y + ypart(angle, 100000);
          play_effectwfvx(w, NWAV_BLAST, 1100 + grand(50), 150, enemy[w][e].x);
        }

         angle = radians_to_angle(atan2(enemy[w][e].y_target [0] - enemy[w][e].y, enemy[w][e].x_target [0] - (enemy[w][e].x - 33000)));
         create_ebullet(w, EBULLET_SHOT, 0, enemy[w][e].x - 33000, enemy[w][e].y, xpart(angle, 4000), ypart(angle, 4000), angle);
         create_cloud(w, CLOUD_EXPLODE, 0, enemy[w][e].x - 33000, enemy[w][e].y, 0, 0, 15 + grand(5));

         angle = radians_to_angle(atan2(enemy[w][e].y_target [0] - enemy[w][e].y, enemy[w][e].x_target [0] - (enemy[w][e].x + 33000)));
         create_ebullet(w, EBULLET_SHOT, 0, enemy[w][e].x + 33000, enemy[w][e].y, xpart(angle, 4000), ypart(angle, 4000), angle);
         create_cloud(w, CLOUD_EXPLODE, 0, enemy[w][e].x + 33000, enemy[w][e].y, 0, 0, 15 + grand(5));

         play_effectwfvx(w, NWAV_ZAP, 1800 + grand(50), 30, enemy[w][e].x);
         enemy[w][e].burst_recycle --;
         enemy[w][e].recycle = 12 - arena.difficulty * 2;
         if (enemy[w][e].burst_recycle == 0)
         {
             enemy[w][e].burst_recycle = 3; // also in creation
             enemy[w][e].recycle = 120 - arena.difficulty * 10;
         }


/*       angle = radians_to_angle(atan2((player.y - 140000) - enemy[w][e].y, player.x - (enemy[w][e].x - 33000)));
         create_ebullet(w, EBULLET_SHOT, 0, enemy[w][e].x - 33000, enemy[w][e].y, xpart(angle, 4000), ypart(angle, 4000), angle);
         create_cloud(w, CLOUD_EXPLODE, 0, enemy[w][e].x - 33000, enemy[w][e].y, 0, 0, 15 + grand(5));
         angle = radians_to_angle(atan2((player.y - 140000) - enemy[w][e].y, player.x - (enemy[w][e].x + 33000)));
         create_ebullet(w, EBULLET_SHOT, 0, enemy[w][e].x + 33000, enemy[w][e].y, xpart(angle, 4000), ypart(angle, 4000), angle);
         create_cloud(w, CLOUD_EXPLODE, 0, enemy[w][e].x + 33000, enemy[w][e].y, 0, 0, 15 + grand(5));*/
         break;
        case ENEMY_SHOTTER1:
         angle = attack_angle(w, e);
         int angle2;
         int speed;
         for (i = 0; i < 9; i ++)
         {
           angle2 = angle + grand(ANGLE_16) - grand(ANGLE_16);
           speed = 3000 + grand(2000);
           create_ebullet(w, EBULLET_SHOT, 1, enemy[w][e].x, enemy[w][e].y + SHOTTER1_Y_ADJUST * 1000, xpart(angle2, speed), ypart(angle2, speed), angle2);
         }
         create_cloud(w, CLOUD_SHOCKWAVE, 1, enemy[w][e].x, enemy[w][e].y + SHOTTER1_Y_ADJUST * 1000, 0, 0, 20);
         enemy[w][e].recycle = 150 - arena.difficulty * 20;
         play_effectwfvx(w, NWAV_BLAST, 1100 + grand(50), 150, enemy[w][e].x);
         break;
        case ENEMY_BURSTER:
         angle = attack_angle(w, e) + ANGLE_2;
         int angle3;
         int speed2;
         switch(enemy[w][e].angle3)
         {
             default:
            case 0:
             for (i = 0; i < 5; i ++)
             {
             angle3 = angle + i * ANGLE_1 / 5;
             speed2 = 2500;
             create_ebullet(w, EBULLET_FATTER_SHOT, 2, enemy[w][e].x, enemy[w][e].y, xpart(angle3, speed2), ypart(angle3, speed2), angle3);
             }
             create_cloud(w, CLOUD_EXPLODE, 2, enemy[w][e].x, enemy[w][e].y, 0, 0, 20);
             enemy[w][e].recycle = 15;
             enemy[w][e].angle3 = 1;
             play_effectwfvx(w, NWAV_BLAST, 1800 + grand(50), 100, enemy[w][e].x);
             break;
            case 1:
             for (i = 0; i < 8; i ++)
             {
             angle3 = angle + i * ANGLE_1 / 8;
             speed2 = 3000;
             create_ebullet(w, EBULLET_FAT_SHOT, 1, enemy[w][e].x, enemy[w][e].y, xpart(angle3, speed2), ypart(angle3, speed2), angle3);
             }
             create_cloud(w, CLOUD_EXPLODE, 1, enemy[w][e].x, enemy[w][e].y, 0, 0, 20);
             enemy[w][e].recycle = 15;
             enemy[w][e].angle3 = 2;
             play_effectwfvx(w, NWAV_BLAST, 1500 + grand(50), 100, enemy[w][e].x);
             break;
            case 2:
             for (i = 0; i < 12; i ++)
             {
             angle3 = angle + i * ANGLE_1 / 12;
             speed2 = 3500;
             create_ebullet(w, EBULLET_SHOT, 0, enemy[w][e].x, enemy[w][e].y, xpart(angle3, speed2), ypart(angle3, speed2), angle3);
             }
             create_cloud(w, CLOUD_EXPLODE, 0, enemy[w][e].x, enemy[w][e].y, 0, 0, 20);
             enemy[w][e].recycle = 80 - arena.difficulty * 15;
             enemy[w][e].angle3 = 0;
             play_effectwfvx(w, NWAV_BLAST, 1200 + grand(50), 100, enemy[w][e].x);
             break;
         }
         break;
/*     case ENEMY_ATTACKER1:
      enemy[w] [e].recycle = 50;
      create_ebullet(EBULLET_SHOT, enemy[w] [e].pole, enemy[w] [e].x + xpart(enemy[w] [e].angle1 + ANGLE_8, 9000), enemy[w] [e].y + ypart(enemy[w] [e].angle1 + ANGLE_8, 9000), xpart(enemy[w] [e].angle1, 5000), ypart(enemy[w] [e].angle1, 5000), enemy[w] [e].angle1);
      create_ebullet(EBULLET_SHOT, enemy[w] [e].pole, enemy[w] [e].x + xpart(enemy[w] [e].angle1 - ANGLE_8, 9000), enemy[w] [e].y + ypart(enemy[w] [e].angle1 - ANGLE_8, 9000), xpart(enemy[w] [e].angle1, 5000), ypart(enemy[w] [e].angle1, 5000), enemy[w] [e].angle1);
      break;
     case ENEMY_ATTACKER2:
      enemy[w] [e].recycle = 50;
      create_ebullet(EBULLET_SHOT, enemy[w] [e].pole, enemy[w] [e].x + xpart(enemy[w] [e].angle1, 9000), enemy[w] [e].y + ypart(enemy[w] [e].angle1, 9000), xpart(enemy[w] [e].angle1, 5000), ypart(enemy[w] [e].angle1, 5000), enemy[w] [e].angle1);
      create_ebullet(EBULLET_SHOT, enemy[w] [e].pole, enemy[w] [e].x + xpart(enemy[w] [e].angle1 - ANGLE_16, 9000), enemy[w] [e].y + ypart(enemy[w] [e].angle1 - ANGLE_8, 9000), xpart(enemy[w] [e].angle1 - ANGLE_16, 5000), ypart(enemy[w] [e].angle1 - ANGLE_16, 5000), enemy[w] [e].angle1 - ANGLE_16);
      create_ebullet(EBULLET_SHOT, enemy[w] [e].pole, enemy[w] [e].x + xpart(enemy[w] [e].angle1 + ANGLE_16, 9000), enemy[w] [e].y + ypart(enemy[w] [e].angle1 + ANGLE_8, 9000), xpart(enemy[w] [e].angle1 + ANGLE_16, 5000), ypart(enemy[w] [e].angle1 + ANGLE_16, 5000), enemy[w] [e].angle1 + ANGLE_16);
      break;
     case ENEMY_ATTACKER3:
      if (enemy[w] [e].burst_recycle > 0)
      {
       enemy[w] [e].recycle = 15;
       enemy[w] [e].burst_recycle --;
       create_ebullet(EBULLET_SHOT2, enemy[w] [e].pole, enemy[w] [e].x + xpart(enemy[w] [e].angle1 + ANGLE_8, 9000), enemy[w] [e].y + ypart(enemy[w] [e].angle1 + ANGLE_8, 9000), xpart(enemy[w] [e].angle1, 8000), ypart(enemy[w] [e].angle1, 8000), enemy[w] [e].angle1);
       create_ebullet(EBULLET_SHOT2, enemy[w] [e].pole, enemy[w] [e].x + xpart(enemy[w] [e].angle1 - ANGLE_8, 9000), enemy[w] [e].y + ypart(enemy[w] [e].angle1 - ANGLE_8, 9000), xpart(enemy[w] [e].angle1, 8000), ypart(enemy[w] [e].angle1, 8000), enemy[w] [e].angle1);
     }
      else
      {
       enemy[w] [e].recycle = 115;
       enemy[w] [e].burst_recycle = 4;
      }
      break;*/
    }
   }
// attack here!
   if (enemy[w][e].target_time <= 0)
   {
    enemy[w][e].target = 2;
    // thrust explosion!
    enemy[w][e].y_speed = 0;
    enemy[w][e].x_speed = 0;
    return;
   }
   return;
 }


// enemy[w] [e].x_speed += xpart(enemy[w] [e].angle1, 280);
 enemy[w][e].y_speed -= eclass[enemy[w][e].type].speed1; //ypart(enemy[w] [e].angle1, 280);
 drag_enemy(w, e, 950);
 move_enemy(w, e);
}




void move_enemy(int w, int e)
{
     enemy[w] [e].x += enemy[w] [e].x_speed;
     enemy[w] [e].y += enemy[w] [e].y_speed;

}

void enemy_fire(int w, int e)
{
 int angle = attack_angle(w, e); //radians_to_angle(atan2(player.y - enemy[w] [e].y, player.x - enemy[w] [e].x));
// int b;
// create_ebullet(

 switch(enemy[w][e].type)
 {
     default:
      create_ebullet(w, EBULLET_SHOT, 0, enemy[w][e].x, enemy[w][e].y, xpart(angle, 3500 + arena.difficulty * 500), ypart(angle, 3500 + arena.difficulty * 500), angle);
      play_effectwfvx(w, NWAV_ZAP, 1800 + grand(50), 30, enemy[w][e].x);
      break;
     case ENEMY_BASIC3:
      play_effectwfvx(w, NWAV_ZAP, 1200 + grand(50), 30, enemy[w][e].x);
      create_ebullet(w, EBULLET_SHOT, 2, enemy[w][e].x, enemy[w][e].y, xpart(angle, 3000 + arena.difficulty * 500), ypart(angle, 3000 + arena.difficulty * 500), angle);
      break;
     case ENEMY_BFIGHTER:
      play_effectwfvx(w, NWAV_ZAP, 1400 + grand(50), 30, enemy[w][e].x);
      create_ebullet(w, EBULLET_SHOT, 1, enemy[w][e].x, enemy[w][e].y, xpart(angle, 2500 + arena.difficulty * 500), ypart(angle, 2500 + arena.difficulty * 500), angle);
      break;
     case ENEMY_CROSS:
/*      b = create_ebullet(w, EBULLET_TRIANGLE, 0, enemy[w][e].x, enemy[w][e].y, 0, 0, angle);
      if (b != -1)
      {
       ebullet[w][b].x2 = ebullet[w][b].x;
       ebullet[w][b].y2 = ebullet[w][b].y;
       ebullet[w][b].status3 = (int) angle / (SMALL_ROTATIONS_ANGLE) & (SMALL_ROTATIONS - 1);
       play_effectwfvx(w, NWAV_ZAP, 1800 + grand(50), 30, enemy[w][e].x);
      }*/
      shoot_triangle(w, enemy[w][e].x, enemy[w][e].y, angle, 0);
      play_effectwfvx(w, NWAV_TAP, 2200, 100, enemy[w][e].x);
//      play_effectwfvx(w, NWAV_ZAP, 1800 + grand(50), 30, enemy[w][e].x);
/*      angle -= ANGLE_32;
      b = create_ebullet(w, EBULLET_TRIANGLE, 1, enemy[w][e].x, enemy[w][e].y, 0, 0, angle);
      if (b != -1)
      {
       ebullet[w][b].x2 = ebullet[w][b].x;
       ebullet[w][b].y2 = ebullet[w][b].y;
       ebullet[w][b].status3 = (int) angle / (SMALL_ROTATIONS_ANGLE) & (SMALL_ROTATIONS - 1);
      }
      angle += ANGLE_16;
      b = create_ebullet(w, EBULLET_TRIANGLE, 2, enemy[w][e].x, enemy[w][e].y, 0, 0, angle);
      if (b != -1)
      {
       ebullet[w][b].x2 = ebullet[w][b].x;
       ebullet[w][b].y2 = ebullet[w][b].y;
       ebullet[w][b].status3 = (int) angle / (SMALL_ROTATIONS_ANGLE) & (SMALL_ROTATIONS - 1);
      }*/
      break;
     case ENEMY_CROSS2:
      shoot_triangle(w, enemy[w][e].x, enemy[w][e].y, angle - ANGLE_32, 2);
      shoot_triangle(w, enemy[w][e].x, enemy[w][e].y, angle + ANGLE_32, 2);
      play_effectwfvx(w, NWAV_TAP, 1900, 100, enemy[w][e].x);
//      play_effectwfvx(w, NWAV_ZAP, 1800 + grand(50), 30, enemy[w][e].x);
      break;

 }
}

int attack_angle(int w, int e)
{
 return radians_to_angle(atan2(player.y - enemy[w][e].y, player.x - enemy[w][e].x));
}

int pbullet_hits_enemy(int w, int b, int e)
{

  int dam = 100;

  switch(pbullet[w][b].type)
  {
      case PBULLET_TURRET:
      case PBULLET_BASIC:
        dam = 100;
        break;
      case PBULLET_CIRCLE:
        dam = 200 + pbullet[w][b].type2 * 25;
        break;
      case PBULLET_HEAVY:
        dam = 1800 + pbullet[w][b].type2 * 400;
        break;
      case PBULLET_WHITE2:
       dam = 400;
       break;
      case PBULLET_WAVE:
        dam = (pbullet[w][b].type2 - 50) / 2;
        break;
      case PBULLET_MULTI1:
      case PBULLET_MULTI2:
      case PBULLET_MULTI3:
      case PBULLET_MULTI4:
      case PBULLET_MULTI5:
       dam = 40;
       break;
      case PBULLET_GREEN2_VORTEX:
        dam = 60;
        break;
  }

//  if (pbullet[w] [b].pole != enemy[w] [e].pole)
//   dam = 2;

  hurt_enemy(w, e, dam, 0, 0);

  return dam;

}

int seeker_hits_enemy(int w, int s, int e)
{
  int dam = 400;

//  if (seeker[s].pole != enemy[w] [e].pole)
//   dam = 20;

  hurt_enemy(w, e, dam, 0, 0);

  return dam;

}


void hurt_enemy(int w, int e, int dam, int pole, int source)
{

 int explode_b = 0;

 if (enemy[w][e].type == ENEMY_BOSS1)
 {
  if (boss.exploding > 0)
   return;
  if (boss.hp - dam < 0)
  {
   boss.hp = 0;
   kill_boss1();
   return;
  }
//  if (boss.hp != boss.max_hp && boss.hp / 4 != (boss.hp - dam) / 4)
  //{
//   boss1_gives_powerup();
  //}
  boss.hp -= dam;
  return;
 }

 if (enemy[w][e].type == ENEMY_MB2)
 {
//  if (boss.exploding > 0)
//   return;
  if (boss.hp - dam < 0)
  {
   boss.hp = 0;
   kill_mb2();
   return;
  }
  boss.hp -= dam;
  return;
 }

 if (enemy[w][e].type == ENEMY_MB1)
 {
//  if (boss.exploding > 0)
//   return;
  if (boss.hp - dam < 0)
  {
   boss.hp = 0;
   kill_mb1();
   return;
  }
  boss.hp -= dam;
  return;
 }

 if (enemy[w][e].type == ENEMY_BOSS2)
 {
//  if (boss.exploding > 0)
//   return;
  if (boss.hp - dam < 0)
  {
   boss.hp = 0;
   kill_boss2();
   return;
  }
  boss.hp -= dam;
  return;
 }



 enemy[w] [e].hp -= dam;
// enemy[w] [e].hurt_pulse += dam * 5;
// if (enemy[w] [e].hurt_pulse >= 15)
//  enemy[w] [e].hurt_pulse = 15;

// if (pole == enemy[w] [e].pole)
//  explode_b = 1;

 if (enemy[w] [e].hp <= 0)
  enemy_explodes(w, e, explode_b, source);


}
/*
void explode_bullets(int e, int pole_match, int p)
{


 int i;
 int angle;
 if (player[p].in_play)
  angle = radians_to_angle(atan2(player[p].y - enemy[w] [e].y, player[p].x - enemy[w] [e].x));
   else
    angle = - ANGLE_4;// straight down?
 int s_angle;
 int speed;

 for (i = 0; i < 4; i ++)
 {
  s_angle = angle + grand(ANGLE_32) - grand(ANGLE_32);
  speed = 2000 + grand(2500);
  create_ebullet(EBULLET_BALL, enemy[w] [e].pole, enemy[w] [e].x, enemy[w] [e].y, xpart(s_angle, speed), ypart(s_angle, speed), s_angle);
 }


}
*/

void enemy_explodes(int w, int e, int bullets, int source)
{
 int i, angle = grand(ANGLE_1), j, angle2;
 int distance, speed;

 switch(enemy[w] [e].type)
 {
/*  case ENEMY_GLIDER1:
  case ENEMY_BEAMER1:
   create_cloud(CLOUD_LARGE_SHOCKWAVE, POLE_NONE, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 50);
   create_cloud(CLOUD_EXPLODE, POLE_NONE, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 25 + grand(10));
   create_cloud(CLOUD_EXPLODE, POLE_NONE, enemy[w] [e].x + grand(10000) - grand(10000), enemy[w] [e].y + grand(10000) - grand(10000), 0, 0, 25 + grand(10));
   create_cloud(CLOUD_EXPLODE, POLE_NONE, enemy[w] [e].x + grand(10000) - grand(10000), enemy[w] [e].y + grand(10000) - grand(10000), 0, 0, 25 + grand(10));
   create_cloud(CLOUD_EXPLODE, POLE_NONE, enemy[w] [e].x + grand(10000) - grand(10000), enemy[w] [e].y + grand(10000) - grand(10000), 0, 0, 25 + grand(10));
   for (i = 0; i < 3; i ++)
   {
    angle += ANGLE_4 + grand(ANGLE_4);
     speed = 2000 + grand(1000) + grand(1000);
     distance = 5000;
     angle2 = angle + grand(ANGLE_16);
//     create_cloud(CLOUD_EXPLODE, POLE_NONE, enemy[w] [e].x + xpart(angle2, distance), enemy[w] [e].y + ypart(angle2, distance), xpart(angle2, speed), ypart(angle2, speed), 34 + grand(7));
     create_cloud(CLOUD_LARGE_SPRAY, POLE_NONE, enemy[w] [e].x + xpart(angle2, distance), enemy[w] [e].y + ypart(angle2, distance), xpart(angle2, speed), ypart(angle2, speed), 20 + grand(7));
   }
  break;*/
  case ENEMY_ZAPPER1:
   create_cloud(w, CLOUD_LARGE_SHOCKWAVE, 0, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 50);
   create_cloud(w, CLOUD_EXPLODE, 0, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 30 + grand(10));
   create_cloud(w, CLOUD_LARGE_SPRAY, 0, enemy[w][e].x + 3000, enemy[w][e].y, - 5000, 0, 20 + grand(7));
   create_cloud(w, CLOUD_LARGE_SPRAY, 0, enemy[w][e].x - 3000, enemy[w][e].y,  5000, 0, 20 + grand(7));
  play_effectwfvx(w, NWAV_BIGBANG, 1550 + grand(350), 100, enemy[w][e].x);
  break;
  case ENEMY_BURSTER:
   create_cloud(w, CLOUD_LARGE_SHOCKWAVE, 2, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 50);
   create_cloud(w, CLOUD_EXPLODE, 2, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 30 + grand(10));
 for (i = 0; i < 4; i ++)
 {
  angle += ANGLE_8 + grand(ANGLE_4);
  for (j = 0; j < 5; j ++)
  {
   speed = 4000 + grand(4500) + grand(4000);
   distance = 15000;
   angle2 = angle + grand(ANGLE_16);
   create_cloud(w, CLOUD_DRAG_EXPLODE, 2, enemy[w] [e].x + xpart(angle2, distance), enemy[w] [e].y + ypart(angle2, distance), xpart(angle2, speed), ypart(angle2, speed), 20 + grand(10));
  }
 }
  play_effectwfvx(w, NWAV_BIGBANG, 1450 + grand(450), 100, enemy[w][e].x);
  break;


 case ENEMY_CARRIER:
  create_pickup(w, grand(4), enemy[w][e].x, enemy[w][e].y);
  create_cloud(w, CLOUD_LARGE_SHOCKWAVE, 0, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 50);
  create_cloud(w, CLOUD_EXPLODE, 0, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 45 + grand(7));
  play_effectwfvx(w, NWAV_BIGBANG, 1850 + grand(150), 100, enemy[w][e].x);
  break;
 case ENEMY_BEAMER1:
  create_cloud(w, CLOUD_LARGE_SHOCKWAVE, 0, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 50);
  create_cloud(w, CLOUD_EXPLODE, 0, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 45 + grand(7));
  create_cloud(w, CLOUD_LARGE_SPRAY, 0, enemy[w][e].x + 5000, enemy[w][e].y - 5000, xpart(-ANGLE_8, 6000), ypart(-ANGLE_8, 6000), 15 + grand(7));
  create_cloud(w, CLOUD_LARGE_SPRAY, 0, enemy[w][e].x + 5000, enemy[w][e].y + 5000, xpart(ANGLE_8, 6000), ypart(ANGLE_8, 6000), 15 + grand(7));
  create_cloud(w, CLOUD_LARGE_SPRAY, 0, enemy[w][e].x - 5000, enemy[w][e].y + 5000, xpart(ANGLE_4 + ANGLE_8, 6000), ypart(ANGLE_4 + ANGLE_8, 6000), 15 + grand(7));
  create_cloud(w, CLOUD_LARGE_SPRAY, 0, enemy[w][e].x - 5000, enemy[w][e].y - 5000, xpart(-ANGLE_4 - ANGLE_8, 6000), ypart(- ANGLE_4 - ANGLE_8, 6000), 15 + grand(7));
//   create_cloud(w, CLOUD_LARGE_SPRAY, 0, enemy[w][e].x - 3000, enemy[w][e].y,  5000, 0, 20 + grand(7));
  play_effectwfvx(w, NWAV_BIGBANG, 1000 + grand(300), 100, enemy[w][e].x);
  break;

 case ENEMY_GLIDER1:
 case ENEMY_SHOTTER1:
 create_cloud(w, CLOUD_LARGE_SHOCKWAVE, 0, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 50);
 create_cloud(w, CLOUD_EXPLODE, 0, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 45 + grand(7));
// create_cloud(w, CLOUD_EXPLODE, 0, enemy[w] [e].x + grand(18000) - grand(18000), enemy[w] [e].y + grand(18000) - grand(18000), grand(3000) - grand(3000),  grand(3000) - grand(3000), 14 + grand(7));
 //create_cloud(w, CLOUD_EXPLODE, 0, enemy[w] [e].x + grand(18000) - grand(18000), enemy[w] [e].y + grand(18000) - grand(18000),   grand(3000) - grand(3000),  grand(3000) - grand(3000), 14 + grand(7));
 angle = 0;
 int col = 0;
 if (enemy[w][e].type == ENEMY_SHOTTER1)
  col = 1;
 for (i = 0; i < 6; i ++)
 {
  angle += ANGLE_6;
  speed = 4000 + grand(1500);
  distance = 12000;
  for (j = 0; j < 5; j ++)
  {
   create_cloud(w, CLOUD_EXPLODE, col, enemy[w][e].x + xpart(angle, distance), enemy[w] [e].y + ypart(angle, distance), xpart(angle, speed), ypart(angle, speed), 20 + grand(10));
  }
 }
 play_effectwfvx(w, NWAV_BIGBANG, 1400 + grand(200), 100, enemy[w][e].x);
 break;
 case ENEMY_DIPPER1:
 case ENEMY_DIPPER2:
 create_cloud(w, CLOUD_SHOCKWAVE, 0, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 30);
 create_cloud(w, CLOUD_EXPLODE, 0, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 35 + grand(7));
 create_cloud(w, CLOUD_EXPLODE, 0, enemy[w] [e].x + grand(28000) - grand(28000), enemy[w] [e].y + grand(28000) - grand(28000), grand(3000) - grand(3000),  grand(3000) - grand(3000), 17 + grand(7));
 create_cloud(w, CLOUD_EXPLODE, 0, enemy[w] [e].x + grand(28000) - grand(28000), enemy[w] [e].y + grand(28000) - grand(28000),   grand(3000) - grand(3000),  grand(3000) - grand(3000), 17 + grand(7));
 int drag_colour = 0;
 if (enemy[w][e].type == ENEMY_DIPPER2)
  drag_colour = 1;
 for (i = 0; i < 4; i ++)
 {
  angle += ANGLE_8 + grand(ANGLE_4);
  for (j = 0; j < 5; j ++)
  {
   speed = 2000 + grand(3500) + grand(3000);
   distance = 9000;
   angle2 = angle + grand(ANGLE_16);
   create_cloud(w, CLOUD_DRAG_EXPLODE, drag_colour, enemy[w] [e].x + xpart(angle2, distance), enemy[w] [e].y + ypart(angle2, distance), xpart(angle2, speed), ypart(angle2, speed), 20 + grand(10));
  }
 }
 play_effectwfvx(w, NWAV_BIGBANG, 2100 + grand(400), 100, enemy[w][e].x);
 break;

 default:
 play_effectwfx(w, NWAV_BANG, 1200 + grand(400), enemy[w][e].x);

// create_cloud(CLOUD_LARGE_SHOCKWAVE, POLE_NONE, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 50);
 create_cloud(w, CLOUD_SHOCKWAVE, 0, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 20);
// create_cloud(w, CLOUD_EXPLODE, 0, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 14 + grand(7));
// create_cloud(w, CLOUD_EXPLODE, 0, enemy[w] [e].x + grand(18000) - grand(18000), enemy[w] [e].y + grand(18000) - grand(18000), grand(3000) - grand(3000),  grand(3000) - grand(3000), 14 + grand(7));
 //create_cloud(w, CLOUD_EXPLODE, 0, enemy[w] [e].x + grand(18000) - grand(18000), enemy[w] [e].y + grand(18000) - grand(18000),   grand(3000) - grand(3000),  grand(3000) - grand(3000), 14 + grand(7));
 create_cloud(w, CLOUD_EXPLODE, 0, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 19 + grand(7));
 create_cloud(w, CLOUD_EXPLODE, 0, enemy[w] [e].x + grand(25000) - grand(25000), enemy[w] [e].y + grand(25000) - grand(25000), grand(3000) - grand(3000),  grand(3000) - grand(3000), 16 + grand(7));
 create_cloud(w, CLOUD_EXPLODE, 0, enemy[w] [e].x + grand(25000) - grand(25000), enemy[w] [e].y + grand(25000) - grand(25000),   grand(3000) - grand(3000),  grand(3000) - grand(3000), 16 + grand(7));
 for (i = 0; i < 3; i ++)
 {
  angle += ANGLE_4 + grand(ANGLE_4);
  for (j = 0; j < 5; j ++)
  {
   speed = 2000 + grand(3500) + grand(3000);
   distance = 3000;
   angle2 = angle + grand(ANGLE_16);
   create_cloud(w, CLOUD_DRAG_EXPLODE, 0, enemy[w] [e].x + xpart(angle2, distance), enemy[w] [e].y + ypart(angle2, distance), xpart(angle2, speed), ypart(angle2, speed), 20 + grand(10));
  }
 }
 break;
 case ENEMY_BFLOWER:
 play_effectwfx(w, NWAV_BLIP, 800 + grand(200), enemy[w][e].x);
// play_effectwfx(w, NWAV_BANG, 1200 + grand(400), enemy[w][e].x);
 create_cloud(w, CLOUD_SHOCKWAVE2, enemy[w][e].angle3, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 20);
 create_cloud(w, CLOUD_EXPLODE2, enemy[w][e].angle3, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 10);
 angle = grand(ANGLE_1);
// create_cloud(w, CLOUD_EXPLODE2, 0, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 20);
 create_cloud(w, CLOUD_EXPLODE2, enemy[w][e].angle3, enemy[w] [e].x + xpart(angle, 14000), enemy[w] [e].y + ypart(angle, 14000), 0, 0, 25);
 create_cloud(w, CLOUD_EXPLODE2, enemy[w][e].angle3, enemy[w] [e].x + xpart(angle + ANGLE_3, 14000), enemy[w] [e].y + ypart(angle + ANGLE_3, 14000), 0, 0, 25);
 create_cloud(w, CLOUD_EXPLODE2, enemy[w][e].angle3, enemy[w] [e].x + xpart(angle - ANGLE_3, 14000), enemy[w] [e].y + ypart(angle - ANGLE_3, 14000), 0, 0, 25);
 break;
 case ENEMY_BFIGHTER:
 play_effectwfx(w, NWAV_BANG, 1300 + grand(400), enemy[w][e].x);
// create_cloud(w, CLOUD_SHOCKWAVE, 0, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 20);
 create_cloud(w, CLOUD_EXPLODE, 0, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 22 + grand(7));
 angle = grand(ANGLE_16);
 for (i = 0; i < 16; i ++)
 {
   angle += ANGLE_16;
   speed = 2000;
   distance = 15000;
   create_cloud(w, CLOUD_DRAG_EXPLODE, 0, enemy[w] [e].x + xpart(angle, distance), enemy[w] [e].y + ypart(angle, distance), xpart(angle, speed), ypart(angle, speed), 30 + grand(10));
  }
 break;
 case ENEMY_CROSS:
// play_effectwfx(w, NWAV_BANG, 1200 + grand(400), enemy[w][e].x);
 play_effectwfx(w, NWAV_BLIP, 800 + grand(200), enemy[w][e].x);
 create_cloud(w, CLOUD_SHOCKWAVE2, 0, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 20);
 create_cloud(w, CLOUD_EXPLODE2, 0, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 20);
 angle = grand(ANGLE_1);
// create_cloud(w, CLOUD_EXPLODE2, 0, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 20);
 create_cloud(w, CLOUD_EXPLODE2, 0, enemy[w] [e].x + xpart(angle, 14000), enemy[w] [e].y + ypart(angle, 14000), 0, 0, 15);
 create_cloud(w, CLOUD_EXPLODE2, 0, enemy[w] [e].x + xpart(angle + ANGLE_3, 14000), enemy[w] [e].y + ypart(angle + ANGLE_3, 14000), 0, 0, 15);
 create_cloud(w, CLOUD_EXPLODE2, 0, enemy[w] [e].x + xpart(angle - ANGLE_3, 14000), enemy[w] [e].y + ypart(angle - ANGLE_3, 14000), 0, 0, 15);
 break;
 case ENEMY_CROSS2:
// play_effectwfx(w, NWAV_BANG, 1200 + grand(400), enemy[w][e].x);
 play_effectwfx(w, NWAV_BLIP, 600 + grand(130), enemy[w][e].x);
 create_cloud(w, CLOUD_SHOCKWAVE2, 2, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 20);
 create_cloud(w, CLOUD_EXPLODE2, 2, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 20);
 angle = grand(ANGLE_1);
// create_cloud(w, CLOUD_EXPLODE2, 0, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 20);
 create_cloud(w, CLOUD_EXPLODE2, 2, enemy[w] [e].x + xpart(angle, 14000), enemy[w] [e].y + ypart(angle, 14000), xpart(angle, 3000), ypart(angle, 3000), 15);
 create_cloud(w, CLOUD_EXPLODE2, 2, enemy[w] [e].x + xpart(angle + ANGLE_4, 14000), enemy[w] [e].y + ypart(angle + ANGLE_4, 14000), xpart(angle + ANGLE_4, 3000), ypart(angle + ANGLE_4, 3000), 15);
 create_cloud(w, CLOUD_EXPLODE2, 2, enemy[w] [e].x + xpart(angle + ANGLE_2, 14000), enemy[w] [e].y + ypart(angle + ANGLE_2, 14000), xpart(angle + ANGLE_2, 3000), ypart(angle + ANGLE_2, 3000), 15);
 create_cloud(w, CLOUD_EXPLODE2, 2, enemy[w] [e].x + xpart(angle - ANGLE_4, 14000), enemy[w] [e].y + ypart(angle - ANGLE_4, 14000), xpart(angle - ANGLE_4, 3000), ypart(angle - ANGLE_4, 3000), 15);
 break;
 case ENEMY_WINGS:
  play_effectwfx(w, NWAV_BLIP2, 600, enemy[w][e].x);
  create_cloud(w, CLOUD_LARGE_SHOCKWAVE2, 2, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 50);
  create_cloud(w, CLOUD_EXPLODE2, 2, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 50);
 break;
 case ENEMY_FLOWER1:
 play_effectwfx(w, NWAV_BLIP2, 400 + grand(100), enemy[w][e].x);
 create_cloud(w, CLOUD_LARGE_SHOCKWAVE2, 2, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 50);
 for (i = 0; i < 8; i ++)
 {
  create_cloud(w, CLOUD_EXPLODE2, 2, enemy[w] [e].x + xpart(i * ANGLE_8, 9000), enemy[w] [e].y + ypart(i * ANGLE_8, 9000), xpart(i * ANGLE_8, 1500), ypart(i * ANGLE_8, 1500), 20);
 }
 break;
 case ENEMY_FLOWER2:
 play_effectwfx(w, NWAV_BLIP2, 300 + grand(50), enemy[w][e].x);
// play_effectwfx(w, NWAV_BANG, 1200 + grand(400), enemy[w][e].x);
 create_cloud(w, CLOUD_LARGE_SHOCKWAVE2, 2, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 50);
 for (i = 0; i < 8; i ++)
 {
  create_cloud(w, CLOUD_EXPLODE2, 1, enemy[w] [e].x + xpart(i * ANGLE_8, 9000), enemy[w] [e].y + ypart(i * ANGLE_8, 9000), xpart(i * ANGLE_8, 1500), ypart(i * ANGLE_8, 1500), 20);
 }
 create_cloud(w, CLOUD_EXPLODE2, 0, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 40);
 break;
 case ENEMY_FLOWER3:
 play_effectwfx(w, NWAV_BLIP2, 600 + grand(50), enemy[w][e].x);
//lay_effectwfx(w, NWAV_BANG, 1200 + grand(400), enemy[w][e].x);
 create_cloud(w, CLOUD_LARGE_SHOCKWAVE2, 0, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 50);
 for (i = 0; i < 8; i ++)
 {
  create_cloud(w, CLOUD_EXPLODE2, 2, enemy[w] [e].x + xpart(i * ANGLE_8, 30000), enemy[w] [e].y + ypart(i * ANGLE_8, 30000), xpart(i * ANGLE_8 + ANGLE_2, 2000), ypart(i * ANGLE_8 + ANGLE_2, 2000), 35);
 }
 create_cloud(w, CLOUD_EXPLODE2, 1, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 40);
 break;
 case ENEMY_FLOWER4:
  play_effectwfx(w, NWAV_BLIP2, 500 + grand(50), enemy[w][e].x);
//  play_effectwfx(w, NWAV_BANG, 1200 + grand(400), enemy[w][e].x);
  create_cloud(w, CLOUD_LARGE_SHOCKWAVE2, 2, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 50);
  for (i = 0; i < 8; i ++)
  {
   create_cloud(w, CLOUD_EXPLODE2, 1, enemy[w] [e].x + xpart(i * ANGLE_8, 30000), enemy[w] [e].y + ypart(i * ANGLE_8, 30000), xpart(i * ANGLE_8, 4000), ypart(i * ANGLE_8, 4000), 25);
  }
  create_cloud(w, CLOUD_EXPLODE2, 0, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 40);
 break;
 case ENEMY_FLOWER5:
  play_effectwfx(w, NWAV_BLIP2, 200 + grand(50), enemy[w][e].x);
//  play_effectwfx(w, NWAV_BANG, 1200 + grand(400), enemy[w][e].x);
  create_cloud(w, CLOUD_LARGE_SHOCKWAVE2, 0, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 50);
  create_cloud(w, CLOUD_EXPLODE2, 0, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 50);
 break;

 case ENEMY_CARRIER2:
  create_pickup(w, grand(4), enemy[w][e].x, enemy[w][e].y);
  create_cloud(w, CLOUD_LARGE_SHOCKWAVE2, 0, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 50);
  for (i = 0; i < 8; i ++)
  {
   create_cloud(w, CLOUD_EXPLODE2, 0, enemy[w] [e].x + xpart(i * ANGLE_8, 45000), enemy[w] [e].y + ypart(i * ANGLE_8, 45000), 0, 0, 30);
  }
  create_cloud(w, CLOUD_EXPLODE2, 0, enemy[w] [e].x, enemy[w] [e].y, 0, 0, 50);
  play_effectwfx(w, NWAV_BLIP2, 500 + grand(50), enemy[w][e].x);
  break;

 }

 //explode_bullets(e, bullets, source);

 player_score(eclass[enemy[w][e].type].score);
 destroy_enemy(w, e);
}

void drag_enemy(int w, int e, int drag)
{
 enemy[w] [e].x_speed *= drag;
 enemy[w] [e].x_speed /= 1000;
 enemy[w] [e].y_speed *= drag;
 enemy[w] [e].y_speed /= 1000;

}




void track_target(int w, int e, int x, int y, int angle, int turn)
{


      int angle_move = delta_turn_towards_xy(enemy[w] [e].x, enemy[w] [e].y,
          x, y, angle, turn);

//      textprintf_ex(screen, font, 5, 5, COLOUR_11, COLOUR_2, "%i %i %i %i %i %i    ", e, enemy[w] [e].type, enemy[w] [e].x, enemy[w] [e].y, turn, angle_move);


      if (angle_move / turn != enemy[w] [e].angle1_turning)
      {
       if (enemy[w] [e].angle1_turning_count <= 0)
       {
        enemy[w] [e].angle1_turning_count = 20;
        if (angle_move < 0)
         enemy[w] [e].angle1_turning = -1;
          else
           enemy[w] [e].angle1_turning = 1;
        return;

       }
       enemy[w] [e].angle1_turning_count --;
//     enemy[w] [e].angle1 += turn * enemy[w] [e].angle1_turning;
       return;
      }

      enemy[w] [e].angle1_turning_count --;
      enemy[w] [e].angle1 += turn * enemy[w] [e].angle1_turning;

      if (enemy[w] [e].angle1 < 0)
       enemy[w] [e].angle1 += ANGLE_1;
      if (enemy[w] [e].angle1 >= ANGLE_1)
       enemy[w] [e].angle1 -= ANGLE_1;

}

void player_score(int amount)
{

 int old_score = player.score;

 player.score += amount;

 if ((int) old_score / 1500 != (int) player.score / 1500)
 {
  if (arena.player_lives < 8)
   arena.player_lives ++;
 }


}

void destroy_enemy(int w, int e)
{
 int s;

 enemy[w] [e].type = ENEMY_NONE;

 for (s = 0; s < NO_SEEKERS; s ++)
 {
  if (seeker[w][s].target == e)
  {
   seeker[w][s].target = -1;
   find_new_seeker_target(w, s);
  }
 }

}
