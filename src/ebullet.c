#include "config.h"

#include <math.h>

#include "allegro.h"

#include "globvars.h"

#include "stuff.h"
#include "enemy.h"
#include "cloud.h"

#include "palette.h"

void run_ebullets(void);
void destroy_ebullet(int w, int b);
//int ebullet_collision(int b);
int check_eat_bullet(int b);
void eat_bullet(int b, int p);
void player_gain_energy(int p, int ev);
void check_beam_absorb(int p, int b);
int shoot_triangle(int w, int x, int y, int angle, int type);



void init_ebullets(void)
{

 int b, w;
 for (w = 0; w < 2; w ++)
 {
  for (b = 0; b < NO_EBULLETS; b ++)
  {
   ebullet[w] [b].type = EBULLET_NONE;
  }
 }

}


int create_ebullet(int w, int type, int status, int x, int y, int xs, int ys, int angle)
{
  int b;

  for (b = 0; b < NO_EBULLETS; b ++)
  {
      if (ebullet[w] [b].type == EBULLET_NONE)
       break;
      if (b == NO_EBULLETS - 1)
       return -1;

  }

 ebullet[w] [b].type = type;
 ebullet[w] [b].x = x;
 ebullet[w] [b].y = y;
 ebullet[w] [b].x_speed = xs;
 ebullet[w] [b].y_speed = ys;
// ebullet[w] [b].pole = pole;
 ebullet[w] [b].energy_value = 40;
 ebullet[w] [b].angle = angle;
 ebullet[w] [b].timeout = 5000;
 ebullet[w] [b].status = status;
 ebullet[w] [b].status2 = 0;
     return b;
}

void run_ebullets(void)
{

  arena.old_beam = arena.beam;
  arena.beam = 0;

  int b, w;
  for (w = 0; w < 2; w ++)
  {
  for (b = 0; b < NO_EBULLETS; b ++)
  {
    if (ebullet[w][b].type == EBULLET_NONE)
     continue;
//    if (ebullet[b].type == EBULLET_BASIC)
    switch(ebullet[w][b].type)
    {

        case EBULLET_SHOT2:
            ebullet[w][b].x += ebullet[w][b].x_speed;
            ebullet[w][b].y += ebullet[w][b].y_speed;
            if (arena.counter % 2 == 0)
           create_cloud(w, CLOUD_DRAG_EXPLODE, ebullet[w][b].status, ebullet[w][b].x + grand(5000) - grand(5000), ebullet[w][b].y  + grand(5000) - grand(5000), 0, 0, 25 + grand(15));
//           create_cloud(w, CLOUD_EXPLODE, 0, ebullet[w][b].x + grand(5000) - grand(5000), ebullet[w][b].y  + grand(5000) - grand(5000), 0, 0, 9 + grand(5));
         break;
        case EBULLET_SHOT3:
            ebullet[w][b].x += ebullet[w][b].x_speed;
            ebullet[w][b].y += ebullet[w][b].y_speed;
            if (arena.counter % 2 == 0)
           create_cloud(w, CLOUD_DRAG_EXPLODE, ebullet[w][b].status, ebullet[w][b].x, ebullet[w][b].y, 0, 0, 12 + grand(10));
         break;

        case EBULLET_SHOT:
        case EBULLET_FAT_SHOT:
        case EBULLET_FATTER_SHOT:
        case EBULLET_BALL:
            ebullet[w][b].x += ebullet[w][b].x_speed;
            ebullet[w][b].y += ebullet[w][b].y_speed;
            break;
        case EBULLET_SPIN:
            ebullet[w][b].x += ebullet[w][b].x_speed;
            ebullet[w][b].y += ebullet[w][b].y_speed;
            ebullet[w][b].angle += ebullet[w][b].turn_direction;
            if (ebullet[w][b].angle < 0)
             ebullet[w][b].angle += ANGLE_1;
            ebullet[w][b].angle %= ANGLE_1;
            break;
        case EBULLET_BEAM1:
        if (ebullet[w][b].status < 150)// && ebullet[w][b].status > 30)
          arena.beam = 1;
          break;
        case EBULLET_TRIANGLE:
         ebullet[w][b].status2 ++;
         if (ebullet[w][b].status2 < 60)
         {
          ebullet[w][b].x_speed += xpart(ebullet[w][b].angle, 50);
          ebullet[w][b].y_speed += ypart(ebullet[w][b].angle, 50);
         }
         ebullet[w][b].x += ebullet[w][b].x_speed;
         ebullet[w][b].y += ebullet[w][b].y_speed;
         break;
        case EBULLET_DIAMOND:
         ebullet[w][b].status2 ++;
            ebullet[w][b].x += ebullet[w][b].x_speed;
            ebullet[w][b].y += ebullet[w][b].y_speed;
            ebullet[w][b].status3 += ebullet[w][b].turn_direction;
            if (ebullet[w][b].status3 >= SMALL_ROTATIONS)
             ebullet[w][b].status3 = 0;
            if (ebullet[w][b].status3 < 0)
             ebullet[w][b].status3 = SMALL_ROTATIONS - 1;
            break;
        case EBULLET_CIRCLE:
         ebullet[w][b].status2 ++;
            ebullet[w][b].x += ebullet[w][b].x_speed;
            ebullet[w][b].y += ebullet[w][b].y_speed;
            break;
        case EBULLET_STAR:
         ebullet[w][b].status2 ++;
            ebullet[w][b].x += ebullet[w][b].x_speed;
            ebullet[w][b].y += ebullet[w][b].y_speed;
            ebullet[w][b].status3 += ebullet[w][b].turn_direction;
            if (ebullet[w][b].status3 >= SMALL_ROTATIONS)
             ebullet[w][b].status3 = 0;
            if (ebullet[w][b].status3 < 0)
             ebullet[w][b].status3 = SMALL_ROTATIONS - 1;
            if (ebullet[w][b].status2 > 100 && grand(80) == 0)
            {
             int angle = grand(ANGLE_1);
             shoot_triangle(w, ebullet[w][b].x + xpart(angle, 4000), ebullet[w][b].y + ypart(angle, 4000), angle, 2);
             shoot_triangle(w, ebullet[w][b].x + xpart(angle + ANGLE_4, 4000), ebullet[w][b].y + ypart(angle + ANGLE_4, 4000), angle + ANGLE_4, 2);
             shoot_triangle(w, ebullet[w][b].x + xpart(angle + ANGLE_2, 4000), ebullet[w][b].y + ypart(angle + ANGLE_2, 4000), angle + ANGLE_2, 2);
             shoot_triangle(w, ebullet[w][b].x + xpart(angle - ANGLE_4, 4000), ebullet[w][b].y + ypart(angle - ANGLE_4, 4000), angle - ANGLE_4, 2);
             create_cloud(w, CLOUD_EXPLODE2, 2, ebullet[w][b].x, ebullet[w][b].y, 0, 0, 12 + grand(5));
             destroy_ebullet(w, b);
             return;
            }
            break;

    }

//    if (check_eat_bullet(b) == 1)
//     continue;

    ebullet[w][b].timeout --;

  if (arena.level == 1)
  {
    if (ebullet[w][b].x <= -20000 || ebullet[w][b].y <= -20000
     || ebullet[w][b].x >= 330000 || ebullet[w][b].y >= 490000
     || ebullet[w][b].timeout <= 0)
     destroy_ebullet(w, b);
  }
   else
   {
    if (ebullet[w][b].x <= -120000 || ebullet[w][b].y <= -120000
     || ebullet[w][b].x >= 430000 || ebullet[w][b].y >= 590000
     || ebullet[w][b].timeout <= 0)
     destroy_ebullet(w, b);
   } // stage 2 bullets have long trails

  }
  }
}

int shoot_triangle(int w, int x, int y, int angle, int type)
{
      int b = create_ebullet(w, EBULLET_TRIANGLE, type, x, y, 0, 0, angle);
      if (b != -1)
      {
       ebullet[w][b].x2 = ebullet[w][b].x;
       ebullet[w][b].y2 = ebullet[w][b].y;
       ebullet[w][b].status3 = (int) angle / (SMALL_ROTATIONS_ANGLE) & (SMALL_ROTATIONS - 1);
      }

 return b;
}

int shoot_circle(int w, int x, int y, int xs, int ys, int angle, int type)
{
      int b = create_ebullet(w, EBULLET_CIRCLE, type, x, y, xs, ys, angle);
      if (b != -1)
      {
       ebullet[w][b].x2 = ebullet[w][b].x;
       ebullet[w][b].y2 = ebullet[w][b].y;
      }

 return b;
}


int shoot_diamond(int w, int x, int y, int xs, int ys, int angle, int type, int turn_direction)
{
      int b = create_ebullet(w, EBULLET_DIAMOND, type, x, y, xs, ys, angle);
      if (b != -1)
      {
       ebullet[w][b].x2 = ebullet[w][b].x;
       ebullet[w][b].y2 = ebullet[w][b].y;
       ebullet[w][b].status3 = (int) angle / (SMALL_ROTATIONS_ANGLE) & (SMALL_ROTATIONS - 1);
       ebullet[w][b].turn_direction = turn_direction;
      }

 return b;
}

int shoot_star(int w, int x, int y, int xs, int ys, int angle, int type, int turn_direction)
{
      int b = create_ebullet(w, EBULLET_STAR, type, x, y, xs, ys, angle);
      if (b != -1)
      {
       ebullet[w][b].x2 = ebullet[w][b].x;
       ebullet[w][b].y2 = ebullet[w][b].y;
       ebullet[w][b].status3 = (int) angle / (SMALL_ROTATIONS_ANGLE) & (SMALL_ROTATIONS - 1);
       ebullet[w][b].turn_direction = turn_direction;
      }

 return b;
}


int check_eat_bullet(int b)
{
/* int p;

 for (p = 0; p < 2; p ++)
 {
   if (player[p].in_play == 0)
    continue;
   if (player[p].pole != ebullet[b].pole || player[p].switch_recycle > 0)
    continue;
   if (ebullet[b].type == EBULLET_BEAM1)
   {
    check_beam_absorb(p, b);
    continue;
   }
   if (ebullet[b].x >= player[p].x - 25000 && ebullet[b].x <= player[p].x + 25000
       && ebullet[b].y >= player[p].y - 25000 && ebullet[b].y <= player[p].y + 25000)
       {
        if (hypot(ebullet[b].y - player[p].y, ebullet[b].x - player[p].x) <= 16000)
        {
         eat_bullet(b, p);
         return 1;
        }

       }

 }
*/
 return 0;
}

void eat_bullet(int b, int p)
{

 //player_gain_energy(p, ebullet[b].energy_value);

 //destroy_ebullet(b);

}

void check_beam_absorb(int p, int b)
{
/*
 if (ebullet[b].type == EBULLET_BEAM1
     && (ebullet[b].status > 180 || ebullet[b].status < 30))
  return;

 int bx = ebullet[b].x / GRAIN;
 int by = ebullet[b].y / GRAIN;
 int px = player[p].x / GRAIN;
 int py = player[p].y / GRAIN;
 int angle =
      radians_to_angle(atan2((py - by), (px - bx)));

 if ((angle > ANGLE_1 - ANGLE_8
     || angle < ANGLE_8)
      && px < bx)
       return; // right quadrant
 if (angle > ANGLE_2 - ANGLE_8
      && angle < ANGLE_2 + ANGLE_8
      && px > bx)
       return; // left quadrant
 if (angle > ANGLE_8
      && angle < ANGLE_2 - ANGLE_8
      && py < by)
       return; // bottom quadrant
 if (angle < ANGLE_1 - ANGLE_8
      && angle > ANGLE_2 + ANGLE_8
      && py > by)
       return; // bottom quadrant

// okay, it's in the right quadrant! let's be more specific:

// int angle2 =
//      radians_to_angle(atan2((py - by), (px - bx)));

 bx = ebullet[b].x / GRAIN - xpart(ebullet[b].angle, 5000);
 by = ebullet[b].y / GRAIN - ypart(ebullet[b].angle, 5000);

 int angle2 =
      radians_to_angle(atan2((py - by), (px - bx)));

 if (angle2 < 16)
 {
  if (angle > ANGLE_1 - 16)
   angle = angle2;
 }

 if (angle < 16)
 {
  if (angle2 > ANGLE_1 - 16)
   angle2 = angle;
 }

 if (abs(angle2 - angle) < 16)
 {
  player_gain_energy(p, 1);
  ebullet[b].status2 = hypot(ebullet[b].y - player[p].y, ebullet[b].x - player[p].x) - grand(4000);
  if (ebullet[b].status2 <= 0)
   ebullet[b].status2 = 1;
//  if (arena.counter % 20 == 0)
//   create_cloud(CLOUD_SHOCKWAVE, POLE_NONE, enemy[e].x, enemy[e].y, 0, 0, 20);




 }
 */
}


void player_gain_energy(int p, int ev)
{
 //player[p].energy += ev;
 //if (player[p].energy >= 800)
  //player[p].energy = 800;
}

void destroy_ebullet(int w, int b)
{
  ebullet[w] [b].type = EBULLET_NONE;
}

