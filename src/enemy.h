
int create_enemy(int w, int type, int pole, int x, int y, int angle, int att1, int att2, int att3, int att4, int att5, int att6, int att7);

void init_enemies(void);

int pbullet_hits_enemy(int w, int b, int e);
int seeker_hits_enemy(int w, int s, int e);
void hurt_enemy(int w, int e, int dam, int pole, int source);
void run_enemies(void);

enum
{
ATT1,
ATT2,
ATT3,
ATT4,
ATT5,
ATT6,
ATT7
};
