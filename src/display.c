/*
Angry Moth
Copyright (C) 2006 Linley Henzell

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public Licence as published by
    the Free Software Foundation; either version 2 of the Licence, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public Licence for more details.

    You should have received a copy of the GNU General Public Licence
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA

    The GPL version 2 is included in this distribution in a file called
    LICENCE.TXT. Use any text editor or the TYPE command to read it.

    You should be able to reach me by sending an email to
    l_henzell@yahoo.com.au.

File: display.c
History:

This file contains:
 - functions which put stuff onto the screen

*/

#include "config.h"

#include "allegro.h"

#include <math.h>
#include <string.h>

#include "globvars.h"
#include "palette.h"
#include "stuff.h"
#include "game.h"
#include "display.h"

#include "cloud.h"
// only necessary for a terrible hack.


#define EGAUGE_Y 300
// Y location of energy gauge
#define PGAUGE_Y 200
// Y location of powerup gauge


int debug_info;


void pline(BITMAP *bmp, int x1, int y1, int x2, int y2, int colour);

int points [30];
void poly4(int w, int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4, int col);


int platform_tile [9] [PLAT_Y];
int platform_position;
int platform_step;
// externed in game.cc


BITMAP *display [3];

RLE_SPRITE *aura_rle [2] [13];
RLE_SPRITE *player_rle [2] [21];
RLE_SPRITE *normal_player_rle [2] [7];
RLE_SPRITE *shock_rle [3] [20];
RLE_SPRITE *large_shock_rle [3] [50];
RLE_SPRITE *straight_bullet [2] [SMALL_ROTATIONS];
RLE_SPRITE *spin_bullet [2] [SMALL_ROTATIONS];
RLE_SPRITE *multi_bullet [2] [5];
RLE_SPRITE *green_ring [40];

struct RLE_STRUCT shock2_rle [3] [20];
struct RLE_STRUCT large_shock2_rle [3] [50];


struct RLE_STRUCT explode_rle [3] [40];
struct RLE_STRUCT triangle_bullet [3] [SMALL_ROTATIONS];
struct RLE_STRUCT diamond_bullet [3] [SMALL_ROTATIONS];;
struct RLE_STRUCT star_bullet [3] [SMALL_ROTATIONS];;
struct RLE_STRUCT eRLE_flower [5] [50];


struct RLE_STRUCT eRLE_twister [2] [MEDIUM_ROTATIONS];
struct RLE_STRUCT eRLE_attacker [3] [2] [2] [MEDIUM_ROTATIONS];
struct RLE_STRUCT eRLE_diver [2] [2] [MEDIUM_ROTATIONS];
// that's attacker#, tail or head, pole, number of rotations
struct RLE_STRUCT eRLE_large [2] [LARGE_ERLES];

RLE_SPRITE *eRLE_basic1 [S_ENEMY_RLES];

RLE_SPRITE *basic_bullet [29];
RLE_SPRITE *eRLE_big [L_ENEMY_RLES];
RLE_SPRITE *platform_RLE [2] [PLATFORM_RLES];
RLE_SPRITE *exhaust [10];

BITMAP *underlay;
BITMAP *underlay2;

#ifdef DEBUG_DISPLAY
#include <stdlib.h>
// for itoa, used in the screenshot code but not needed for ports etc
extern volatile int frames_per_second;
extern volatile int ticked;
extern int slacktime;
extern int long_slacktime_store;
extern int debug_sound [5];


int slack_graph [100];
int slack_graph_pos;
int fps_graph [100];
int fps_graph_pos;
#endif

extern volatile int frames_per_second;
extern int slacktime;
extern int long_slacktime_store;

extern int overposition;
extern int mtrack_position [4];
extern int mtrack_count [4];
extern int mtrack_music [4];
extern int music [3] [51] [4];

extern struct optionstruct options;


FONT *font2;
FONT *small_font;

void indicate_fps(BITMAP *bmp, int play);

float angle_to_radians(int angle);

void draw_player(void);
void draw_enemies(void);

void draw_an_enemy(int w, int e);
void draw_pbullets(void);
void draw_a_pbullet(int w, int b);
void draw_a_seeker(int w, int s);
void draw_ebullets(void);
void draw_an_ebullet(int w, int b);
void draw_clouds(void);
void draw_a_cloud(int w, int c);
void draw_pickups(void);
void make_underlay(void);
void draw_energy_gauge(int x);
void draw_platform(void);
void draw_underlay(void);

int detect_player_collision(void);
int check_pixel(int w, int x, int y);

extern RGB palet [256];
extern RGB palet2 [256];

void level_display_init(void)
{

 vsync();
 clear_bitmap(screen);
 if (arena.level == 1)
  set_palette(palet);
   else
    set_palette(palet2);

}


void run_display(int shown)
{

//  if (options.run_vsync > 0)
//   vsync();
//  clear_to_color(display, COLOUR_6);
  if (shown == 1)
  {
   draw_underlay();

   draw_platform();

   draw_enemies();
  }
  draw_player();
  draw_ebullets();
  if (player.in_play == 1 && player.grace == 0)
  {
   int side_hit = detect_player_collision();
   if (side_hit != -1)
    player_hit(side_hit);
  }

  if (shown == 0)
   return; // skip the frame

  draw_pbullets();
  draw_clouds();
  draw_pickups();

  draw_energy_gauge(305);
/*
  textprintf_ex(display [0], font, 10, 20, -1, -1, "%i", frames_per_second);
  textprintf_ex(display [0], font, 10, 30, -1, -1, "%i", slacktime);
  textprintf_ex(display [0], font, 10, 40, -1, -1, "%i", long_slacktime_store);
  textprintf_ex(display [0], font, 10, 50, -1, -1, "%i", overposition);
  textprintf_ex(display [0], font, 10, 60, -1, -1, "%i", mtrack_position [0]);
  textprintf_ex(display [0], font, 10, 70, -1, -1, "%i", mtrack_count [0]);
  textprintf_ex(display [0], font, 10, 80, -1, -1, "%i", mtrack_music [0]);
  textprintf_ex(display [0], font, 10, 90, -1, -1, "%i", music [mtrack_music [0]] [mtrack_position [0]] [0]);
*/
  if (arena.game_over > 0)
  {
   textprintf_centre_ex(display [0], font, 160, 200, -1, -1, "G A M E   O V E R");
   textprintf_centre_ex(display [1], font, 160, 200, -1, -1, "B A D   L U C K");
  }

  if (arena.new_level_sign > 0)
  {
   textprintf_centre_ex(display [0], font, 160, 200, -1, -1, "S T A G E   %i", arena.level);
   switch(arena.difficulty)
   {
    case 0: textprintf_centre_ex(display [1], font, 160, 230, -1, -1, "EASY"); break;
    case 1: textprintf_centre_ex(display [1], font, 160, 230, -1, -1, "NORMAL"); break;
    case 2: textprintf_centre_ex(display [1], font, 160, 230, -1, -1, "HARD"); break;
   }
   switch(arena.level)
   {
    case 1:
     textprintf_centre_ex(display [0], font, 160, 230, -1, -1, "ORBITAL PLATFORM");
     break;
    case 2:
     textprintf_centre_ex(display [0], font, 160, 230, -1, -1, "NERVE CENTRE");
     break;
   }
  }

  if (arena.level_finished > 0)
  {
   textprintf_centre_ex(display [0], font, 160, 200, -1, -1, "S T A G E   %i   C O M P L E T E", arena.level);
   if (arena.level == 2)
   {
    textprintf_centre_ex(display [1], font, 160, 200, -1, -1, "Y O U   H A V E   W O N !");
    textprintf_centre_ex(display [1], font, 160, 230, -1, -1, "C O N G R A T U L A T I O N S ! !");
   }
    else
    {
     textprintf_centre_ex(display [1], font, 160, 200, -1, -1, "W E L L   D O N E !");
    }
  }



  //textprintf_ex(display [0], font, 10, 50, -1, -1, "%i", X_MAX);

//  textprintf_ex(display [2], font, 100, 120, -1, -1, "THE QUICK BROWN FOX JUMPS OVER THE LAZY DOG");


//  vsync();


/*  drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);
  rectfill(display, 50, 50, 300, 150, TRANS_WHITE1);
  rectfill(display, 60, 60, 300, 150, TRANS_WHITE2);
  rectfill(display, 70, 70, 300, 150, TRANS_WHITE3);
  rectfill(display, 80, 80, 300, 150, TRANS_WHITE4);
  rectfill(display, 90, 90, 300, 150, TRANS_WHITE5);
  rectfill(display, 100, 100, 300, 150, TRANS_WHITE6);
  rectfill(display, 110, 110, 300, 150, TRANS_WHITE4);
  drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);
*/
//  blit(display [0], screen, 0, 0, 120, 0, 400, 480);
  if (options.run_vsync > 0)
   vsync();
  blit(display [0], screen, 0, 0, 0, 0, 640, 480);
  //  blit(display [1], screen, 0, 0, 320, 0, 320, 480);



}

void draw_player(void)
{
 if (player.in_play == 0)
  return;

 int x = player.x / GRAIN;
 int y = player.y / GRAIN;

// rectfill(display, player.x / GRAIN - 10, player.y / GRAIN - 10,
//                   player.x / GRAIN + 10, player.y / GRAIN + 10, COLOUR_11);

 //int col1, col2, col3;
/*
 int aura_size = 0, aura_colour;

 if (player.switch_recycle >= 0)
 {
  if (player.switch_recycle < 10)
  {
   aura_colour = 0;
   if (player.switching == POLE_BLACK)
    aura_colour = 1;
   aura_size = player.switch_recycle;
   if (aura_size > 9)
    aura_size = 9;
   draw_trans_rle_sprite(display, aura_rle [aura_colour] [aura_size], x - 25, y - 21);
  }
  if (player.switch_recycle > 10)
   {
    aura_colour = 0;
    if (player.switching == POLE_WHITE)
     aura_colour = 1;
    aura_size = 20 - player.switch_recycle;
     if (aura_size > 9)
     aura_size = 9;
    draw_trans_rle_sprite(display, aura_rle [aura_colour] [aura_size], x - 25, y - 21);
   }

 }
  else
  {
   aura_colour = 0;
   if (player.pole == POLE_BLACK)
    aura_colour = 1;
    if (grand(4) == 0)
     draw_trans_rle_sprite(display, aura_rle [aura_colour] [0], x - 25, y - 21);
      else
       draw_trans_rle_sprite(display, aura_rle [aura_colour] [10 + grand(3)], x - 25, y - 21);
  }
*/
/* switch(player.pole)
 {
  default:
 col1 = COLOUR_4;
   col2 = COLOUR_5;
   col3 = COLOUR_6;
    break;
  case POLE_WHITE:
   col1 = COLOUR_12;
   col2 = COLOUR_11;
   col3 = COLOUR_10;
    break;
case POLE_BLACK:
   col1 = COLOUR_1;
   col2 = COLOUR_2;
   col3 = COLOUR_3;
    break;

 }

 if (player.switch_recycle >= 0)
 {
  int which_rle = player.switch_recycle;
  if (which_rle > 19)
   which_rle = 19;
  if (player.switching == POLE_WHITE)
   draw_rle_sprite(display, player_rle [0] [which_rle], x - 18, y - 9);
    else
     draw_rle_sprite(display, player_rle [1] [19 - which_rle], x - 18, y - 9);
 }
  else
 {
      if (player.pole == POLE_WHITE)
      {
        draw_rle_sprite(display, normal_player_rle [0] [3 + player.bank / 10], x - 18, y - 9);
      }
       else
        draw_rle_sprite(display, normal_player_rle [1] [3 + player.bank / 10], x - 18, y - 9);
 }
*/

//    draw_rle_sprite(display [0], normal_player_rle [1] [3 + player.bank / 10], x - 18, y - 9);
    //draw_rle_sprite(display [1], normal_player_rle [1] [3 + player.bank / 10], x - 18, y - 9);
int bank_sprite1, bank_sprite2;

//    draw_rle_sprite(display [0], player_rle [0] [0], x - 10, y - 6);

    bank_sprite1 = player.bank / 5;
    bank_sprite2 = player.bank / 5;

  int little = 0;
  int big = 1;
  if (player.sides == 1)
  {
   little = 1;
   big = 0;
  }

  if (player.switch_recycle > 10)
  {
   int bankx;

   if (little == 0)
   {
       bankx = -20 + player.switch_recycle;
       if (bankx < bank_sprite1)
        bank_sprite1 = bankx;
       bankx = 20 - player.switch_recycle;
       if (bankx > bank_sprite2)
        bank_sprite2 = bankx;
   }
    else
    {
       bankx = 20 - player.switch_recycle;
       if (bankx > bank_sprite1)
        bank_sprite1 = bankx;
       bankx = -20 + player.switch_recycle;
       if (bankx < bank_sprite2)
        bank_sprite2 = bankx;
    }
  }
   else
    if (player.switch_recycle > 0)
    {
     int bankx;

     if (little == 0)
     {
       bankx = 0 - player.switch_recycle; // yes!
       bank_sprite1 = bankx;
//       bankx = 10 - player.switch_recycle;
       bankx = player.switch_recycle;
       bank_sprite2 = bankx;
     }
      else
      {
       bankx = player.switch_recycle; // yes!
       bank_sprite1 = bankx;
       bankx = 0 - player.switch_recycle;
       bank_sprite2 = bankx;
      }
    }



/*

THIS ONE WORKS!!!:
  if (player.switch_recycle > 10)
  {
   int bankx;

   if (little == 0)
   {
       bankx = -20 + player.switch_recycle; // yes!
       bank_sprite1 = bankx;
       bankx = 20 - player.switch_recycle;
       bank_sprite2 = bankx;
   }
    else
    {
       bankx = 20 - player.switch_recycle;
       bank_sprite1 = bankx;
       bankx = -20 + player.switch_recycle;
       bank_sprite2 = bankx;
    }
  }
   else
    if (player.switch_recycle > 0)
    {
     int bankx;

     if (little == 0)
     {
       bankx = 0 - player.switch_recycle; // yes!
       bank_sprite1 = bankx;
//       bankx = 10 - player.switch_recycle;
       bankx = player.switch_recycle;
       bank_sprite2 = bankx;
     }
      else
      {
       bankx = player.switch_recycle; // yes!
       bank_sprite1 = bankx;
       bankx = 0 - player.switch_recycle;
       bank_sprite2 = bankx;
      }
    }

*/

#define BIG_Y 19
#define LITTLE_Y 3


   switch(bank_sprite1)
   {
    case -10:
    case -9:
     draw_rle_sprite(display [big], player_rle [1] [5], x - 14, y - BIG_Y);
     break;
    case -8:
    case -7:
     draw_rle_sprite(display [big], player_rle [1] [4], x - 14, y - BIG_Y);
     break;
    case -6:
    case -5:
     draw_rle_sprite(display [big], player_rle [1] [3], x - 14, y - BIG_Y);
     break;
    case -4:
    case -3:
     draw_rle_sprite(display [big], player_rle [1] [2], x - 14, y - BIG_Y);
     break;
    case -2:
    case -1:
     draw_rle_sprite(display [big], player_rle [1] [1], x - 14, y - BIG_Y);
     break;

    case 10:
    case 9:
     draw_rle_sprite(display [big], player_rle [1] [10], x - 14, y - BIG_Y);
     break;
    case 8:
    case 7:
     draw_rle_sprite(display [big], player_rle [1] [9], x - 14, y - BIG_Y);
     break;
    case 6:
    case 5:
     draw_rle_sprite(display [big], player_rle [1] [8], x - 14, y - BIG_Y);
     break;
    case 4:
    case 3:
     draw_rle_sprite(display [big], player_rle [1] [7], x - 14, y - BIG_Y);
     break;
    case 2:
    case 1:
     draw_rle_sprite(display [big], player_rle [1] [6], x - 14, y - BIG_Y);
     break;

    default:
    draw_rle_sprite(display [big], player_rle [1] [0], x - 14, y - BIG_Y);
    break;
   }

//    textprintf_centre_ex(display [big], font, x, y, COL_LRED, -1, "%i", bank_sprite1);


   switch(bank_sprite2)
   {
    case -10:
    case -9:
     draw_rle_sprite(display [little], player_rle [0] [5], x - 10, y - LITTLE_Y);
     break;
    case -8:
    case -7:
     draw_rle_sprite(display [little], player_rle [0] [4], x - 10, y - LITTLE_Y);
     break;
    case -6:
    case -5:
     draw_rle_sprite(display [little], player_rle [0] [3], x - 10, y - LITTLE_Y);
     break;
    case -4:
    case -3:
     draw_rle_sprite(display [little], player_rle [0] [2], x - 10, y - LITTLE_Y);
     break;
    case -2:
    case -1:
     draw_rle_sprite(display [little], player_rle [0] [1], x - 10, y - LITTLE_Y);
     break;

    case 10:
    case 9:
     draw_rle_sprite(display [little], player_rle [0] [10], x - 10, y - LITTLE_Y);
     break;
    case 8:
    case 7:
     draw_rle_sprite(display [little], player_rle [0] [9], x - 10, y - LITTLE_Y);
     break;
    case 6:
    case 5:
     draw_rle_sprite(display [little], player_rle [0] [8], x - 10, y - LITTLE_Y);
     break;
    case 4:
    case 3:
     draw_rle_sprite(display [little], player_rle [0] [7], x - 10, y - LITTLE_Y);
     break;
    case 2:
    case 1:
     draw_rle_sprite(display [little], player_rle [0] [6], x - 10, y - LITTLE_Y);
     break;

    default:
    draw_rle_sprite(display [little], player_rle [0] [0], x - 10, y - LITTLE_Y);
    break;
   }


   draw_trans_rle_sprite(display[little], exhaust [arena.counter % 4], x - 2, y + 5);

   draw_trans_rle_sprite(display[big], exhaust [arena.counter % 4], x - 4 + abs(bank_sprite1) / 4, y  + 6);
   draw_trans_rle_sprite(display[big], exhaust [arena.counter % 4], x + 0 - abs(bank_sprite1) / 4, y + 6);

   int col1, col2, col3;
   int which_explode;

   if (player.blue_fire > 0)
   {
       int x1 = x - 5 + abs(player.bank / 25);
       int x2 = x + 5 - abs(player.bank / 25);

       int xa = 2 + grand(3);//5 + grand(3);
       int xb = (explode_rle [0] [xa].x);

       switch(player.power [0])
       {
        case 1: col1 = TRANS_LRED; col2 = TRANS_DRED; col3 = TRANS_DDRED; which_explode = 0; break;
        case 2: col1 = TRANS_YELLOW; col2 = TRANS_LRED; col3 = TRANS_DRED; which_explode = 0; break;
        case 3: col1 = TRANS_YELLOW; col2 = TRANS_LGREEN; col3 = TRANS_DGREEN; which_explode = 1; break;
        case 4: col1 = TRANS_LBLUE; col2 = TRANS_DBLUE; col3 = TRANS_DDBLUE; which_explode = 2; break;
        case 5: col1 = TRANS_WHITE; col2 = TRANS_LPURPLE; col3 = TRANS_LBLUE; which_explode = 2; break;
       }

       draw_trans_rle_sprite(display[little], explode_rle [which_explode] [xa].sprite, x1 - xb, y - xb - 3);
       draw_trans_rle_sprite(display[little], explode_rle [which_explode] [xa].sprite, x2 - xb, y - xb - 3);

       drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);

       if (grand(3) == 0)
        {
            col1 = col2;
            col2 = col3;
        }
         else
         if (grand(5) == 0)
         {
            col3 = col2;
            col2 = col1;
         }

       vline(display[little], x1, y - 3, 0, col1);
       vline(display[little], x1 - 1, y - 3, 0, col2);
       vline(display[little], x1 + 1, y - 3, 0, col2);
       vline(display[little], x2, y - 3, 0, col1);
       vline(display[little], x2 - 1, y - 3, 0, col2);
       vline(display[little], x2 + 1, y - 3, 0, col2);

       drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);

   }

   if (player.blue2_fire > 0)
   {

       int x2a = player.blue2_fire + 2 + grand(4);//5 + grand(3);
       int x2b = explode_rle [0] [x2a].x;
       int which_explode;

       switch(player.power [1])
       {
        case 1: col1 = TRANS_YELLOW; col2 = TRANS_LRED; col3 = TRANS_DRED; which_explode = 0; break;
        case 2: col1 = TRANS_WHITE; col2 = TRANS_YELLOW; col3 = TRANS_LRED; which_explode = 0; break;
        case 3: col1 = TRANS_YELLOW; col2 = TRANS_LGREEN; col3 = TRANS_DGREEN; which_explode = 1; break;
        case 4: col1 = TRANS_WHITE; col2 = TRANS_LBLUE; col3 = TRANS_DBLUE; which_explode = 2; break;
        case 5: col1 = TRANS_WHITE; col2 = TRANS_LPURPLE; col3 = TRANS_DPURPLE; which_explode = 2; break;
       }

       draw_trans_rle_sprite(display[big], explode_rle [which_explode] [x2a].sprite, x - x2b, y - x2b - 13);

       drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);

        int b2f = player.blue2_fire;// / 3;

       if (b2f >= 3)
       {
        vline(display[big], x, y - 9, 0, col1);
        vline(display[big], x - 1, y - 9, 0, col1);
        vline(display[big], x + 1, y - 9, 0, col1);
        vline(display[big], x - 2, y - 9, 0, col2);
        vline(display[big], x + 2, y - 9, 0, col2);
        vline(display[big], x - 3, y - 9, 0, col3);
        vline(display[big], x + 3, y - 9, 0, col3);
       }

       if (b2f == 2)
       {
        vline(display[big], x, y - 9, 0, col1);
        vline(display[big], x - 1, y - 9, 0, col2);
        vline(display[big], x + 1, y - 9, 0, col2);
        vline(display[big], x - 2, y - 9, 0, col3);
        vline(display[big], x + 2, y - 9, 0, col3);
       }

       if (b2f == 1)
       {
        vline(display[big], x, y - 9, 0, col2);
        vline(display[big], x - 1, y - 9, 0, col3);
        vline(display[big], x + 1, y - 9, 0, col3);
       }

       if (b2f == 0)
       {
        vline(display[big], x, y - 9, 0, col3);
       }


       drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);

   }



/*     xa = 3 + grand(3);
     xb = (explode_rle [0] [xa]->w) / 2;
     draw_trans_rle_sprite(display[big], explode_rle [0] [xa], x - xb, y + 5 - xb);
     xa = 5 + grand(3);
     xb = (explode_rle [0] [xa]->w) / 2;
     draw_trans_rle_sprite(display[big], explode_rle [0] [xa], x - xb, y + 9 - xb);*/


 if (player.grace > 0)
 {

     int i, w, xa, xb, angle;

     for (w = 0; w < 2; w ++)
     {
      for (i = 0; i < 6; i ++)
      {
         xa = 1 + grand(2) + player.grace / 10;
         if (xa > 4)
          xa = 4 + grand(2);
         xb = explode_rle [1] [xa].x;
         if (w == 1)
          angle = player.grace * 20 + i * ANGLE_6;
           else
            angle = player.grace *-20 + i * ANGLE_6;
         draw_trans_rle_sprite(display[w], explode_rle [2] [xa].sprite, x + xpart(angle, 10 + xa * 2) - xb, y + ypart(angle, 10 + xa * 2) - xb);
      }
     }

 }

  //  textprintf_centre_ex(display [little], font, x, y, COL_LRED, -1, "%i", bank_sprite2);

/*hline(display[0], 0, y, 320, TRANS_DBLUE);
hline(display[1], 0, y, 320, TRANS_DBLUE);
vline(display[0], x, 0, 480, TRANS_DBLUE);
vline(display[1], x, 0, 480, TRANS_DBLUE);*/


/*   switch(bank_sprite)
   {
    case -6:
    case -5:
     draw_rle_sprite(display [1], player_rle [1] [3], x - 14, y - 10);
     break;
    case -4:
    case -3:
     draw_rle_sprite(display [1], player_rle [1] [2], x - 14, y - 10);
     break;
    case -2:
    case -1:
     draw_rle_sprite(display [1], player_rle [1] [1], x - 14, y - 10);
     break;

    case 6:
    case 5:
     draw_rle_sprite(display [1], player_rle [1] [6], x - 14, y - 10);
     break;
    case 4:
    case 3:
     draw_rle_sprite(display [1], player_rle [1] [5], x - 14, y - 10);
     break;
    case 2:
    case 1:
     draw_rle_sprite(display [1], player_rle [1] [4], x - 14, y - 10);
     break;

    default:
    draw_rle_sprite(display [1], player_rle [1] [0], x - 14, y - 10);
    break;
   }*/

// rect(display, x - 13, y - 5, x - 7, y + 18, COLOUR_0);
// rect(display, x + 13, y - 5, x + 7, y + 18, COLOUR_0);

}

// also draws lives
void draw_energy_gauge(int x)
{

 drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);


 int line_x = 310, line_x2 = 320;

 if (player.sides == 1)
 {
  line_x = 320;
  line_x2 = 310;
 }

  int col1 = TRANS_YELLOW; //8 - (arena.counter % 32) / 4;

  int col2 = TRANS_LRED;

 if (player.charge > 0)
 {
  int seekers = player.charge / 100;

  rectfill(display [2], line_x + 1, EGAUGE_Y, line_x + 9, EGAUGE_Y - seekers * 10, col1);

  rectfill(display [2], line_x + 1, EGAUGE_Y - seekers * 10, line_x + 9, EGAUGE_Y - seekers * 10 - (player.charge % 100) / 10, col2);

  if (player.charge > 300)
  {
    col1 = TRANS_DBLUE;
    col2 = (arena.counter / 4) % 3;
    if (col2 == 1)
     col1 = TRANS_LBLUE;
    if (col2 == 2)
     col1 = TRANS_WHITE;
   rectfill(display [2], line_x2 + 1, EGAUGE_Y, line_x2 + 9, EGAUGE_Y - player.charge / 10, col1);//COL_LBLUE);
  }
    else
     rectfill(display [2], line_x2 + 1, EGAUGE_Y, line_x2 + 9, EGAUGE_Y - player.charge / 10, TRANS_LBLUE);

 }

// rect(display[0], x, 420, x + 10, 340, TRANS_LBLUE);

  drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);

 int i;

 hline(display[2], line_x2, EGAUGE_Y - 30, line_x2 + 9, TRANS_WHITE);
 hline(display[2], line_x2, EGAUGE_Y - 29, line_x2 + 9, COL_OUTLINE);
 hline(display[2], line_x2, EGAUGE_Y - 31, line_x2 + 9, COL_OUTLINE);

 rect(display [2], 310, EGAUGE_Y - 80, 330, EGAUGE_Y, TRANS_WHITE);
 rect(display [2], 309, EGAUGE_Y - 81, 331, EGAUGE_Y + 1, COL_OUTLINE);
 rect(display [2], 311, EGAUGE_Y - 79, 329, EGAUGE_Y - 1, COL_OUTLINE);
 vline(display [2], 320, EGAUGE_Y, EGAUGE_Y - 80, TRANS_WHITE);
 vline(display [2], 319, EGAUGE_Y, EGAUGE_Y - 80, COL_OUTLINE);
 vline(display [2], 321, EGAUGE_Y, EGAUGE_Y - 80, COL_OUTLINE);

 for (i = EGAUGE_Y - 70; i < EGAUGE_Y; i += 10)
 {
     hline(display[2], line_x, i, line_x + 9, TRANS_WHITE);
     hline(display[2], line_x, i - 1, line_x + 9, COL_OUTLINE);
     hline(display[2], line_x, i + 1, line_x + 9, COL_OUTLINE);
//     hline(display[2], 0, 380 + i * 10, line_x + 9, TRANS_WHITE);
 }

// Now for powerups!

 drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);

 if (player.power [0] > 0)
 {
  switch(player.weapon [0])
  {
   case WPN_RED: col1 = TRANS_LRED; break;
   case WPN_BLUE: col1 = TRANS_LBLUE; break;
   case WPN_GREEN: col1 = TRANS_LGREEN; break;
   case WPN_WHITE: col1 = TRANS_WHITE; break;
  }
  rectfill(display [2], line_x + 1, PGAUGE_Y, line_x + 9, PGAUGE_Y - player.power [0] * 10, col1);
 }

 if (player.power [1] > 0)
 {
  switch(player.weapon [1])
  {
   case WPN_RED: col1 = TRANS_LRED; break;
   case WPN_BLUE: col1 = TRANS_LBLUE; break;
   case WPN_GREEN: col1 = TRANS_LGREEN; break;
   case WPN_WHITE: col1 = TRANS_WHITE; break;
  }
  rectfill(display [2], line_x2 + 1, PGAUGE_Y, line_x2 + 9, PGAUGE_Y - player.power [1] * 10, col1);
 }


  drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);

 line_x = 310;

 rect(display [2], 310, PGAUGE_Y - 50, 330, PGAUGE_Y, TRANS_WHITE);
 rect(display [2], 309, PGAUGE_Y - 51, 331, PGAUGE_Y + 1, COL_OUTLINE);
 rect(display [2], 311, PGAUGE_Y - 49, 329, PGAUGE_Y - 1, COL_OUTLINE);
 vline(display [2], 320, PGAUGE_Y, PGAUGE_Y - 50, TRANS_WHITE);
 vline(display [2], 319, PGAUGE_Y, PGAUGE_Y - 50, COL_OUTLINE);
 vline(display [2], 321, PGAUGE_Y, PGAUGE_Y - 50, COL_OUTLINE);

 for (i = PGAUGE_Y - 40; i < PGAUGE_Y; i += 10)
 {
     hline(display[2], line_x, i, line_x + 19, TRANS_WHITE);
     hline(display[2], line_x, i - 1, line_x + 20, COL_OUTLINE);
     hline(display[2], line_x, i + 1, line_x + 20, COL_OUTLINE);
//     hline(display[2], 0, 380 + i * 10, line_x + 9, TRANS_WHITE);
 }


 if (boss.fight == 0)
 {
  vline(display [2], 320, 0, 480, TRANS_WHITE);//COL_OUTLINE);
  vline(display [2], 319, 0, 480, COL_OUTLINE);
  vline(display [2], 321, 0, 480, COL_OUTLINE);
 }
  else
  {
   vline(display [2], 320, 0, 7, TRANS_WHITE);//COL_OUTLINE);
   vline(display [2], 319, 0, 7, COL_OUTLINE);
   vline(display [2], 321, 0, 7, COL_OUTLINE);
   vline(display [2], 320, 18, 480, TRANS_WHITE);//COL_OUTLINE);
   vline(display [2], 319, 18, 480, COL_OUTLINE);
   vline(display [2], 321, 18, 480, COL_OUTLINE);
  }



 if (boss.fight)
 {
  line_x = (boss.hp * 100) / boss.max_hp;
  drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);
  rectfill(display [2], 320 - line_x, 10, 320 + line_x, 15, TRANS_YELLOW);
  drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);
//  rect(display [2], 320 - line_x - 1, 9, 320 + line_x + 1, 16, COL_OUTLINE);
  rect(display [2], 220 - 2, 8, 420 + 2, 17, COL_OUTLINE);
  rect(display [2], 220 - 3, 7, 420 + 3, 18, COL_WHITE);
  rect(display [2], 220 - 4, 6, 420 + 4, 19, COL_OUTLINE);
  putpixel(display [2], 320, 6, COL_WHITE);
  putpixel(display [2], 320, 19, COL_WHITE);
//  textprintf_ex(display [2], font, 210, 20, -1, -1, "%i", boss.move_time);

 }

 for (i = 0; i < arena.player_lives; i ++)
 {
  rectfill(display[2], 315, 130 - i * 10, 325, 133 - i * 10, TRANS_YELLOW);
  rect(display[2], 314, 129 - i * 10, 326, 134 - i * 10, COL_OUTLINE);
  rect(display[2], 313, 128 - i * 10, 327, 135 - i * 10, TRANS_WHITE);
  rect(display[2], 312, 127 - i * 10, 328, 136 - i * 10, COL_OUTLINE);
  putpixel(display[2], 320, 127 - i * 10, TRANS_WHITE);
  putpixel(display[2], 320, 136 - i * 10, TRANS_WHITE);
 }



  textprintf_right_ex(display [2], font, 315, 25, -1, -1, "%i", player.score);
  textprintf_ex(display [2], font, 325, 25, -1, -1, "%i", options.highscore [arena.difficulty]);




//rectfill(display [2], 310, EGAUGE_Y - 80, 330, EGAUGE_Y, 0);
// if (player.charge > 0)
//  rectfill(display [0], 309, 450 - player.charge / 10, 319, 450, TRANS_YELLOW);
/*
 int line_x = 310, line_x2 = 320;

 if (player.sides == 1)
 {
  line_x = 320;
  line_x2 = 310;
 }

 if (player.charge > 0)
 {
  int seekers = player.charge / 100;
  int col1 = COL_YELLOW; //8 - (arena.counter % 32) / 4;

  int col2 = COL_LRED;

  rectfill(display [2], line_x + 1, EGAUGE_Y, line_x + 9, EGAUGE_Y - seekers * 10, col1);

  rectfill(display [2], line_x + 1, EGAUGE_Y - seekers * 10, line_x + 9, EGAUGE_Y - seekers * 10 - (player.charge % 100) / 10, col2);

  if (player.charge > 300)
   rectfill(display [2], line_x2 + 1, EGAUGE_Y, line_x2 + 9, EGAUGE_Y - player.charge / 10, TRANS_DBLUE + COL_WHITE + (arena.counter / 4) % 3 - 1);//COL_LBLUE);
    else
     rectfill(display [2], line_x2 + 1, EGAUGE_Y, line_x2 + 9, EGAUGE_Y - player.charge / 10, COL_DBLUE);

 }

// rect(display[0], x, 420, x + 10, 340, TRANS_LBLUE);

 int i;

 hline(display[2], line_x2, EGAUGE_Y - 30, line_x2 + 9, TRANS_WHITE);
 hline(display[2], line_x2, EGAUGE_Y - 29, line_x2 + 9, COL_OUTLINE);
 hline(display[2], line_x2, EGAUGE_Y - 31, line_x2 + 9, COL_OUTLINE);

 rect(display [2], 310, EGAUGE_Y - 80, 330, EGAUGE_Y, TRANS_WHITE);
 rect(display [2], 309, EGAUGE_Y - 81, 331, EGAUGE_Y + 1, COL_OUTLINE);
 rect(display [2], 311, EGAUGE_Y - 79, 329, EGAUGE_Y - 1, COL_OUTLINE);
 vline(display [2], 320, EGAUGE_Y, EGAUGE_Y - 80, TRANS_WHITE);
 vline(display [2], 319, EGAUGE_Y, EGAUGE_Y - 80, COL_OUTLINE);
 vline(display [2], 321, EGAUGE_Y, EGAUGE_Y - 80, COL_OUTLINE);

 for (i = EGAUGE_Y - 70; i < EGAUGE_Y; i += 10)
 {
     hline(display[2], line_x, i, line_x + 9, TRANS_WHITE);
     hline(display[2], line_x, i - 1, line_x + 9, COL_OUTLINE);
     hline(display[2], line_x, i + 1, line_x + 9, COL_OUTLINE);
//     hline(display[2], 0, 380 + i * 10, line_x + 9, TRANS_WHITE);
 }

// Now for powerups!


 line_x = 310;

 rect(display [2], 310, PGAUGE_Y - 50, 330, PGAUGE_Y, TRANS_WHITE);
 rect(display [2], 309, PGAUGE_Y - 51, 331, PGAUGE_Y + 1, COL_OUTLINE);
 rect(display [2], 311, PGAUGE_Y - 49, 329, PGAUGE_Y - 1, COL_OUTLINE);
 vline(display [2], 320, PGAUGE_Y, PGAUGE_Y - 50, TRANS_WHITE);
 vline(display [2], 319, PGAUGE_Y, PGAUGE_Y - 50, COL_OUTLINE);
 vline(display [2], 321, PGAUGE_Y, PGAUGE_Y - 50, COL_OUTLINE);

 for (i = PGAUGE_Y - 40; i < PGAUGE_Y; i += 10)
 {
     hline(display[2], line_x, i, line_x + 19, TRANS_WHITE);
     hline(display[2], line_x, i - 1, line_x + 20, COL_OUTLINE);
     hline(display[2], line_x, i + 1, line_x + 20, COL_OUTLINE);
//     hline(display[2], 0, 380 + i * 10, line_x + 9, TRANS_WHITE);
 }
*/

 /*hline(display[0], x, 440, x + 9, TRANS_LBLUE);
 hline(display[0], x, 430, x + 9, TRANS_LBLUE);
 hline(display[0], x, 420, x + 9, TRANS_LBLUE);
 hline(display[0], x, 410, x + 9, TRANS_LBLUE);
 hline(display[0], x, 400, x + 9, TRANS_LBLUE);
 hline(display[0], x, 390, x + 9, TRANS_LBLUE);
 hline(display[0], x, 380, x + 9, TRANS_LBLUE);*/

// if (arena.players == 2 && p == 1)
//  return;

/* for (i = 0; i < arena.player_lives; i ++)
 {
  rectfill(display[2], x, 320 - i * 20, x + 10, 330 - i * 20, TRANS_YELLOW);
  rect(display[2], x, 320 - i * 20, x + 10, 330 - i * 20, TRANS_LBLUE);
 }*/


}


void display_quit_query(void)
{
// if (game.mode_void == 0)
// {
//  drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);
//  rectfill(screen, 140, 150, 500, 280, TRANS_DGREY);
// }
/*
 rect(screen, 140, 150, 500, 280, COLOUR_1);
 rect(screen, 139, 149, 501, 281, COLOUR_1);
 rect(screen, 138, 148, 502, 282, COLOUR_2);
 rect(screen, 137, 147, 503, 283, COLOUR_3);
 rect(screen, 136, 146, 504, 284, COLOUR_4);
*/
 textprintf_centre_ex(screen, font, 320, 180, COL_WHITE, -1, "quit?");
 textprintf_centre_ex(screen, font, 320, 230, COL_WHITE, -1, "y or n");

}

void draw_enemies(void)
{

 int e, w;

 for (w = 0; w < 2; w ++)
 {
  for (e = 0; e < NO_ENEMIES; e ++)
  {
   if (enemy[w][e].type == ENEMY_NONE)
    continue;
   draw_an_enemy(w, e);
  }
 }

}


void draw_an_enemy(int w, int e)
{


/*      circlefill(display [w], x, y, 8, COL_WHITE + enemy[w] [e].hurt_pulse);
      circle(display [w], x, y, 8, COL_OUTLINE);

      circlefill(display [w], x - 7, y + 4, 6, COL_LGREY);
      circle(display [w], x - 7, y + 4, 6, COL_OUTLINE);

      circlefill(display [w], x + 7, y + 4, 6, COL_DGREY);
      circle(display [w], x + 7, y + 4, 6, COL_OUTLINE);*/


    int xa, ya, xb, yb, i;

    int x = enemy[w] [e].x / GRAIN;
    int y = enemy[w] [e].y / GRAIN;


    switch(enemy[w][e].type)
    {
      case ENEMY_MB1:
       xa = 6 + grand(4);
       xb = explode_rle [0] [xa].x;
       draw_rle_sprite(display [w], eRLE_big [L_ENEMY_MB1], x - 74, y - 31 + 20);
       draw_trans_rle_sprite(display[w], explode_rle [0] [xa].sprite, x - xb - 38 - grand(3), y - xb + 47 + grand(4));
       draw_trans_rle_sprite(display[w], explode_rle [0] [xa].sprite, x - xb + 38 + grand(3), y - xb + 47 + grand(4));
       circlefill(display [w], x - 39, y + 20, 4, COL_DBLUE);
       circle(display [w], x - 39, y + 20, 4, COL_OUTLINE);
       circlefill(display [w], x - 39 + xpart(boss.angle_1, 3), y + 20 + ypart(boss.angle_1, 3), 2, COL_LBLUE);
       circle(display [w], x - 39 + xpart(boss.angle_1, 3), y + 20 + ypart(boss.angle_1, 3), 2, COL_OUTLINE);

       circlefill(display [w], x + 39, y + 20, 4, COL_DBLUE);
       circle(display [w], x + 39, y + 20, 4, COL_OUTLINE);
       circlefill(display [w], x + 39 + xpart(boss.angle_2, 3), y + 20 + ypart(boss.angle_2, 3), 2, COL_LBLUE);
       circle(display [w], x + 39 + xpart(boss.angle_2, 3), y + 20 + ypart(boss.angle_2, 3), 2, COL_OUTLINE);

       circlefill(display [w], x, y + 28, 4, COL_DBLUE);
       circle(display [w], x, y + 28, 4, COL_OUTLINE);

       circlefill(display [w], x + xpart(boss.angle_3, 5), y + 28 + ypart(boss.angle_3, 5), 2, COL_LBLUE);
       circle(display [w], x + xpart(boss.angle_3, 5), y + 28 + ypart(boss.angle_3, 5), 2, COL_OUTLINE);
       circlefill(display [w], x + xpart(boss.angle_3 + ANGLE_3, 5), y + 28 + ypart(boss.angle_3 + ANGLE_3, 5), 2, COL_LBLUE);
       circle(display [w], x + xpart(boss.angle_3 + ANGLE_3, 5), y + 28 + ypart(boss.angle_3 + ANGLE_3, 5), 2, COL_OUTLINE);
       circlefill(display [w], x + xpart(boss.angle_3 - ANGLE_3, 5), y + 28 + ypart(boss.angle_3 - ANGLE_3, 5), 2, COL_LBLUE);
       circle(display [w], x + xpart(boss.angle_3 - ANGLE_3, 5), y + 28 + ypart(boss.angle_3 - ANGLE_3, 5), 2, COL_OUTLINE);


       break;
      case ENEMY_BFLOWER:
       xa = 1;//(10 * enemy[w][e].angle2) / 100;
       int c1, c2;
       switch(enemy[w][e].angle3)
       {
        case 0: c1 = COL_LRED; c2 = COL_DRED; break;
        case 1: c1 = COL_YELLOW; c2 = COL_DGREEN; break;
        case 2: c1 = COL_LBLUE; c2 = COL_DBLUE; break;
       }
       xa = (7 * enemy[w][e].angle2) / 100;
       xb = (14 * enemy[w][e].angle2) / 100;
       circlefill(display[w], x + xpart(enemy[w][e].angle1, xb), y + ypart(enemy[w][e].angle1, xb), xa, c2);
       circle(display[w], x + xpart(enemy[w][e].angle1, xb), y + ypart(enemy[w][e].angle1, xb), xa, COL_OUTLINE);
       circlefill(display[w], x + xpart(enemy[w][e].angle1 + ANGLE_3, xb), y + ypart(enemy[w][e].angle1 + ANGLE_3, xb), xa, c2);
       circle(display[w], x + xpart(enemy[w][e].angle1 + ANGLE_3, xb), y + ypart(enemy[w][e].angle1 + ANGLE_3, xb), xa, COL_OUTLINE);
       circlefill(display[w], x + xpart(enemy[w][e].angle1 - ANGLE_3, xb), y + ypart(enemy[w][e].angle1 - ANGLE_3, xb), xa, c2);
       circle(display[w], x + xpart(enemy[w][e].angle1 - ANGLE_3, xb), y + ypart(enemy[w][e].angle1 - ANGLE_3, xb), xa, COL_OUTLINE);

       xa = (10 * enemy[w][e].angle2) / 100;
       circlefill(display[w], x, y, xa, c1);
       circle(display[w], x, y, xa, COL_OUTLINE);
       break;
      case ENEMY_BOSS2:
       xa = (20 * boss.size) / 100;

       int col1;
       int col2;

       switch(boss.colour)
       {
        case 0: col1 = COL_LRED; break;
        case 1: col1 = COL_YELLOW; break;
        case 2: col1 = COL_LBLUE; break;
       }

       circlefill(display [w], x, y, xa, col1);
       circle(display [w], x, y, xa, COL_OUTLINE);
       xa = (11 * boss.size) / 100;
       xb = (7 * boss.size) / 100;
       switch(boss.colour)
       {
        case 0: col1 = COL_LBLUE; col2 = COL_DBLUE; break;
        case 1: col1 = COL_LRED; col2 = COL_DRED; break;
        case 2: col1 = COL_YELLOW; col2 = COL_DGREEN; break;
       }

       for (i = 0; i < 8; i ++)
       {
        circlefill(display [w], x + xpart(enemy[w][e].angle1 + i * ANGLE_8, boss.size), y + ypart(enemy[w][e].angle1 + i * ANGLE_8, boss.size), xa, col2);
        circle(display [w], x + xpart(enemy[w][e].angle1 + i * ANGLE_8, boss.size), y + ypart(enemy[w][e].angle1 + i * ANGLE_8, boss.size), xa, COL_OUTLINE);
        circlefill(display [w], x + xpart(enemy[w][e].angle1 + i * ANGLE_8, boss.size - 3), y + ypart(enemy[w][e].angle1 + i * ANGLE_8, boss.size - 3), xb, col1);
        circle(display [w], x + xpart(enemy[w][e].angle1 + i * ANGLE_8, boss.size - 3), y + ypart(enemy[w][e].angle1 + i * ANGLE_8, boss.size - 3), xb, COL_OUTLINE);
       }
       xa = boss.size / 2;
       xb = (15 * boss.size) / 100;
       switch(boss.colour)
       {
        case 0: col1 = COL_YELLOW; col2 = COL_DGREEN; break;
        case 1: col1 = COL_LBLUE; col2 = COL_DBLUE; break;
        case 2: col1 = COL_LRED; col2 = COL_DRED; break;
       }
       for (i = 0; i < 6; i ++)
       {
        circlefill(display [w], x + xpart(ANGLE_1 - (enemy[w][e].angle1 + i * ANGLE_6), xa), y + ypart(ANGLE_1 - (enemy[w][e].angle1 + i * ANGLE_6), xa), xb, col2);
        circle(display [w], x + xpart(ANGLE_1 - (enemy[w][e].angle1 + i * ANGLE_6), xa), y + ypart(ANGLE_1 - (enemy[w][e].angle1 + i * ANGLE_6), xa), xb, COL_OUTLINE);
       }
       xa = (45 * boss.size) / 100;
       xb = (9 * boss.size) / 100;
       for (i = 0; i < 6; i ++)
       {
        circlefill(display [w], x + xpart(ANGLE_1 - (enemy[w][e].angle1 + i * ANGLE_6), xa), y + ypart(ANGLE_1 - (enemy[w][e].angle1 + i * ANGLE_6), xa), xb, col1);
        circle(display [w], x + xpart(ANGLE_1 - (enemy[w][e].angle1 + i * ANGLE_6), xa), y + ypart(ANGLE_1 - (enemy[w][e].angle1 + i * ANGLE_6), xa), xb, COL_OUTLINE);
       }
       break;
      case ENEMY_FLOWER1:
        xb = enemy[w][e].angle1;
        if (w == 1)
         xb = 16 - xb;
        if (xb < 0)
         xb = 0;
        if (xb > 15)
         xb = 15;
        xa = eRLE_flower [0] [xb].x;
        ya = eRLE_flower [0] [xb].y;
        draw_rle_sprite(display [w], eRLE_flower [0] [xb].sprite, x - xa, y - ya);
//        circlefill(display [w], x, y, 10, COL_WHITE);
        break;
      case ENEMY_FLOWER2:
        xb = enemy[w][e].angle1;
        if (w == 1)
         xb = 20 - xb;
        if (xb < 0)
         xb = 0;
        if (xb > 19)
         xb = 19;
        xa = eRLE_flower [1] [xb].x;
        ya = eRLE_flower [1] [xb].y;
        draw_rle_sprite(display [w], eRLE_flower [1] [xb].sprite, x - xa, y - ya);
        break;
      case ENEMY_FLOWER3:
        xb = enemy[w][e].angle1;
        if (w == 1)
         xb = 20 - xb;
        if (xb < 0)
         xb = 0;
        if (xb > 19)
         xb = 19;
        xa = eRLE_flower [2] [xb].x;
        ya = eRLE_flower [2] [xb].y;
        draw_rle_sprite(display [w], eRLE_flower [2] [xb].sprite, x - xa, y - ya);
        break;
      case ENEMY_FLOWER4:
        xb = enemy[w][e].angle1;
        if (w == 1)
         xb = 20 - xb;
        if (xb < 0)
         xb = 0;
        if (xb > 19)
         xb = 19;
        xa = eRLE_flower [3] [xb].x;
        ya = eRLE_flower [3] [xb].y;
        draw_rle_sprite(display [w], eRLE_flower [3] [xb].sprite, x - xa, y - ya);
        break;
      case ENEMY_FLOWER5:
        xb = enemy[w][e].angle1;
        if (w == 1)
         xb = 32 - xb;
        if (xb < 0)
         xb = 0;
        if (xb > 31)
         xb = 31;
        xa = eRLE_flower [4] [xb].x;
        ya = eRLE_flower [4] [xb].y;
        draw_rle_sprite(display [w], eRLE_flower [4] [xb].sprite, x - xa, y - ya);
        break;
      case ENEMY_GLIDER1:
//       xa = x - 39;
//       ya = y - 2;
       xa = x - 34;
       ya = y - 0;
       xa += xpart(-ANGLE_4 -ANGLE_8 - ANGLE_32, enemy[w][e].angle3 / 3);
       ya += ypart(-ANGLE_4 -ANGLE_8 - ANGLE_32, enemy[w][e].angle3 / 3);
       draw_rle_sprite(display [w], eRLE_basic1 [S_ENEMY_GLIDER1_LFOOT], xa, ya + GLIDER_Y_ADJUST);
       xa = x + 6;
       ya = y;
       xa -= xpart(-ANGLE_4 -ANGLE_8 - ANGLE_32, enemy[w][e].angle3 / 3);
       ya += ypart(-ANGLE_4 -ANGLE_8 - ANGLE_32, enemy[w][e].angle3 / 3);
       draw_rle_sprite(display [w], eRLE_basic1 [S_ENEMY_GLIDER1_RFOOT], xa, ya + GLIDER_Y_ADJUST);
       draw_rle_sprite(display [w], eRLE_big [L_ENEMY_GLIDER1], x - 43, y - 50 + GLIDER_Y_ADJUST);
       circlefill(display [w], x, y + GLIDER_Y_ADJUST, 10, COL_DRED);
       circle(display [w], x, y + GLIDER_Y_ADJUST, 10, COL_OUTLINE);

       for (i = 0; i < 6; i ++)
       {
        xa = enemy[w][e].angle2 + i * (ANGLE_6);
        circlefill(display [w], x + xpart(xa, 9), y + ypart(xa, 9) + GLIDER_Y_ADJUST, 3, COL_LRED);
        circle(display [w], x + xpart(xa, 9), y + ypart(xa, 9) + GLIDER_Y_ADJUST, 3, COL_OUTLINE);
       }
       break;
       case ENEMY_BURSTER:
        xa = xpart(-ANGLE_4 - ANGLE_8, enemy[w][e].angle2 / 2);
        ya = ypart(-ANGLE_4 - ANGLE_8, enemy[w][e].angle2 / 2);
        draw_rle_sprite(display [w], eRLE_big [L_ENEMY_BURSTER_LWING1], x - 28 + xa, y - 28 + ya);
        draw_rle_sprite(display [w], eRLE_big [L_ENEMY_BURSTER_RWING1], x -1 - xa, y - 28 + ya);

        xa = xpart(ANGLE_4 + ANGLE_8, enemy[w][e].angle2 / 2);
        ya = ypart(ANGLE_4 + ANGLE_8, enemy[w][e].angle2 / 2);
        draw_rle_sprite(display [w], eRLE_big [L_ENEMY_BURSTER_LWING2], x - 25+ xa, y - 1 + ya);
        draw_rle_sprite(display [w], eRLE_big [L_ENEMY_BURSTER_RWING2], x -1 - xa, y - 1 + ya);

        draw_rle_sprite(display [w], eRLE_big [L_ENEMY_BURSTER], x - 13, y - 13);
        xa = 6 + grand(3);
        xb = explode_rle [2] [xa].x;
        draw_trans_rle_sprite(display[w], explode_rle [2] [xa].sprite, x - xb, y - xb);
        break;
       case ENEMY_CARRIER:
//      enemy[w][e].angle2 = 0;
        //enemy[w][e].angle3 = 0;
        xa = x - 31 - enemy[w][e].angle2 / 5;
        draw_rle_sprite(display [w], eRLE_big [L_ENEMY_CARRIER_LWING1], xa, y - 31);
        xa = x - 3 + enemy[w][e].angle2 / 5;
        draw_rle_sprite(display [w], eRLE_big [L_ENEMY_CARRIER_RWING1], xa, y - 31);
        xa = x - 31 - enemy[w][e].angle3 / 5;
        draw_rle_sprite(display [w], eRLE_big [L_ENEMY_CARRIER_LWING2], xa, y + 0);
        xa = x - 3 + enemy[w][e].angle3 / 5;
        draw_rle_sprite(display [w], eRLE_big [L_ENEMY_CARRIER_RWING2], xa, y + 0);
        draw_rle_sprite(display [w], eRLE_big [L_ENEMY_CARRIER], x - 22, y - 22);
        circlefill(display [w], x, y, 9, COL_LGREY);
        circle(display [w], x, y, 9, COL_OUTLINE);
        circlefill(display [w], x + xpart(enemy[w][e].angle1, 9), y + ypart(enemy[w][e].angle1, 9), 3, COL_WHITE);
        circle(display [w], x + xpart(enemy[w][e].angle1, 9), y + ypart(enemy[w][e].angle1, 9), 3, COL_OUTLINE);
        circlefill(display [w], x + xpart(enemy[w][e].angle1 + ANGLE_2, 9), y + ypart(enemy[w][e].angle1 + ANGLE_2, 9), 3, COL_WHITE);
        circle(display [w], x + xpart(enemy[w][e].angle1 + ANGLE_2, 9), y + ypart(enemy[w][e].angle1 + ANGLE_2, 9), 3, COL_OUTLINE);

/*        xa = 4 + grand(3);
        xb = (explode_rle [0] [xa]->w) / 2;
        draw_trans_rle_sprite(display[w], explode_rle [0] [xa], x - xb - 7, y - xb + 25 - grand(2));
        draw_trans_rle_sprite(display[w], explode_rle [0] [xa], x - xb + 8, y - xb + 25 - grand(2));
        xa = x - 20 - enemy[w][e].angle2 / 10;
        draw_rle_sprite(display [w], eRLE_basic1 [S_ENEMY_CARRIER_LWING1], xa, y - 18);
        xa = x + 10 + enemy[w][e].angle2 / 10;
        draw_rle_sprite(display [w], eRLE_basic1 [S_ENEMY_CARRIER_RWING1], xa, y - 18);
        xa = x - 28 - enemy[w][e].angle3 / 10;
        draw_rle_sprite(display [w], eRLE_basic1 [S_ENEMY_CARRIER_LWING2], xa, y - 2);
        xa = x + 13 + enemy[w][e].angle3 / 10;
        draw_rle_sprite(display [w], eRLE_basic1 [S_ENEMY_CARRIER_RWING2], xa, y - 2);*/
        break;
       case ENEMY_BEAMER1:
        xb = enemy[w][e].angle2 / 3;
        xa = xpart(-ANGLE_4 - ANGLE_8, xb);
        ya = ypart(-ANGLE_4 - ANGLE_8, xb);
        draw_rle_sprite(display [w], eRLE_big [L_ENEMY_BEAMER1_LFIN], x - 32 + xa, y + 19 + ya);
        xa = xpart(-ANGLE_8, xb);
        ya = ypart(-ANGLE_8, xb);
        draw_rle_sprite(display [w], eRLE_big [L_ENEMY_BEAMER1_RFIN], x + 11 + xa, y + 19 + ya);
        xa = xpart(ANGLE_4 + ANGLE_8, xb);
        ya = ypart(ANGLE_4 + ANGLE_8, xb);
        draw_rle_sprite(display [w], eRLE_big [L_ENEMY_BEAMER1_LFIN2], x - 32 + xa, y - 71 + ya);
        xa = xpart(ANGLE_8, xb);
        ya = ypart(ANGLE_8, xb);
        draw_rle_sprite(display [w], eRLE_big [L_ENEMY_BEAMER1_RFIN2], x + 11 + xa, y - 71 + ya);
        draw_rle_sprite(display [w], eRLE_big [L_ENEMY_BEAMER1], x - 34, y - 33);
        circlefill(display[w], x, y, 8, COL_DBLUE);
        circle(display[w], x, y, 8, COL_OUTLINE);
        circlefill(display[w], x - xpart(enemy[w][e].angle1, 7), y - ypart(enemy[w][e].angle1, 7), 4, COL_LBLUE);
        circle(display[w], x - xpart(enemy[w][e].angle1, 7), y - ypart(enemy[w][e].angle1, 7), 4, COL_OUTLINE);
        break;
       case ENEMY_BOSS1:
        draw_rle_sprite(display [w], eRLE_big [L_ENEMY_BOSS1], x - 47, y - 45);
        xa = boss.arm1 / 16;
        xb = boss.arm1 / 8;
        ya = boss.arm2 / 8;
        yb = xpart(boss.pulse, 4);
        draw_rle_sprite(display [w], eRLE_big [L_ENEMY_BOSS1_L1], x - 57 - xb, y - 25 + ya + yb);
        draw_rle_sprite(display [w], eRLE_big [L_ENEMY_BOSS1_L2], x - 40 - xa, y - 48 - ya - yb);
        draw_rle_sprite(display [w], eRLE_big [L_ENEMY_BOSS1_R1], x + 19 + xa, y - 48 - ya - yb);
    draw_rle_sprite(display [w], eRLE_big [L_ENEMY_BOSS1_R2], x + 36 + xb, y - 25 + ya + yb);
/*        draw_rle_sprite(display [w], eRLE_big [L_ENEMY_BOSS1_L1], x - 59 - xb, y - 25 + ya + yb);
        draw_rle_sprite(display [w], eRLE_big [L_ENEMY_BOSS1_L2], x - 49 - xa, y - 48 - ya - yb);
        draw_rle_sprite(display [w], eRLE_big [L_ENEMY_BOSS1_R1], x + 20 + xa, y - 48 - ya - yb);
        draw_rle_sprite(display [w], eRLE_big [L_ENEMY_BOSS1_R2], x + 30 + xb, y - 25 + ya + yb);*/

         int xa2 = 5 + grand(3);
         if (xa2 > boss.arm_change / 4)
          xa2 = boss.arm_change / 4;
          if (xa2 < 1)
           xa2 = 1;

         int xb2 = explode_rle [1] [xa2].x;
         if (boss.arm_colour != -1)
         {
          draw_trans_rle_sprite(display[w], explode_rle [boss.arm_colour] [xa2].sprite, x - xb2 - 29 - xb, y - xb2 + 40 + ya + yb);
          draw_trans_rle_sprite(display[w], explode_rle [boss.arm_colour] [xa2].sprite, x - xb2 + 29 + xb, y - xb2 + 40 + ya + yb);
          draw_trans_rle_sprite(display[w], explode_rle [boss.arm_colour] [xa2].sprite, x - xb2 - 48 - xa, y - xb2 - 40 - ya - yb);
          draw_trans_rle_sprite(display[w], explode_rle [boss.arm_colour] [xa2].sprite, x - xb2 + 48 + xa, y - xb2 - 40 - ya - yb);
         }

        if (boss.exploding == 0)
        {
         circlefill(display[w], x, y + 13, 9, COL_LGREY);
         circle(display[w], x, y + 13, 9, COL_OUTLINE);
         for (i = 0; i < 8; i ++)
         {
          xa = (enemy[w][e].angle1) + i * (ANGLE_8);
          circlefill(display [w], x + xpart(xa, 9), y + ypart(xa, 9) + 13, 3, COL_WHITE);
          circle(display [w], x + xpart(xa, 9), y + ypart(xa, 9) + 13, 3, COL_OUTLINE);
         }
        }
        break;

       case ENEMY_SHOTTER1:
        draw_rle_sprite(display [w], eRLE_big [L_ENEMY_SHOTTER1], x - 54, y - 51 + SHOTTER1_Y_ADJUST);
        if (enemy[w][e].recycle < 50)
        {
         xa = 5 + grand(3) + (50 - enemy[w][e].recycle) / 7;
         xb = explode_rle [1] [xa].x;
         draw_trans_rle_sprite(display[w], explode_rle [1] [xa].sprite, x - xb, y - xb + SHOTTER1_Y_ADJUST);
         for (i = 0; i < 6; i ++)
         {
          xa = i * ANGLE_6;
          xb = (50 - enemy[w][e].recycle) / 12;
          ya = enemy[w][e].recycle + 10;
//        yb = (explode_rle [1] [xb]->w) / 2;
          drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);
          circlefill(display [w], x + xpart(xa, ya), y + ypart(xa, ya) + SHOTTER1_Y_ADJUST, xb + grand(2), TRANS_LGREEN);
          circlefill(display [w], x + xpart(xa, ya), y + ypart(xa, ya) + SHOTTER1_Y_ADJUST, xb + grand(5), TRANS_DGREEN);
          drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);
         }
        }
         else
         {
          xa = 5 + grand(3);
          xb = explode_rle [1] [xa].x;
          draw_trans_rle_sprite(display[w], explode_rle [1] [xa].sprite, x - xb, y - xb + SHOTTER1_Y_ADJUST);
         }

        break;
       case ENEMY_ZAPPER1:
        draw_rle_sprite(display [w], eRLE_big [L_ENEMY_ZAPPER1], x - 44, y - 34);
        circlefill(display [w], x, y, 15, COL_DRED);
        circle(display [w], x, y, 15, COL_OUTLINE);
        xa = enemy[w][e].angle1;
        circlefill(display [w], x + xpart(xa, 14), y + ypart(xa, 14), 3, COL_LRED);
        circle(display [w], x + xpart(xa, 14), y + ypart(xa, 14), 3, COL_OUTLINE);

        xa = 5 + grand(3);
        xb = explode_rle [0] [xa].x;
        draw_trans_rle_sprite(display[w], explode_rle [0] [xa].sprite, x - 33 - xb, y - xb);
        draw_trans_rle_sprite(display[w], explode_rle [0] [xa].sprite, x + 33 - xb, y - xb);


/*        circlefill(display [w], x - 33, y, 8, COL_DRED);
        circle(display [w], x - 33, y, 8, COL_OUTLINE);
        xa = enemy[w][e].angle2);
        circlefill(display [w], x - 33 + xpart(xa, 9), y + ypart(xa, 9), 3, COL_LRED);
        circle(display [w], x - 33 + xpart(xa, 9), y + ypart(xa, 9), 3, COL_OUTLINE);

        circlefill(display [w], x + 33, y, 8, COL_DRED);
        circle(display [w], x + 33, y, 8, COL_OUTLINE);
        xa = enemy[w][e].angle3);
        circlefill(display [w], x + 33 + xpart(xa, 9), y + ypart(xa, 9), 3, COL_LRED);
        circle(display [w], x + 33 + xpart(xa, 9), y + ypart(xa, 9), 3, COL_OUTLINE);*/

        break;
       case ENEMY_DIPPER1:
        xa = enemy[w][e].angle2 / 5;
        if (xa < 0) xa = 0;
        if (xa > 2) xa = 2;
        draw_rle_sprite(display [w], eRLE_big [L_ENEMY_DIPPER1_1 + xa], x - 23, y - 19);
//        xa = enemy[w][e].angle1;
        xa = 5 + grand(3);
        xb = explode_rle [0] [xa].x;
        draw_trans_rle_sprite(display[w], explode_rle [0] [xa].sprite, x - xb - 1 + grand(3), y - xb - 1 + grand(3));

//        circlefill(display [w], x + xpart(xa, 4), y + ypart(xa, 4), 3, COL_LRED);
//        circle(display [w], x + xpart(xa, 4), y + ypart(xa, 4), 3, COL_OUTLINE);
        break;
       case ENEMY_DIPPER2:
        xa = enemy[w][e].angle2 / 5;
        if (xa < 0) xa = 0;
        if (xa > 2) xa = 2;
        draw_rle_sprite(display [w], eRLE_big [L_ENEMY_DIPPER2_1 + xa], x - 23, y - 19);
//        xa = enemy[w][e].angle1;
        xa = 3 + grand(3);
        xb = explode_rle [1] [xa].x;
        draw_trans_rle_sprite(display[w], explode_rle [1] [xa].sprite, x - xb - 1 + grand(3), y - xb - 1 + grand(3));

//        circlefill(display [w], x + xpart(xa, 4), y + ypart(xa, 4), 3, COL_LRED);
//        circle(display [w], x + xpart(xa, 4), y + ypart(xa, 4), 3, COL_OUTLINE);
        break;
      case ENEMY_CROSS:
       drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);
       triangle(display[w], x + 5, y,
        x + 5 + xpart(enemy[w][e].angle2, 25), y + ypart(enemy[w][e].angle2, 25),
        x + 5 + xpart(enemy[w][e].angle3, 25), y + ypart(enemy[w][e].angle3, 25),
        TRANS_BLACK);
       triangle(display[w], x - 5, y,
        x - 5 - xpart(ANGLE_1 - enemy[w][e].angle2, 25), y - ypart(ANGLE_1 - enemy[w][e].angle2, 25),
        x - 5 - xpart(ANGLE_1 - enemy[w][e].angle3, 25), y - ypart(ANGLE_1 - enemy[w][e].angle3, 25),
        TRANS_BLACK);
       drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);
       xa = (enemy[w][e].angle2 + ANGLE_4) / 150;//xpart(arena.counter * 32, 2) + 2;
//       if (xa < 0)
//        xa = 0;
       draw_rle_sprite(display [w], eRLE_basic1 [S_ENEMY_CROSS_L], x - 9 - 4 + xa, y - 11);
       draw_rle_sprite(display [w], eRLE_basic1 [S_ENEMY_CROSS_R], x  + 4 - xa, y - 11);
       circlefill(display [w], x, y, 3, COL_DGREEN);
       circle(display [w], x, y, 3, COL_OUTLINE);
       break;
      case ENEMY_CROSS2:
       drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);
       triangle(display[w], x + 3, y,
        x + 5 + xpart(enemy[w][e].angle2, 21), y + ypart(enemy[w][e].angle2, 21),
        x + 5 + xpart(enemy[w][e].angle3, 21), y + ypart(enemy[w][e].angle3, 21),
        TRANS_BLACK);
       triangle(display[w], x - 3, y,
        x - 5 - xpart(ANGLE_1 - enemy[w][e].angle2, 21), y - ypart(ANGLE_1 - enemy[w][e].angle2, 21),
        x - 5 - xpart(ANGLE_1 - enemy[w][e].angle3, 21), y - ypart(ANGLE_1 - enemy[w][e].angle3, 21),
        TRANS_BLACK);
       drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);
       xa = (enemy[w][e].angle2 + ANGLE_4) / 150;//xpart(arena.counter * 32, 2) + 2;
       if (xa < -2)
        xa = -2;
       if (xa > 2)
        xa = 2;
       draw_rle_sprite(display [w], eRLE_basic1 [S_ENEMY_CROSS2_1], x - 7 + xa, y + 2);
       draw_rle_sprite(display [w], eRLE_basic1 [S_ENEMY_CROSS2_2], x - xa, y + 2);
       draw_rle_sprite(display [w], eRLE_basic1 [S_ENEMY_CROSS2_3], x - 7, y - 8);
       break;
      case ENEMY_WINGS:
       drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);
       triangle(display[w], x + 19, y - 14,
        x + 14 + xpart(enemy[w][e].angle2, 41), y + ypart(enemy[w][e].angle2, 41) - 14,
        x + 14 + xpart(enemy[w][e].angle3, 41), y + ypart(enemy[w][e].angle3, 41) - 14,
        TRANS_YELLOW);
       triangle(display[w], x + 19, y - 14,
        x + 14 + xpart(enemy[w][e].angle3, 41), y + ypart(enemy[w][e].angle3, 41) - 14,
        x + 14 + xpart(enemy[w][e].c1, 41), y + ypart(enemy[w][e].c1, 41) - 14,
        TRANS_LGREEN);
       triangle(display[w], x + 19, y - 14,
        x + 14 + xpart(enemy[w][e].c1, 41), y + ypart(enemy[w][e].c1, 41) - 14,
        x + 14 + xpart(enemy[w][e].d1, 41), y + ypart(enemy[w][e].d1, 41) - 14,
        TRANS_DGREEN);

       triangle(display[w], x - 19, y - 14,
        x - 14 - xpart(ANGLE_1 - enemy[w][e].angle2, 41), y - ypart(ANGLE_1 - enemy[w][e].angle2, 41) - 14,
        x - 14 - xpart(ANGLE_1 - enemy[w][e].angle3, 41), y - ypart(ANGLE_1 - enemy[w][e].angle3, 41) - 14,
        TRANS_YELLOW);
       triangle(display[w], x - 19, y - 14,
        x - 14 - xpart(ANGLE_1 - enemy[w][e].angle3, 41), y - ypart(ANGLE_1 - enemy[w][e].angle3, 41) - 14,
        x - 14 - xpart(ANGLE_1 - enemy[w][e].c1, 41), y - ypart(ANGLE_1 - enemy[w][e].c1, 41) - 14,
        TRANS_LGREEN);
       triangle(display[w], x - 19, y - 14,
        x - 14 - xpart(ANGLE_1 - enemy[w][e].c1, 41), y - ypart(ANGLE_1 - enemy[w][e].c1, 41) - 14,
        x - 14 - xpart(ANGLE_1 - enemy[w][e].d1, 41), y - ypart(ANGLE_1 - enemy[w][e].d1, 41) - 14,
        TRANS_DGREEN);
       drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);
//        draw_rle_sprite(display [w], eRLE_big [L_ENEMY_WINGS], x - 20, y - 27);
        rectfill(display [w], x - 8, y - 12, x + 8, y - 6, COL_DRED);
        hline(display [2], x-8, y-13, x + 8, COL_OUTLINE);
        hline(display [2], x-8, y-5, x + 8, COL_OUTLINE);

       xa = (enemy[w][e].angle2 + ANGLE_4) / 150;//150;
       draw_rle_sprite(display [w], eRLE_big [L_ENEMY_WINGS_L], x - 19 - 4 + xa, y - 24);
       draw_rle_sprite(display [w], eRLE_big [L_ENEMY_WINGS_R], x + 4 - xa, y - 24);
//       circlefill(display [w], x, y - 8, 3, COL_YELLOW);
//       circle(display [w], x, y - 8, 3, COL_OUTLINE);

       break;
      case ENEMY_CARRIER2:
       circlefill(display [w], x, y, 20, COL_LGREY);
       circle(display [w], x, y, 20, COL_OUTLINE);
       for (i = 0; i < 8; i ++)
       {
        circlefill(display [w], x + xpart(enemy[w][e].angle1 + i * ANGLE_8, 21), y + ypart(enemy[w][e].angle1 + i * ANGLE_8, 21), 4, COL_WHITE);
        circle(display [w], x + xpart(enemy[w][e].angle1 + i * ANGLE_8, 21), y + ypart(enemy[w][e].angle1 + i * ANGLE_8, 21), 4, COL_OUTLINE);
       }
       for (i = 0; i < 4; i ++)
       {
        circlefill(display [w], x + xpart(ANGLE_1 - (enemy[w][e].angle1 + i * ANGLE_4), 9), y + ypart(ANGLE_1 - (enemy[w][e].angle1 + i * ANGLE_4), 9), 4, COL_LBLUE);
        circle(display [w], x + xpart(ANGLE_1 - (enemy[w][e].angle1 + i * ANGLE_4), 9), y + ypart(ANGLE_1 - (enemy[w][e].angle1 + i * ANGLE_4), 9), 4, COL_OUTLINE);
       }

       break;
      case ENEMY_MB2:
       drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);
       ya = boss.move_time;
       triangle(display[w], x + 19, y - 14,
        x + 14 + xpart(enemy[w][e].angle2, ya), y + ypart(enemy[w][e].angle2, ya) - 14,
        x + 14 + xpart(boss.old_angle [1], ya), y + ypart(boss.old_angle [1], ya) - 14,
        TRANS_YELLOW);

       triangle(display[w], x + 19, y - 14,
        x + 14 + xpart(boss.old_angle [3], ya), y + ypart(boss.old_angle [3], ya) - 14,
        x + 14 + xpart(boss.old_angle [5], ya), y + ypart(boss.old_angle [5], ya) - 14,
        TRANS_LGREEN);

       triangle(display[w], x + 19, y - 14,
        x + 14 + xpart(boss.old_angle [7], ya), y + ypart(boss.old_angle [7], ya) - 14,
        x + 14 + xpart(boss.old_angle [9], ya), y + ypart(boss.old_angle [9], ya) - 14,
        TRANS_DGREEN);

       triangle(display[w], x - 19, y - 14,
        x - 14 - xpart(ANGLE_1 - enemy[w][e].angle2, ya), y - ypart(ANGLE_1 - enemy[w][e].angle2, ya) - 14,
        x - 14 - xpart(ANGLE_1 - boss.old_angle [1], ya), y - ypart(ANGLE_1 - boss.old_angle [1], ya) - 14,
        TRANS_YELLOW);

       triangle(display[w], x - 19, y - 14,
        x - 14 - xpart(ANGLE_1 - boss.old_angle [3], ya), y - ypart(ANGLE_1 - boss.old_angle [3], ya) - 14,
        x - 14 - xpart(ANGLE_1 - boss.old_angle [5], ya), y - ypart(ANGLE_1 - boss.old_angle [5], ya) - 14,
        TRANS_LGREEN);

       triangle(display[w], x - 19, y - 14,
        x - 14 - xpart(ANGLE_1 - boss.old_angle [7], ya), y - ypart(ANGLE_1 - boss.old_angle [7], ya) - 14,
        x - 14 - xpart(ANGLE_1 - boss.old_angle [9], ya), y - ypart(ANGLE_1 - boss.old_angle [9], ya) - 14,
        TRANS_DGREEN);

/*       triangle(display[w], x + 19, y - 14,
        x + 14 + xpart(enemy[w][e].angle2, ya), y + ypart(enemy[w][e].angle2, ya) - 14,
        x + 14 + xpart(enemy[w][e].angle3, ya), y + ypart(enemy[w][e].angle3, ya) - 14,
        TRANS_BLACK);

       ya -= 4;
       triangle(display[w], x + 19, y - 14,
        x + 14 + xpart(enemy[w][e].angle2 + ANGLE_8, ya), y + ypart(enemy[w][e].angle2 + ANGLE_8, ya) - 14,
        x + 14 + xpart(enemy[w][e].angle3 + ANGLE_8, ya), y + ypart(enemy[w][e].angle3 + ANGLE_8, ya) - 14,
        TRANS_BLACK);

       ya -= 4;
       triangle(display[w], x + 19, y - 14,
        x + 14 + xpart(enemy[w][e].angle2 + ANGLE_4, ya), y + ypart(enemy[w][e].angle2 + ANGLE_4, ya) - 14,
        x + 14 + xpart(enemy[w][e].angle3 + ANGLE_4, ya), y + ypart(enemy[w][e].angle3 + ANGLE_4, ya) - 14,
        TRANS_BLACK);*/

/*       triangle(display[w], x - 19, y - 14,
        x - 14 - xpart(ANGLE_1 - enemy[w][e].angle2, 41), y - ypart(ANGLE_1 - enemy[w][e].angle2, 41) - 14,
        x - 14 - xpart(ANGLE_1 - enemy[w][e].angle3, 41), y - ypart(ANGLE_1 - enemy[w][e].angle3, 41) - 14,
        TRANS_YELLOW);*/
       drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);
       xa = (enemy[w][e].angle2 + ANGLE_4) / 100;
       draw_rle_sprite(display [w], eRLE_big [L_ENEMY_MB2_L], x - 31 - 4 + xa, y - 35);
       draw_rle_sprite(display [w], eRLE_big [L_ENEMY_MB2_R], x + 5 - xa, y - 35);

        y -= 14;
       circlefill(display[w], x, y, 8, COL_LRED);
       circle(display[w], x, y, 8, COL_OUTLINE);
//     xa /= 2;
       for (i = 0; i < 4; i ++)
       {
        circlefill(display[w], x + xpart(ANGLE_8 + ANGLE_4 * i, 6 + xa), y + ypart(ANGLE_8 + ANGLE_4 * i, 6 + xa), 3, COL_YELLOW);
        circle(display[w], x + xpart(ANGLE_8 + ANGLE_4 * i, 6 + xa), y + ypart(ANGLE_8 + ANGLE_4 * i, 6 + xa), 3, COL_OUTLINE);
       }
/*       circlefill(display[w], x + 4, y + 4, 3, COL_YELLOW);
       circle(display[w], x + 4, y + 4, 3, COL_OUTLINE);
       circlefill(display[w], x + 4, y - 4, 3, COL_YELLOW);
       circle(display[w], x + 4, y - 4, 3, COL_OUTLINE);
       circlefill(display[w], x - 4, y - 4, 3, COL_YELLOW);
       circle(display[w], x - 4, y - 4, 3, COL_OUTLINE);
       circlefill(display[w], x - 4, y + 4, 3, COL_YELLOW);
       circle(display[w], x - 4, y + 4, 3, COL_OUTLINE);*/
       drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);

       circlefill(display[w], x + xpart(boss.spot_angle, 100), y + ypart(boss.spot_angle, 100), 3, TRANS_LRED);
       circle(display[w], x + xpart(boss.spot_angle, 100), y + ypart(boss.spot_angle, 100), 3, TRANS_YELLOW);

       circlefill(display[w], x + xpart(boss.spot_angle + ANGLE_3, 100), y + ypart(boss.spot_angle + ANGLE_3, 100), 3, TRANS_LGREEN);
       circle(display[w], x + xpart(boss.spot_angle + ANGLE_3, 100), y + ypart(boss.spot_angle + ANGLE_3, 100), 3, TRANS_YELLOW);

       circlefill(display[w], x + xpart(boss.spot_angle - ANGLE_3, 100), y + ypart(boss.spot_angle - ANGLE_3, 100), 3, TRANS_LBLUE);
       circle(display[w], x + xpart(boss.spot_angle - ANGLE_3, 100), y + ypart(boss.spot_angle - ANGLE_3, 100), 3, TRANS_WHITE);

       drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);

       int w2 = 0;
       if (w == 0)
         w2 = 1;

       circlefill(display[w2], x + xpart(boss.spot_angle, 100), y + ypart(boss.spot_angle, 100), 7, TRANS_LRED);
       circle(display[w2], x + xpart(boss.spot_angle, 100), y + ypart(boss.spot_angle, 100), 7, TRANS_YELLOW);

       for (i = 0; i < 3; i ++)
       {
        circlefill(display[w2], x + xpart(boss.spot_angle, 100) + xpart(ANGLE_1 - boss.spot_angle + i * ANGLE_3, 14), y + ypart(boss.spot_angle, 100) + ypart(ANGLE_1 - boss.spot_angle + i * ANGLE_3, 14), 3, TRANS_DRED);
        circle(display[w2], x + xpart(boss.spot_angle, 100) + xpart(ANGLE_1 - boss.spot_angle + i * ANGLE_3, 14), y + ypart(boss.spot_angle, 100) + ypart(ANGLE_1 - boss.spot_angle + i * ANGLE_3, 14), 3, TRANS_YELLOW);
       }

       circlefill(display[w2], x + xpart(boss.spot_angle + ANGLE_3, 100), y + ypart(boss.spot_angle + ANGLE_3, 100), 7, TRANS_LGREEN);
       circle(display[w2], x + xpart(boss.spot_angle + ANGLE_3, 100), y + ypart(boss.spot_angle + ANGLE_3, 100), 7, TRANS_YELLOW);

       for (i = 0; i < 3; i ++)
       {
        circlefill(display[w2], x + xpart(boss.spot_angle + ANGLE_3, 100) + xpart(ANGLE_1 - boss.spot_angle + i * ANGLE_3, 14), y + ypart(boss.spot_angle + ANGLE_3, 100) + ypart(ANGLE_1 - boss.spot_angle + i * ANGLE_3, 14), 3, TRANS_DGREEN);
        circle(display[w2], x + xpart(boss.spot_angle + ANGLE_3, 100) + xpart(ANGLE_1 - boss.spot_angle + i * ANGLE_3, 14), y + ypart(boss.spot_angle + ANGLE_3, 100) + ypart(ANGLE_1 - boss.spot_angle + i * ANGLE_3, 14), 3, TRANS_YELLOW);
       }


       circlefill(display[w2], x + xpart(boss.spot_angle - ANGLE_3, 100), y + ypart(boss.spot_angle - ANGLE_3, 100), 7, TRANS_LBLUE);
       circle(display[w2], x + xpart(boss.spot_angle - ANGLE_3, 100), y + ypart(boss.spot_angle - ANGLE_3, 100), 7, TRANS_WHITE);

       for (i = 0; i < 3; i ++)
       {
        circlefill(display[w2], x + xpart(boss.spot_angle - ANGLE_3, 100) + xpart(ANGLE_1 - boss.spot_angle + i * ANGLE_3, 14), y + ypart(boss.spot_angle - ANGLE_3, 100) + ypart(ANGLE_1 - boss.spot_angle + i * ANGLE_3, 14), 3, TRANS_DBLUE);
        circle(display[w2], x + xpart(boss.spot_angle - ANGLE_3, 100) + xpart(ANGLE_1 - boss.spot_angle + i * ANGLE_3, 14), y + ypart(boss.spot_angle - ANGLE_3, 100) + ypart(ANGLE_1 - boss.spot_angle + i * ANGLE_3, 14), 3, TRANS_WHITE);
       }


       break;


      case ENEMY_BASIC1:
        xa = -3;
        if (enemy[w][e].recycle <= 30)
         xa += (30 - enemy[w][e].recycle) / 5;
        if (enemy[w][e].recycle >= 4970)
         xa = 3 - (5000 - enemy[w][e].recycle) / 5;
//        xa = xpart(arena.counter * 16, 3);
        draw_rle_sprite(display [w], eRLE_basic1 [S_ENEMY_BASIC1_CENTRE], x - 8, y - 8);
        draw_rle_sprite(display [w], eRLE_basic1 [S_ENEMY_BASIC1_LWING], x - 15 - xpart(0, xa), y - 11);
        draw_rle_sprite(display [w], eRLE_basic1 [S_ENEMY_BASIC1_RWING], x - 15 + 18 + xpart(0, xa), y - 11);

/*      xa = xpart(arena.counter * 16, 3);
        draw_rle_sprite(display [w], eRLE_basic1 [S_ENEMY_BASIC1_CENTRE], x - 8, y - 8);
        draw_rle_sprite(display [w], eRLE_basic1 [S_ENEMY_BASIC1_LWING], x - 15 - xpart(0, xa), y - 11);
        draw_rle_sprite(display [w], eRLE_basic1 [S_ENEMY_BASIC1_RWING], x - 15 + 18 + xpart(0, xa), y - 11);
*/
//      textprintf_ex(display [w], font, x, y, TRANS_WHITE, -1, "%i %i", xa, arena.counter);
/*
         if (enemy[w][e].x_speed > 2500)
         {
          draw_rle_sprite(display [w], eRLE_basic1 [6], x - 8, y - 8);
          draw_rle_sprite(display [w], eRLE_basic1 [7], x - 15 - xpart(angle1, xa), y - 12 - ypart(angle1, xa));
          draw_rle_sprite(display [w], eRLE_basic1 [8], x - 15 + 18 + xpart(angle1, xa), y - 10 + ypart(angle1, xa));
      textprintf_ex(display [w], font, x, y, TRANS_WHITE, -1, "%i %i", xa, arena.counter);
          break;
         }

         if (enemy[w][e].x_speed > 900)
         {
          draw_rle_sprite(display [w], eRLE_basic1 [3], x - 8, y - 8);
          draw_rle_sprite(display [w], eRLE_basic1 [4], x - 16 - xpart(angle2, xa), y - 12 - ypart(angle2, xa));
          draw_rle_sprite(display [w], eRLE_basic1 [5], x - 15 + 18 + xpart(angle2, xa), y - 10 + ypart(angle2, xa));
      textprintf_ex(display [w], font, x, y, TRANS_WHITE, -1, "%i %i", xa, arena.counter);
          break;
         }

//xa = 0;
         if (enemy[w][e].x_speed < -2500)
         {
          draw_rle_sprite(display [w], eRLE_basic1 [12], x - 8, y - 8);
          draw_rle_sprite(display [w], eRLE_basic1 [14], x - 15 + xpart(angle1, xa), y - 10 + ypart(angle1, xa));
          draw_rle_sprite(display [w], eRLE_basic1 [13], x - 15 + 18 - xpart(angle1, xa), y - 12 - ypart(angle1, xa));
      textprintf_ex(display [w], font, x, y, TRANS_WHITE, -1, "%i %i", xa, arena.counter);
          break;
         }

         if (enemy[w][e].x_speed < -900)
         {
          draw_rle_sprite(display [w], eRLE_basic1 [9], x - 8, y - 8);
          draw_rle_sprite(display [w], eRLE_basic1 [11], x - 16 + xpart(angle2, xa), y - 10 + ypart(angle2, xa));
          draw_rle_sprite(display [w], eRLE_basic1 [10], x - 15 + 18 - xpart(angle2, xa), y - 12 - ypart(angle2, xa));
      textprintf_ex(display [w], font, x, y, TRANS_WHITE, -1, "%i %i", xa, arena.counter);
          break;
         }


         draw_rle_sprite(display [w], eRLE_basic1 [0], x - 8, y - 8);
         draw_rle_sprite(display [w], eRLE_basic1 [1], x - 15 - xpart(0, xa), y - 11);
         draw_rle_sprite(display [w], eRLE_basic1 [2], x - 15 + 18 + xpart(0, xa), y - 11);

      textprintf_ex(display [w], font, x, y, TRANS_WHITE, -1, "%i %i", xa, arena.counter);

*/
         break;
      case ENEMY_BASIC2:
        if (enemy[w][e].x_speed > 600)
        {
         xa = 2 + grand(4);
         xb = explode_rle [0] [xa].x;
         draw_trans_rle_sprite(display[w], explode_rle [2] [xa].sprite, x - xb - 3, y - xb + 10 - grand(2));
         draw_trans_rle_sprite(display[w], explode_rle [2] [xa].sprite, x - xb + 5, y - xb + 10 - grand(2));
         draw_rle_sprite(display [w], eRLE_basic1 [S_ENEMY_BASIC2_5], x - 14, y - 10);
         break;
        }
        if (enemy[w][e].x_speed > 150)
        {
         xa = 2 + grand(4);
         xb = explode_rle [0] [xa].x;
         draw_trans_rle_sprite(display[w], explode_rle [2] [xa].sprite, x - xb - 5, y - xb + 10 - grand(2));
         draw_trans_rle_sprite(display[w], explode_rle [2] [xa].sprite, x - xb + 6, y - xb + 10 - grand(2));
         draw_rle_sprite(display [w], eRLE_basic1 [S_ENEMY_BASIC2_4], x - 14, y - 10);
         break;
        }
        if (enemy[w][e].x_speed < -600)
        {
         xa = 2 + grand(4);
         xb = explode_rle [0] [xa].x;
         draw_trans_rle_sprite(display[w], explode_rle [2] [xa].sprite, x - xb - 4, y - xb + 10 - grand(2));
         draw_trans_rle_sprite(display[w], explode_rle [2] [xa].sprite, x - xb + 4, y - xb + 10 - grand(2));
         draw_rle_sprite(display [w], eRLE_basic1 [S_ENEMY_BASIC2_3], x - 14, y - 10);
         break;
        }
        if (enemy[w][e].x_speed < -150)
        {
         xa = 2 + grand(4);
         xb = explode_rle [0] [xa].x;
         draw_trans_rle_sprite(display[w], explode_rle [2] [xa].sprite, x - xb - 5, y - xb + 10 - grand(2));
         draw_trans_rle_sprite(display[w], explode_rle [2] [xa].sprite, x - xb + 6, y - xb + 10 - grand(2));
         draw_rle_sprite(display [w], eRLE_basic1 [S_ENEMY_BASIC2_2], x - 14, y - 10);
         break;
        }
        xa = 2 + grand(4);
        xb = explode_rle [0] [xa].x;
        draw_trans_rle_sprite(display[w], explode_rle [2] [xa].sprite, x - xb - 5, y - xb + 10 - grand(2));
        draw_trans_rle_sprite(display[w], explode_rle [2] [xa].sprite, x - xb + 6, y - xb + 10 - grand(2));
        draw_rle_sprite(display [w], eRLE_basic1 [S_ENEMY_BASIC2_1], x - 14, y - 10);
        break;

      case ENEMY_BFIGHTER:
        if (enemy[w][e].x_speed > 600)
        {
        xa = 2 + grand(4);
        xb = explode_rle [0] [xa].x;
        draw_trans_rle_sprite(display[w], explode_rle [1] [xa].sprite, x - xb - 3, y - xb + 10 - grand(2));
//         xa = 2 + grand(4);
//         xb = explode_rle [0] [xa].x;
//         draw_trans_rle_sprite(display[w], explode_rle [1] [xa].sprite, x - xb - 2, y - xb + 10 - grand(2));
//         draw_trans_rle_sprite(display[w], explode_rle [1] [xa].sprite, x - xb + 4, y - xb + 10 - grand(2));
         draw_rle_sprite(display [w], eRLE_basic1 [S_ENEMY_BFIGHTER_5], x - 9, y - 10);
         break;
        }
        if (enemy[w][e].x_speed > 150)
        {
        xa = 2 + grand(4);
        xb = explode_rle [0] [xa].x;
        draw_trans_rle_sprite(display[w], explode_rle [1] [xa].sprite, x - xb - 1, y - xb + 10 - grand(2));
//         xa = 2 + grand(4);
//         xb = explode_rle [0] [xa].x;
//         draw_trans_rle_sprite(display[w], explode_rle [1] [xa].sprite, x - xb - 4, y - xb + 10 - grand(2));
//         draw_trans_rle_sprite(display[w], explode_rle [1] [xa].sprite, x - xb + 5, y - xb + 10 - grand(2));
         draw_rle_sprite(display [w], eRLE_basic1 [S_ENEMY_BFIGHTER_4], x - 9, y - 10);
         break;
        }
        if (enemy[w][e].x_speed < -600)
        {
        xa = 2 + grand(4);
        xb = explode_rle [0] [xa].x;
        draw_trans_rle_sprite(display[w], explode_rle [1] [xa].sprite, x - xb + 3, y - xb + 10 - grand(2));
         xa = 2 + grand(4);
         xb = explode_rle [0] [xa].x;
//         draw_trans_rle_sprite(display[w], explode_rle [1] [xa].sprite, x - xb - 3, y - xb + 10 - grand(2));
         //draw_trans_rle_sprite(display[w], explode_rle [1] [xa].sprite, x - xb + 3, y - xb + 10 - grand(2));
         draw_rle_sprite(display [w], eRLE_basic1 [S_ENEMY_BFIGHTER_3], x - 9, y - 10);
         break;
        }
        if (enemy[w][e].x_speed < -150)
        {
        xa = 2 + grand(4);
        xb = explode_rle [0] [xa].x;
        draw_trans_rle_sprite(display[w], explode_rle [1] [xa].sprite, x - xb + 1, y - xb + 10 - grand(2));
//         xa = 2 + grand(4);
//         xb = explode_rle [0] [xa].x;
//         draw_trans_rle_sprite(display[w], explode_rle [1] [xa].sprite, x - xb - 4, y - xb + 10 - grand(2));
//         draw_trans_rle_sprite(display[w], explode_rle [1] [xa].sprite, x - xb + 5, y - xb + 10 - grand(2));
         draw_rle_sprite(display [w], eRLE_basic1 [S_ENEMY_BFIGHTER_2], x - 9, y - 10);
         break;
        }
        xa = 2 + grand(4);
        xb = explode_rle [0] [xa].x;
        draw_trans_rle_sprite(display[w], explode_rle [1] [xa].sprite, x - xb, y - xb + 10 - grand(2));
//        xa = 2 + grand(4);
//        xb = explode_rle [0] [xa].x;
//        draw_trans_rle_sprite(display[w], explode_rle [1] [xa].sprite, x - xb - 4, y - xb + 10 - grand(2));
//        draw_trans_rle_sprite(display[w], explode_rle [1] [xa].sprite, x - xb + 5, y - xb + 10 - grand(2));
        draw_rle_sprite(display [w], eRLE_basic1 [S_ENEMY_BFIGHTER_1], x - 9, y - 10);
        break;

      case ENEMY_BASIC3:
/*        xa = -3;
        if (enemy[w][e].recycle <= 30)
         xa += (30 - enemy[w][e].recycle) / 5;
        if (enemy[w][e].recycle >= 4970)
         xa = 3 - (5000 - enemy[w][e].recycle) / 5;*/
        xa = xpart(arena.counter * 32, 2);
        draw_rle_sprite(display [w], eRLE_basic1 [S_ENEMY_BASIC3], x - 13, y - 11);
        draw_rle_sprite(display [w], eRLE_basic1 [S_ENEMY_BASIC3_LFOOT], x - 11 - xa - 2, y + 4);
        draw_rle_sprite(display [w], eRLE_basic1 [S_ENEMY_BASIC3_RFOOT], x + 0 + xa + 2, y + 4);
       break;
      default:
      circlefill(display [w], x, y, 8, COL_WHITE + enemy[w] [e].hurt_pulse);
      circle(display [w], x, y, 8, COL_OUTLINE);

      circlefill(display [w], x - 7, y + 4, 6, COL_LGREY);
      circle(display [w], x - 7, y + 4, 6, COL_OUTLINE);

      circlefill(display [w], x + 7, y + 4, 6, COL_DGREY);
      circle(display [w], x + 7, y + 4, 6, COL_OUTLINE);
      break;

    }

//    int size = eclass[enemy[w][e].type].size / 1000;

//    rect(display [w], x - size, y - size, x + size, y + size, TRANS_WHITE);

        /*
        case ENEMY_ATTACKER2:
            while (enemy[w] [e].angle1 < 0)
             enemy[w] [e].angle1 += ANGLE_1;
            xb = enemy[w] [e].angle1 / (ANGLE_1 / MEDIUM_ROTATIONS);
            if (xb > MEDIUM_ROTATIONS - 1) xb = MEDIUM_ROTATIONS - 1;
         draw_rle_sprite(display[w], eRLE_attacker [0] [0] [pole] [xb].sprite, x - eRLE_attacker [0] [0] [pole] [xb].x, y - eRLE_attacker [0] [0] [pole] [xb].y);

          xa = x - xpart(enemy[w] [e].angle1, enemy[w] [e].recycle / 8 - 3);
          ya = y - ypart(enemy[w] [e].angle1, enemy[w] [e].recycle / 8 - 3);

//         draw_rle_sprite(display, eRLE_attacker [0] [1] [pole] [xb], xa - 28, ya - 28);
         draw_rle_sprite(display [w], eRLE_attacker [1] [1] [pole] [xb].sprite, xa - eRLE_attacker [1] [1] [pole] [xb].x, ya - eRLE_attacker [1] [1] [pole] [xb].y);
              break;
        case ENEMY_ATTACKER1:

            while (enemy[w] [e].angle1 < 0)
             enemy[w] [e].angle1 += ANGLE_1;
            xb = enemy[w] [e].angle1 / (ANGLE_1 / MEDIUM_ROTATIONS);
            if (xb > MEDIUM_ROTATIONS - 1) xb = MEDIUM_ROTATIONS - 1;
//       draw_rle_sprite(display, eRLE_attacker [0] [0] [pole] [xb], x - 28, y - 28);
         draw_rle_sprite(display [w], eRLE_attacker [0] [0] [pole] [xb].sprite, x - eRLE_attacker [0] [0] [pole] [xb].x, y - eRLE_attacker [0] [0] [pole] [xb].y);

          xa = x - xpart(enemy[w] [e].angle1, enemy[w] [e].recycle / 8 - 3);
          ya = y - ypart(enemy[w] [e].angle1, enemy[w] [e].recycle / 8 - 3);

//         draw_rle_sprite(display, eRLE_attacker [0] [1] [pole] [xb], xa - 28, ya - 28);
         draw_rle_sprite(display [w], eRLE_attacker [0] [1] [pole] [xb].sprite, xa - eRLE_attacker [0] [1] [pole] [xb].x, ya - eRLE_attacker [0] [1] [pole] [xb].y);
//circle(display, x, y, 9, COLOUR_1);
             break;
        case ENEMY_ATTACKER3:
            while (enemy[w] [e].angle1 < 0)
             enemy[w] [e].angle1 += ANGLE_1;
            xb = enemy[w] [e].angle1 / (ANGLE_1 / MEDIUM_ROTATIONS);
            if (xb > MEDIUM_ROTATIONS - 1) xb = MEDIUM_ROTATIONS - 1;
         draw_rle_sprite(display [w], eRLE_attacker [0] [0] [pole] [xb].sprite, x - eRLE_attacker [0] [0] [pole] [xb].x, y - eRLE_attacker [0] [0] [pole] [xb].y);

          yb = enemy[w] [e].recycle * 4;
          if (yb > 50)
           yb = 50;

          xa = x - xpart(enemy[w] [e].angle1, yb / 8 - 3);
          ya = y - ypart(enemy[w] [e].angle1, yb / 8 - 3);

         draw_rle_sprite(display [w], eRLE_attacker [2] [1] [pole] [xb].sprite, xa - eRLE_attacker [2] [1] [pole] [xb].x, ya - eRLE_attacker [2] [1] [pole] [xb].y);
              break;
    }*/
//      textprintf_ex(display, font, enemy[e].x / GRAIN, y, TRANS_WHITE6, -1, "%i %i", enemy[e].angle1, radians_to_angle(atan2(player[0].y - enemy[e].y, player[0].x - enemy[e].x)));

//      textprintf_ex(display, font, enemy[e].x / GRAIN, y, TRANS_BLACK2, -1, "%i %i %i %i", enemy[e].target, enemy[e].target_time, enemy[e].max_target_time, enemy[e].x_target [enemy[e].target]);
//      textprintf_ex(display, font, enemy[e].x / GRAIN, enemy[e].y / GRAIN, TRANS_BLACK2, -1, "%i %i", enemy[e].target, enemy[e].target_time);

// line(display, enemy[e].x / GRAIN, enemy[e].y / GRAIN, enemy[e].x_target [enemy[e].target] / GRAIN, enemy[e].y_target [enemy[e].target] / GRAIN, COLOUR_10);

}


void draw_pbullets(void)
{

 int b, w;

 for (w =0; w < 2; w ++)
 {
  for (b = 0; b < NO_PBULLETS; b ++)
  {
   if (pbullet[w] [b].type == PBULLET_NONE)
    continue;
   draw_a_pbullet(w, b);
  }
 }

 for (w = 0; w < 2; w ++)
 {
  for (b = 0; b < NO_SEEKERS; b ++)
  {
   if (seeker[w][b].active == 1)
    draw_a_seeker(w, b);
  }
 }

}

void draw_a_pbullet(int w, int b)
{

// int col1 = TRANS_YELLOW, col2 = TRANS_DRED;

 int x = pbullet[w] [b].x / GRAIN;
 int y = pbullet[w] [b].y / GRAIN;
 int xa, xb, i;

 switch(pbullet[w][b].type)
 {
   case PBULLET_BASIC:
    draw_trans_rle_sprite(display[w], basic_bullet [0 + grand(2) ], x - 3, y - 2);
    break;
   case PBULLET_HEAVY:
    draw_trans_rle_sprite(display[w], basic_bullet [2 + (pbullet[w][b].type2 * 4) + grand(4)], x - 5, y - 4);
    break;
   case PBULLET_MULTI1:
   case PBULLET_MULTI2:
   case PBULLET_MULTI3:
   case PBULLET_MULTI4:
   case PBULLET_MULTI5:
    draw_trans_rle_sprite(display[w], multi_bullet [pbullet[w][b].type2] [pbullet[w][b].type - PBULLET_MULTI1], x - 17, y - 10);
    break;
   case PBULLET_TURRET:
    xa = 3 + grand(4);
    xb = explode_rle [2] [xa].x;
    draw_trans_rle_sprite(display[w], explode_rle [2] [xa].sprite, x - xb, y - xb);
    xa = 2 + grand(4);
    xb = explode_rle [2] [xa].x;
    draw_trans_rle_sprite(display[w], explode_rle [2] [xa].sprite, x - xb - xpart(pbullet[w][b].type2, 6), y - xb - ypart(pbullet[w][b].type2, 6));
    xa = 1 + grand(4);
    xb = explode_rle [2] [xa].x;
    draw_trans_rle_sprite(display[w], explode_rle [2] [xa].sprite, x - xb - xpart(pbullet[w][b].type2, 12), y - xb - ypart(pbullet[w][b].type2, 12));
    break;
   case PBULLET_WHITE2:
    xa = 4 + grand(4);
    xb = explode_rle [2] [xa].x;
    draw_trans_rle_sprite(display[w], explode_rle [2] [xa].sprite, x - xb, y - xb);
    xa = 3 + grand(4);
    xb = explode_rle [2] [xa].x;
//  draw_trans_rle_sprite(display[w], explode_rle [2] [xa], x - xb, y - xb + pbullet[w][b].type2 / 2500);
    draw_trans_rle_sprite(display[w], explode_rle [2] [xa].sprite, x - xb, y - xb + 8);
    xa = 2 + grand(4);
    xb = explode_rle [2] [xa].x;
//  draw_trans_rle_sprite(display[w], explode_rle [2] [xa], x - xb, y - xb + pbullet[w][b].type2 / 1400);
  draw_trans_rle_sprite(display[w], explode_rle [2] [xa].sprite, x - xb, y - xb + 16);
    break;
   case PBULLET_CIRCLE:
    xa = 1000 - pbullet[w][b].timeout;
    if (xa > 20)
     xa = 20;
    xa *= (8 + pbullet[w][b].type2);
    xa /= 8;
//    xa += grand(3);
    if (xa > 39)
     xa = 39;
    draw_trans_rle_sprite(display[w], green_ring [xa], x - xa, y - xa);
/*    xa = 1000 - pbullet[w][b].timeout;
    if (xa > 20)
     xa = 20;
    xa *= (8 + pbullet[w][b].type2);
    xa /= 8;
    xa += grand(3);
    drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);
    circle(display[w], x, y, xa, TRANS_LGREEN);
    circle(display[w], x, y, xa + 1, TRANS_LGREEN);
    circle(display[w], x, y, xa + 2, TRANS_DGREEN);
    drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);*/
    break;
   case PBULLET_GREEN2:
    xa = 6;//1000 - pbullet[w][b].timeout;
/*    if (xa > 20)
     xa = 20;
    xa *= (8 + pbullet[w][b].type2);
    xa /= 8;
    if (xa > 40)
     xa = 40;*/
    draw_trans_rle_sprite(display[w], green_ring [xa], x - xa, y - xa);
    break;
   case PBULLET_GREEN2_VORTEX:
//    xa = 6 + grand(10);
    //draw_trans_rle_sprite(display[w], green_ring [xa], x - xa, y - xa);
    xa = 1;
    int dist = 10;
    if (pbullet[w][b].timeout < 10)
     dist = pbullet[w][b].timeout + 1;
    xa = 4 + grand(3);
    if (xa > dist)
     xa = dist;
    xb = explode_rle [2] [xa].x;
    int angle = pbullet[w][b].timeout * 25;
    if (w == 1)
    {
     angle &= 1023;
     angle = ANGLE_1 - angle;
    }
    for (i = 0; i < 4; i ++)
    {
     draw_trans_rle_sprite(display[w], explode_rle [1] [xa].sprite, x - xb + xpart(angle + i * ANGLE_4, dist), y - xb + ypart(angle + i * ANGLE_4, dist));
    }

    xa = dist - grand(3);
    if (xa < 1)
     xa = 1;
    xb = explode_rle [2] [xa].x;
    draw_trans_rle_sprite(display[w], explode_rle [1] [xa].sprite, x - xb, y - xb);

    angle &= 1023;
    angle = ANGLE_1 - angle;
    dist *= 2;
    dist -= 3;
    if (dist < 0)
     dist = 0;
    xa = 3 + grand(3);
    if (xa > dist)
     xa = dist + 1;
    xb = explode_rle [1] [xa].x;
    int ya, yb, angle_diff = - ANGLE_16;
    if (w == 1)
     angle_diff = ANGLE_16;
    for (i = 0; i < 6; i ++)
    {
     draw_trans_rle_sprite(display[w], explode_rle [1] [xa].sprite, x - xb + xpart(angle + i * ANGLE_4, dist), y - xb + ypart(angle + i * ANGLE_4, dist));
     ya = xa - 1;
     yb = explode_rle [1] [xa].x;
     if (ya > 0)
      draw_trans_rle_sprite(display[w], explode_rle [1] [ya - 1].sprite, x - yb + xpart(angle + i * ANGLE_4 + angle_diff, dist), y - yb + ypart(angle + i * ANGLE_4 + angle_diff, dist));
    }

    break;
 }


}

void draw_a_seeker(int w, int s)
{

 int col1 = TRANS_YELLOW, col2 = TRANS_LRED;
/* if (seeker[s].pole == POLE_BLACK)
 {
  col1 = TRANS_BLACK5;
  col2 = TRANS_BLACK3;
 }*/
// int x = (seeker[s].x - seeker[s].x_speed) / GRAIN;
// int y = (seeker[s].y - seeker[s].y_speed) / GRAIN;
 int x = (seeker[w][s].x) / GRAIN;
 int y = (seeker[w][s].y) / GRAIN;

  drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);
   circlefill(display [w], x, y, 1 + grand(2), col1);
   circlefill(display [w], x, y, 3 + grand(2), col2);

//     circlefill(display, x + xpart(seeker[s].angle, 5), y + ypart(seeker[s].angle, 5), 3, col2);
//triangle(display, x

//     triangle(display, x - 3, y, x + 3, y, x, y + 15, col2);
//     rectfill(display, x - 2, y - 2, x + 2, y + 19, col1);
//     rectfill(display, x - 1, y - 1, x + 1, y + 7, col2);
  drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);
 //if (seeker[w][s].target != -1)
//  line(display[w], x, y, enemy[w][seeker[w][s].target].x / GRAIN, enemy[w][seeker[w][s].target].y / GRAIN, TRANS_LGREEN);

}


void draw_ebullets(void)
{

 int b, w;

 for (w = 0; w < 2; w ++)
 {
  for (b = 0; b < NO_EBULLETS; b ++)
  {
   if (ebullet[w][b].type == EBULLET_NONE)
    continue;
   draw_an_ebullet(w, b);
  }
 }

}

void draw_an_ebullet(int w, int b)
{

 int col [4] = {TRANS_YELLOW, TRANS_LRED, TRANS_DRED, TRANS_DDRED};

 int x = ebullet[w][b].x / GRAIN;
 int y = ebullet[w][b].y / GRAIN;

 int xa, xb, ya, yb, beam_length, arc_angle;

 switch(ebullet[w][b].type)
 {
  case EBULLET_STAR:
   drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);
   if (ebullet[w][b].status2 < 21)
   {
        xa = ebullet[w][b].status2 / 3;
    switch(ebullet[w][b].status)
    {
     case 1:
      col [1] = TRANS_LGREEN;
      col [2] = TRANS_DGREEN;
      col [3] = TRANS_DDGREEN;
      break;
     case 2:
      col [0] = TRANS_WHITE;
      col [1] = TRANS_LBLUE;
      col [2] = TRANS_DBLUE;
      col [3] = TRANS_DDBLUE;
      break;
    }
        switch(xa / 2)
        {
         case 0: xb = col [3]; break;
         case 1: xb = col [2]; break;
         case 2: xb = col [1]; break;
         case 3: xb = col [0]; break;
        }
        poly4(w,
         x + xpart(ebullet[w][b].angle, xa), y + ypart(ebullet[w][b].angle, xa),
         x + xpart(ebullet[w][b].angle + ANGLE_4, xa / 2 + 1), y + ypart(ebullet[w][b].angle + ANGLE_4, xa / 2 + 1),
         x - xpart(ebullet[w][b].angle, xa), y - ypart(ebullet[w][b].angle, xa),
         x + xpart(ebullet[w][b].angle - ANGLE_4, xa / 2 + 1), y + ypart(ebullet[w][b].angle - ANGLE_4, xa / 2 + 1),
         xb);
   }
    else
    {
     xb = star_bullet [ebullet[w][b].status] [ebullet[w][b].status3].x;
     yb = star_bullet [ebullet[w][b].status] [ebullet[w][b].status3].y;
     draw_trans_rle_sprite(display[w], star_bullet [ebullet[w][b].status] [ebullet[w][b].status3].sprite, x - xb, y - yb);
    }
   if (ebullet[w][b].status2 < 60)
   {
    switch(ebullet[w][b].status)
    {
     case 1:
      col [1] = TRANS_LGREEN;
      col [2] = TRANS_DGREEN;
      col [3] = TRANS_DDGREEN;
      break;
     case 2:
      col [0] = TRANS_WHITE;
      col [1] = TRANS_LBLUE;
      col [2] = TRANS_DBLUE;
      col [3] = TRANS_DDBLUE;
      break;
    }
    xa = col [1];
    if (ebullet[w][b].status2 > 50)
     xa = col [2];
    if (ebullet[w][b].status2 > 55)
     xa = col [3];
    xb = ebullet[w][b].x2 / GRAIN;
    yb = ebullet[w][b].y2 / GRAIN;
//    pline(display[w], x - xpart(ebullet[w][b].angle, 5), y - ypart(ebullet[w][b].angle, 5), xb, yb, xa);
    pline(display[w], x, y, xb, yb, xa);

    circlefill(display[w], xb, yb, (90 - ebullet[w][b].status2) / 20, col [2]);
    circlefill(display[w], xb, yb, (90 - ebullet[w][b].status2) / 30, col [1]);
    circle(display[w], xb, yb, (90 - ebullet[w][b].status2) / 25, col [1]);
   }
   drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);
//  textprintf_ex(display [w], font, x, y, -1, -1, "%i", ebullet[w][b].status3);
   break;

  case EBULLET_DIAMOND:
   drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);
   if (ebullet[w][b].status2 < 21)
   {
        xa = ebullet[w][b].status2 / 3;
    switch(ebullet[w][b].status)
    {
     case 1:
      col [1] = TRANS_LGREEN;
      col [2] = TRANS_DGREEN;
      col [3] = TRANS_DDGREEN;
      break;
     case 2:
      col [0] = TRANS_WHITE;
      col [1] = TRANS_LBLUE;
      col [2] = TRANS_DBLUE;
      col [3] = TRANS_DDBLUE;
      break;
    }
        switch(xa / 2)
        {
         case 0: xb = col [3]; break;
         case 1: xb = col [2]; break;
         case 2: xb = col [1]; break;
         case 3: xb = col [0]; break;
        }
        poly4(w,
         x + xpart(ebullet[w][b].angle, xa), y + ypart(ebullet[w][b].angle, xa),
         x + xpart(ebullet[w][b].angle + ANGLE_4, xa / 2 + 1), y + ypart(ebullet[w][b].angle + ANGLE_4, xa / 2 + 1),
         x - xpart(ebullet[w][b].angle, xa), y - ypart(ebullet[w][b].angle, xa),
         x + xpart(ebullet[w][b].angle - ANGLE_4, xa / 2 + 1), y + ypart(ebullet[w][b].angle - ANGLE_4, xa / 2 + 1),
         xb);
   }
    else
    {
     xb = diamond_bullet [ebullet[w][b].status] [ebullet[w][b].status3].x;
     yb = diamond_bullet [ebullet[w][b].status] [ebullet[w][b].status3].y;
     draw_trans_rle_sprite(display[w], diamond_bullet [ebullet[w][b].status] [ebullet[w][b].status3].sprite, x - xb, y - yb);
    }
   if (ebullet[w][b].status2 < 60)
   {
    switch(ebullet[w][b].status)
    {
     case 1:
      col [1] = TRANS_LGREEN;
      col [2] = TRANS_DGREEN;
      col [3] = TRANS_DDGREEN;
      break;
     case 2:
      col [0] = TRANS_WHITE;
      col [1] = TRANS_LBLUE;
      col [2] = TRANS_DBLUE;
      col [3] = TRANS_DDBLUE;
      break;
    }
    xa = col [1];
    if (ebullet[w][b].status2 > 50)
     xa = col [2];
    if (ebullet[w][b].status2 > 55)
     xa = col [3];
    xb = ebullet[w][b].x2 / GRAIN;
    yb = ebullet[w][b].y2 / GRAIN;
//    pline(display[w], x - xpart(ebullet[w][b].angle, 5), y - ypart(ebullet[w][b].angle, 5), xb, yb, xa);
    pline(display[w], x, y, xb, yb, xa);

    circlefill(display[w], xb, yb, (90 - ebullet[w][b].status2) / 20, col [2]);
    circlefill(display[w], xb, yb, (90 - ebullet[w][b].status2) / 30, col [1]);
    circle(display[w], xb, yb, (90 - ebullet[w][b].status2) / 25, col [1]);
   }
   drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);
//  textprintf_ex(display [w], font, x, y, -1, -1, "%i", ebullet[w][b].status3);
   break;


  case EBULLET_TRIANGLE:
   drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);
   if (ebullet[w][b].status2 < 21)
   {
        xa = ebullet[w][b].status2 / 3;
    switch(ebullet[w][b].status)
    {
     case 1:
      col [1] = TRANS_LGREEN;
      col [2] = TRANS_DGREEN;
      col [3] = TRANS_DDGREEN;
      break;
     case 2:
      col [0] = TRANS_WHITE;
      col [1] = TRANS_LBLUE;
      col [2] = TRANS_DBLUE;
      col [3] = TRANS_DDBLUE;
      break;
    }
        switch(xa / 2)
        {
         case 0: xb = col [3]; break;
         case 1: xb = col [2]; break;
         case 2: xb = col [1]; break;
         case 3: xb = col [0]; break;
        }
        triangle(display[w],
         x + xpart(ebullet[w][b].angle, xa), y + ypart(ebullet[w][b].angle, xa),
         x + xpart(ebullet[w][b].angle + ANGLE_4 + ANGLE_8, xa), y + ypart(ebullet[w][b].angle + ANGLE_4 + ANGLE_8, xa),
         x + xpart(ebullet[w][b].angle - ANGLE_4 - ANGLE_8, xa), y + ypart(ebullet[w][b].angle - ANGLE_4 - ANGLE_8, xa),
         xb);

   }
    else
    {
     xb = triangle_bullet [0] [ebullet[w][b].status3].x;
     yb = triangle_bullet [0] [ebullet[w][b].status3].y;
     draw_trans_rle_sprite(display[w], triangle_bullet [ebullet[w][b].status] [ebullet[w][b].status3].sprite, x - xb, y - yb);
    }
//   circlefill(display [w], x, y, 2 + grand(2), col [0]);
//   circlefill(display [w], x, y, 3 + grand(2), col [2]);
   if (ebullet[w][b].status2 < 90)
   {
    switch(ebullet[w][b].status)
    {
     case 1:
      col [1] = TRANS_LGREEN;
      col [2] = TRANS_DGREEN;
      col [3] = TRANS_DDGREEN;
      break;
     case 2:
      col [0] = TRANS_WHITE;
      col [1] = TRANS_LBLUE;
      col [2] = TRANS_DBLUE;
      col [3] = TRANS_DDBLUE;
      break;
    }
    switch((int) ebullet[w][b].status2 / 9)
    {
     case 0:
     case 1:
     case 2:
     case 3:
     case 4:
     case 5:
     case 6:
     xa = col [1]; break;
     case 7: xa = col [1]; break;
     case 8: xa = col [2]; break;
     default:
     case 9: xa = col [3]; break;
    }
    xb = ebullet[w][b].x2 / GRAIN;
    yb = ebullet[w][b].y2 / GRAIN;
    pline(display[w], x - xpart(ebullet[w][b].angle, 5), y - ypart(ebullet[w][b].angle, 5), xb, yb, xa);

    circlefill(display[w], xb, yb, (90 - ebullet[w][b].status2) / 20, col [2]);
    circlefill(display[w], xb, yb, (90 - ebullet[w][b].status2) / 30, col [1]);
    circle(display[w], xb, yb, (90 - ebullet[w][b].status2) / 25, col [1]);
   }
   drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);
//  textprintf_ex(display [w], font, x, y, -1, -1, "%i", ebullet[w][b].status3);
   break;





  case EBULLET_CIRCLE:
   drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);
    switch(ebullet[w][b].status)
    {
     case 1:
      col [1] = TRANS_LGREEN;
      col [2] = TRANS_DGREEN;
      col [3] = TRANS_DDGREEN;
      break;
     case 2:
      col [0] = TRANS_WHITE;
      col [1] = TRANS_LBLUE;
      col [2] = TRANS_DBLUE;
      col [3] = TRANS_DDBLUE;
      break;
    }
   if (ebullet[w][b].status2 < 21)
   {
        xa = ebullet[w][b].status2 / 3;
        switch(xa / 2)
        {
         case 0: xb = col [3]; break;
         case 1: xb = col [2]; break;
         case 2: xb = col [1]; break;
         case 3: xb = col [0]; break;
        }
        circlefill(display[w], x, y, xa, xb);

   }
    else
    {
     circlefill(display [w], x, y, 1, col [0]);
     circlefill(display [w], x, y, 5, col [1]);
     circle(display [w], x, y, 4, col [0]);
    }
//   circlefill(display [w], x, y, 2 + grand(2), col [0]);
//   circlefill(display [w], x, y, 3 + grand(2), col [2]);
//   circlefill(display [w], x, y, 2 + grand(2), col [0]);
//   circlefill(display [w], x, y, 3 + grand(2), col [2]);
   if (ebullet[w][b].status2 < 90)
   {
    switch(ebullet[w][b].status)
    {
     case 1:
      col [1] = TRANS_LGREEN;
      col [2] = TRANS_DGREEN;
      col [3] = TRANS_DDGREEN;
      break;
     case 2:
      col [0] = TRANS_WHITE;
      col [1] = TRANS_LBLUE;
      col [2] = TRANS_DBLUE;
      col [3] = TRANS_DDBLUE;
      break;
    }
    switch((int) ebullet[w][b].status2 / 9)
    {
     case 0:
     case 1:
     case 2:
     case 3:
     case 4:
     case 5:
     case 6:
     xa = col [1]; break;
     case 7: xa = col [1]; break;
     case 8: xa = col [2]; break;
     default:
     case 9: xa = col [3]; break;
    }
    xb = ebullet[w][b].x2 / GRAIN;
    yb = ebullet[w][b].y2 / GRAIN;
    pline(display[w], x - xpart(ebullet[w][b].angle, 5), y - ypart(ebullet[w][b].angle, 5), xb, yb, xa);

    circlefill(display[w], xb, yb, (90 - ebullet[w][b].status2) / 20, col [2]);
    circlefill(display[w], xb, yb, (90 - ebullet[w][b].status2) / 30, col [1]);
    circle(display[w], xb, yb, (90 - ebullet[w][b].status2) / 25, col [1]);
   }
   drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);
//  textprintf_ex(display [w], font, x, y, -1, -1, "%i", ebullet[w][b].status3);
   break;

  case EBULLET_SHOT:
  switch(ebullet[w][b].status)
  {
   case 1:
    col [1] = TRANS_DGREEN;
    col [2] = TRANS_DGREEN;
    col [3] = TRANS_DGREEN;
    break;
   case 2:
    col [0] = TRANS_WHITE;
    col [1] = TRANS_LBLUE;
    col [2] = TRANS_DBLUE;
    col [3] = TRANS_DDBLUE;
    break;
  }
  drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);
  circlefill(display [w], x, y, 2 + grand(2), col [0]);
  circlefill(display [w], x, y, 3 + grand(2), col [1]);
  circlefill(display [w], x, y, 3 + grand(3), col [3]);
  circlefill(display [w], x - xpart(ebullet[w][b].angle, 4), y - ypart(ebullet[w][b].angle, 4), 1 + grand(3), col [0]);
  circlefill(display [w], x - xpart(ebullet[w][b].angle, 4), y - ypart(ebullet[w][b].angle, 4), 2 + grand(3), col [1]);
  circlefill(display [w], x - xpart(ebullet[w][b].angle, 4), y - ypart(ebullet[w][b].angle, 4), 2 + grand(3), col [3]);
  circlefill(display [w], x - xpart(ebullet[w][b].angle, 8), y - ypart(ebullet[w][b].angle, 8), 2 + grand(2), col [1]);
  circlefill(display [w], x - xpart(ebullet[w][b].angle, 8), y - ypart(ebullet[w][b].angle, 8), 1 + grand(2), col [3]);
  drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);
  break;
  case EBULLET_FAT_SHOT:
  switch(ebullet[w][b].status)
  {
   case 1:
    col [1] = TRANS_DGREEN;
    col [2] = TRANS_DGREEN;
    col [3] = TRANS_DGREEN;
    break;
   case 2:
    col [0] = TRANS_WHITE;
    col [1] = TRANS_LBLUE;
    col [2] = TRANS_DBLUE;
    col [3] = TRANS_DDBLUE;
    break;
  }
  drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);
  circlefill(display [w], x, y, 3 + grand(2), col [0]);
  circlefill(display [w], x, y, 4 + grand(2), col [1]);
  circlefill(display [w], x, y, 4 + grand(3), col [3]);
  circlefill(display [w], x - xpart(ebullet[w][b].angle, 6), y - ypart(ebullet[w][b].angle, 6), 1 + grand(3), col [0]);
  circlefill(display [w], x - xpart(ebullet[w][b].angle, 6), y - ypart(ebullet[w][b].angle, 6), 2 + grand(3), col [1]);
  circlefill(display [w], x - xpart(ebullet[w][b].angle, 6), y - ypart(ebullet[w][b].angle, 6), 2 + grand(3), col [3]);
  circlefill(display [w], x - xpart(ebullet[w][b].angle, 10), y - ypart(ebullet[w][b].angle, 10), 2 + grand(2), col [1]);
  circlefill(display [w], x - xpart(ebullet[w][b].angle, 10), y - ypart(ebullet[w][b].angle, 10), 1 + grand(2), col [3]);
  drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);
  break;
  case EBULLET_FATTER_SHOT:
  switch(ebullet[w][b].status)
  {
   case 1:
    col [1] = TRANS_DGREEN;
    col [2] = TRANS_DGREEN;
    col [3] = TRANS_DGREEN;
    break;
   case 2:
    col [0] = TRANS_WHITE;
    col [1] = TRANS_LBLUE;
    col [2] = TRANS_DBLUE;
    col [3] = TRANS_DDBLUE;
    break;
  }
  drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);
  circlefill(display [w], x, y, 4 + grand(2), col [0]);
  circlefill(display [w], x, y, 5 + grand(2), col [1]);
  circlefill(display [w], x, y, 6 + grand(3), col [3]);
  circlefill(display [w], x - xpart(ebullet[w][b].angle, 8), y - ypart(ebullet[w][b].angle, 8), 2 + grand(3), col [1]);
  circlefill(display [w], x - xpart(ebullet[w][b].angle, 8), y - ypart(ebullet[w][b].angle, 8), 3 + grand(3), col [2]);
  circlefill(display [w], x - xpart(ebullet[w][b].angle, 8), y - ypart(ebullet[w][b].angle, 8), 3 + grand(3), col [3]);
  circlefill(display [w], x - xpart(ebullet[w][b].angle, 12), y - ypart(ebullet[w][b].angle, 12), 3 + grand(2), col [2]);
  circlefill(display [w], x - xpart(ebullet[w][b].angle, 12), y - ypart(ebullet[w][b].angle, 12), 2 + grand(2), col [3]);
  drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);
  break;
  case EBULLET_SPIN:
  switch(ebullet[w][b].status)
  {
   case 1:
    col [1] = TRANS_LGREEN;
    col [2] = TRANS_DGREEN;
    col [3] = TRANS_DDGREEN;
    break;
   case 2:
    col [0] = TRANS_WHITE;
    col [1] = TRANS_LBLUE;
    col [2] = TRANS_DBLUE;
    col [3] = TRANS_DDBLUE;
    break;
  }
  drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);
  circlefill(display[w], x, y, 1 + grand(3), col [0]);
  circlefill(display[w], x, y, 2 + grand(3), col [1]);
  circlefill(display[w], x, y, 4 + grand(2), col [3]);
  circlefill(display[w], x - xpart(ebullet[w][b].angle, 5), y - ypart(ebullet[w][b].angle, 4), 1 + grand(2), col [0]);
  circlefill(display[w], x - xpart(ebullet[w][b].angle, 5), y - ypart(ebullet[w][b].angle, 4), 2 + grand(2), col [1]);
  circlefill(display[w], x - xpart(ebullet[w][b].angle + ANGLE_2, 5), y - ypart(ebullet[w][b].angle + ANGLE_2, 4), 1 + grand(2), col [0]);
  circlefill(display[w], x - xpart(ebullet[w][b].angle + ANGLE_2, 5), y - ypart(ebullet[w][b].angle + ANGLE_2, 4), 2 + grand(2), col [1]);
  drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);
  break;
  case EBULLET_SHOT2:
         xa = 8 + grand(4);
         xb = explode_rle [ebullet[w][b].status] [xa].x;
         draw_trans_rle_sprite(display[w], explode_rle [ebullet[w][b].status] [xa].sprite, x - xb, y - xb);
/*

  switch(ebullet[w][b].status)
  {
   case 1:
    col [1] = TRANS_DGREEN;
    col [2] = TRANS_DGREEN;
    col [3] = TRANS_DGREEN;
    break;
  }
  drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);
  circlefill(display [w], x, y, 5 + grand(4), col [0]);
  circlefill(display [w], x, y, 7 + grand(3), col [1]);
  circlefill(display [w], x, y, 8 + grand(3), col [3]);
  drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);*/
  break;

  case EBULLET_SHOT3:
         xa = 5 + grand(3);
         xb = explode_rle [ebullet[w][b].status] [xa].x;
         draw_trans_rle_sprite(display[w], explode_rle [ebullet[w][b].status] [xa].sprite, x - xb, y - xb);
/*         xa = 3 + grand(3);
         xb = (explode_rle [ebullet[w][b].status] [xa]->w) / 2;
         draw_trans_rle_sprite(display[w], explode_rle [ebullet[w][b].status] [xa], x - xb - xpart(ebullet[w][b].angle, 5), y - xb - ypart(ebullet[w][b].angle, 5));*/
    break;

  case EBULLET_BEAM1:
  switch(ebullet[w][b].status2)
  {
   case 1:
    col [1] = TRANS_LGREEN;
    col [2] = TRANS_DGREEN;
    col [3] = TRANS_DDGREEN;
    break;
  }

   drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);
        beam_length = 500;

        while(beam_length > 0)
        {
         if (x + xpart(ebullet[w][b].angle, beam_length) > -5
          && y + ypart(ebullet[w][b].angle, beam_length) > -5
          && x + xpart(ebullet[w][b].angle, beam_length) < 330
          && y + ypart(ebullet[w][b].angle, beam_length) < 490)
          {
           beam_length += 50;
           break;
          }
          beam_length -= 50;
        }

        if (beam_length <= 0)
         break;

/*         if (w == 0)
          circle(display[2], x + xpart(ebullet[w][b].angle, beam_length), y + ypart(ebullet[w][b].angle, beam_length), 100, COL_WHITE);
         if (w == 1)
          circle(display[2], x - 320 + xpart(ebullet[w][b].angle, beam_length), y + ypart(ebullet[w][b].angle, beam_length), 100, COL_WHITE);*/

        xa = 12 + grand(4);
        if (ebullet[w][b].status > 240)
         xa = (300 - ebullet[w][b].status) / 7 + 2 + grand(4);
        if (ebullet[w][b].status < 30)
         xa = ebullet[w][b].status / 4 + 2 + grand(4);
//       xa /= 3;
//       xa *= 2;
        if (xa < 0)
         xa = 0;
        if (xa > 39)
         xa = 39;
        xb = 0;
        xb = ebullet[w][b].status2;
     ya = explode_rle [xb] [xa].x;
     draw_trans_rle_sprite(display[w], explode_rle [xb] [xa].sprite, x - ya, y - ya);
/*
        if (ebullet[w][b].status2 != -1)
        {
         beam_length = ebullet[w][b].status2 / GRAIN;
         xa = 14 + grand(5);
         ya = (explode_rle [xb] [xa]->w) / 2;
         draw_trans_rle_sprite(display, explode_rle [xb] [xa], x - ya + xpart(ebullet[w][b].angle, beam_length), y - ya + ypart(ebullet[w][b].angle, beam_length));
         if (arena.counter % 15 == 0)
          create_cloud(w, CLOUD_SHOCKWAVE, 0, (x + xpart(ebullet[w][b].angle, beam_length)) * GRAIN, (y + ypart(ebullet[w][b].angle, beam_length)) * GRAIN, 0, 0, 20);
// THIS IS REALLY WRONG AND SHOULDN'T BE HERE!
//  HACK HACK HACK
// Remember to make sure it's placed even when not all bullets are drawn
        }*/
//   if (ebullet[w][b].status > 200)


//ebullet[w][b].angle += grand(3) - grand(3);

    xa = x + xpart(ebullet[w][b].angle, beam_length);
    ya = y + ypart(ebullet[w][b].angle, beam_length);


 if (ebullet[w][b].status < 30)
 {

/*    arc_angle = (ebullet[w][b].status) / 4 + 1;
    if (arc_angle > 8)
     arc_angle = 8;
    arc_angle += grand(2);
    poly4(w, x + xpart(ebullet[w][b].angle + ANGLE_4, arc_angle),
     y + ypart(ebullet[w][b].angle + ANGLE_4, arc_angle),
     x + xpart(ebullet[w][b].angle - ANGLE_4, arc_angle),
     y + ypart(ebullet[w][b].angle - ANGLE_4, arc_angle),
     xa + xpart(ebullet[w][b].angle - ANGLE_4, arc_angle),
     ya + ypart(ebullet[w][b].angle - ANGLE_4, arc_angle),
     xa + xpart(ebullet[w][b].angle + ANGLE_4, arc_angle),
     ya + ypart(ebullet[w][b].angle + ANGLE_4, arc_angle), col [4]);*/



    int wcol = 1;
    if (ebullet[w][b].status < 27)
     wcol = 2;
    if (ebullet[w][b].status < 19)
     wcol = 3;

    arc_angle = ebullet[w][b].status / 5;
    if (arc_angle > 6)
     arc_angle = 6;
//    arc_angle += grand(2);
    poly4(w, x + xpart(ebullet[w][b].angle + ANGLE_4, arc_angle),
     y + ypart(ebullet[w][b].angle + ANGLE_4, arc_angle),
     x + xpart(ebullet[w][b].angle - ANGLE_4, arc_angle),
     y + ypart(ebullet[w][b].angle - ANGLE_4, arc_angle),
     xa + xpart(ebullet[w][b].angle - ANGLE_4, arc_angle),
     ya + ypart(ebullet[w][b].angle - ANGLE_4, arc_angle),
     xa + xpart(ebullet[w][b].angle + ANGLE_4, arc_angle),
     ya + ypart(ebullet[w][b].angle + ANGLE_4, arc_angle), col [wcol]);


   drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);

  break;
 }


   {
    arc_angle = (300 - ebullet[w][b].status) / 15 + 2;
    if (arc_angle > 7)
     arc_angle = 7;
//    arc_angle += grand(2);
    xa = x + xpart(ebullet[w][b].angle, beam_length);
    ya = y + ypart(ebullet[w][b].angle, beam_length);
    poly4(w, x + xpart(ebullet[w][b].angle + ANGLE_4, arc_angle),
     y + ypart(ebullet[w][b].angle + ANGLE_4, arc_angle),
     x + xpart(ebullet[w][b].angle - ANGLE_4, arc_angle),
     y + ypart(ebullet[w][b].angle - ANGLE_4, arc_angle),
     xa + xpart(ebullet[w][b].angle - ANGLE_4, arc_angle),
     ya + ypart(ebullet[w][b].angle - ANGLE_4, arc_angle),
     xa + xpart(ebullet[w][b].angle + ANGLE_4, arc_angle),
     ya + ypart(ebullet[w][b].angle + ANGLE_4, arc_angle), col [3]);

   }

/*   if (ebullet[w][b].status < 270)
   {
    arc_angle = (270 - ebullet[w][b].status) / 15 + 2;
    if (arc_angle > 8)
     arc_angle = 8;
    arc_angle += grand(2);
    poly4(w, x + xpart(ebullet[w][b].angle + ANGLE_4, arc_angle),
     y + ypart(ebullet[w][b].angle + ANGLE_4, arc_angle),
     x + xpart(ebullet[w][b].angle - ANGLE_4, arc_angle),
     y + ypart(ebullet[w][b].angle - ANGLE_4, arc_angle),
     xa + xpart(ebullet[w][b].angle - ANGLE_4, arc_angle),
     ya + ypart(ebullet[w][b].angle - ANGLE_4, arc_angle),
     xa + xpart(ebullet[w][b].angle + ANGLE_4, arc_angle),
     ya + ypart(ebullet[w][b].angle + ANGLE_4, arc_angle), col [4]);

   }*/

   if (ebullet[w][b].status < 240)
   {
    arc_angle = (240 - ebullet[w][b].status) / 15 + 2;
    if (arc_angle > 7)
     arc_angle = 7;
//    arc_angle += grand(2);
    poly4(w, x + xpart(ebullet[w][b].angle + ANGLE_4, arc_angle),
     y + ypart(ebullet[w][b].angle + ANGLE_4, arc_angle),
     x + xpart(ebullet[w][b].angle - ANGLE_4, arc_angle),
     y + ypart(ebullet[w][b].angle - ANGLE_4, arc_angle),
     xa + xpart(ebullet[w][b].angle - ANGLE_4, arc_angle),
     ya + ypart(ebullet[w][b].angle - ANGLE_4, arc_angle),
     xa + xpart(ebullet[w][b].angle + ANGLE_4, arc_angle),
     ya + ypart(ebullet[w][b].angle + ANGLE_4, arc_angle), col [3]);

   }

   if (ebullet[w][b].status < 210)
   {
    arc_angle = (210 - ebullet[w][b].status) / 15 + 2;
    if (arc_angle > 6)
     arc_angle = 6;
//    arc_angle += grand(2);
    poly4(w, x + xpart(ebullet[w][b].angle + ANGLE_4, arc_angle),
     y + ypart(ebullet[w][b].angle + ANGLE_4, arc_angle),
     x + xpart(ebullet[w][b].angle - ANGLE_4, arc_angle),
     y + ypart(ebullet[w][b].angle - ANGLE_4, arc_angle),
     xa + xpart(ebullet[w][b].angle - ANGLE_4, arc_angle),
     ya + ypart(ebullet[w][b].angle - ANGLE_4, arc_angle),
     xa + xpart(ebullet[w][b].angle + ANGLE_4, arc_angle),
     ya + ypart(ebullet[w][b].angle + ANGLE_4, arc_angle), col [2]);

   }

   if (ebullet[w][b].status < 180)
   {
    arc_angle = (180 - ebullet[w][b].status) / 15 + 2;
    if (arc_angle > 5)
     arc_angle = 5;
    arc_angle += grand(2);
    poly4(w, x + xpart(ebullet[w][b].angle + ANGLE_4, arc_angle),
     y + ypart(ebullet[w][b].angle + ANGLE_4, arc_angle),
     x + xpart(ebullet[w][b].angle - ANGLE_4, arc_angle),
     y + ypart(ebullet[w][b].angle - ANGLE_4, arc_angle),
     xa + xpart(ebullet[w][b].angle - ANGLE_4, arc_angle),
     ya + ypart(ebullet[w][b].angle - ANGLE_4, arc_angle),
     xa + xpart(ebullet[w][b].angle + ANGLE_4, arc_angle),
     ya + ypart(ebullet[w][b].angle + ANGLE_4, arc_angle), col [1]);

   }

   if (ebullet[w][b].status < 150)
   {
    arc_angle = (150 - ebullet[w][b].status) / 15 + 2;
    if (arc_angle > 3)
     arc_angle = 3;
//  arc_angle += grand(2);
    poly4(w, x + xpart(ebullet[w][b].angle + ANGLE_4, arc_angle),
     y + ypart(ebullet[w][b].angle + ANGLE_4, arc_angle),
     x + xpart(ebullet[w][b].angle - ANGLE_4, arc_angle),
     y + ypart(ebullet[w][b].angle - ANGLE_4, arc_angle),
     xa + xpart(ebullet[w][b].angle - ANGLE_4, arc_angle),
     ya + ypart(ebullet[w][b].angle - ANGLE_4, arc_angle),
     xa + xpart(ebullet[w][b].angle + ANGLE_4, arc_angle),
     ya + ypart(ebullet[w][b].angle + ANGLE_4, arc_angle), col [0]);

   }

//  textprintf_ex(display, font, x, y - 20, COLOUR_11, -1, "%i", radians_to_angle(atan2((player[0].y / GRAIN - y), (player[0].x / GRAIN - x))));
   drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);
   break;



/*switch(ebullet[w][b].status)
  {
   case 1:
    col [1] = TRANS_DGREEN;
    col [2] = TRANS_DGREEN;
    col [3] = TRANS_DGREEN;
    break;
  }
  drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);
  circlefill(display[w], x, y, 2 + grand(2), col [0]);
  circlefill(display[w], x, y, 3 + grand(2), col [1]);
  circlefill(display[w], x, y, 3 + grand(3), col [2]);
  circlefill(display[w], x - xpart(ebullet[w][b].angle, 5), y - ypart(ebullet[w][b].angle, 6), 2 + grand(2), col [0]);
  circlefill(display[w], x - xpart(ebullet[w][b].angle, 5), y - ypart(ebullet[w][b].angle, 6), 3 + grand(2), col [1]);
  circlefill(display[w], x - xpart(ebullet[w][b].angle, 5), y - ypart(ebullet[w][b].angle, 6), 3 + grand(2), col [2]);
  circlefill(display[w], x - xpart(ebullet[w][b].angle, 10), y - ypart(ebullet[w][b].angle, 12), 1 + grand(2), col [0]);
  circlefill(display[w], x - xpart(ebullet[w][b].angle, 10), y - ypart(ebullet[w][b].angle, 12), 2 + grand(2), col [1]);
  circlefill(display[w], x - xpart(ebullet[w][b].angle, 10), y - ypart(ebullet[w][b].angle, 12), 2 + grand(2), col [2]);
  circlefill(display[w], x - xpart(ebullet[w][b].angle, 13), y - ypart(ebullet[w][b].angle, 17), 1 + grand(3), col [1]);
  circlefill(display[w], x - xpart(ebullet[w][b].angle, 13), y - ypart(ebullet[w][b].angle, 17), 1 + grand(3), col [2]);
  drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);*/
  break;

 }

/*
 int col [6] = {TRANS_WHITE6, TRANS_WHITE5, TRANS_WHITE4, TRANS_WHITE3, TRANS_WHITE2, TRANS_WHITE1};

 if (ebullet[b].pole == POLE_BLACK)
 {
   col [0] = TRANS_BLACK6;
   col [1] = TRANS_BLACK5;
   col [2] = TRANS_BLACK4;
   col [3] = TRANS_BLACK3;
   col [4] = TRANS_BLACK2;
   col [5] = TRANS_BLACK1;
 }

 int x = ebullet[b].x / GRAIN;
 int y = ebullet[b].y / GRAIN;

 int xb, xa, ya, arc_angle, beam_length;



 switch(ebullet[b].type)
 {
  case EBULLET_BEAM1:
   drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);
        beam_length = 500;

        xa = 12 + grand(4);
        if (ebullet[b].status > 240)
         xa = (300 - ebullet[b].status) / 7 + 2 + grand(4);
        if (ebullet[b].status < 30)
         xa = ebullet[b].status / 4 + 2 + grand(4);
         xa /= 3;
         xa *= 2;
        if (xa < 0)
         xa = 0;
        if (xa > 39)
         xa = 39;
        xb = 0;
        if (ebullet[b].pole == POLE_BLACK)
         xb = 1;
        if (ebullet[b].pole == POLE_NONE)
         xb = 2;
     ya = (explode_rle [xb] [xa]->w) / 2;
     draw_trans_rle_sprite(display[w], explode_rle [xb] [xa], x - ya, y - ya);

        if (ebullet[b].status2 != -1)
        {
         beam_length = ebullet[b].status2 / GRAIN;
         xa = 14 + grand(5);
         ya = (explode_rle [xb] [xa]->w) / 2;
         draw_trans_rle_sprite(display, explode_rle [xb] [xa], x - ya + xpart(ebullet[b].angle, beam_length), y - ya + ypart(ebullet[b].angle, beam_length));
         if (arena.counter % 15 == 0)
          create_cloud(CLOUD_SHOCKWAVE, ebullet[b].pole, (x + xpart(ebullet[b].angle, beam_length)) * GRAIN, (y + ypart(ebullet[b].angle, beam_length)) * GRAIN, 0, 0, 20);
// THIS IS REALLY WRONG AND SHOULDN'T BE HERE!
//  HACK HACK HACK
// Remember to make sure it's placed even when not all bullets are drawn
        }
//   if (ebullet[b].status > 200)
ebullet[b].angle += grand(3) - grand(3);

    xa = x + xpart(ebullet[b].angle, beam_length);
    ya = y + ypart(ebullet[b].angle, beam_length);


 if (ebullet[b].status < 30)
 {

    arc_angle = (ebullet[b].status) / 4 + 1;
    if (arc_angle > 8)
     arc_angle = 8;
    arc_angle += grand(2);
    poly4(x + xpart(ebullet[b].angle + ANGLE_4, arc_angle),
     y + ypart(ebullet[b].angle + ANGLE_4, arc_angle),
     x + xpart(ebullet[b].angle - ANGLE_4, arc_angle),
     y + ypart(ebullet[b].angle - ANGLE_4, arc_angle),
     xa + xpart(ebullet[b].angle - ANGLE_4, arc_angle),
     ya + ypart(ebullet[b].angle - ANGLE_4, arc_angle),
     xa + xpart(ebullet[b].angle + ANGLE_4, arc_angle),
     ya + ypart(ebullet[b].angle + ANGLE_4, arc_angle), col [5]);





    arc_angle = ebullet[b].status / 5;
    if (arc_angle > 6)
     arc_angle = 6;
    arc_angle += grand(2);
    poly4(x + xpart(ebullet[b].angle + ANGLE_4, arc_angle),
     y + ypart(ebullet[b].angle + ANGLE_4, arc_angle),
     x + xpart(ebullet[b].angle - ANGLE_4, arc_angle),
     y + ypart(ebullet[b].angle - ANGLE_4, arc_angle),
     xa + xpart(ebullet[b].angle - ANGLE_4, arc_angle),
     ya + ypart(ebullet[b].angle - ANGLE_4, arc_angle),
     xa + xpart(ebullet[b].angle + ANGLE_4, arc_angle),
     ya + ypart(ebullet[b].angle + ANGLE_4, arc_angle), col [3]);


   drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);

  break;
 }


   {
    arc_angle = (300 - ebullet[b].status) / 15 + 2;
    if (arc_angle > 7)
     arc_angle = 7;
    arc_angle += grand(2);
    xa = x + xpart(ebullet[b].angle, beam_length);
    ya = y + ypart(ebullet[b].angle, beam_length);
    poly4(x + xpart(ebullet[b].angle + ANGLE_4, arc_angle),
     y + ypart(ebullet[b].angle + ANGLE_4, arc_angle),
     x + xpart(ebullet[b].angle - ANGLE_4, arc_angle),
     y + ypart(ebullet[b].angle - ANGLE_4, arc_angle),
     xa + xpart(ebullet[b].angle - ANGLE_4, arc_angle),
     ya + ypart(ebullet[b].angle - ANGLE_4, arc_angle),
     xa + xpart(ebullet[b].angle + ANGLE_4, arc_angle),
     ya + ypart(ebullet[b].angle + ANGLE_4, arc_angle), col [5]);

   }

   if (ebullet[b].status < 270)
   {
    arc_angle = (270 - ebullet[b].status) / 15 + 2;
    if (arc_angle > 8)
     arc_angle = 8;
    arc_angle += grand(2);
    poly4(x + xpart(ebullet[b].angle + ANGLE_4, arc_angle),
     y + ypart(ebullet[b].angle + ANGLE_4, arc_angle),
     x + xpart(ebullet[b].angle - ANGLE_4, arc_angle),
     y + ypart(ebullet[b].angle - ANGLE_4, arc_angle),
     xa + xpart(ebullet[b].angle - ANGLE_4, arc_angle),
     ya + ypart(ebullet[b].angle - ANGLE_4, arc_angle),
     xa + xpart(ebullet[b].angle + ANGLE_4, arc_angle),
     ya + ypart(ebullet[b].angle + ANGLE_4, arc_angle), col [4]);

   }

   if (ebullet[b].status < 240)
   {
    arc_angle = (240 - ebullet[b].status) / 15 + 2;
    if (arc_angle > 7)
     arc_angle = 7;
    arc_angle += grand(2);
    poly4(x + xpart(ebullet[b].angle + ANGLE_4, arc_angle),
     y + ypart(ebullet[b].angle + ANGLE_4, arc_angle),
     x + xpart(ebullet[b].angle - ANGLE_4, arc_angle),
     y + ypart(ebullet[b].angle - ANGLE_4, arc_angle),
     xa + xpart(ebullet[b].angle - ANGLE_4, arc_angle),
     ya + ypart(ebullet[b].angle - ANGLE_4, arc_angle),
     xa + xpart(ebullet[b].angle + ANGLE_4, arc_angle),
     ya + ypart(ebullet[b].angle + ANGLE_4, arc_angle), col [3]);

   }

   if (ebullet[b].status < 210)
   {
    arc_angle = (210 - ebullet[b].status) / 15 + 2;
    if (arc_angle > 6)
     arc_angle = 6;
    arc_angle += grand(2);
    poly4(x + xpart(ebullet[b].angle + ANGLE_4, arc_angle),
     y + ypart(ebullet[b].angle + ANGLE_4, arc_angle),
     x + xpart(ebullet[b].angle - ANGLE_4, arc_angle),
     y + ypart(ebullet[b].angle - ANGLE_4, arc_angle),
     xa + xpart(ebullet[b].angle - ANGLE_4, arc_angle),
     ya + ypart(ebullet[b].angle - ANGLE_4, arc_angle),
     xa + xpart(ebullet[b].angle + ANGLE_4, arc_angle),
     ya + ypart(ebullet[b].angle + ANGLE_4, arc_angle), col [2]);

   }

   if (ebullet[b].status < 180)
   {
    arc_angle = (180 - ebullet[b].status) / 15 + 2;
    if (arc_angle > 3)
     arc_angle = 3;
    arc_angle += grand(2);
    poly4(x + xpart(ebullet[b].angle + ANGLE_4, arc_angle),
     y + ypart(ebullet[b].angle + ANGLE_4, arc_angle),
     x + xpart(ebullet[b].angle - ANGLE_4, arc_angle),
     y + ypart(ebullet[b].angle - ANGLE_4, arc_angle),
     xa + xpart(ebullet[b].angle - ANGLE_4, arc_angle),
     ya + ypart(ebullet[b].angle - ANGLE_4, arc_angle),
     xa + xpart(ebullet[b].angle + ANGLE_4, arc_angle),
     ya + ypart(ebullet[b].angle + ANGLE_4, arc_angle), col [1]);

   }

   if (ebullet[b].status < 150)
   {
    arc_angle = (150 - ebullet[b].status) / 15 + 2;
    if (arc_angle > 2)
     arc_angle = 2;
    arc_angle += grand(2);
    poly4(x + xpart(ebullet[b].angle + ANGLE_4, arc_angle),
     y + ypart(ebullet[b].angle + ANGLE_4, arc_angle),
     x + xpart(ebullet[b].angle - ANGLE_4, arc_angle),
     y + ypart(ebullet[b].angle - ANGLE_4, arc_angle),
     xa + xpart(ebullet[b].angle - ANGLE_4, arc_angle),
     ya + ypart(ebullet[b].angle - ANGLE_4, arc_angle),
     xa + xpart(ebullet[b].angle + ANGLE_4, arc_angle),
     ya + ypart(ebullet[b].angle + ANGLE_4, arc_angle), col [0]);

   }

//  textprintf_ex(display, font, x, y - 20, COLOUR_11, -1, "%i", radians_to_angle(atan2((player[0].y / GRAIN - y), (player[0].x / GRAIN - x))));
   drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);
   break;
  case EBULLET_SHOT:
  drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);
  circlefill(display, x, y, 2 + grand(2), col [0]);
  circlefill(display, x, y, 3 + grand(2), col [3]);
  circlefill(display, x, y, 3 + grand(3), col [5]);
  circlefill(display, x - xpart(ebullet[b].angle, 4), y - ypart(ebullet[b].angle, 4), 1 + grand(3), col [0]);
  circlefill(display, x - xpart(ebullet[b].angle, 4), y - ypart(ebullet[b].angle, 4), 2 + grand(3), col [3]);
  circlefill(display, x - xpart(ebullet[b].angle, 4), y - ypart(ebullet[b].angle, 4), 2 + grand(3), col [5]);
  circlefill(display, x - xpart(ebullet[b].angle, 8), y - ypart(ebullet[b].angle, 8), 2 + grand(2), col [3]);
  circlefill(display, x - xpart(ebullet[b].angle, 8), y - ypart(ebullet[b].angle, 8), 1 + grand(2), col [5]);
  drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);
  break;
  case EBULLET_SHOT2:
  drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);
  circlefill(display, x, y, 3 + grand(2), col [0]);
  circlefill(display, x, y, 4 + grand(2), col [3]);
  circlefill(display, x, y, 4 + grand(3), col [5]);
  circlefill(display, x - xpart(ebullet[b].angle, 6), y - ypart(ebullet[b].angle, 6), 2 + grand(3), col [1]);
  circlefill(display, x - xpart(ebullet[b].angle, 6), y - ypart(ebullet[b].angle, 6), 3 + grand(3), col [4]);
  circlefill(display, x - xpart(ebullet[b].angle, 6), y - ypart(ebullet[b].angle, 6), 3 + grand(3), col [5]);
  circlefill(display, x - xpart(ebullet[b].angle, 12), y - ypart(ebullet[b].angle, 12), 1 + grand(2), col [1]);
  circlefill(display, x - xpart(ebullet[b].angle, 12), y - ypart(ebullet[b].angle, 12), 2 + grand(3), col [4]);
  circlefill(display, x - xpart(ebullet[b].angle, 12), y - ypart(ebullet[b].angle, 12), 2 + grand(3), col [5]);
  circlefill(display, x - xpart(ebullet[b].angle, 17), y - ypart(ebullet[b].angle, 17), 1 + grand(3), col [4]);
  circlefill(display, x - xpart(ebullet[b].angle, 17), y - ypart(ebullet[b].angle, 17), 1 + grand(3), col [5]);
  drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);
  break;
 case EBULLET_BALL:

// textprintf_ex(display, font, x, y, COLOUR_11, -1, "%i", which_bullet);

// draw_rle_sprite(display, straight_bullet [0], x - 6, y - 6);


  drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);
  circlefill(display, x, y, 2 + grand(2), col [0]);
  circlefill(display, x, y, 3 + grand(2), col [3]);
  circlefill(display, x, y, 3 + grand(3), col [5]);
  drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);
  break;
  case EBULLET_SPIN:
  drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);
  circlefill(display, x, y, 2 + grand(3), col [0]);
  circlefill(display, x, y, 2 + grand(3), col [3]);
  circlefill(display, x, y, 4 + grand(2), col [4]);
  circlefill(display, x - xpart(ebullet[b].angle, 5), y - ypart(ebullet[b].angle, 4), 1 + grand(2), col [0]);
  circlefill(display, x - xpart(ebullet[b].angle, 5), y - ypart(ebullet[b].angle, 4), 2 + grand(2), col [3]);
//  circlefill(display, x - xpart(ebullet[b].angle + ANGLE_2, 4), y - ypart(ebullet[b].angle + ANGLE_2, 4), 1 + grand(3), col);
  circlefill(display, x - xpart(ebullet[b].angle + ANGLE_2, 5), y - ypart(ebullet[b].angle + ANGLE_2, 4), 1 + grand(2), col [0]);
  circlefill(display, x - xpart(ebullet[b].angle + ANGLE_2, 5), y - ypart(ebullet[b].angle + ANGLE_2, 4), 2 + grand(2), col [3]);
//  circlefill(display, x - xpart(ebullet[b].angle, 8), y - ypart(ebullet[b].angle, 8), 2 + grand(2), col2);
//  circlefill(display, x - xpart(ebullet[b].angle, 8), y - ypart(ebullet[b].angle, 8), 1 + grand(2), col3);
  drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);
  break;

 }
*/
}

void draw_pickups(void)
{
   drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);

 int p, w, x, y, size1, size2, col1, col2, col3, col4, i;
// int col [4] = {TRANS_YELLOW, TRANS_LRED, TRANS_DRED, TRANS_DDRED};
 int col [4] = {TRANS_WHITE, TRANS_YELLOW, TRANS_LRED, TRANS_DRED};
 int angle = arena.counter * 32;
 angle *= -1;
 angle &= 1023;
 int xa, xb;

     int bsize1 = arena.counter % 16;
     int bsize2 = (arena.counter + 8) % 16;


     size1 = arena.counter % 16;
     size2 = (arena.counter + 8) % 16;

//     size1 = (arena.counter / 8) % 16; size2 = ((arena.counter / 8) + 8) % 16;
     //bsize1 = (arena.counter / 8) % 16; bsize2 = ((arena.counter / 8) + 8) % 16;


     col1 = size1 - 6;
     col2 = size1 - 12;
     if (size1 < 6)
     {
        col1 = 0;
     }
     if (size1 < 12)
     {
        col2 = 0;
     }

     col3 = size2 - 6;
     col4 = size2 - 12;
     if (size2 < 6)
     {
        col3 = 0;
     }
     if (size2 < 12)
     {
        col4 = 0;
     }

 for (w = 0; w < 2; w ++)
 {
     angle *= -1;
     angle &= 1023;
  for (p = 0; p < NO_PICKUPS; p ++)
  {
   if (pickup[w][p].exists == 0)
    continue;
   x = pickup[w][p].x / GRAIN;
   y = pickup[w][p].y / GRAIN;
   size1 = bsize1;
   size2 = bsize2;
   switch(pickup[w][p].colour)
   {
/*    case WPN_RED:

     col [0] = TRANS_YELLOW;
     col [1] = TRANS_LRED;
     col [2] = TRANS_DRED;
     col [3] = TRANS_DDRED;
     size1 += 5;
     size2 += 5;

     if (col1 == 0)
      col1 = 1;
     if (col3 == 0)
      col3 = 1;

     if (col1 < 4 && col1 >= 0)
     {
      triangle(display[w],
       x + xpart(angle - ANGLE_4, size1), y + ypart(angle - ANGLE_4, size1),
       x + xpart(angle + ANGLE_16, size1), y + ypart(angle + ANGLE_16, size1),
       x + xpart(angle + ANGLE_2 - ANGLE_16, size1), y + ypart(angle + ANGLE_2 - ANGLE_16, size1),
       col [col1]);
     }
     if (col2 < 4 && col2 >= 0)
     {
      pline(display[w],
       x + xpart(angle - ANGLE_4, size1), y + ypart(angle - ANGLE_4, size1),
       x + xpart(angle + ANGLE_16, size1), y + ypart(angle + ANGLE_16, size1),
       col [col2]);
      pline(display[w],
       x + xpart(angle + ANGLE_16, size1), y + ypart(angle + ANGLE_16, size1),
       x + xpart(angle + ANGLE_2 - ANGLE_16, size1), y + ypart(angle + ANGLE_2 - ANGLE_16, size1),
       col [col2]);
      pline(display[w],
       x + xpart(angle + ANGLE_2 - ANGLE_16, size1), y + ypart(angle + ANGLE_2 - ANGLE_16, size1),
       x + xpart(angle - ANGLE_4, size1), y + ypart(angle - ANGLE_4, size1),
       col [col2]);
     }

     if (col3 < 4 && col3 >= 0)
      triangle(display[w],
       x + xpart(angle - ANGLE_4, size2), y + ypart(angle - ANGLE_4, size2),
       x + xpart(angle + ANGLE_16, size2), y + ypart(angle + ANGLE_16, size2),
       x + xpart(angle + ANGLE_2 - ANGLE_16, size2), y + ypart(angle + ANGLE_2 - ANGLE_16, size2),
       col [col3]);
     if (col4 < 4 && col4 >= 0)
     {
      pline(display[w],
       x + xpart(angle - ANGLE_4, size2), y + ypart(angle - ANGLE_4, size2),
       x + xpart(angle + ANGLE_16, size2), y + ypart(angle + ANGLE_16, size2),
       col [col4]);
      pline(display[w],
       x + xpart(angle + ANGLE_16, size2), y + ypart(angle + ANGLE_16, size2),
       x + xpart(angle + ANGLE_2 - ANGLE_16, size2), y + ypart(angle + ANGLE_2 - ANGLE_16, size2),
       col [col4]);
      pline(display[w],
       x + xpart(angle + ANGLE_2 - ANGLE_16, size2), y + ypart(angle + ANGLE_2 - ANGLE_16, size2),
       x + xpart(angle - ANGLE_4, size2), y + ypart(angle - ANGLE_4, size2),
       col [col4]);
     }
    break;
    case WPN_BLUE:

     col [0] = TRANS_WHITE;
     col [1] = TRANS_LBLUE;
     col [2] = TRANS_DBLUE;
     col [3] = TRANS_DDBLUE;
     //size1 += 5;
//     size2 += 5;

     if (col1 == 0)
      col1 = 1;
     if (col3 == 0)
      col3 = 1;

     if (col1 < 4 && col1 >= 0)
     {
      poly4(w,
       x + xpart(angle + ANGLE_4, size1), y + ypart(angle + ANGLE_4, size1),
       x + xpart(angle + ANGLE_2, size1), y + ypart(angle + ANGLE_2, size1),
       x + xpart(angle - ANGLE_4, size1), y + ypart(angle - ANGLE_4, size1),
       x + xpart(angle, size1), y + ypart(angle, size1),
       col [col1]);
     }
     if (col2 < 4 && col2 >= 0)
     {
      pline(display[w],
       x + xpart(angle + ANGLE_4, size1), y + ypart(angle + ANGLE_4, size1),
       x + xpart(angle + ANGLE_2, size1), y + ypart(angle + ANGLE_2, size1),
       col [col2]);
      pline(display[w],
       x + xpart(angle + ANGLE_2, size1), y + ypart(angle + ANGLE_2, size1),
       x + xpart(angle - ANGLE_4, size1), y + ypart(angle - ANGLE_4, size1),
       col [col2]);
      pline(display[w],
       x + xpart(angle - ANGLE_4, size1), y + ypart(angle - ANGLE_4, size1),
       x + xpart(angle, size1), y + ypart(angle, size1),
       col [col2]);
      pline(display[w],
       x + xpart(angle, size1), y + ypart(angle, size1),
       x + xpart(angle + ANGLE_4, size1), y + ypart(angle + ANGLE_4, size1),
       col [col2]);
     }


     if (col3 < 4 && col3 >= 0)
     {
      poly4(w,
       x + xpart(angle + ANGLE_4, size2), y + ypart(angle + ANGLE_4, size2),
       x + xpart(angle + ANGLE_2, size2), y + ypart(angle + ANGLE_2, size2),
       x + xpart(angle - ANGLE_4, size2), y + ypart(angle - ANGLE_4, size2),
       x + xpart(angle, size2), y + ypart(angle, size2),
       col [col3]);
     }
     if (col4 < 4 && col4 >= 0)
     {
      pline(display[w],
       x + xpart(angle + ANGLE_4, size2), y + ypart(angle + ANGLE_4, size2),
       x + xpart(angle + ANGLE_2, size2), y + ypart(angle + ANGLE_2, size2),
       col [col4]);
      pline(display[w],
       x + xpart(angle + ANGLE_2, size2), y + ypart(angle + ANGLE_2, size2),
       x + xpart(angle - ANGLE_4, size2), y + ypart(angle - ANGLE_4, size2),
       col [col4]);
      pline(display[w],
       x + xpart(angle - ANGLE_4, size2), y + ypart(angle - ANGLE_4, size2),
       x + xpart(angle, size2), y + ypart(angle, size2),
       col [col4]);
      pline(display[w],
       x + xpart(angle, size2), y + ypart(angle, size2),
       x + xpart(angle + ANGLE_4, size2), y + ypart(angle + ANGLE_4, size2),
       col [col4]);
     }



     break;*/
    case WPN_BLUE:
//size1 = (arena.counter / 4) % 16; size2 = ((arena.counter / 4) + 8) % 16;
     col [0] = TRANS_WHITE;
     col [1] = TRANS_LBLUE;
     col [2] = TRANS_DBLUE;
     col [3] = TRANS_DDBLUE;

     if (col1 == 0)
      col1 = 1;
     if (col3 == 0)
      col3 = 1;

     if (col1 < 4 && col1 >= 0)
      circlefill(display[w], x, y, size1, col [col1]);
     if (col2 < 4 && col2 >= 0)
      circle(display[w], x, y, size1, col [col2]);

     if (col3 < 4 && col3 >= 0)
      circlefill(display[w], x, y, size2, col [col3]);
     if (col4 < 4 && col4 >= 0)
      circle(display[w], x, y, size2, col [col4]);
    break;
    case WPN_WHITE:
     xa = 6 + grand(3);
     xb = explode_rle [2] [xa].x;
//     draw_trans_rle_sprite(display[w], explode_rle [2] [xa], x - xb, y - xb);
     size1 = 16 - size1;
     size2 = 16 - size2;
     xa = 3 + grand(3) - size1 / 5;
     if (xa < 1)
      xa = 1;
     xb = explode_rle [2] [xa].x;
     for (i = 0; i < 6; i ++)
     {
      draw_trans_rle_sprite(display[w], explode_rle [2] [xa].sprite, x + xpart(angle + i * (ANGLE_1 / 6), size1) - xb, y + ypart(angle + i * (ANGLE_1 / 6), size1) - xb);
     }
     xa = 3 + grand(3) - size2 / 5;
     if (xa < 1)
      xa = 1;
     xb = explode_rle [2] [xa].x;
     for (i = 0; i < 6; i ++)
     {
      draw_trans_rle_sprite(display[w], explode_rle [2] [xa].sprite, x + xpart(ANGLE_1 - angle + i * (ANGLE_1 / 6), size2) - xb, y + ypart(ANGLE_1 - angle + i * (ANGLE_1 / 6), size2) - xb);
     }

     break;
    case WPN_RED:
     xa = 3 + grand(3);
     xb = explode_rle [2] [xa].x;
     for (i = 0; i < 3; i ++)
     {
      draw_trans_rle_sprite(display[w], explode_rle [0] [xa].sprite, x + xpart(angle + i * ANGLE_3, 12) - xb, y + ypart(angle + i * ANGLE_3, 12) - xb);
     }
     for (i = 0; i < 3; i ++)
     {
      draw_trans_rle_sprite(display[w], explode_rle [0] [xa].sprite, x + xpart(ANGLE_1 - (angle + i * ANGLE_3), 6) - xb, y + ypart(ANGLE_1 - (angle + i * ANGLE_3), 6) - xb);
     }
     break;
    case WPN_GREEN:
     xa = 10 + grand(3);
     xb = (green_ring [xa]->w) / 2;
     draw_trans_rle_sprite(display[w], green_ring [xa], x - xb, y - xb);
     size1 = 5 + grand(3);
     xb = (green_ring [size1]->w) / 2;
     for (i = 0; i < 3; i ++)
     {
      draw_trans_rle_sprite(display[w], green_ring [size1], x - xb + xpart(angle + ANGLE_3 * i, xa), y - xb + ypart(angle + ANGLE_3 * i, xa));
     }
     break;


   }
//   circlefill(display [w], x, y, 7, col1);
  }
 }

   drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);

}




void draw_clouds(void)
{

 int c ,w;

 for (w = 0; w < 2; w ++)
 {
  for (c = 0; c < NO_CLOUDS; c ++)
  {
   if (cloud[w][c].type == CLOUD_NONE)
    continue;
   draw_a_cloud(w, c);
  }
 }

}

void draw_a_cloud(int w, int c)
{


 int x = cloud[w][c].x / GRAIN;
 int y = cloud[w][c].y / GRAIN;

 int xa, xb, col1, col2, ya;

 switch(cloud[w][c].type)
 {
  case CLOUD_EXPLODE2:
   drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);
   switch(cloud[w][c].type2)
   {
    case 0:
     circlefill(display[w], x, y, cloud[w][c].timeout, TRANS_LRED);
     circle(display[w], x, y, cloud[w][c].timeout, TRANS_YELLOW);
     break;
    case 1:
     circlefill(display[w], x, y, cloud[w][c].timeout, TRANS_LGREEN);
     circle(display[w], x, y, cloud[w][c].timeout, TRANS_YELLOW);
     break;
    case 2:
     circlefill(display[w], x, y, cloud[w][c].timeout, TRANS_LBLUE);
     circle(display[w], x, y, cloud[w][c].timeout, TRANS_WHITE);
     break;


   }
   drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);
   break;
  case CLOUD_SHOCKWAVE2:
        xa = cloud[w][c].timeout;
        if (xa < 0)
         xa = 0;
        if (xa > 19)
         xa = 19;
        xb = cloud[w][c].type2;
     ya = shock2_rle [xb] [19 - xa].x;
     draw_trans_rle_sprite(display[w], shock2_rle [xb] [19 - xa].sprite, x - ya, y - ya);
   break;
  case CLOUD_LARGE_SHOCKWAVE2:
        xa = cloud[w][c].timeout;
        if (xa < 0)
         xa = 0;
        if (xa > 49)
         xa = 49;
        xb = cloud[w][c].type2;
     ya = large_shock2_rle [xb] [49 - xa].x;
     draw_trans_rle_sprite(display[w], large_shock2_rle [xb] [49 - xa].sprite, x - ya, y - ya);
   break;
  case CLOUD_SHOCKWAVE:
        xa = cloud[w][c].timeout;
        if (xa < 0)
         xa = 0;
        if (xa > 19)
         xa = 19;
        xb = cloud[w][c].type2;
     ya = (shock_rle [xb] [19 - xa]->w) / 2;
     draw_trans_rle_sprite(display[w], shock_rle [xb] [19 - xa], x - ya, y - ya);
   break;
  case CLOUD_LARGE_SHOCKWAVE:
        xa = cloud[w][c].timeout;
        if (xa < 0)
         xa = 0;
        if (xa > 49)
         xa = 49;
        xb = cloud[w][c].type2;
     ya = (large_shock_rle [xb] [49 - xa]->w) / 2;
     draw_trans_rle_sprite(display[w], large_shock_rle [xb] [49 - xa], x - ya, y - ya);
   break;
  case CLOUD_SPRAY:
  case CLOUD_EXPLODE:
  case CLOUD_DELAY_EXPLODE:
        xa = cloud[w][c].timeout - 1;
        if (xa < 0)
         xa = 0;
        if (xa > 39)
         xa = 39;
        xb = cloud[w][c].type2;
     ya = explode_rle [xb] [xa].x;
     draw_trans_rle_sprite(display[w], explode_rle [xb] [xa].sprite, x - ya, y - ya);
   break;
  case CLOUD_SLOW_EXPLODE:
  case CLOUD_DRAG_EXPLODE:
        xa = cloud[w][c].timeout / 5;
        if (xa < 0)
         xa = 0;
        if (xa > 39)
         xa = 39;
        xb = cloud[w][c].type2;
//     if (xa < 2) xa = 2;
     ya = explode_rle [xb] [xa].x;
     draw_trans_rle_sprite(display[w], explode_rle [xb] [xa].sprite, x - ya, y - ya);
   break;
  case CLOUD_SEEKER_TRAIL:
   xa = cloud[w][c].timeout / 2;
   if (xa > 6)
    xa = 6;
   ya = xa - 2;
   switch(xa)
   {
    default:
    case 6:
    case 5: col1 = TRANS_YELLOW; break;
    case 4:
    case 3: col1 = TRANS_LRED; break;
    case 2:
    case 1: col1 = TRANS_DRED; break;
    case 0: col1 = TRANS_DDRED; break;
   }

   drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);
   pline(display[w], x, y, cloud[w][c].x2 / GRAIN, cloud[w][c].y2 / GRAIN, col1);
   drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);
    break;
  case CLOUD_ZAP:
   xa = cloud[w][c].timeout / 2;
   if (xa > 6)
    xa = 6;
   ya = xa - 2;
   switch(cloud[w][c].type2)
   {
    case 1:
     switch(xa)
     {
      default:
      case 6:
      case 5: col1 = TRANS_YELLOW; break;
      case 4:
      case 3: col1 = TRANS_LRED; break;
      case 2:
      case 1: col1 = TRANS_DRED; break;
      case 0: col1 = TRANS_DDRED; break;
     }
      break;
    case 2:
     switch(xa)
     {
      default:
      case 6:
      case 5: col1 = TRANS_WHITE; break;
      case 4:
      case 3: col1 = TRANS_YELLOW; break;
      case 2: col1 = TRANS_LRED; break;
      case 1: col1 = TRANS_DRED; break;
      case 0: col1 = TRANS_DDRED; break;
     }
      break;
    case 3:
     switch(xa)
     {
      default:
      case 6:
      case 5: col1 = TRANS_YELLOW; break;
      case 4:
      case 3: col1 = TRANS_LGREEN; break;
      case 2:
      case 1: col1 = TRANS_DGREEN; break;
      case 0: col1 = TRANS_DDGREEN; break;
     }
      break;
    case 4:
     switch(xa)
     {
      default:
      case 6:
      case 5: col1 = TRANS_WHITE; break;
      case 4:
      case 3: col1 = TRANS_LBLUE; break;
      case 2:
      case 1: col1 = TRANS_DBLUE; break;
      case 0: col1 = TRANS_DDBLUE; break;
     }
      break;
    case 5:
     switch(xa)
     {
      default:
      case 6:
      case 5: col1 = TRANS_WHITE; break;
      case 4:
      case 3: col1 = TRANS_LPURPLE; break;
      case 2:
      case 1: col1 = TRANS_DPURPLE; break;
      case 0: col1 = TRANS_DDBLUE; break;
     }
      break;
   }
   drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);
   pline(display[w], x, y, cloud[w][c].x2 / GRAIN, cloud[w][c].y2 / GRAIN, col1);
   drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);
    break;
   case CLOUD_BLUE_WAVE:
    col1 = TRANS_LBLUE;
//    col1 = TRANS_LPURPLE;
//  if (cloud[w][c].type2 < 8) col1 = TRANS_DBLUE;
//    if (cloud[w][c].type2 < 4) col1 = TRANS_DBLUE;
//    if (cloud[w][c].type2 < 4) col1 = TRANS_DPURPLE;
    drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);
    rectfill(display[w], x - cloud[w][c].type2, y - 5, x + cloud[w][c].type2, y + 4, col1);
    drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);
    break;
   case CLOUD_PURPLE_WAVE:
    col1 = TRANS_LPURPLE;
    drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);
    rectfill(display[w], x - cloud[w][c].type2, y - 5, x + cloud[w][c].type2, y + 4, col1);
    drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);
    break;
   case CLOUD_GREEN_WAVE:
    col1 = TRANS_YELLOW;
    if (cloud[w][c].type2 < 22) col1 = TRANS_LGREEN;
    if (cloud[w][c].type2 < 9) col1 = TRANS_DGREEN;
    drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);
    rectfill(display[w], x - cloud[w][c].type2, y - 5, x + cloud[w][c].type2, y + 4, col1);
    drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);
    break;
   case CLOUD_RED_WAVE:
    col1 = TRANS_LRED;
//    if (cloud[w][c].type2 < 42) col1 = TRANS_LRED;
    if (cloud[w][c].type2 < 20) col1 = TRANS_DRED;
    if (cloud[w][c].type2 < 10) col1 = TRANS_DDRED;
    drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);
    rectfill(display[w], x - cloud[w][c].type2, y - 5, x + cloud[w][c].type2, y + 4, col1);
    drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);
    break;
   case CLOUD_GREEN_RING:
   /*    col1 = TRANS_YELLOW;
    col2 = TRANS_LGREEN;
    if (cloud[w][c].timeout < 7)
    {
     col1 = TRANS_LGREEN;
     col2 = TRANS_DGREEN;
    }
    if (cloud[w][c].timeout < 5)
     col1 = TRANS_DGREEN;
    drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);
    if (cloud[w][c].timeout >= 7)
    {
     circle(display[w], x, y, cloud[w][c].type2 / GRAIN + 2, TRANS_DGREEN);
     circle(display[w], x, y, cloud[w][c].type2 / GRAIN - 2, TRANS_DGREEN);
    }
    if (cloud[w][c].timeout >= 5)
    {
     circle(display[w], x, y, cloud[w][c].type2 / GRAIN + 1, col2);
     circle(display[w], x, y, cloud[w][c].type2 / GRAIN - 1, col2);
    }
    circle(display[w], x, y, cloud[w][c].type2 / GRAIN, col1);
    drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);*/
    col1 = TRANS_YELLOW;
    col2 = TRANS_LGREEN;
    if (cloud[w][c].timeout < 11)
    {
     col1 = TRANS_LGREEN;
     col2 = TRANS_DGREEN;
    }
    if (cloud[w][c].timeout < 7)
     col1 = TRANS_DGREEN;
    drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);
    if (cloud[w][c].timeout >= 11)
    {
     circle(display[w], x, y, cloud[w][c].type2 / GRAIN + 2, TRANS_DGREEN);
     circle(display[w], x, y, cloud[w][c].type2 / GRAIN - 2, TRANS_DGREEN);
    }
    if (cloud[w][c].timeout >= 7)
    {
     circle(display[w], x, y, cloud[w][c].type2 / GRAIN + 1, col2);
     circle(display[w], x, y, cloud[w][c].type2 / GRAIN - 1, col2);
    }
    circle(display[w], x, y, cloud[w][c].type2 / GRAIN, col1);
    drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);
    break;
   case CLOUD_GREEN2_RING:
    xa = cloud[w][c].timeout;
    if (xa > 39)
     xa = 39;
    xb = (green_ring [xa]->w) / 2;
    draw_trans_rle_sprite(display[w], green_ring [xa], x - xb, y - xb);
    break;

/*    case CLOUD_SEEKER_TRAIL:
   xa = cloud[w][c].timeout / 2;
   if (xa > 6)
    xa = 6;
   ya = xa - 2;
   col1 = TRANS_YELLOW;
   col2 = TRANS_LRED;
//   col1 += xa * 12;
//   col2 += ya * 12;
   drawing_mode(DRAW_MODE_TRANS, NULL, 0, 0);
   if (cloud[w][c].timeout <= 2)
    triangle(display[w], x + xpart(cloud[w][c].angle + ANGLE_4, 2), y + ypart(cloud[w][c].angle + ANGLE_4, 2),
          x - xpart(cloud[w][c].angle + ANGLE_4, 2), y - ypart(cloud[w][c].angle + ANGLE_4, 2),
          (cloud[w][c].x2 / GRAIN), (cloud[w][c].y2 / GRAIN), col1);
     else
     {
       poly4(w, x + xpart(cloud[w][c].angle + ANGLE_4, 2), y + ypart(cloud[w][c].angle + ANGLE_4, 2),
          x - xpart(cloud[w][c].angle + ANGLE_4, 2), y - ypart(cloud[w][c].angle + ANGLE_4, 2),
          (cloud[w][c].x2 / GRAIN) - xpart(cloud[w][c].angle + ANGLE_4, 2), (cloud[w][c].y2 / GRAIN) - ypart(cloud[w][c].angle + ANGLE_4, 2),
          (cloud[w][c].x2 / GRAIN) + xpart(cloud[w][c].angle + ANGLE_4, 2), (cloud[w][c].y2 / GRAIN) + ypart(cloud[w][c].angle + ANGLE_4, 2),
          col1);
      if (ya > 0)
       poly4(w, x + xpart(cloud[w][c].angle + ANGLE_4, 4), y + ypart(cloud[w][c].angle + ANGLE_4, 4),
          x - xpart(cloud[w][c].angle + ANGLE_4, 4), y - ypart(cloud[w][c].angle + ANGLE_4, 4),
          (cloud[w][c].x2 / GRAIN) - xpart(cloud[w][c].angle + ANGLE_4, 4), (cloud[w][c].y2 / GRAIN) - ypart(cloud[w][c].angle + ANGLE_4, 4),
          (cloud[w][c].x2 / GRAIN) + xpart(cloud[w][c].angle + ANGLE_4, 4), (cloud[w][c].y2 / GRAIN) + ypart(cloud[w][c].angle + ANGLE_4, 4),
          col2);
     }
   drawing_mode(DRAW_MODE_SOLID, NULL, 0, 0);

*/
    break;
 }

}


void poly4(int w, int x1, int y1, int x2, int y2, int x3, int y3, int x4, int y4, int col)
{

  points [0] = x1;
  points [1] = y1;
  points [2] = x2;
  points [3] = y2;
  points [4] = x3;
  points [5] = y3;
  points [6] = x4;
  points [7] = y4;

  polygon(display[w], 4, points, col);


}


void draw_underlay(void)
{

 switch(arena.level)
 {
  case 1:
   blit(underlay, display [0], 0, arena.underlay_position, 0, 0, 320, 480);
   blit(underlay2, display [1], 0, arena.underlay_position, 0, 0, 320, 480);
   break;
  case 2:
   clear_to_color(display [0], COL_LOWER_BG1);
   clear_to_color(display [1], COL_UPPER_BG1);
   int i;
   for (i = 0; i < 13; i ++)
   {
    vline(display[0], i * 25 + 1 + 15 + arena.underlay2_position2, 0, 480, COL_OUTLINE);
    vline(display[0], i * 25 - 1 + 15 + arena.underlay2_position2, 0, 480, COL_OUTLINE);
    vline(display[1], i * 25 + 1 + 15 + arena.underlay2_position2, 0, 480, COL_OUTLINE);
    vline(display[1], i * 25 - 1 + 15 + arena.underlay2_position2, 0, 480, COL_OUTLINE);
   }
   for (i = 0; i < 20; i ++)
   {
    hline(display[0], 0, i * 25 - 1 + arena.underlay2_position, 320, COL_OUTLINE);
    hline(display[0], 0, i * 25 + 1 + arena.underlay2_position, 320, COL_OUTLINE);
    hline(display[1], 0, i * 25 - 1 + arena.underlay2_position, 320, COL_OUTLINE);
    hline(display[1], 0, i * 25 + 1 + arena.underlay2_position, 320, COL_OUTLINE);
   }
   for (i = 0; i < 13; i ++)
   {\
    vline(display[0], i * 25 + 15 + arena.underlay2_position2, 0, 480, COL_LOWER_BG2);
    vline(display[1], i * 25 + 15 + arena.underlay2_position2, 0, 480, COL_UPPER_BG2);
   }
   for (i = 0; i < 20; i ++)
   {
    hline(display[0], 0, i * 25 + arena.underlay2_position, 320, COL_LOWER_BG2);
    hline(display[1], 0, i * 25 + arena.underlay2_position, 320, COL_UPPER_BG2);
   }


   for (i = 0; i < 5; i ++)
   {
    vline(display[0], i * 60 + 1 + 35 + arena.underlay_position2, 0, 480, COL_OUTLINE);
    vline(display[0], i * 60 - 1 + 35 + arena.underlay_position2, 0, 480, COL_OUTLINE);
    vline(display[1], i * 60 + 1 + 35 + arena.underlay_position2, 0, 480, COL_OUTLINE);
    vline(display[1], i * 60 - 1 + 35 + arena.underlay_position2, 0, 480, COL_OUTLINE);
   }
   for (i = 0; i < 8; i ++)
   {
    hline(display[0], 0, i * 60 - 1 + arena.underlay_position, 320, COL_OUTLINE);
    hline(display[0], 0, i * 60 + 1 + arena.underlay_position, 320, COL_OUTLINE);
    hline(display[1], 0, i * 60 - 1 + arena.underlay_position, 320, COL_OUTLINE);
    hline(display[1], 0, i * 60 + 1 + arena.underlay_position, 320, COL_OUTLINE);
   }
   for (i = 0; i < 5; i ++)
   {
    vline(display[0], i * 60 + 35 + arena.underlay_position2, 0, 480, COL_LOWER_BG3);
    vline(display[1], i * 60 + 35 + arena.underlay_position2, 0, 480, COL_UPPER_BG3);
   }
   for (i = 0; i < 8; i ++)
   {
    hline(display[0], 0, i * 60 + arena.underlay_position, 320, COL_LOWER_BG3);
    hline(display[1], 0, i * 60 + arena.underlay_position, 320, COL_UPPER_BG3);
   }
   break;
 }

}

void run_underlay(void)
{

 switch(arena.level)
 {
  case 1:
   if (arena.counter % 2 == 0)
    arena.underlay_position -= 1;
   if (arena.underlay_position <= 0)
    arena.underlay_position += 480;
   break;
  case 2:
   arena.underlay_position += 3;
   if (arena.underlay_position >= 60)
    arena.underlay_position -= 60;
   arena.underlay2_position += 1;
   if (arena.underlay2_position >= 25)
    arena.underlay2_position -= 25;

   arena.underlay_position2 = xpart(arena.counter * 8, 5);
   arena.underlay2_position2 = xpart(arena.counter * 4, 3);
   break;
 }


}

void draw_platform(void)
{
 if (arena.level == 2)
  return;

 int i, j;

 int start = platform_step;
 int across = 0;
 int w;

 for (w = 0; w < 2; w ++)
 {
 for (j = 0; j < 10; j ++)
 {
  across = start - j;
  if (across < 0)
    across += PLAT_Y;
  if (across >= PLAT_Y)
   across -= PLAT_Y;

  for (i = 0; i < 9; i ++)
  {
      if (platform_tile [i] [across] != PLATFORM_EMPTY)
       draw_rle_sprite(display [w], platform_RLE [w] [platform_tile [i] [across]], i * 55 - 82, j * 55 - 75 + platform_position);
//       if ( i % 2 == 0)
//  textprintf_ex(display [0], font, i * 56, j * 56 + platform_position, COL_WHITE, -1, "(%i.%i.%i)", i, across, platform_tile [i] [across]);


//       draw_rle_sprite(display [0], platform_RLE [1], i * 55 - 27, j * 55 - 27 + platform_position);
  }
 }
 }

}

int detect_player_collision(void)
{


  int x = player.x / GRAIN;
  int y = player.y / GRAIN;

  if (check_pixel(player.sides, x, y)
   || check_pixel(player.sides, x + 1, y + 1)
   || check_pixel(player.sides, x + 1, y - 1))
    return player.sides;

 int w = 0;
 if (player.sides == 0)
  w = 1;

// centre is at 15, 20 of large sprite

  if (check_pixel(w, x, y)
   || check_pixel(w, x - 2, y - 2)
   || check_pixel(w, x + 2, y - 2)
   || check_pixel(w, x - 2, y + 2)
   || check_pixel(w, x + 2, y + 2)
   || check_pixel(w, x + 2, y + 2)
   || check_pixel(w, x, y + 2)
   || check_pixel(w, x, y - 3)
   || check_pixel(w, x - 1, y - 3)
   || check_pixel(w, x + 1, y - 3))
    return w;

  return -1;

}

int check_pixel(int w, int x, int y)
{
  int pix = getpixel(display [w], x, y);

  if (pix >= TRANS_WHITE && pix < TRANS_LBLUE)
   return 1;

return 0;
}

void pline(BITMAP *bmp, int x1, int y1, int x2, int y2, int colour)
{
 if (x1 < -500 || x1 > 900
     || x2 < -500 || x2 > 900
     || y2 < -500 || y2 > 900
     || y2 < -500 || y2 > 900)
      return;

 line(bmp, x1, y1, x2, y2, colour);

}


