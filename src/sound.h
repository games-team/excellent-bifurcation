
void init_sound(void);

void init_mtracks(void);

void run_loops();
void run_mtracks();


void play_sound(int sample);
void play_sound2(int sample, int frq, int vol, int pan);
void play_soundf(int sample, int frq);
void play_sound_pos(int sample, int frq, int vol, int x2, int y2);

void play_wav(int sample);
void play_wav2(int sample, int frq, int vol, int pan);
void play_wavf(int sample, int frq);
void play_wav_pos(int sample, int frq, int vol, int x2, int y2);


void play_effect(int sample);
void play_effectf(int sample, int freq);
void play_effectw(int w, int sample);
void play_effectwf(int w, int sample, int f);
void play_effectwfx(int w, int sample, int f, int x);
void play_effectwfvx(int w, int sample, int f, int v, int x);

enum
{
SOUNDMODE_OFF,
SOUNDMODE_MONO,
SOUNDMODE_STEREO,
SOUNDMODE_REVERSED
};

enum
{
NWAV_NONE,
NWAV_BANG,
NWAV_BIGBANG,
NWAV_FIRE,
NWAV_FIRE2,
NWAV_BLAST,
NWAV_ZAP,
NWAV_BEAM1,
NWAV_BEAM2,
NWAV_CIRCLE,
NWAV_PBEAM2,
NWAV_PBEAM1,
NWAV_SHOT,
NWAV_POP,
NWAV_BLIP,
NWAV_BLIP2,
NWAV_TAP,
NWAV_TONE,
NWAV_BLAST2,
NWAV_BEAM3,
NWAV_WAVE,
NWAV_POWERUP,
NWAV_BLOP,
NO_NWAVS
};

