#include "config.h"

#include "allegro.h"

#include "globvars.h"

#include "stuff.h"
#include "cloud.h"

#include "palette.h"



void run_pickups(void);
void destroy_pickup(int w, int p);
int create_pickup(int w, int colour, int x, int y);



void init_pickups(void)
{

 int p, w;
 for (w = 0; w < 2; w ++)
 {
  for (p = 0; p < NO_PICKUPS; p ++)
  {
   pickup[w][p].exists = 0;
  }
 }

}


int create_pickup(int w, int colour, int x, int y)
{
 int p;

  for (p = 0; p < NO_PICKUPS; p ++)
  {
    if (pickup[w][p].exists == 0)
    {
     pickup[w][p].colour = colour;
     pickup[w][p].exists = 1;
     pickup[w][p].x = x;
     pickup[w][p].y = y;
     return p;
    }
  }

  return -1;
}

void run_pickups(void)
{

  int p, w;

  for (w = 0; w < 2; w ++)
  {
  for (p = 0; p < NO_PICKUPS; p ++)
  {
    if (pickup[w][p].exists == 0)
     continue;

    pickup[w][p].y += 1000;

    if (pickup[w][p].y >= 510000)
     destroy_pickup(w, p);

  }
  }
}


void destroy_pickup(int w, int p)
{
  pickup[w][p].exists = 0;
}

void explode_pickup(int w, int p)
{
  int col = 0;
  switch(pickup[w][p].colour)
  {
   case WPN_WHITE:
   case WPN_BLUE: col = 2; break;
   case WPN_GREEN: col = 1; break;

  }
  create_cloud(w, CLOUD_SHOCKWAVE, col, pickup[w][p].x, pickup[w][p].y, 0, 0, 20);
  destroy_pickup(w, p);
}







