// Global variables:

extern struct gamestruct game;

extern struct arenastruct arena;

extern struct playerstruct player;

extern struct ebulletstruct ebullet [2] [NO_EBULLETS];

extern struct pbulletstruct pbullet [2] [NO_PBULLETS];

extern unsigned char counter;

extern struct enemystruct enemy [2] [NO_ENEMIES];

extern struct eclassstruct eclass [NO_ENEMY_TYPES];

extern struct cloudstruct cloud [2] [NO_CLOUDS];

extern struct seekerstruct seeker [2] [NO_SEEKERS];

extern struct pickupstruct pickup [2] [NO_PICKUPS];

extern struct bossstruct boss;

extern struct optionstruct options;


// Remember: They still have to be defined somewhere

//   --- end global variables
