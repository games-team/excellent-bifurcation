
void init_ebullets(void);
int create_ebullet(int w, int type, int status, int x, int y, int xs, int ys, int angle);
int shoot_triangle(int w, int x, int y, int angle, int type);
int shoot_diamond(int w, int x, int y, int xs, int ys, int angle, int type, int turn_direction);
int shoot_star(int w, int x, int y, int xs, int ys, int angle, int type, int turn_direction);
int shoot_circle(int w, int x, int y, int xs, int ys, int angle, int type);
void run_ebullets(void);
