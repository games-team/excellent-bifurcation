
#include "config.h"

#include "allegro.h"

#include "globvars.h"

#include "palette.h"
#include "game.h"

 RGB splash_palette [256];

 BITMAP *splash_bitmap;

void key_box(const char ktext [], int command);
int acceptable_char(int scode);
void define_keys(void);


extern volatile unsigned char ticked;

extern BITMAP *display [3];
extern struct optionstruct options;

int menu_select;
int key_wait;
int thing;


void startup_menu(void)
{

 int counter = 0;
 menu_select = 0;
 key_wait = 30;


 int y1 = 170;
 int y2 = 190 + menu_select * 30;
 int y3 = 218 + menu_select * 30;
 int y4 = 218 + menu_select * 30;

 int anykey = 0;

 int i;

 while (TRUE)
 {

 clear_bitmap(display[2]);

 counter += 4;
 if (counter >= 40)
  counter = 0;

 vline(display [2], 320, 0, 480, COL_WHITE);
 vline(display [2], 319, 0, 480, COL_LRED);
 vline(display [2], 321, 0, 480, COL_LBLUE);

 vline(display [2], 318 - counter / 8, 0, 480, TRANS_LRED + 0);
 vline(display [2], 313 - counter / 8, 0, 480, TRANS_LRED + 0);
 vline(display [2], 308 - counter / 8, 0, 480, TRANS_DRED + 0);
 if (counter < 20)
  vline(display [2], 303 - counter / 8, 0, 480, TRANS_DRED + 0);
   else
    vline(display [2], 303 - counter / 8, 0, 480, TRANS_DDRED + 0);

 vline(display [2], 322 + counter / 8, 0, 480, TRANS_LBLUE + 0);
 vline(display [2], 327 + counter / 8, 0, 480, TRANS_LBLUE + 0);
 vline(display [2], 332 + counter / 8, 0, 480, TRANS_DBLUE + 0);
 if (counter < 20)
  vline(display [2], 337 + counter / 8, 0, 480, TRANS_DBLUE + 0);
   else
    vline(display [2], 337 + counter / 8, 0, 480, TRANS_DDBLUE + 0);


 textprintf_right_ex(display [2], font, 320, 100, -1, -1, "E X C E L L E N T          ");
 textprintf_ex(display [2], font, 320, 100, -1, -1, "          B I F U R C A T I O N");

 y1 = 177;
 y2 = 190 + menu_select * 30;
 y3 = 219 + menu_select * 30;
 y4 = 350;


 rectfill(display [2], 370, y1, 600, y2, TRANS_DGREEN);
 rectfill(display [2], 370, y2 + 5, 600, y3 - 5, TRANS_LGREEN);
 rectfill(display [2], 370, y3, 600, y4, TRANS_DGREEN);

 textprintf_ex(display [2], font, 400, 200, -1, -1, "START GAME");
 switch(arena.difficulty)
 {
      case 0: textprintf_ex(display [2], font, 400, 230, -1, -1, "DIFFICULTY - EASY"); break;
      case 1: textprintf_ex(display [2], font, 400, 230, -1, -1, "DIFFICULTY - NORMAL"); break;
      case 2: textprintf_ex(display [2], font, 400, 230, -1, -1, "DIFFICULTY - HARD"); break;
 }
 textprintf_ex(display [2], font, 400, 260, -1, -1, "STAGE - %i", arena.starting_level);
 textprintf_ex(display [2], font, 400, 290, -1, -1, "SET  CONTROLS");
 textprintf_ex(display [2], font, 400, 320, -1, -1, "EXIT");

// arena.just_got_highscore = 1;


 y1 = 197;
 y2 = 235 + arena.just_got_highscore * 30;
 y3 = 264 + arena.just_got_highscore * 30;
 y4 = 341;

 rectfill(display [2], 50, 197, 250, 227, TRANS_LRED);

 if (arena.just_got_highscore == -1)
 {
  rectfill(display [2], 50, 227 + 5, 250, y4, TRANS_DRED);
 }
  else
  {
   rectfill(display [2], 50, 227 + 5, 250, y2, TRANS_DRED);
   rectfill(display [2], 50, y2 + 5, 250, y3, TRANS_YELLOW);
   rectfill(display [2], 50, y3 + 5, 250, y4, TRANS_DRED);
  }

 textprintf_right_ex(display [2], font, 137, 207, -1, -1, "HIGH");
 textprintf_ex(display [2], font, 157, 207, -1, -1, "SCORES");

 textprintf_right_ex(display [2], font, 137, 247, -1, -1, "EASY");
 textprintf_ex(display [2], font, 157, 247, -1, -1, "%i", options.highscore [0]);

 textprintf_right_ex(display [2], font, 137, 277, -1, -1, "NORMAL");
 textprintf_ex(display [2], font, 157, 277, -1, -1, "%i", options.highscore [1]);

 textprintf_right_ex(display [2], font, 137, 307, -1, -1, "HARD");
 textprintf_ex(display [2], font, 157, 307, -1, -1, "%i", options.highscore [2]);

 rectfill(display [2], 144, 232, 149, 341, 0);

 /*if (keypressed())
 {
  key_wait = 0;
  circle(display [2], 100, 100, 5, 10);
 }*/

 anykey = 0;

  for (i = KEY_A; i < KEY_CAPSLOCK + 1; i ++)
  {
   if (key [i])
   {
    anykey = 1;
   }
  }

  if (anykey == 0)
   key_wait = 0;



 if (key_wait == 0)
 {
  if (key [KEY_UP] || key [KEY_8_PAD])
  {
   menu_select --;
   if (menu_select < 0)
    menu_select = 4;
   key_wait = 7;
  }
  if (key [KEY_DOWN] || key [KEY_2_PAD])
  {
   menu_select ++;
   if (menu_select > 4)
    menu_select = 0;
   key_wait = 7;
  }
  if (key [KEY_LEFT] || key [KEY_4_PAD])
  {
   if (menu_select == 2)
    arena.starting_level = 1;
   if (menu_select == 1)
   {
    arena.difficulty --;
    if (arena.difficulty < 0)
     arena.difficulty = 0;
   }
   key_wait = 7;
  }
  if (key [KEY_RIGHT] || key [KEY_6_PAD])
  {
   if (menu_select == 2)
    arena.starting_level = 2;
   if (menu_select == 1)
   {
    arena.difficulty ++;
    if (arena.difficulty > 2)
     arena.difficulty = 2;
   }
   key_wait = 7;
  }

//  if (key [KEY_ESC])
//   exit(0);
  if (key [KEY_ENTER] || key [KEY_SPACE] || key [KEY_Z])
  {
   if (menu_select == 4)
    exit(0);

   if (menu_select == 3)
   {
    key_wait = 10;
    define_keys();
    key_wait = 10;
   }

   if (menu_select == 0)
   {
    arena.level = arena.starting_level;
    ticked = 0;
    new_game();
    game_loop();
   }
   key_wait = 20;
  }
 }
  else
   key_wait --;

/*
 textprintf_centre_ex(screen, font, 320, 210, -1, -1, "SEE README FOR INSTRUCTIONS");

 textprintf_centre_ex(screen, font, 320, 240, -1, -1, "PRESS 1 FOR EASY");
 textprintf_centre_ex(screen, font, 320, 255, -1, -1, "PRESS 2 FOR NORMAL");
 textprintf_centre_ex(screen, font, 320, 270, -1, -1, "PRESS 3 FOR HARD");

 textprintf_centre_ex(screen, font, 320, 300, -1, -1, "PRESS ESCAPE TO EXIT");
*/
// textprintf_centre_ex(screen, font, 320, 330, 10, 15, "HAVE YOU REMOVED THE DEBUG KEYS???");

 rectfill(display [2], 360, 430, 635, 461, TRANS_DBLUE);

 textprintf_right_ex(display [2], font, 625, 440, -1, -1, "COPYRIGHT 2007 LINLEY HENZELL");



    do
    {
        thing ++;
    } while (ticked == 0);
    ticked = 0;

 vsync();
 blit(display [2], screen, 0, 0, 0, 0, 640, 480);



 };


}

void define_keys(void)
{

 key_box("PRESS KEY FOR UP", CKEY_UP);
 key_box("PRESS KEY FOR LEFT", CKEY_LEFT);
 key_box("PRESS KEY FOR RIGHT", CKEY_RIGHT);
 key_box("PRESS KEY FOR DOWN", CKEY_DOWN);
 key_box("PRESS KEY FOR FIRE", CKEY_FIRE);
 key_box("PRESS KEY FOR CHARGE", CKEY_CHARGE);
 key_box("PRESS KEY FOR SWAP SIDES", CKEY_SWITCH);
 key_box("PRESS KEY FOR AUTOFIRE", CKEY_AUTOFIRE);

 set_config_int("Misc", "key_up", player.key [CKEY_UP]);
 set_config_int("Misc", "key_left", player.key [CKEY_LEFT]);
 set_config_int("Misc", "key_right", player.key [CKEY_RIGHT]);
 set_config_int("Misc", "key_down", player.key [CKEY_DOWN]);
 set_config_int("Misc", "key_fire", player.key [CKEY_FIRE]);
 set_config_int("Misc", "key_charge", player.key [CKEY_CHARGE]);
 set_config_int("Misc", "key_switch", player.key [CKEY_SWITCH]);
 set_config_int("Misc", "key_autofire", player.key [CKEY_AUTOFIRE]);

}


void key_box(const char ktext [], int command)
{
 rectfill(display [2], 210, 200, 430, 250, COL_OUTLINE);
 rect(display [2], 211, 201, 429, 249, COL_LBLUE);

 textprintf_centre_ex(display [2], font, 320, 222, -1, -1, ktext);

 vsync();
 blit(display [2], screen, 0, 0, 0, 0, 640, 480);

 int inputted = KEY_ESC;

 int i;
 int anykey = 0;

 do
 {

    do
    {
        thing ++;
    } while (ticked == 0);
    ticked = 0;
  key_wait --;


 anykey = 0;

  for (i = KEY_A; i < KEY_CAPSLOCK + 1; i ++)
  {
   if (key [i])
   {
    anykey = 1;
   }
  }

  if (anykey == 0)
   key_wait = 0;


  if (key_wait > 0)
   continue;


  if (key_wait <= 0)
  {

  for (i = KEY_A; i < KEY_CAPSLOCK + 1; i ++)
  {
   if (key [i])
   {
    inputted = i;
   }
  }
 }
//   while(acceptable_char(inputted) == 0);
  if (acceptable_char(inputted) != 0)
   break;

 } while (TRUE);


 player.key [command] = inputted;

 key_wait = 10;

}


int acceptable_char(int scode)
{

 switch(scode)
 {
  case KEY_ESC:
   key_wait = 7;
   return 0;
  case KEY_ENTER:
  case KEY_ENTER_PAD:
   key_wait = 7;
   return 0;
 }
 return 1;

}




void loading_screen(void)
{

/* splash_bitmap = load_bitmap("gfx//splash.bmp", splash_palette);

 if (splash_bitmap == NULL)
 {
  set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
  allegro_message("Failed to load in bitmap! (File not found?)  \n%s", "splash.bmp");
  exit(1);
 }
*/

 vsync();
 clear_bitmap(screen);

 BITMAP *wait_bitmap = load_bitmap("gfx//wait.bmp", splash_palette);

 if (wait_bitmap == NULL)
 {
  set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
  allegro_message("Failed to load in bitmap! (File not found?)  \n%s", "wait.bmp");
  exit(1);
 }


 set_palette(splash_palette);

 vsync();
 //blit(splash_bitmap, screen, 0, 0, 0, 0, 640, 480);
// blit(wait_bitmap, screen, 0, 0, 0, 0, 440, 300);
 draw_sprite(screen, wait_bitmap, 320 - (wait_bitmap->w / 2), 200);

    destroy_bitmap(wait_bitmap);

// textprintf_centre_ex(screen, font, 500, 300, -1, -1, "LOADING");
 //textprintf_centre_ex(screen, font, 500, 320, -1, -1, "WAIT A SECOND");
/*
 BITMAP *temp_bmp = create_bitmap(640, 480);
 blit(screen, temp_bmp, 0, 0, 0, 0, 640, 480);
 save_bitmap("wait.bmp", temp_bmp, splash_palette);
 destroy_bitmap(temp_bmp);
*/

}

void loading_screen_wait(void)
{
/*
 BITMAP *press_bitmap = load_bitmap("gfx//press.bmp", splash_palette);

 if (press_bitmap == NULL)
 {
  set_gfx_mode(GFX_TEXT, 0, 0, 0, 0);
  allegro_message("Failed to load in bitmap! (File not found?)  \n%s", "press.bmp");
  exit(1);
 }

 vsync();
// blit(splash_bitmap, screen, 0, 0, 0, 0, 640, 480);
// blit(press_bitmap, screen, 0, 0, 0, 0, 440, 300);
 draw_sprite(screen, press_bitmap, 320 - press_bitmap->w / 2, 400);
// textprintf_centre_ex(screen, font, 500, 300, -1, -1, "PRESS SPACE TO CONTINUE");


    do
    {
     if (key [KEY_ESC])
      exit(0);
    } while (key [KEY_SPACE] == 0);

//    destroy_bitmap(splash_bitmap);
    destroy_bitmap(press_bitmap);
*/
    vsync();
    clear_bitmap(screen);
    init_palette();

}
