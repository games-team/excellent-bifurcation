#include "config.h"


// note: NO_ENEMY_TYPES is in the ENEMY_ enum

struct eclassstruct eclass [NO_ENEMY_TYPES] =
{
{
0, // int max_hp
0, // int size
0, // ai
0, // speed1
0, // speed2
0, // speed3
0
}, // NONE
{
2, // int max_hp
12000, // int size
AI_DANCER,
4000, // speed1
8, // speed2
0, // speed3
0
}, // TWISTER
{
4, // int max_hp
18000,
AI_DIVER,
280, // speed1
2, // speed2
3000, // speed3
0
}, // DIVER
{
8000, // int max_hp
44000,
AI_GLIDER,
1100, // speed1
1, // speed2
0, // speed3
40 // score
}, // GLIDER1
{
2, // int max_hp
20000,
AI_MARCH,
3000, // speed1
1, // speed2
0, // speed3
0
}, // MARCHER1
{
4, // int max_hp
20000,
AI_ATTACKER,
220, // speed1
2, // speed2
0, // speed3
0
}, // ATTACKER1
{
4, // int max_hp
20000,
AI_ATTACKER,
220, // speed1
2, // speed2
0, // speed3
0
}, // ATTACKER2
{
4, // int max_hp
20000,
AI_ATTACKER,
160, // speed1
1, // speed2
0, // speed3
0
}, // ATTACKER3
{
8000, // int max_hp
38000,
AI_BEAMER,
2000, // speed1
4, // speed2
250, // speed3
50
}, // BEAMER1
{
200, // int max_hp
20000, // int size
AI_DANCER,
3000, // speed1
8, // speed2
0, // speed3
5
}, // BASIC1
{
5000, // int max_hp
38000,
AI_PAUSER,
120, // speed1
1, // speed2
0, // speed3
25
}, // SHOTTER1
{
5000, // int max_hp
38000,
AI_PAUSER,
120, // speed1
8, // speed2
0, // speed3
25
}, // ZAPPER1
{
1000, // int max_hp
24000,
AI_PAUSER,
120, // speed1
5, // speed2
0, // speed3
10
}, // DIPPER1
{
1500, // int max_hp
24000,
AI_PAUSER,
140, // speed1
8, // speed2
0, // speed3
15
}, // DIPPER2
{
200, // int max_hp
19000, // int size
AI_DANCER,
3000, // speed1
8, // speed2
0, // speed3
5
}, // BASIC2
{
200, // int max_hp
16000, // int size
AI_MARCH,
3000, // speed1
8, // speed2
0, // speed3
5
}, // BASIC3
{
5000, // int max_hp
44000,
AI_GLIDER,
800, // speed1
1, // speed2
0, // speed3
30
}, // CARRIER
{
5000, // int max_hp
38000,
AI_PAUSER,
120, // speed1
1, // speed2
0, // speed3
40
}, // BURSTER
{
50000, // int max_hp
55000,
AI_BOSS1,
120, // speed1
1, // speed2
0, // speed3
250
}, // BOSS1

// Stage 2

{
300, // int max_hp
20000, // int size
AI_DANCER,
4000, // speed1
12, // speed2
0, // speed3
5
}, // CROSS

{
5000, // int max_hp
37000,
AI_GLIDER,
900, // speed1
1, // speed2
0, // speed3
20 // score
}, // Flower1

{
6000, // int max_hp
37000,
AI_GLIDER,
900, // speed1
1, // speed2
0, // speed3
20 // score
}, // Flower2

{
6000, // int max_hp
44000,
AI_GLIDER,
900, // speed1
1, // speed2
0, // speed3
20 // score
}, // Flower3

{
7000, // int max_hp
44000,
AI_GLIDER,
900, // speed1
1, // speed2
0, // speed3
20 // score
}, // Flower4

{
5000, // int max_hp
44000,
AI_GLIDER,
900, // speed1
1, // speed2
0, // speed3
20 // score
}, // Flower5

{
300, // int max_hp
20000, // int size
AI_DANCER,
3000, // speed1
24, // speed2
0, // speed3
5
}, // CROSS2

{
4000, // int max_hp
42000, // int size
AI_DANCER,
2200, // speed1
12, // speed2
0, // speed3
25
}, // WINGS

{
50000, // int max_hp
35000,
AI_MB2,
120, // speed1
1, // speed2
0, // speed3
200
}, // MB2

{
5000, // int max_hp
24000,
AI_GLIDER,
900, // speed1
1, // speed2
0, // speed3
30
}, // CARRIER2
{
50000, // int max_hp
30000,
AI_BOSS2,
0, // speed1
0, // speed2
0, // speed3
350
}, // BOSS2
{
200, // int max_hp
13000,
AI_BFLOWER,
0, // speed1
0, // speed2
0, // speed3
0 // to stop people milking them for points, as they are infinite
}, // BFLOWER
{
50000, // int max_hp
60000,
AI_MB1,
120, // speed1
1, // speed2
0, // speed3
200
}, // MB2
{
200, // int max_hp
13000,
AI_BFIGHTER,
0, // speed1
0, // speed2
0, // speed3
0 // to stop people milking them for points, as they are infinite
}, // BFIGHTER


/*{
200, // int max_hp
20000, // int size
AI_MARCHER,
3000, // speed1
8, // speed2
0, // speed3
0
} // BASIC3*/
};

/*
NEED fields for:
rotation speed
accel speed
drag?
recycle
various bullet data (speed etc)
*/

/*
AI_DANCER
speed1 = speed
speed2 = rotation

AI_DIVER
speed1 = acceleration
speed2 = rotation

AI_ATTACKER
speed1 = speed
speed2 = rotation

*/
