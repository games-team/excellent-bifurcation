
void init_palette(void);


void level_start_palette(void);
void reset_palette(void);

// now, the colour zero doesn't form
/*
0 base
1 White
2 Yellow
3 LBlue
4 LRed
5 LGreen
6 LPurple
7 DBlue
8 Dred
9 Dgreen
10 DPurple
11 DDred
12 Black
13 Light 1
14 Light 2
15 Light 3
*/


enum
{
TRANS_WHITE = 17,
TRANS_YELLOW = 33,
TRANS_LBLUE = 49,
TRANS_LPURPLE = 65,
TRANS_LGREEN = 81,
TRANS_LRED = 97,
TRANS_DBLUE = 113,
TRANS_DPURPLE = 129,
TRANS_DGREEN = 145,
TRANS_DRED = 161,
TRANS_DDRED = 177,
TRANS_BLACK = 193,
TRANS_DDBLUE = 209,
//TRANS_DDPURPLE = 225,
TRANS_DDGREEN = 225
// must be the last trans + 1
};

#define TRANS_REVERSE 225
#define TRANS_DARKEN 226

#define CONVERT_POLE 243
#define FIX_BITMAP 244

int colour_to_trans(int y);

enum
{
COL_OUTLINE = 1,
COL_LOWER_BG1,
COL_LOWER_BG2,
COL_LOWER_BG3,
COL_UPPER_BG1,
COL_UPPER_BG2,
COL_UPPER_BG3,
COL_LRED,
COL_DRED,
COL_LBLUE,
COL_DBLUE,
COL_YELLOW,
COL_WHITE,
COL_LGREY,
COL_DGREY,
COL_DGREEN
};


#define COLOUR_OUTLINE 9




